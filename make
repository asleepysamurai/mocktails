#!/bin/bash

BUILDFOR=`uname | grep cygwin -i`
if [ $BUILDFOR ]
then echo "Building for Cygwin"
else echo "Building for UNIX"
fi

if [ $BUILDFOR ] 
then MIN="`cygpath -m /cygdrive/h/tools/yui/yuicompressor.jar`"
else MIN="/media/sda7/tools/yui/yuicompressor.jar"
fi

cd ../
rm -rf mock-prod
mkdir mock-prod

echo "Copying fonts..."
cp -r mock/client/font mock-prod/
echo "Copying images..."
cp -r mock/client/img mock-prod/
echo "Copying css..."
cp -r mock/client/css mock-prod/
echo "Copying js..."
cp -r mock/client/js mock-prod/
echo "Copying client..."
cp mock/client/index.html mock-prod/index.html
chmod -R 755 mock-prod

cd mock-prod/css
echo "Merging third party css..."
CSS1="jquery-ui-1.8.23.custom.css"
CSS2="bootstrap.css"
CSS3="farbtastic.css"
CSS4="font-awesome.css"
awk 'FNR==1{print ""}1' $CSS1 >> lib.css
awk 'FNR==1{print ""}1' $CSS2 >> lib.css
awk 'FNR==1{print ""}1' $CSS3 >> lib.css
awk 'FNR==1{print ""}1' $CSS4 >> lib.css
rm $CSS1
rm $CSS2
rm $CSS3
rm $CSS4

cd ../js/lib
echo "Merging third party javascripts..."
JSL1="jquery-1.8.2.min.js"
JSL2="jquery-ui-1.8.23.custom.min.js"
JSL3="jquery-touch-ui.min.js"
JSL4="jquery.mousewheel.js"
JSL5="jquery.plugins.custom.js"
JSL6="jquery.ui.dscroll.js"
JSL7="jquery-ui-spinner.min.js"
JSL8="json2.js"
JSL9="bootstrap.js"
JSL10="farbtastic.js"
awk 'FNR==1{print ""}1' $JSL1 >> lib.js
awk 'FNR==1{print ""}1' $JSL2 >> lib.js
awk 'FNR==1{print ""}1' $JSL3 >> lib.js
awk 'FNR==1{print ""}1' $JSL4 >> lib.js
awk 'FNR==1{print ""}1' $JSL5 >> lib.js
awk 'FNR==1{print ""}1' $JSL6 >> lib.js
awk 'FNR==1{print ""}1' $JSL7 >> lib.js
awk 'FNR==1{print ""}1' $JSL8 >> lib.js
awk 'FNR==1{print ""}1' $JSL9 >> lib.js
awk 'FNR==1{print ""}1' $JSL10 >> lib.js
rm $JSL1
rm $JSL2
rm $JSL3
rm $JSL4
rm $JSL5
rm $JSL6
rm $JSL7
rm $JSL8
rm $JSL9
rm $JSL10
rm versions

cd ../app
echo "Merging app javascripts..."
JSA1="actionManager.js"
JSA2="stateManager.js"
JSA3="persistanceManager.js"
JSA4="ui.js"
JSA5="mockup.js"
JSA6="modal.js"
JSA7="project.js"
JSA8="page.js"
JSA9="../app.js"
awk 'FNR==1{print ""}1' $JSA1 >> app.js
awk 'FNR==1{print ""}1' $JSA2 >> app.js
awk 'FNR==1{print ""}1' $JSA3 >> app.js
awk 'FNR==1{print ""}1' $JSA4 >> app.js
awk 'FNR==1{print ""}1' $JSA5 >> app.js
awk 'FNR==1{print ""}1' $JSA6 >> app.js
awk 'FNR==1{print ""}1' $JSA7 >> app.js
awk 'FNR==1{print ""}1' $JSA8 >> app.js
awk 'FNR==1{print ""}1' $JSA9 >> app.js
rm $JSA1
rm $JSA2
rm $JSA3
rm $JSA4
rm $JSA5
rm $JSA6
rm $JSA7
rm $JSA8
rm $JSA9

cd ../app
echo "Merging element definition javascripts..."
JSA10="paper.common.js"
JSA11="paper.base.js"
JSA12="paper.ios.js"
awk 'FNR==1{print ""}1' $JSA10 >> paper.js
awk 'FNR==1{print ""}1' $JSA11 >> paper.js
awk 'FNR==1{print ""}1' $JSA12 >> paper.js
rm $JSA10
rm $JSA11
rm $JSA12

cd ../../js/lib
echo "Minifying third party library javascripts..."
java -jar $MIN --type js raphael.js >> raphael.min.js
java -jar $MIN --type js lib.js >> lib.min.js
rm raphael.js
rm lib.js

cd ../app
echo "Minifying app javascripts..."
java -jar $MIN --type js app.js >> app.min.js
java -jar $MIN --type js paper.js >> paper.min.js
rm app.js
rm paper.js

cd ../../css/
echo "Minifying css..."
java -jar $MIN --type css app.css >> app.min.css
java -jar $MIN --type css lib.css >> lib.min.css
rm app.css
rm lib.css

cd ../
echo "Modifying client to refer to minified scripts and styles..."
sed -i '/'$CSS1'/d' index.html
sed -i '/'$CSS2'/d' index.html
sed -i '/'$CSS3'/d' index.html
sed -i '/'$CSS4'/ i\\t\t<link type=\"text\/css\" href=\"css\/lib.min.css\" rel=\"stylesheet\" \/>' index.html
sed -i '/app.css/d' index.html
sed -i '/'$CSS4'/ i\\t\t<link type=\"text\/css\" href=\"css\/app.min.css\" rel=\"stylesheet\" \/>' index.html
sed -i '/'$CSS4'/d' index.html

sed -i '/'$JSL1'/d' index.html
sed -i '/'$JSL2'/d' index.html
sed -i '/'$JSL3'/d' index.html
sed -i '/'$JSL4'/d' index.html
sed -i '/'$JSL5'/d' index.html
sed -i '/'$JSL6'/d' index.html
sed -i '/'$JSL7'/d' index.html
sed -i '/'$JSL8'/d' index.html
sed -i '/'$JSL9'/d' index.html
sed -i '/'$JSL10'/ i\\t\t<script type=\"text\/javascript\" src=\"js\/lib\/lib.min.js\"><\/script>' index.html
sed -i '/raphael.js/d' index.html
sed -i '/'$JSL10'/ i\\t\t<script type=\"text\/javascript\" src=\"js\/lib\/raphael.min.js\"><\/script>' index.html
sed -i '/'$JSL10'/d' index.html

sed -i '/'$JSA1'/d' index.html
sed -i '/'$JSA2'/d' index.html
sed -i '/'$JSA3'/d' index.html
sed -i '/'$JSA4'/d' index.html
sed -i '/'$JSA5'/d' index.html
sed -i '/'$JSA6'/d' index.html
sed -i '/'$JSA7'/d' index.html
sed -i '/app.js/d' index.html
sed -i '/'$JSA8'/ i\\t\t<script type=\"text\/javascript\" src=\"js\/app\/app.min.js\"><\/script>' index.html
sed -i '/'$JSA8'/d' index.html

sed -i '/'$JSA10'/d' index.html
sed -i '/'$JSA11'/d' index.html
sed -i '/'$JSA12'/ i\\t\t<script type=\"text\/javascript\" src=\"js\/app\/paper.min.js\"><\/script>' index.html
sed -i '/'$JSA12'/d' index.html

echo "DONE"

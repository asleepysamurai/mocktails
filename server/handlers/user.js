var crypto = require('crypto');
var bcrypt = require('bcrypt');
var db = require('../db.js');
var cookie = require('../cookie.js');

user = {
	_createVerificationCode: function(email, callback){
		var code = crypto.createHash('sha1').update(crypto.randomBytes(32).toString('ascii')).digest('hex');
		db.insert('verification', ['email', 'code', 'expires'], [email, code, Date.now() + (1000 * 60 * 60 * 24)], callback);
	},
	_validateUser: function(email, pass, callback){
		db.select('users', ['*'], 'email', email, function(err, results){
			if(err)
				callback(err);
			else if(!results || results.rowCount <= 0)
				callback({cc: 404});
			else{
				bcrypt.compare(pass, results.rows[0].pass, function(err, valid){
					if(err)
						callback({cc: 500});
					else if(valid && !results.rows[0].verified)
						callback({cc: 201});
					else
						callback(null, valid)
				});
			}
		});
	},
	_initSession: function(email, ip, callback){
		var sessionKey = crypto.createHash('sha1').update(crypto.randomBytes(32).toString('ascii')).digest('hex');
		var csrfToken = crypto.createHash('sha1').update(crypto.randomBytes(32).toString('ascii')).digest('hex');

		db.upsert('sessions', 'email', ['key', 'email', 'ip'], [sessionKey, email, ip], function(err, result){
			if(err)
				callback({code: 500, err: err});
			else
				callback({code: 200, cookies: [cookie.make('PHPSESSID', sessionKey, 'example.com', '/', Date.now() + (1000 * 60 * 60 * 24), true),
												cookie.make('_csrf', csrfToken, 'example.com', '/')]});
		});
	},
	create: {
		requires: ['e', 'p'],
		handle: function(req, resp){
			if(req.query.p.length < 6)//add email verification condition
				resp.writer({code: 400, body: 'pass'});
			else{
				bcrypt.hash(req.query.p, 16, function(err, hash){
					db.insert('users', ['email', 'pass'], [req.query.e, hash], function(err, results){
						if(err && err.code == 23505) //Account already exists. Check if valid user and login
							user.authenticate.handle(req, resp);
						else if(err)
							resp.writer({code: 500});
						else{
							//Account created successfully. Create a verification code.
							user._createVerificationCode(req.query.e, function(err, results){
								if(err)
									console.log('Creating verification code failed.' + err.code == 23503 ? 'No such user exists.' : '');
							});
							resp.writer({code: 201});
						}
					});
				});
			}
		}
	},
	authenticate: {
		requires: ['e', 'p'],
		handle: function(req, resp){
			user._validateUser(req.query.e, req.query.p, function(err, valid){
				if(err)
					resp.writer({code: err.cc ? err.cc : 500});
				else if(valid){
					//User logged in - create session
					user._initSession(req.query.e, req.connection.remoteAddress, function(respData){
						resp.writer(respData);
					});
				}
				else
					resp.writer({code: 401});
			});			
		}
	},
	verify: {
		requires: ['c'],
		handle: function(req, resp){
			db._query('DELETE FROM verification WHERE code=$1 RETURNING *', [req.query.c], function(err, results){
				if(err)
					resp.writer({code: 500});
				else if(!results || results.rowCount <= 0)
					resp.writer({code: 401});
				else if(Date.now() < results.rows[0].expires){
					db.update('users', ['verified'], [true], 'email', results.rows[0].email, function(err, results){
						if(err)
							resp.writer({code: 500});
						else
							resp.writer({code: 200});
					});
				}
				else
					resp.writer({code: 401});
			});
		}
	},
	resend: {
		requires: ['e'],
		handle: function(req, resp){
			//Create new code and send - delete old code
			db.remove('verification', 'email', req.query.e, function(err, results){
				if(err)
					resp.writer({code: 500});//('Error occurred while trying to destroy old verification code');
				else{
					user._createVerificationCode(req.query.e, function(err, results){
						if(err)
							resp.writer({code: err.code == 23503 ? 404 : 500});
						else{
							resp.writer({code: 200});
						}
					});
				}
			});
		}
	},
	reset: {
		requires: ['e'],
		handle: function(req, resp){
			db.remove('reset', 'email', req.query.e, function(err, results){
				if(err)
					resp.writer({code: 500});//('Error occurred while trying to destroy old reset code');
				else{
					var code = crypto.createHash('sha1').update(crypto.randomBytes(32).toString('ascii')).digest('hex'); 
					db.insert('reset', ['email', 'code', 'expires'], [req.query.e, code, Date.now() + (1000 * 60 * 60 * 24)], function(err, results){
						if(err)
							resp.writer({code: err.code == 23503 ? 404 : 500});
						else
							resp.writer({code: 200});
					});
				}
			});
		}
	},
	change: {
		requires: ['c', 'p'],
		handle: function(req, resp){
			if(req.query.p.length < 6)
				resp.writer({code: 400, body: 'pass'});
			else{
				db._query('DELETE FROM reset WHERE code=$1 RETURNING *', [req.query.c], function(err, results){
					if(err)
						resp.writer({code: 500});
					else if(!results || results.rowCount <= 0)
						resp.writer({code: 401});
					else if(Date.now() < results.rows[0].expires){
						bcrypt.hash(req.query.p, 16, function(err, hash){
							if(err)
								resp.writer({code: 500});
							db.update('users', ['pass'], [hash], 'email', results.rows[0].email, function(err, results){
								if(err)
									resp.writer({code: 500});
								else
									resp.writer({code: 200});
							});
						});
					}
					else
						resp.writer({code: 401});
				});				
			}
		}
	},
	logout: {
		checkCSRF: true,
		requires: null,		
		handle: function(req, resp){
			var session = cookie.get('PHPSESSID', req);
			if(session){
				db._query('DELETE FROM sessions WHERE key=$1 RETURNING *', [session], function(err, results){
					if(err)
						resp.writer({code: 500});
					else if(!results || results.rowCount <= 0)
						resp.writer({code: 404});
					else{
						var date = Date.now() - (1000 * 60 * 60 * 24);
						resp.writer({code: 200, cookies: [cookie.make('PHPSESSID', 'expired', 'example.com', '/', date, true),
												cookie.make('_csrf', 'expired', 'example.com', '/')]});
					}
				});
			}
			else
				resp.writer({code: 404});
		}
	}
}
module.exports = user;
var db = require('../db.js');
updates = {
	subscribe: {
		requires: ['e'],
		handle: function(req, resp){
			db.insert('subscriptions', ['email'], [req.query.e], function(err, result){
				if(err && err.code == 23505)
					resp.writer({code: 409});
				else if(err)
					resp.writer({code: 500});
				else
					resp.writer({code: 200});
			});
		}
	},
	unsubscribe: {
		requires: ['e'],
		handle: function(req, resp){
			db.update('subscriptions', ['del'], ['TRUE'], 'email', req.query.e, function(err, result){
				if(err)
					resp.writer({code: 500});
				else
					resp.writer({code: 200});
			});
		}
	}
}
module.exports = updates;
var pg = require('pg');
var connectionString = "tcp://apireceiver:qwaszx@localhost/mocktailsdb";

db = {
	_valueSelectorString: function(count){
		var vals = [];
		for(var i=1;i<=count;++i)
			vals.push('$'+i);
		return vals.join(', ');
	},
	_query: function(query, values, callback){
		if(!callback && values && typeof values == 'function')
			callback = values, values = null;
		pg.connect(connectionString, function(err, client){
			if(err)
				callback(err, null);
			else if(values)
				client.query(query, values, callback);
			else
				client.query(query, callback);
		});
	},
	upsert: function(table, selector, fields, values, callback){
		var vals = [], nsf = [], i=0;
		for(var f in fields){
			vals.push('$'+(++i));
			if(fields[f] != selector)
				nsf.push(fields[f]+' = nt.'+fields[f]);
		}
		var ftext = fields.join(', ');
		var query = 'WITH new_table ('+ftext+') AS (values ('+vals.join(', ')+')), '+
			'upsert AS (UPDATE '+table+' t SET '+nsf.join(', ')+' FROM new_table nt WHERE t.'+selector+' = nt.'+selector+' RETURNING t.*) '+
			'INSERT INTO '+table+' ('+ftext+') SELECT * FROM new_table WHERE NOT EXISTS '+
			'(SELECT 1 FROM upsert up WHERE up.'+selector+' = new_table.'+selector+')';
		db._query(query, values, callback);
	},
	insert: function(table, fields, values, callback){
		var query = 'INSERT INTO '+table+' ('+fields.join(', ')+') values('+db._valueSelectorString(fields.length)+')';
		db._query(query, values, callback);
	},
	update: function(table, fields, values, selector, value, callback){
		var vals = [], sf = [], count=fields.length;
		for(var i=0;i<count;)
			sf.push(fields[i]+'=$'+(++i));
		var query = 'UPDATE '+table+' SET '+sf.join(', ')+' WHERE '+selector+'=$'+(++i);
		values.push(value);
		db._query(query, values, callback);
	},
	select: function(table, fields, selector, value, callback){
		var query = 'SELECT '+fields.join(', ')+' FROM '+table+' WHERE '+selector+'=$1';
		db._query(query, [value], callback);
	},
	remove: function(table, selector, value, callback){
		var query = 'DELETE FROM '+table+' WHERE '+selector+'=$1';
		db._query(query, [value], callback);
	},
	closePool: function(callback){
		//Never call closePool in production
		pg.end();
	}
}
module.exports = db;
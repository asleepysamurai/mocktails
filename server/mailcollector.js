var qs = require('querystring');
var http = require('http');
var pg = require('pg');
var conString = "tcp://apireceiver:qwaszx@localhost/emaildb";
var client = new pg.Client(conString);
client.connect();
client.on('error', function(err, result){
    console.log(err);
});

http.createServer(function (request, response) {
    if (request.method == 'POST') {
        var body = '';
        request.on('data', function (data) {
            body += data;
            if (body.length > 1e6) {
                response.writeHead(413);
                response.end();
            }
        });
        request.on('end', function () {

            var POST = qs.parse(body);
            client.query("INSERT INTO list (email) values($1)", [POST.e], function(err, result){
                if(err){
                    response.writeHead(418, {'Content-Type': 'text/plain'});
                    response.end('Email already present.');
                }
                else{
                    response.writeHead(200, {'Content-Type': 'text/plain'});
                    response.end('Email saved.');
                }
            });
        });
    }
    else{
        response.writeHead(405, {'Content-Type': 'text/plain'});
        response.end('This server does not accept GET requests.');
    }
}).listen(3000, '127.0.0.1');
console.log('Server running at http://127.0.0.1:3000/');
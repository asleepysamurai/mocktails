var http = require('http');
var router = require('./router.js');

http.createServer(function (request, response){
	router.route(request, response);
}).listen(3000, '127.0.0.1');
console.log('Server running at http://127.0.0.1:3000/');
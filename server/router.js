var qs = require('querystring');
var url = require('url');
var cookie = require('./cookie.js');

//import required handlers
var updates = require('./handlers/updates.js');
var user = require('./handlers/user.js');

router = {
	//Map handlers to pathnames
	_handlers: {
		'/api/updates/subscribe': updates.subscribe,
		'/api/updates/unsubscribe': updates.unsubscribe,
		'/api/user/create': user.create,
		'/api/user/authenticate': user.authenticate,
		'/api/user/verify': user.verify,
		'/api/user/resend': user.resend,
		'/api/user/reset': user.reset,
		'/api/user/change': user.change,
		'/api/user/logout': user.logout
	},
	_hasRoute: function(path){
		if(router._handlers[path]) return true;
		else return false;
	},
	_writeResponse: function(options){
		if(!options || !options.code) return;
		var headers = {};
		options.ctype = options.ctype ? options.ctype : 'text/plain';
		headers['Content-Type'] = options.ctype;
		if(options.cookies){
			headers['Set-Cookie'] = [];
			for(var c in options.cookies)
				headers['Set-Cookie'].push(options.cookies[c]);
		}
		this.writeHead(options.code, headers);
		this.end(options.body);
	},
	route: function(req, resp){
		resp.writer = router._writeResponse;

		// Do not respond to any method other than POST
		if(req.method != 'POST')
			return resp.writer({code: 405});

		var _url = url.parse(req.url);

		// Do not respond to unknown endpoints
		if(!router._hasRoute(_url.pathname))
			return resp.writer({code: 404});

		// Get complete post data before responding to known endpoints
		req.body = '';
		req.on('data', function(data){
			req.body += data;
			//If request body is too large terminate request
			if (req.body.length > 1e6)
				return resp.writer({code: 413});
		});
		req.on('end', function(){
			req.query = qs.parse(req.body);
			var _handler = router._handlers[_url.pathname];
			
			// CSRF Check
			var _csrfCookie = cookie.get('_csrf', req);
			if(_handler.checkCSRF && req.query['_csrf'] != _csrfCookie && _csrfCookie != 'expired')
				return resp.writer({code: 420, body: 'CSRF Detected. Request not fulfilled.'})

			// Do not respond if required query params not present
			for(var p in _handler.requires)
				if(!req.query[_handler.requires[p]])
					return resp.writer({code: 400});

			_handler.handle(req, resp);
		});
	}
};
module.exports = router;
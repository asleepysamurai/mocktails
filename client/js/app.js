app = {
	init: function(){
		//load the app
		$('#appcontainer').css({'position': 'static', 'left': '0px'});	
		ui.init();

		//All ready, show page
		ui.initScrollers();
		if(!persistanceManager.getBetaNoticeShown())
			modal.show('betaNotice');
		$('#splash').hide();

		//init sharing code	
		//fb like
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
		
		//twitter tweet
		!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
	}
}
$(window).load(function(){
	// Check if IE. 
	if($.browser.msie){
		_gaq.push(['_trackEvent', 'init', 'msie']);
		return $('#splash').html('Unfortunately, Internet Explorer is not supported yet. Please use a modern version of Mozilla Firefox, Apple Safari or Google Chrome.');
	}

	// Check if Mobile browser
	if(/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
		var $splash = $('#splash');
		$splash.html('Unfortunately, mobile devices are not fully supported yet. Mocktails may not work as expected. Please use a desktop version of Mozilla Firefox, Apple Safari or Google Chrome.<br><br><br><a href="#">Try anyway.</a>');
		$splash.find('a').click(function(ev){
			$('#splash').html('Loading and Initializing App... Please wait...<br>');
			_gaq.push(['_trackEvent', 'init', 'mobile']);
			window.setTimeout(app.init, 100);
		});
		return;
	}

	_gaq.push(['_trackEvent', 'init', 'desktop']);
	app.init();
});

//Below is test code - remove for production
var testElement = function(top,left,width,height,bg){
	var ne = $('#label-definition', '#element-definitions').clone().removeAttr('id');
	ne.css({
		top: top,
		left: left,
		width: width,
		height: height,
		background: bg,
		'z-index': ++stateManager.lastZIndex
	});
	$('#' + stateManager.currentState.currentPage).append(ne);
	mockup.initElement(ne);
}

var tegen = function(){
	testElement(10,10,150,150,'#ff0000');
	testElement(20,20,130,130,'#00ff00');
	testElement(30,30,110,110,'#0000ff');
	testElement(40,40,90,90,'#00ffff');
}
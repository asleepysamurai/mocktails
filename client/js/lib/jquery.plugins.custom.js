(function( $ ){
	$.support.transition = (function () {

		var transitionEnd = (function () {

			var el = document.createElement('bootstrap')
			, transEndEventNames = {
				'WebkitTransition' : 'webkitTransitionEnd'
				,  'MozTransition'    : 'transitionend'
				,  'OTransition'      : 'oTransitionEnd otransitionend'
				,  'transition'       : 'transitionend'
			}
			, name

			for (name in transEndEventNames){
				if (el.style[name] !== undefined) {
					return transEndEventNames[name]
				}
			}

		}())

		return transitionEnd && {
			end: transitionEnd
		}

	})();
	$.escapeHTML = function(str){
		if(typeof str == 'string')
			return str.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;');
		else
			return str;
	};
	String.prototype.nl2br = function(is_xhtml) {   
		var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';    
		return (this + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
	}
})(jQuery);
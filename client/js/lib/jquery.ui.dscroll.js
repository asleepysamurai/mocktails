(function( $ ){

  $.fn.sbscroller = function( options ) {  

    var settings = {//defaults
	handleImage : false,
	handleTopImage : false,
	handleBottomImage : false,
	handleGripImage : false,
	mousewheel : false,
	autohide : false
    };
	
	return this.each(function() { 
	  if (options === 'refresh') {
		 $.extend( settings, $(this).data() );//include any previously stored options for this scroll pane
		 if (!$(this).hasClass('scroll-pane')){//traps the case where refresh has been called on slider that has not been set up
			  $(this).addClass('scroll-pane').data(options);//add a class and store the options as data against the element in case they are needed later
			  $(this).children().wrapAll('<div class="scroll-content"/>');
		 }
		 setSlider($(this));
	  }  
	  else if (options === 'reset') {
		$(this).find('.slider-vertical').slider({ value: 100 });
		$(this).find('.slider-horizontal').slider({ value: 0 });
	  } 
	  else {
		  if ( options ) { 
			$.extend( settings, options );
		  }
		  $(this).addClass('scroll-pane').data(options);//add a class and store the options as data against the element in case they are needed later
		  $(this).children().wrapAll('<div class="scroll-content"/>');
		  setSlider($(this));
	  }
	  
    });
	
	function setSlider($scrollpane){
		
		//change the main div to overflow-hidden as we can use the slider now
		$scrollpane.css('overflow','hidden');
		
		//compare the height of the scroll content to the scroll pane to see if we need a scrollbar
		var hdifference = $scrollpane.find('.scroll-content').height()-$scrollpane.height();//eg it's 200px longer
		$scrollpane.data('hdifference',hdifference); 
		var wdifference = $scrollpane.find('.scroll-content').width()-$scrollpane.width();//eg it's 200px longer
		//$scrollpane.data('wdifference',wdifference);  // -Not required since mousewheel is used only for vscroll
		
		if(hdifference<=0 && $scrollpane.find('.hslider-wrap').length>0)//scrollbar exists but is no longer required
			{
				$scrollpane.find('.hslider-wrap').remove();//remove the scrollbar
				$scrollpane.find('.scroll-content').css({top:0});//and reset the top position
			}
		if(wdifference<=0 && $scrollpane.find('.wslider-wrap').length>0)//scrollbar exists but is no longer required
			{
				$scrollpane.find('.wslider-wrap').remove();//remove the scrollbar
				$scrollpane.find('.scroll-content').css({left:0});//and reset the top position
			}
		
		if(hdifference>0)//if the scrollbar is needed, set it up...
			{
			   var proportion = hdifference / $scrollpane.find('.scroll-content').height();//eg 200px/500px
			
			   var handleHeight = Math.round((1-proportion)*$scrollpane.height());//set the proportional height - round it to make sure everything adds up correctly later on
			   handleHeight -= handleHeight%2; 
			
			   //if the slider has already been set up and this function is called again, we may need to set the position of the slider handle
			   var contentposition = $scrollpane.find('.scroll-content').position();	
			   var sliderInitial = 100*(1-Math.abs(contentposition.top)/hdifference);
			
			   
			   if($scrollpane.find('.hslider-wrap').length==0)//if the slider-wrap doesn't exist, insert it and set the initial value
				   {
					  $scrollpane.append('<\div class="hslider-wrap" ><\div class="slider-vertical"><\/div><\/div>');//append the necessary divs so they're only there if needed
					  sliderInitial = 100;
				   }
			   
			   	$scrollpane.find('.hslider-wrap').height(Math.round($scrollpane.height()));//set the height of the slider bar to that of the scroll pane

			   
			   //set up the slider 
			   $scrollpane.find('.slider-vertical').slider({
				  orientation: 'vertical',
				  min: 0,
				  max: 100,
				  value: sliderInitial,
				  slide: function(event, ui) {
					 var topValue = -((100-ui.value)*hdifference/100);
					 $scrollpane.find('.scroll-content').css({top:topValue});//move the top up (negative value) by the percentage the slider has been moved times the difference in height
				  },
				  change: function(event, ui) {
				  var topValue = -((100-ui.value)*($scrollpane.find('.scroll-content').height()-$scrollpane.height())/100);//recalculate the difference on change
					 $scrollpane.find('.scroll-content').css({top:topValue});//move the top up (negative value) by the percentage the slider has been moved times the difference in height
				  }	  
			   });
			
			   //set the handle height and bottom margin so the middle of the handle is in line with the slider
			   $scrollpane.find(".slider-vertical .ui-slider-handle").css({height:handleHeight,'margin-bottom':-0.5*handleHeight});
			   var origSliderHeight = $scrollpane.height();//read the original slider height
			   var sliderHeight = origSliderHeight - handleHeight ;//the height through which the handle can move needs to be the original height minus the handle height
			   var sliderMargin =  (origSliderHeight - sliderHeight)*0.5;//so the slider needs to have both top and bottom margins equal to half the difference
			   $scrollpane.find(".slider-vertical.ui-slider").css({height:sliderHeight,'margin-top':sliderMargin});//set the slider height and margins
			   $scrollpane.find(".slider-vertical.ui-slider-range").css({top:-sliderMargin});//position the slider-range div at the top of the slider container
			   
			   //create elements to hold the images for the scrollbar handle if needed
			   if(settings.handleTopImage) $scrollpane.find(".slider-vertical .ui-slider-handle").css({backgroundImage:'url('+settings.handleImage+')',backgroundRepeat:'repeat-y'});
			   if(settings.handleTopImage) $scrollpane.find(".slider-vertical .ui-slider-handle").append('<img class="scrollbar-top" src="'+settings.handleTopImage+'"/>');
			   if(settings.handleBottomImage) $scrollpane.find(".slider-vertical .ui-slider-handle").append('<img class="scrollbar-bottom" src="'+settings.handleBottomImage+'"/>');
			   if(settings.handleGripImage) {
				   $scrollpane.find(".slider-vertical .ui-slider-handle").append('<img class="scrollbar-grip" src="'+settings.handleGripImage+'"/>');	
				   $scrollpane.find('.slider-vertical .scrollbar-grip').load(function(){//wait till the image loads for Webkit
					   $scrollpane.find(".slider-vertical .scrollbar-grip").css({marginTop:-1*Math.round(0.5*$scrollpane.find(".slider-vertical .scrollbar-grip").height()+0.5)+'px'});
				   });
			   }
			}//end if
		if(wdifference>0)//if the scrollbar is needed, set it up...
			{
			   var proportion = wdifference / $scrollpane.find('.scroll-content').width();//eg 200px/500px			   
			
			   var handleWidth = Math.round((1-proportion)*$scrollpane.width());//set the proportional height - round it to make sure everything adds up correctly later on
			   handleWidth -= handleWidth%2; 
			
			   //if the slider has already been set up and this function is called again, we may need to set the position of the slider handle
			   var contentposition = $scrollpane.find('.scroll-content').position();	
			   var sliderInitial = 100*(Math.abs(contentposition.left)/wdifference);
			
			   
			   if($scrollpane.find('.wslider-wrap').length==0)//if the slider-wrap doesn't exist, insert it and set the initial value
				   {
					  $scrollpane.append('<\div class="wslider-wrap" ><\div class="slider-horizontal"><\/div><\/div>');//append the necessary divs so they're only there if needed
					  sliderInitial = 0;
				   }
			   
			   	$scrollpane.find('.wslider-wrap').width(Math.round($scrollpane.width()));//set the height of the slider bar to that of the scroll pane

			   
			   //set up the slider 
			   $scrollpane.find('.slider-horizontal').slider({
				  orientation: 'horizontal',
				  min: 0,
				  max: 100,
				  value: sliderInitial,
				  slide: function(event, ui) {
					 var leftValue = ((100-ui.value)*wdifference/100) - wdifference;
					 $scrollpane.find('.scroll-content').css({left:leftValue});//move the top up (negative value) by the percentage the slider has been moved times the difference in height
				  },
				  change: function(event, ui) {
				  var leftValue = ((100-ui.value)*($scrollpane.find('.scroll-content').width()-$scrollpane.width())/100) - wdifference;//recalculate the difference on change
					 $scrollpane.find('.scroll-content').css({left:leftValue});//move the top up (negative value) by the percentage the slider has been moved times the difference in height
				  }	  
			   });
			
			   //set the handle height and bottom margin so the middle of the handle is in line with the slider
			   $scrollpane.find(".slider-horizontal .ui-slider-handle").css({width:handleWidth,'margin-left':-0.5*handleWidth});
			   var origSliderWidth = $scrollpane.width();//read the original slider height
			   var sliderWidth = origSliderWidth - handleWidth ;//the height through which the handle can move needs to be the original height minus the handle height
			   var sliderMargin =  (origSliderWidth - sliderWidth)*0.5;//so the slider needs to have both top and bottom margins equal to half the difference
			   $scrollpane.find(".slider-horizontal.ui-slider").css({width:sliderWidth,'margin-left':sliderMargin});//set the slider height and margins
			   $scrollpane.find(".slider-horizontal.ui-slider-range").css({left:-sliderMargin});//position the slider-range div at the top of the slider container
			   
			   //create elements to hold the images for the scrollbar handle if needed
			   if(settings.handleTopImage) $scrollpane.find(".slider-horizontal .ui-slider-handle").css({backgroundImage:'url('+settings.handleImage+')',backgroundRepeat:'repeat-y'});
			   if(settings.handleTopImage) $scrollpane.find(".slider-horizontal .ui-slider-handle").append('<img class="scrollbar-top" src="'+settings.handleTopImage+'"/>');
			   if(settings.handleBottomImage) $scrollpane.find(".slider-horizontal .ui-slider-handle").append('<img class="scrollbar-bottom" src="'+settings.handleBottomImage+'"/>');
			   if(settings.handleGripImage) {
				   $scrollpane.find(".slider-horizontal .ui-slider-handle").append('<img class="scrollbar-grip" src="'+settings.handleGripImage+'"/>');	
				   $scrollpane.find('.slider-horizontal .scrollbar-grip').load(function(){//wait till the image loads for Webkit
					   $scrollpane.find(".slider-horizontal .scrollbar-grip").css({marginLeft:-1*Math.round(0.5*$scrollpane.find(".slider-horizontal .scrollbar-grip").width()+0.5)+'px'});
				   });
			   }
			}//end if
			 
		 //code for clicks on the scrollbar outside the slider
		 /*
		$(".ui-slider").click(function(event){//stop any clicks on the slider propagating through to the code below
			event.stopPropagation();
		});
	   
		$(".hslider-wrap").click(function(event){//clicks on the wrap outside the slider range
			var offsetTop = $(this).offset().top;//read the offset of the scroll pane
			var clickValue = (event.pageY-offsetTop)*100/$(this).height();//find the click point, subtract the offset, and calculate percentage of the slider clicked
			$(this).find(".slider-vertical").slider("value", 100-clickValue);//set the new value of the slider
		}); 
		$(".wslider-wrap").click(function(event){//clicks on the wrap outside the slider range
			var offsetLeft = $(this).offset().left;//read the offset of the scroll pane
			var clickValue = (event.pageY-offsetLeft)*100/$(this).width();//find the click point, subtract the offset, and calculate percentage of the slider clicked
			$(this).find(".slider-horizontal").slider("value", 100-clickValue);//set the new value of the slider
		}); 
		*/
		 
		//additional code for mousewheel
		if($.fn.mousewheel){			
			$scrollpane.unmousewheel();//remove any previously attached mousewheel events
			$scrollpane.mousewheel(function(event, delta){
				var speed = Math.round(5000/$scrollpane.data('hdifference'));
				if (speed <1) speed = 1;
				if (speed >100) speed = 100;
				var sliderVal = $(this).find(".slider-vertical").slider("value");//read current value of the slider
				
				sliderVal += (delta*speed);//increment the current value
		 
				$(this).find(".slider-vertical").slider("value", sliderVal);//and set the new value of the slider
				
				event.preventDefault();//stop any default behaviour
			});
		}
		
		//autohide
		if(settings.autohide){
		   if (!$scrollpane.find(".hslider-wrap").hasClass('slider-wrap-active')) $scrollpane.find(".hslider-wrap").hide();//only hide if it's not already active - this could be the case if content is added or removed from within the scroll pane
		   $scrollpane.hover(function(){
				$scrollpane.find(".hslider-wrap").show().addClass('slider-wrap-active');
				},
				function(){
				$scrollpane.find(".hslider-wrap").hide().removeClass('slider-wrap-active');
				})
		   if (!$scrollpane.find(".wslider-wrap").hasClass('slider-wrap-active')) $scrollpane.find(".wslider-wrap").hide();//only hide if it's not already active - this could be the case if content is added or removed from within the scroll pane
		   $scrollpane.hover(function(){
				$scrollpane.find(".wslider-wrap").show().addClass('slider-wrap-active');
				},
				function(){
				$scrollpane.find(".wslider-wrap").hide().removeClass('slider-wrap-active');
				})
		}
		
		
	}

  };
})( jQuery );
var modal = {};
modal._getValue = function(input, defaultNull){
	var title = input.val();
	return title ? title : defaultNull ? null : input.attr('placeholder');
};
modal.newPage = {
	title: 'Add New Page',
	body: function(){ var title = page.suffix(); title = $.escapeHTML(title); return '<form class="form-horizontal"><label class="control-label" for="inputTitle">Page Title</label><div class="controls"><input type="text" id="inputTitle" placeholder="'+title+'" value="'+title+'"></div></form>'; },
	action: function(){
		var $inp = $('#modalDialog input');
		$('#modalDialog').on('shown', function(){$inp.focus()});
		$inp.add(document.body).keydown(function(ev){
			if(ev.which == 27 || ev.which == 13)
				ev.preventDefault();
			if(ev.which == 27)
				$('#modal-cancel').click();
			else if(ev.which == 13)
				$('#modal-confirm').click();
		});
		var inpChanged = function(ev){
			var $t = $(this);
			if($t.val().length > 0)
				$('#modal-confirm').prop('disabled', false);
			else
				$('#modal-confirm').prop('disabled', true);
		};
		$inp.keyup(inpChanged).change(inpChanged);
		if($inp.length > 0 && $inp.val().length == 0)
			$('#modal-confirm').prop('disabled', true);
		else
			$('#modal-confirm').prop('disabled', false);
	},
	cancel: { text: 'Cancel' },
	confirm: {
		text: 'Add', 
		action: function(){ 
			page._new(modal._getValue($('#inputTitle')));
		}
	}
};
modal.editPage = {
	title: 'Rename Page',
	body: function(){ var title = $('#' + stateManager.currentState.currentPage).data('title'); title = $.escapeHTML(title); return '<form class="form-horizontal"><label class="control-label" for="inputTitle">Rename \'' + title + '\' to</label><div class="controls"><input type="text" id="inputTitle" placeholder="' + title + '" value="' + title + '"></div></form>'; },
	action: modal.newPage.action,
	cancel: { text: 'Cancel' },
	confirm: {
		text: 'Rename', 
		action: function(){ 
			page.rename(modal._getValue($('#inputTitle'), true), $('#modalDialog').data('mData'));
		}
	}
};
modal.duplicatePage = {
	title: 'Duplicate this Page',
	body: function(){ var title = page.duplicateSuffix()+')'; title = $.escapeHTML(title); return '<form class="form-horizontal"><label class="control-label" for="inputTitle">Page Title</label><div class="controls"><input type="text" id="inputTitle" placeholder="'+title+'" value="'+title+'"></div></form>'; },
	action: modal.newPage.action,
	cancel: { text: 'Cancel' },
	confirm: {
		text: 'Duplicate',
		action: function(){
			page.duplicate(modal._getValue($('#inputTitle'), true), $('#modalDialog').data('mData'));
		}
	}
};
modal.deletePage = {
	title: 'Delete Page',
	body: function(){ return '<p>Are you sure you want to delete this page? This action cannot be undone.<p>'},
	action: function(){
		var $inp = $('#inputTitle');
		$('#modalDialog').on('shown', function(){$inp.focus()});
		$inp.add(document.body).keydown(function(ev){
			if(ev.which == 27 || ev.which == 13){
				ev.preventDefault();
				$('#modal-confirm').click();
			}
		});
	},
	confirm: { text: 'Cancel', type: 'btn' },
	cancel: { 
		text: 'Delete',
		action: function(){
			page._delete(stateManager.currentState.currentPage);
		},
		type: 'btn btn-danger'
	}
};
modal.newProject = {
	title: 'New Project',
	body: function(){ var title = project.suffix(); title = $.escapeHTML(title); return '<form class="form-horizontal"><label class="control-label" for="inputTitle">Project Title</label><div class="controls"><input type="text" id="inputTitle" placeholder="'+title+'" value="'+title+'"></div></form>'; },
	action: modal.newPage.action,
	cancel: { text: 'Cancel' },
	confirm: {
		text: 'Create',
		action: function(){
			project._new(modal._getValue($('#inputTitle')));
		}
	}
};
modal.duplicateProject = {
	title: 'Duplicate this Project',
	body: function(){ var title = project.duplicateSuffix()+')'; title = $.escapeHTML(title); return '<form class="form-horizontal"><label class="control-label" for="inputTitle">Project Title</label><div class="controls"><input type="text" id="inputTitle" placeholder="'+title+'" value="'+title+'"></div></form>'; },
	action: modal.newProject.action,
	cancel: { text: 'Cancel' },
	confirm: {
		text: 'Duplicate',
		action: function(){
			project.duplicate(modal._getValue($('#inputTitle')));
		}
	}
};
modal.editProject = {
	title: 'Rename Project',
	body: function(){ var title = $('#project-title').text(); title = $.escapeHTML(title); return '<form class="form-horizontal"><label class="control-label" for="inputTitle">Rename \'' + title + '\' to</label><div class="controls"><input type="text" id="inputTitle" placeholder="' + title + '" value="' + title + '"></div></form>'; },
	action: modal.newPage.action,
	cancel: { text: 'Cancel' },
	confirm: {
		text: 'Rename', 
		action: function(){ 
			project.rename(modal._getValue($('#inputTitle'), true));
		}
	}
};
modal.deleteProject = {
	title: 'Delete Project',
	body: function(){ return '<p>Are you sure you want to delete this entire project? This action cannot be undone.<p>'},
	action: modal.deletePage.action,
	confirm: { text: 'Cancel', 'type': 'btn' },
	cancel: { 
		text: 'Delete',
		action: function(){
			project._delete();
		},
		type: 'btn btn-danger'
	}
};
modal.defineElementTemplate = {
	title: 'Define Custom Element Template',
	body: function(){ return '<form class="form-horizontal"><label class="control-label" for="inputTitle">Custom template name:</label><div class="controls"><input type="text" id="inputTitle" placeholder="Name"></div></form>'; },
	action: modal.newPage.action,
	cancel: { text: 'Cancel'},
	confirm: {
		text: 'Define',
		action: function(){
			mockup.defineElementTemplate($('#inputTitle').val());
		}
	},
	disableConfirmIfEmpty: true
};
modal.defineMasterElement = {
	title: 'Define Master Element',
	body: function(){ return '<form class="form-horizontal"><label class="control-label" for="inputTitle">Custom master element name:</label><div class="controls"><input type="text" id="inputTitle" placeholder="Name"></div></form>'; },
	action: modal.defineElementTemplate.action,
	cancel: { text: 'Cancel'},
	confirm: {
		text: 'Define',
		action: function(){
			mockup.defineMasterElement($('#inputTitle').val());
		}
	},
	disableConfirmIfEmpty: true
};
modal.deleteMasterElement = {
	title: 'Delete Master Element',
	body: function(){ return '<p>Are you sure you want to delete this master element? This action cannot be undone.<p>'},
	action: modal.deletePage.action,
	confirm: { text: 'Cancel', 'type': 'btn' },
	cancel: { 
		text: 'Delete',
		action: function(){
			var rgid = $('#modalDialog').data('mData');
			console.log(rgid);
			mockup.deleteGroupedElement(rgid.id,rgid.master);
		},
		type: 'btn btn-danger'
	}
};
modal.deleteElementTemplate = {
	title: 'Delete Element Template',
	body: function(){ return '<p>Are you sure you want to delete this element template? This action cannot be undone.<p>'},
	action: modal.deletePage.action,
	confirm: { text: 'Cancel', 'type': 'btn' },
	cancel: modal.deleteMasterElement.cancel
};
modal.notificationRequest = {
	track: true,
	title: 'Signup for updates',	
	body: function(){ var title = persistanceManager.getUserEmailForNotifications(); title = $.escapeHTML(title); title = title ? title : 'your+email@domain.tld'; return '<div><p class="modalParagraph">Thanks for playing with Mocktails so far. Would you like to signup and get updates about Mocktails delivered right to your inbox?</p><form class="form-horizontal"><label class="control-label" for="inputTitle">Yes, send updates to me at </label><div class="controls"><input type="text" id="inputTitle" placeholder="' + title + '"></div></form></div>'; },
	action: modal.newPage.action,
	cancel: { text: 'No Thanks' },
	confirm: {
		track: true,
		text: 'Sign Up', 
		action: function(){ 
			persistanceManager.setUserEmailForNotifications($('#inputTitle').val());
			window.setTimeout(function(){ modal.show('shareRequest'); }, 600);
		}
	}
};
modal.shareRequest = {
	title: 'Spread the word',	
	body: function(){ return '<div><p class="modalParagraph">Thanks for signing up. We\'ll make sure we keep you updated as we make changes to Mocktails.</p><p class="modalParagraph">Would you like to share Mocktails with your social circle?</p><div id="share-buttons"></div></div>'; },
	action: function(){
		modal.newPage.action();
		$('#fb-like-orig').clone(true).appendTo('#share-buttons').removeAttr('id');
		$('#twitter-orig').clone(true).appendTo('#share-buttons').removeAttr('id');
		$('#gplus-orig').clone(true).addClass('g-plusone').appendTo('#share-buttons').removeAttr('id').attr('id', 'plusone-div').wrap('<div style="top:2px; position: relative;"></div>');

		//google +1 button
		(function() {
			var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;// po.innerHTML = '{"parsetags": "explicit"}';
			po.src = 'https://apis.google.com/js/plusone.js';
			po.id = 'gplus-jssdk';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
		})();

		$('#modalDialog').on('hidden', function(ev){
			$('#gplus-jssdk').remove();
		});

		//$('#modal-confirm').prop('disabled', false);
	},
	cancel: { text: 'No Thanks' },
	confirm: { text: 'Done!' }
};
modal.betaNotice = {
	track: true,
	title: 'Important Notification',	
	body: function(){ return '<div><p class="modalParagraph">Mocktails is still in initial stages of construction. It is not even in beta yet. As such, please do not use Mocktails for anything mission critical. </p><p class="modalParagraph">Currently, your mockups are persisted to your browser\'s localStorage. You <strong>may lose data</strong> if Mocktails exceeds its localStorage quota. You <strong>will lose data</strong> if your browsing data is cleared.</p><p class="modalParagraph">Lastly, if you find any bugs, please report them to bugs@mocktailsapp.in. Ensure that you include your OS (with version), browser (with version) and a list of steps to reproduce the bug.</p><p class="modalParagraph">Thank you for helping us by playing with Mocktails.</p></div>'; },
	action: function(){ persistanceManager.setBetaNoticeShown(true); },
	confirm: { text: 'OK', type: 'btn btn-danger' }
};
modal.signup = {
	title: 'Signup or Login',
	body: function(){
		return '<form class="form-horizontal">'+
					'<div class="control-group" id="control-group-email">'+
						'<label class="control-label" for="inputEmail">Email:</label>'+
						'<div class="controls"><input type="text" id="inputEmail" placeholder="your+email@domain.tld"><span class="help-inline" id="help-email">Emails with \'+\' character accepted</span></div>'+
					'</div>'+
					'<div class="control-group" id="control-group-pass">'+
						'<label class="control-label" for="inputPass">Password:</label>'+
						'<div class="controls"><input type="password" id="inputPass" placeholder="Password"><span class="help-inline" id="help-pass">Minimum six characters</span></div>'+
					'</div>'+
				'</form>'; },
	action: function(){
		//$('#modalDialog .control-label').css('width', '100px');
		var $inp = $('#modalDialog input');
		$inp.add(document.body).keydown(function(ev){
			if(ev.which == 27 || ev.which == 13)
				ev.preventDefault();
		});
		$inp.keyup(function(ev){
			if(modal.signup.validateEmail() && modal.signup.validatePass())
				$('#modal-confirm').prop('disabled', false);
			else
				$('#modal-confirm').prop('disabled', true);
		});
		modal.signup.blurEmail();
		$('#inputPass').blur(function(ev){
			if(!modal.signup.validatePass()){
				$('#help-pass').text('Password should be greater than 6 characters.');
				$('#control-group-pass').addClass('error');
				$('#modal-confirm').prop('disabled', true);
			}
		});
		$('#modal-confirm').prop('disabled', true);
		$('#modalDialog').on('shown', function(){$inp[0].focus()});
	},
	blurEmail: function(){
		$('#inputEmail').blur(function(ev){
			if(!modal.signup.validateEmail()){
				$('#help-email').text('Please enter a valid email');
				$('#control-group-email').addClass('error');
				$('#modalDialog .modal-footer button').prop('disabled', true);
			}
			$(this).mailcheck({suggested: function(element, suggestion){
				$(element).popover('destroy');
				$(element).popover({placement:'top', title: 'Did you mean', content: '<div><form><span class="uneditable-input" id="suggestion" style="width:195px;">'+suggestion.full+'</span><div class="btn-group"><button class="btn btn-danger" id="retainEmail" style="width:105px;">No</button><button class="btn" id="changeEmail" style="width:105px;">Yes</button></span></form></div>', attachTo: '#tbs-modal', onShow: function(){
					$('#tbs-modal .popover').css('z-index', '105000');
					$('#changeEmail').click(function(){
						$('#inputEmail').val($('#suggestion').text());
						modal.signup.dismissSuggestion();
					});
					$('#retainEmail').click(function(){
						modal.signup.dismissSuggestion();
					});
				}});
				$(element).popover('show');
			}, empty: function(element){
				$(element).popover('destroy');
			}});
		});
	},
	dismissSuggestion: function(){
		$('#inputEmail').popover('destroy');
		$('#inputPass').focus();
		if(modal.signup.validateEmail() && modal.signup.validatePass())
			$('#modalDialog .modal-footer button').prop('disabled', false);
		if($('#modalDialog').data('overrideMailcheck'))
			modal.resendVerification.submitRequest();
	},
	validateEmail: function(){
		var flag = false, email = $('#inputEmail').val(), eatpos = email.indexOf('@');
		if(email.length > 4 && eatpos != -1 && eatpos > 0 && eatpos < email.length-1)
			flag = true;
		if(flag){
			$('#help-email').text('Emails with \'+\' character accepted');
			$('#control-group-email').removeClass('error');
			$('#modalDialog .modal-footer button').prop('disabled', false);			
		}
		return flag;
	},
	validatePass: function(){
		pass = $('#inputPass');
		if(pass.length == 0) return true;
		var flag = false, pass = pass.val();
		if(pass.length > 5)
			flag = true;
		if(flag){
			$('#help-pass').text('Minimum six characters');
			$('#control-group-pass').removeClass('error');
			$('#modalDialog .modal-footer button').prop('disabled', false);
		}
		return flag;
	},	
	handle200: function(data, textStatus, xhr){
		modal.hideSpinner();
		persistanceManager.setLoggedInUser($('#inputEmail').val());
		ui.toggleLoginState(persistanceManager.getLoggedInUser());
		$('#modalDialog').modal('hide');
	},	
	handle201: function(data, textStatus, xhr){
		//not yet verified
		console.log('verifyEmail');
		window.setTimeout(function(){ modal.show('verifyEmail'); }, 600);
		$('#modalDialog').modal('hide');
	},
	handle400: function(xhr, textStatus, error){
		//Bad request - username/password failed validation server side
		modal.hideSpinner();
		if(xhr.responseText.indexOf('email') != -1){
			$('#help-email').text('Please enter a valid email');
			$('#inputEmail').focus();
			$('#control-group-email').addClass('error');
		}
		else if(xhr.responseText.indexOf('pass') != -1){
			$('#help-pass').text('Password should be greater than 6 characters.');
			$('#inputPass').focus();
			$('#control-group-pass').addClass('error');
		}
	},
	cancel: {
		'text': 'Signup', 
		type: 'btn',
		dontClose: true,
		action: function(){
			$('#inputEmail').popover('destroy');
			modal.showSpinner();
			api.createUserAccount($('#inputEmail').val(), $('#inputPass').val(), {
				200: modal.signup.handle200,
				201: modal.signup.handle201,
				400: modal.signup.handle400,
				401: function(xhr, textStatus, error){
					//account already existed but password did not match
					modal.hideSpinner();
					$('#help-email').html('This email has already been registered.<br>&nbsp;<a href="#" id="forgotpassword">Forgot your password?</a>')
					$('#control-group-email').addClass('error');
					$('#forgotpassword').click(function(ev){
						modal.show('forgotPassword');
					});
				}
			});
		}
	},
	confirm: {
		'text': 'Login', 
		type: 'btn btn-primary',
		dontClose: true,
		action: function(){
			$('#inputEmail').popover('destroy');
			modal.showSpinner();
			api.authenticateUser($('#inputEmail').val(), $('#inputPass').val(), {
				200: modal.signup.handle200,
				201: modal.signup.handle201,
				400: modal.signup.handle400,
				401: function(xhr, textStatus, error){
					//email exists but password did not match
					modal.hideSpinner();
					$('#help-pass').html('Password did not match.<br>&nbsp;<a href="#" id="forgotpassword">Forgot your password?</a>')
					$('#control-group-pass').addClass('error');
					$('#forgotpassword').click(function(ev){
						modal.show('forgotPassword');
					});
				},
				404: function(xhr, textStatus, error){
					//email does not exist
					modal.hideSpinner();
					$('#help-email').html('This email has not been registered.<br>&nbsp;<a href="#" id="createaccount">Create new account?</a>')
					$('#control-group-email').addClass('error');
					$('#createaccount').click(function(ev){
						$('#modal-cancel').click();
					});
				}
			});
		}
	}
};
modal.verifyEmail = {
	title: 'Verify Email',
	body: function(){ return '<p class="modalParagraph">A verification code has been sent to your email. Please enter the code to verify your email.</p><form class="form-horizontal"><div class="control-group" id="control-group-code"><label class="control-label" for="inputTitle">Verification Code:</label><div class="controls"><input type="text" id="inputTitle" placeholder="Code"><span class="help-inline" id="help-code">Code as it appears in the email without quotes</span></div></div></form>'; },
	action: function(){
		modal.newPage.action();
		$('#modal-cancel').prop('disabled', false);
	},
	cancel: { text: 'Cancel'},
	confirm: {
		text: 'Verify',
		dontClose: true,
		action: function(){
			modal.showSpinner();
			api.verifyEmail($('#inputTitle').val(), {
				200: function(data, textStatus, xhr){
					// Verified email
					console.log('Email verified successfully');
					modal.hideSpinner();
					$('#modalDialog').modal('hide');
				},
				401: function(xhr, textStatus, error){
					modal.hideSpinner();
					$('#help-code').html('Invalid code.&nbsp;<a id="resendverification" href="#">Resend Verification Email?</a>');
					$('#control-group-code').addClass('error');
					$('#resendverification').click(function(){
						window.setTimeout(function(){ modal.show('resendVerification'); }, 600);
						$('#modalDialog').modal('hide');
					});
				}
			});
		}
	},
	disableConfirmIfEmpty: true
};
modal.resendVerification = {
	title: 'Resend Verification Email',
	body: function(){ return '<form class="form-horizontal"><div class="control-group" id="control-group-code"><label class="control-label" for="inputEmail">Registered Email:</label><div class="controls"><input type="text" id="inputEmail" placeholder="your+email@domain.tld"><span class="help-inline" id="help-code">Email with which you registered for Mocktails</span></div></div></form>'; },
	action: modal.signup.action,
	cancel: { text: 'Cancel'},
	confirm: {
		text: 'Resend',
		dontClose: true,
		action: function(){
			if($('#modalDialog').data('overrideMailcheck'))
				modal.resendVerification.submitRequest();
			else{
				if($('#inputEmail').mailcheck({suggested: function(){
					$('#modal-confirm').prop('disabled', true);
					$('#modalDialog').data('overrideMailcheck', true);
				}, empty: function(){
					modal.resendVerification.submitRequest();
				}}));
			}
		}
	},
	submitRequest: function(){
		$('#modalDialog').removeData('overrideMailcheck');
		$('#modal-confirm').prop('disabled', false);
		modal.showSpinner();
		api.resendVerification($('#inputEmail').val(), {
			200: function(data, textStatus, xhr){
				// Verified email
				console.log('Email sent');
				modal.hideSpinner();
				window.setTimeout(function(){ modal.show('verifyEmail'); }, 600);
				$('#modalDialog').modal('hide');
			},
			404: function(xhr, textStatus, error){
				modal.hideSpinner();
				$('#help-code').html('Invalid email.<a id="createaccount" href="#">Create new account?</a>');
				$('#control-group-code').addClass('error');
				$('#createaccount').click(function(){
					window.setTimeout(function(){ modal.show('signup'); }, 600);
					$('#modalDialog').modal('hide');
				});
			},
			409: function(xhr, textStatus, error){				
				modal.hideSpinner();
				$('#help-code').html('This email has already been verified.');
				$('#control-group-code').addClass('error');
			}
		});
	},
	disableConfirmIfEmpty: true
};
modal.forgotPassword = {	
	title: 'Forgot Password',
	body: modal.resendVerification.body,
	action: modal.resendVerification.action,
	cancel: modal.resendVerification.cancel,
	confirm: {
		text: 'Send Reset Code',
		dontClose: true,
		action: function(){
			if($('#modalDialog').data('overrideMailcheck'))
				modal.forgotPassword.submitRequest();
			else{
				if($('#inputEmail').mailcheck({suggested: function(){
					$('#modal-confirm').prop('disabled', true);
					$('#modalDialog').data('overrideMailcheck', true);
				}, empty: function(){
					modal.forgotPassword.submitRequest();
				}}));
			}
		}
	},
	submitRequest: function(){
		$('#modalDialog').removeData('overrideMailcheck');
		$('#modal-confirm').prop('disabled', false);
		modal.showSpinner();
		api.forgotPassword($('#inputEmail').val(), {
			200: function(data, textStatus, xhr){
				// Verified email
				console.log('Email sent');
				modal.hideSpinner();
				//window.setTimeout(function(){ modal.show('verifyEmail'); }, 600);
				$('#modalDialog').modal('hide');
			},
			404: function(xhr, textStatus, error){
				modal.hideSpinner();
				$('#help-code').html('Invalid email.<a id="createaccount" href="#">Create new account?</a>');
				$('#control-group-code').addClass('error');
				$('#createaccount').click(function(){
					window.setTimeout(function(){ modal.show('signup'); }, 600);
					$('#modalDialog').modal('hide');
				});
			}
		});
	},
	disableConfirmIfEmpty: true
};
modal.changePassword = {	
	title: 'Change Password',
	body: function(){
		return '<form class="form-horizontal">'+
					'<div class="control-group" id="control-group-code">'+
						'<label class="control-label" for="inputCode">Reset Code:</label>'+
						'<div class="controls"><input type="text" id="inputCode" placeholder=""><span class="help-inline" id="help-code">Code as it appears in the email without quotes</span></div>'+
					'</div>'+
					'<div class="control-group" id="control-group-pass">'+
						'<label class="control-label" for="inputPass">Password:</label>'+
						'<div class="controls"><input type="password" id="inputPass" placeholder="Password"><span class="help-inline" id="help-pass">Minimum six characters</span></div>'+
					'</div>'+
				'</form>'; },
	action: function(){
		var $inp = $('#modalDialog input');
		$inp.add(document.body).keydown(function(ev){
			if(ev.which == 27 || ev.which == 13)
				ev.preventDefault();
		});
		$inp.keyup(function(ev){
			if(modal.signup.validatePass() && $('#inputCode').val().length > 5)
				$('#modal-confirm').prop('disabled', false);
			else
				$('#modal-confirm').prop('disabled', true);
		});
		$('#inputPass').blur(function(ev){
			if(modal.signup.validatePass()){
				if($('#inputCode').val().length <= 5)
					$('#modal-confirm').prop('disabled', true);
				else
					$('#modal-confirm').prop('disabled', false);
			}
			else{
				$('#help-pass').text('Password should be greater than 6 characters.');
				$('#control-group-pass').addClass('error');
				$('#modal-confirm').prop('disabled', true);
			}
		});
		$('#modal-confirm').prop('disabled', true);
		$('#modalDialog').on('shown', function(){$inp[0].focus()});
	},
	cancel: modal.resendVerification.cancel,
	confirm: {
		text: 'Change',
		dontClose: true,
		action: function(){
			modal.showSpinner();
			api.changePassword($('#inputCode').val(), $('#inputPass').val(), {
				200: function(data, textStatus, xhr){
					console.log('Changed password successfully');
					modal.hideSpinner();
					$('#modalDialog').modal('hide');
				},
				400: modal.signup.handle400,
				401: function(xhr, textStatus, error){
					modal.hideSpinner();
					$('#help-code').html('Invalid code.&nbsp;<a id="resendreset" href="#">Resend Reset Code?</a>');
					$('#control-group-code').addClass('error');
					$('#resendreset').click(function(){
						window.setTimeout(function(){ modal.show('forgotPassword'); }, 600);
						$('#modalDialog').modal('hide');
					});
				}
			});
		}
	},
	disableConfirmIfEmpty: true
};
modal.show = function(dialog, data){
	var options = modal[dialog];
	if(options.confirm && options.confirm.track)
		options.confirm.dialog = dialog;
	modal.initModal(options.title, options.body(), options.action, options.cancel, options.confirm, data, options.disableConfirmIfEmpty);
	if(options.track)
		_gaq.push(['_trackEvent', 'modal', 'show', dialog]);
};
modal.initModal = function(title, body, action, cancel, confirm, data){
	$(document.body).data('showingModal', true);
	$('#modal-title').text(title);
	$('div .modal-body').html(body);
	var $mCancel = $('#modal-cancel');
	if(cancel){
		$mCancel.text(cancel.text);
		$mCancel.unbind('click');
		$mCancel.bind('click', function(){ 
			if(typeof cancel.action == 'function') 
				cancel.action(); 
			if(!cancel.dontClose)
				$('#modalDialog').modal('hide');
		});
		$mCancel.removeClass().addClass(cancel.type ? cancel.type : 'btn');
		$mCancel.show();
	}
	else
		$mCancel.hide();
	var $mConfirm = $('#modal-confirm');
	if(confirm){
		$mConfirm.text(confirm.text);
		$mConfirm.unbind('click');
		$mConfirm.bind('click', function(){
			if(typeof confirm.action == 'function') confirm.action();
			if(confirm.track)
				_gaq.push(['_trackEvent', 'modal', 'confirm', confirm.dialog]);
			if(!confirm.dontClose)
				$('#modalDialog').modal('hide');
		});
		$mConfirm.removeClass().addClass(confirm.type ? confirm.type : 'btn btn-primary');
		$mConfirm.show();
	}
	else
		$mConfirm.hide();
	var $mDialog = $('#modalDialog');
	if(data) $mDialog.data('mData', data);
	if(typeof action == 'function') action();
	$mDialog.on('hide', function(ev){
		$('#inputTitle').add(document.body).unbind('keydown');
		$mDialog.removeData('mData');
		var $inputEmail = $('#inputEmail');
		if($inputEmail.length > 0)
			$inputEmail.popover('destroy');
		$(document.body).removeData('showingModal');
	});
	$('#spinner .spinner').remove();
	$mDialog.modal('show');
};
modal.showSpinner = function(){
	$('#modalDialog input').prop('disabled', true);
	$('#modalDialog button').prop('disabled', true);
	$('#tbs-modal .modal-backdrop').unbind('click');
	var spinner = new Spinner({ lines: 9, length: 5, radius: 6, width: 3, color: '#000', left:'2', top: '2'}).spin($('#spinner')[0]);	
};
modal.hideSpinner = function(){
	$('#modalDialog input').prop('disabled', false);
	$('#modalDialog button').prop('disabled', false);
	$('#tbs-modal .modal-backdrop').bind('click', function(){
		$('#modalDialog').modal('hide');
	});
	$('#spinner .spinner').remove();
};
api = {};
api._getCSRFToken = function(){
	var cookies = document.cookie, key='_csrf';
	if(!cookies || cookies.length < 1) return null;
	cookies = cookies.split(';');
	for(var c in cookies){
		cookies[c] = cookies[c].trim();
		if(cookies[c].indexOf(key+'=') == 0)
			return cookies[c].replace(key+'=','');
	}
	return null;
}
api._safePost = function(url, postData, callbackMap){
	//postData should always be passed in as an object. If passed as string urlencoding issues happen. So pass as object and let jQuery do the heavy lifting
	//append csrf token to postdata params
	$.extend(callbackMap, {420: function(xhr, textStatus, error){
		console.log('420: Possible CSRF attack detected. The server dropped this request.');
	}});

	var _csrf = api._getCSRFToken();
	if(_csrf){
		if(!postData)
			postData = {};
		$.extend(postData, {'_csrf': _csrf});
	}

	$.ajax({
		type: 'POST',
		url: url,
		data: postData,
		statusCode: callbackMap
	});
};
api.subscribe = function(email){	
	api._safePost('/api/updates/subscribe', {e: email});
};
api.unsubscribe = function(email){
	api._safePost('/api/updates/unsubscribe', {e: email});
};
api.createUserAccount = function(email, pass, callbackMap){
	api._safePost('/api/user/create', {e: email, p: pass}, callbackMap);
};
api.authenticateUser = function(email, pass, callbackMap){
	api._safePost('/api/user/authenticate', {e: email, p: pass}, callbackMap);	
};
api.verifyEmail = function(code, callbackMap){
	api._safePost('/api/user/verify', {c: code}, callbackMap);
};
api.resendVerification = function(email, callbackMap){
	api._safePost('/api/user/resend', {e: email}, callbackMap);
};
api.forgotPassword = function(email, callbackMap){
	api._safePost('/api/user/reset', {e: email}, callbackMap);
};
api.changePassword = function(code, pass, callbackMap){
	api._safePost('/api/user/change', {c: code, p: pass}, callbackMap);
};
api.logout = function(callbackMap){
	api._safePost('/api/user/logout', null, callbackMap);
};
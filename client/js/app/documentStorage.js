documentStorage = {};
documentStorage.setItem = function(key, val){
	if(!val) return;
	val = $.isArray(val) ? $.merge([], val) : typeof val == 'object' ? $.extend(true, {}, val) : val;
	$(document).data(key, val);
};
documentStorage.getItem = function(key){
	var val = $(document).data(key);
	val = val ? val : null;
	val = $.isArray(val) ? $.merge([], val) : val ? typeof val == 'object' ? $.extend(true, {}, val) : val : null;
	return val;
};
documentStorage.removeItem = function(key){
	$(document).removeData(key);
};
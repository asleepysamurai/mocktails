var mockup = {};
mockup.deselectAll = function(showPopovers){
	if(!showPopovers) ui.dismissPopovers();
	$('#page-container .selected').removeClass('selected');
	//$('#element-resizer').remove();
	mockup.removeBorder();
};
mockup.selectAll = function(){
	mockup.deselectAll();

	$('#page-container .element').addClass('selected');
	$('input').blur();
	mockup.createBorder();
}
mockup.deleteElement = function(e,wa){
	ui.dismissPopovers();
	var $e = e ? $(e) : $('#page-container .selected');
	var se = [];
	$e.each(function(){var $t = $(this); se.push($t.attr('id')); mockup.removeElement($t);});				
	if(wa !== false)
		stateManager.writeAction(wa ? wa : 'remove',{elements:se});
	$('#' + stateManager.currentState.currentPage).click();
}
mockup.reinitElement = function(e){
	mockup.initClick(e);
}
mockup.initElement = function(e, clone, eid, wa, dontDraw){
	eid = eid ? eid : 'element-'+(++stateManager.currentState.elementCount);
	var $e = $(e);
	$e.removeAttr('id').attr('id',eid);
	$e.removeClass('element-definition').addClass('element');
	if(!dontDraw){
		if(paper[$e.attr('data-element')])
			$e.data('properties', $.extend(true, {}, paper[$e.attr('data-element')].properties.defaults));
		paper.draw($e);
	}
	//initGuides(e);
	mockup.reinitElement(e);
	if(!clone){
		//$e.click();
		wa = (wa === false) ? false : true;
		if(wa)
			stateManager.writeAction('add',{element:$e.attr('id')});
	}
	//actionsRegistry.push({type:'add',element:$(e).attr('id')});
}
mockup.initClick = function(e){
	$(e).click(function(ev){
		//$propsPanels = $('#propsPanels');
		//$propsPanels.hide();
		//$propsPanels.css('opacity',1);
		if(!ev.ctrlKey && !ev.metaKey && !$(this).hasClass('showingprops'))
			mockup.deselectAll();
		mockup.selectElementGroup($(this));
		ui.setInterface();
		//ev.stopPropagation();
	});					
}
//offset = {top:0, left:0};
mockup.initDrag = function(e){
	var $e = e ? $(e) : $('#page-container .selected');
	var $cPage = $('#' + stateManager.currentState.currentPage);
	var cpm = {top: parseInt($cPage.css('margin-top')), left: parseInt($cPage.css('margin-left'))};
	$e.draggable({
		snap: false,
		scroll: false,
		start: function(ev, _ui) {
			var $t = $(this), $s = $('#page-container .selected');
			mockup.startSnapElement($s,ev,_ui);
			$t.data('moved', true);
			if( !$t.hasClass('selected')) $t.click();
			$s.not($t).each(function() {
				var el = $(this);
				el.data('offset', el.offset());
			});
			$t.data('offset',$t.offset());
		},
		drag: function(ev, _ui) {
			var $t = $(this), $s = $('#page-container .selected')
			var offset = $t.data('offset');
			var dt = _ui.position.top - offset.top, dl = _ui.position.left - offset.left;
			$s.each(function(){
				 var el = $(this), off = el.data("offset");
				 el.css({top: off.top + dt, left: off.left + dl});
			});
			var md = mockup.actionSnapElement($s,ev,_ui);
			if(md.x){
				$t.css('top', md.x.ac);
				_ui.position.top = parseInt(md.x.ac);
			}
			if(md.y){
				$t.css('left', md.y.ac);
				_ui.position.left = parseInt(md.y.ac);
			}
			dt = _ui.position.top - offset.top, dl = _ui.position.left - offset.left;
			$s.each(function(){
				 var el = $(this), off = el.data("offset");
				 el.css({top: off.top + dt, left: off.left + dl});
			});
			ui.showMoveTooltip($t, {top: ev.pageY, left: ev.pageX});
		},
		stop: function(ev,_ui){
			var $s = $('#page-container .selected');
			mockup.stopSnapElement($s,ev,_ui);
			ui.hideTooltip();
			mockup.moveAndResizeEnded($s, null, null, null, true);	
			ui.initScrollers();
		}
	});
}
mockup.startSnapElement = function($e,ev,_ui){
	var $candidates = $('#'+stateManager.currentState.currentPage+' .element, #'+stateManager.currentState.currentPage).not($e);
	var snapCandidates = [];
	$candidates.each(function(i,l){
		var $t = $(this);
		var sp = mockup.getBoxCoords($t);
		sp.xmid = Math.floor(sp.top+(sp.height)/2);
		sp.ymid = Math.floor(sp.left+(sp.width)/2);
		snapCandidates.push({e: $t, p: sp});
	});
	$e.data('snapCandidates', snapCandidates);
}
mockup.actionSnapElement = function($e,ev,_ui, posmod, handle){
	var snapTolerance = 5;
	var snapCandidates = $e.data('snapCandidates');
	var pos = mockup.getBoxCoords($e, posmod);
	$('#guide-x').remove();
	$('#guide-y').remove();
	pos.l = {}, pos.r = {}, pos.t = {}, pos.b = {}, pos.xm = {}, pos.ym = {};
	pos.l.l = pos.left - snapTolerance, pos.l.t = pos.left + snapTolerance;
	pos.r.l = pos.right - snapTolerance, pos.r.t = pos.right + snapTolerance;
	pos.t.l = pos.top - snapTolerance, pos.t.t = pos.top + snapTolerance;
	pos.b.l = pos.bottom - snapTolerance, pos.b.t = pos.bottom + snapTolerance;
	pos.xmid = (pos.top+(pos.height)/2), pos.xm.l = pos.xmid - snapTolerance, pos.xm.t = pos.xmid + snapTolerance;
	pos.ymid = (pos.left+(pos.width)/2), pos.ym.l = pos.ymid - snapTolerance, pos.ym.t = pos.ymid + snapTolerance;
	var shortlist = [];
	for(var c in snapCandidates){
		var sp = snapCandidates[c].p;
		var sx = $e.data('snapX'), sy = $e.data('snapY');
		if(sp.left >= pos.l.l && sp.left <= pos.l.t && (!handle || handle.indexOf('ui-resizable-nw') != -1 || handle.indexOf('ui-resizable-sw') != -1 || handle.indexOf('ui-resizable-w') != -1)){
			var d = pos.left - sp.left;
			shortlist.y = shortlist.y && Math.abs(shortlist.y.d)<Math.abs(d) ? shortlist.y : {st: 'll', d: d, sp: sp};
		}
		if(sp.right >= pos.l.l && sp.right <= pos.l.t && (!handle || handle.indexOf('ui-resizable-nw') != -1 || handle.indexOf('ui-resizable-sw') != -1 || handle.indexOf('ui-resizable-w') != -1)){
			var d = pos.left - sp.right;
			shortlist.y = shortlist.y && Math.abs(shortlist.y.d)<Math.abs(d) ? shortlist.y : {st: 'lr', d: d, sp: sp};
		}
		if(sp.left >= pos.r.l && sp.left <= pos.r.t && (!handle || handle.indexOf('ui-resizable-ne') != -1 || handle.indexOf('ui-resizable-se') != -1 || handle.indexOf('ui-resizable-e') != -1)){
			var d = pos.right - sp.left;
			shortlist.y = shortlist.y && Math.abs(shortlist.y.d)<Math.abs(d) ? shortlist.y : {st: 'rl', d: d, sp: sp};
		}
		if(sp.right >= pos.r.l && sp.right <= pos.r.t && (!handle || handle.indexOf('ui-resizable-ne') != -1 || handle.indexOf('ui-resizable-se') != -1 || handle.indexOf('ui-resizable-e') != -1)){
			var d = pos.right - sp.right;
			shortlist.y = shortlist.y && Math.abs(shortlist.y.d)<Math.abs(d) ? shortlist.y : {st: 'rr', d: d, sp: sp};
		}
		if(sp.ymid >= pos.ym.l && sp.ymid <= pos.ym.t && !handle){
			var d = pos.ymid - sp.ymid;
			shortlist.y = shortlist.y && Math.abs(shortlist.y.d)<Math.abs(d) ? shortlist.y : {st: 'ym', d: d, sp: sp};
		}
		if(sp.top >= pos.t.l && sp.top <= pos.t.t && (!handle || handle.indexOf('ui-resizable-nw') != -1 || handle.indexOf('ui-resizable-ne') != -1 || handle.indexOf('ui-resizable-n') != -1)){
			var d = pos.top - sp.top;
			shortlist.x = shortlist.x && Math.abs(shortlist.x.d)<Math.abs(d) ? shortlist.x : {st: 'tt', d: d, sp: sp};
		}
		if(sp.bottom >= pos.t.l && sp.bottom <= pos.t.t && (!handle || handle.indexOf('ui-resizable-nw') != -1 || handle.indexOf('ui-resizable-ne') != -1 || handle.indexOf('ui-resizable-n') != -1)){
			var d = pos.top - sp.bottom;
			shortlist.x = shortlist.x && Math.abs(shortlist.x.d)<Math.abs(d) ? shortlist.x : {st: 'tb', d: d, sp: sp};
		}
		if(sp.top >= pos.b.l && sp.top <= pos.b.t && (!handle || handle.indexOf('ui-resizable-sw') != -1 || handle.indexOf('ui-resizable-se') != -1 || handle.indexOf('ui-resizable-s') != -1)){
			var d = pos.bottom - sp.top;
			shortlist.x = shortlist.x && Math.abs(shortlist.x.d)<Math.abs(d) ? shortlist.x : {st: 'bt', d: d, sp: sp};
		}
		if(sp.bottom >= pos.b.l && sp.bottom <= pos.b.t && (!handle || handle.indexOf('ui-resizable-sw') != -1 || handle.indexOf('ui-resizable-se') != -1 || handle.indexOf('ui-resizable-s') != -1)){
			var d = pos.bottom - sp.bottom;
			shortlist.x = shortlist.x && Math.abs(shortlist.x.d)<Math.abs(d) ? shortlist.x : {st: 'bb', d: d, sp: sp};
		}
		if(sp.xmid >= pos.xm.l && sp.xmid <= pos.xm.t && !handle){
			var d = pos.xmid - sp.xmid;
			shortlist.x = shortlist.x && Math.abs(shortlist.x.d)<Math.abs(d) ? shortlist.x : {st: 'xm', d: d, sp: sp};
		}
	}
	if(shortlist.x){
		var left = Math.min(shortlist.x.sp.left, pos.left), top = (shortlist.x.st.charAt(1) == 't' ? shortlist.x.sp.top : shortlist.x.st.charAt(1) == 'b' ? shortlist.x.sp.bottom : shortlist.x.sp.xmid);
		var $gy = $('<div id="guide-y" class="boxdiv guide" style="width:'+Math.abs(Math.max(shortlist.x.sp.right, pos.right)-left)+'px;height:2px;left:'+left+'px;top:'+top+'px;"></div>').appendTo('#'+stateManager.currentState.currentPage);
		shortlist.x.ac = Math.round(shortlist.x.st.charAt(0) == 't' ? top : shortlist.x.st.charAt(0) == 'b' ? top - pos.height : top - pos.height/2) + 'px';
	}
	if(shortlist.y){
		var top = Math.min(shortlist.y.sp.top, pos.top), left = (shortlist.y.st.charAt(1) == 'l' ? shortlist.y.sp.left : shortlist.y.st.charAt(1) == 'r' ? shortlist.y.sp.right : shortlist.y.sp.ymid);
		var $gx = $('<div id="guide-x" class="boxdiv guide" style="height:'+Math.abs(Math.max(shortlist.y.sp.bottom, pos.bottom)-top)+'px;width:2px;left:'+left+'px;top:'+top+'px;"></div>').appendTo('#'+stateManager.currentState.currentPage);
		shortlist.y.ac = Math.round(shortlist.y.st.charAt(0) == 'l' ? left : shortlist.y.st.charAt(0) == 'r' ? left - pos.width : left - pos.width/2) + 'px';
	}
	return {x: shortlist.x ? shortlist.x : null, y: shortlist.y ? shortlist.y : null};
}
mockup.stopSnapElement = function($e,ev,_ui){
	$('#guide-x').remove();
	$('#guide-y').remove();
	$e.removeData('snapCandidates');
}
mockup.refreshElementSize = function(){
	var s = $('#page-container .selected');
	if(s.length > 1) return;
	var $er = $('#element-resizer');
	var bp = $er.position();
	s.css({top: bp.top, left: bp.left, width: $er.width(), height: $er.height()});
}
mockup.initResizable = function(e){
	var $e = $(e);
	if(!$e || ($(stateManager.currentState.currentSelectedElement).data('properties') && $(stateManager.currentState.currentSelectedElement).data('properties').autosize)) return;
	var minprops = paper[$('#'+$e.data('resizerfor')).attr('data-element')].properties.min;
	var props = $('#'+$e.data('resizerfor')).data('properties');
	$e.resizable({
		handles: minprops && minprops.handles ? minprops.handles : 'all',
		snap: false,
		minHeight: minprops && minprops.height ? (props && props.orient != 1 ? minprops.height : minprops.width) : 20,
		minWidth: minprops && minprops.width ? (props && props.orient != 1 ? minprops.width : minprops.height) : 20,
		start: function(ev,_ui){
			$s = $('#page-container .selected');
			paper.redraw($s,true);
			mockup.startSnapElement($s,ev,_ui);
		},
		resize: function(e,_ui){
			//console.log('resizing');
			var $t = $(this), $s = $('#page-container .selected');
			//mockup.refreshElementSize();
			$s.css({'width': _ui.helper.css('width'), 'height': _ui.helper.css('height'), 'left': _ui.helper.css('left'), 'top': _ui.helper.css('top')});
			var md = mockup.actionSnapElement($s,e,_ui, null, $t.data('handle'));
			if(md.x && md.x.st != 'xm'){
				if(md.x.st.charAt(0) == 't'){
					$s.css('height', '+=' + md.x.d + 'px');
					_ui.helper.css('height', '+=' + md.x.d + 'px');
					$s.css('top',md.x.ac);
					_ui.helper.css('top',md.x.ac);
				}
				else if(md.x.st.charAt(0) == 'b'){
					$s.css('height', '-=' + md.x.d + 'px');
					_ui.helper.css('height', '-=' + md.x.d + 'px');
				}
			}
			if(md.y && md.y.st != 'ym'){
				if(md.y.st.charAt(0) == 'l'){
					$s.css('width', '+=' + md.y.d + 'px');
					_ui.helper.css('width', '+=' + md.y.d + 'px');
					$s.css('left',md.y.ac);
					_ui.helper.css('left',md.y.ac);
				}
				else if(md.y.st.charAt(0) == 'r'){
					$s.css('width', '-=' + md.y.d + 'px');
					_ui.helper.css('width', '-=' + md.y.d + 'px');
				}
			}
			ui.showResizeTooltip($t, {top: e.pageY, left: e.pageX});
			paper.redraw($s);
		},
		stop: function(ev,_ui){
			var $s = $('#page-container .selected');
			mockup.stopSnapElement($s,ev,_ui);
			mockup.refreshElementSize();
			mockup.resizeEnded($s);
			ui.hideTooltip();
			ui.initScrollers();
			$s.removeData('sanitized');
		}
	});
	$e.find('.ui-resizable-handle').mousedown(function(ev){
		var $t = $(this);
		$t.parent().data('handle', $t.attr('class'));
	});
	//console.log($(e).attr('class'));
	//$(e).resizable('option','disabled',true);
}
mockup.getBoxCoords = function(e, offset){
	var $e = e ? $(e) : $('#page-container .selected');
	var c = {};
	$e.each(function(){
		var $t = $(this);
		var tp = $t.position();
		if(offset)
			tp.left += offset.left, tp.top += offset.top;
		tp.right = (tp.left + $t.width());
		tp.bottom = (tp.top + $t.height());
		if((!c.left && c.left !== 0) || c.left > tp.left)
			c.left = tp.left;
		if((!c.top && c.top !== 0) || c.top > tp.top)
			c.top = tp.top;
		if((!c.right && c.right !== 0) || c.right < tp.right)
			c.right = tp.right;
		if((!c.bottom && c.bottom !== 0) || c.bottom < tp.bottom)
			c.bottom = tp.bottom;
	});
	c.width = c.right - c.left;
	c.height = c.bottom - c.top;
	return c;
}
mockup.offsetElements = function(e,i){
	var $e = e ? $(e) : $('#page-container .selected');
	i = i ? i *20 : 20;
	$e.each(function(){
		var $t = $(this);
		$t.css({
			top: (parseInt($t.css('top'))+ i),
			left: (parseInt($t.css('left'))+ i)
		});
		//console.log([$(this).css('top'),$(this).css('left')]);
	});
}
mockup.moveAndResizeEnded = function(e, action, o, wa, dontPerform){
	var se=[];
	$(e).each(function(){
		var $t = $(this);
		var tid = $t.attr('id');
		se.push(tid);
		if(action) action($t);
	});
	mockup.refreshBorder(null, null, true);
	var $er = $('#element-resizer');
	if(!dontPerform) ui.showMoveTooltip($er);
	wa = (wa === false) ? false : true;
	if(wa)
		stateManager.writeAction('moveandresize',{elements:se});
}
mockup.resizeEnded = function(e, action, wa){
	var $sel = $('#page-container .selected');
	if($sel.length > 1) return;
	diff = [], se=[], ndiff = [], $e = $(e);
	$e.each(function(){
		var $t = $(this);
		se.push($t.attr('id'));
		if(action) action($t);
	});
	if(!$e.is('.current-page'))
		mockup.refreshBorder($e, null, true);
	wa = (wa === false) ? false : true;
	if(wa){
		stateManager.writeAction('resize',{elements:se});
		$sel.removeData('sanitized');
	}
	else{
		var $er = $('#element-resizer');
		ui.showResizeTooltip($er);
		paper.redraw($sel, true);
	}
}
mockup.sendToClipboard = function(e, append){
	$('#clipboard').html('');
	$(e).clone(true).removeAttr('id').appendTo('#clipboard');
}
mockup.removeElement = function(e){
	var $e = e ? $(e) : $('#page-container .selected');
	$e.each(function(){
		var $t = $(this);
		if($t.is('#element-resizer'))
			$('#page-container .selected').remove();
		$t.remove();
	});
	//$('#element-resizer').remove();
	mockup.removeBorder();
}
mockup.copy = function(e,move){
	var $e = e ? $(e) : $('#page-container .selected');
	mockup.sendToClipboard($e);
	var $clipboard = $('#clipboard');
	$clipboard.data('pastecount',0);
	$clipboard.data('type','copy');
	$clipboard.data('from',stateManager.currentState.currentPage);
}
mockup.cut = function(e,wa){
	var $e = e ? $(e) : $('#page-container .selected');
	mockup.sendToClipboard($e);
	mockup.removeElement($e);
	var $clipboard = $('#clipboard');
	$clipboard.data('pastecount',0);
	$clipboard.data('type','cut');
	$clipboard.data('from',stateManager.currentState.currentPage);
	if(wa !== false)
		stateManager.writeAction('cut',{elements: $e.map(function() { return $(this).attr('id') }).get()});
}
mockup.paste = function(e,wa){
	mockup.deselectAll();
	var $e = e ? $(e) : $('#clipboard').children();
	var $clipboard = $('#clipboard');
	var pc = $clipboard.data('pastecount');
	$clipboard.data('pastecount',++pc);
	var ng = {}, cei = [];
	$e.each(function(){
		var ceid = 'element-'+(++stateManager.currentState.elementCount);
		var $t = $(this);
		var $ce = $t.clone().removeAttr('id').attr('id',ceid).appendTo('.current-page');
		stateManager.currentState.currentSelectedElement = $ce;
		cei.push(ceid);
		$ce.data('properties', $.extend(true, {}, $t.data('properties')));
		$ce.css({'z-index': ++stateManager.currentState.lastZIndex});
		mockup.initElement($ce, true, ceid, false, true);
		if($clipboard.data('from') == stateManager.currentState.currentPage)
			mockup.offsetElements($ce,$clipboard.data('pastecount'));
		var g = $t.data('groups'), cg = [];
		if(g){
			$.each(g,function(i,l){
				if(!ng[this])
					ng[this] = ++stateManager.currentState.pagewiseUID;
				cg[i] = ng[this];
				$ce.removeClass('member-group-'+this).addClass('member-group-'+cg[i]);
			});
			$ce.data('groups',cg);
		}
		mockup.selectElement($ce, true);
	});
	if($clipboard.data('type') == 'cut')
		$clipboard.html('');
	if(wa !== false)
		stateManager.writeAction('paste',{elements: cei});
}
mockup.boxCoordsForElement = function(e){
	if(!e) return;
	var $e = $(e);
	$e.show();
	var bc = $e.position();
	bc = {top: bc.top, left: bc.left, width: $e.width(), height: $e.height()};
	bc.bottom = bc.top + bc.height, bc.right = bc.left + bc.width;
	return bc;				
}
mockup.refreshBorder = function(ne, remove, force, showPopovers){
	if(!showPopovers) ui.dismissPopovers();
	var $er = $('#element-resizer'), ac = {};	
	if($er.length == 0) return mockup.createBorder();
	var $se = $('#page-container .selected');
	if($se.length == 0 && !showPopovers) return mockup.removeBorder();
	if($se.length > 1) $er.resizable('destroy');
	else if($se.length == 1){ 
		$er.data('resizerfor',$se.attr('id'));
		stateManager.currentState.currentSelectedElement = $se[0];
		mockup.initResizable($er);
	}
	var bc = mockup.boxCoordsForElement($er);
	var ec = mockup.boxCoordsForElement(ne);
	if(force || (remove && !(ec.top < bc.top || ec.bottom > bc.bottom || ec.left < bc.left || ec.right > bc.right))){
		ac = mockup.getBoxCoords($se);
	}
	else{
		var ac = {
			top : ec.top < bc.top ? ec.top : bc.top,
			bottom: ec.bottom > bc.bottom ? ec.bottom : bc.bottom,
			left : ec.left < bc.left ? ec.left : bc.left,
			right : ec.right > bc.right ? ec.right : bc.right,
		};
		ac.height = ac.bottom - ac.top, ac.width = ac.right - ac.left;
	}
	$er.css({'top': ac.top, 'left': ac.left, 'width': ac.width, 'height': ac.height});		
	ui.setInterface();
	//showProperties();
}
mockup.removeBorder = function(){
	$('#element-resizer').tooltip('destroy').remove();
}
mockup.createBorder = function(e){
	var $e = e ? $(e) : $('#page-container .selected');
	if($e.length < 1) return;
	var bc = mockup.getBoxCoords($e);
	//$('#element-resizer').remove();
	mockup.removeBorder();
	var $be = $('<div id="element-resizer"></div>').appendTo('#page-container .current-page');
	$be.css({'height': bc.height, 'width': bc.width, 'top': bc.top, 'left': bc.left,'background-color':'none','z-index':'1000001','position':'absolute'});
	$be.data('resizerfor', $e.attr('id'));
	mockup.initDrag($be);
	if($e.length == 1) mockup.initResizable($be);
	$be.click(function(ev){
		if(!$('#page-container').data('dblc')){
			var $t = $(this), ar = [];
			ev.stopPropagation();
			$t.hide();
			/*
			var $el, ela = [];
			do{
				$el = $(document.elementFromPoint(ev.pageX, ev.pageY));
				ela.push($el);
				$el.hide();
			}while(!$el.is('.element') || $el.is($e))
			*/
			$el = $(document.elementFromPoint(ev.pageX, ev.pageY));
			var e = jQuery.Event('click');
			//e.ctrlKey = $(el).attr('id') == $(this).data('resizerfor') ? true : (ev.metaKey || ev.ctrlKey);
			e.ctrlKey = ev.ctrlKey || ev.metaKey;
			$el.trigger(e);		
			/*
			for(var el in ela)
				ela[el].show();
			*/
			$t.show();
			if(!(ev.ctrlKey || ev.metaKey || $el.hasClass('page')))
				ui.showProperties();
			if($('#page-container .selected').length == 0)
				$('#page-container .showingprops').removeClass('showingprops');
			//$e.popover('destroy');
			$('#page-container').data('dblc', true);
			window.setTimeout(function(){$('#page-container').removeData('dblc');}, 500);
		}
	});
}
mockup.selectElementGroup = function(e, forceSelect){
	var $e = $(e);
	var eg = mockup.effectiveGroup($e);
	//console.log('Selecting members of group: ' + eg);
	if(eg)
		$('#page-container .member-group-' + eg).not($e).each(function(){mockup.selectElement($(this), forceSelect);});
	mockup.selectElement($e, forceSelect, true);
}
mockup.selectElement = function(e, forceSelect, thisClicked){
	var $e = $(e);
	if(thisClicked) stateManager.currentState.currentSelectedElement = $e;
	if(forceSelect && $e.hasClass('selected')) return;
	$e.toggleClass('selected');
	if($e.hasClass('showingprops')) $e.addClass('selected');
	if($e.hasClass('selected')){
		$('input:focus').blur();
		if($('#page-container .selected').length > 1)
			mockup.refreshBorder($e);
		else
			mockup.createBorder($e);
	}
	else{
		mockup.refreshBorder($e, true,null,$e.hasClass('showingprops') ? true : false);
	}
}
mockup.zIndexSort = function(e, rev){
	return $(e).sort(function(a,b){
		var zi = parseInt($(b).css('z-index')) - parseInt($(a).css('z-index'));
		return rev ? (zi * -1) : zi;
	});					
}
mockup.getElementsIntersecting = function(e,rev){
	var ig = [];
	var $e = $(e);
	var ie = $('#page-container .element').filter(function(){
		var $t = $(this);
		var tp = $t.position();
		var tc = {left: tp.left, right: (tp.left + $t.width()), top: tp.top, bottom: (tp.top + $t.height())};
		var ege = mockup.effectiveGroup($e), intersects = false;
		if(ege){
			$('#page-container .member-group-'+ege).each(function(){
				var ep = $t.position();
				var ec = {left: ep.left, right: (ep.left + $t.width()), top: ep.top, bottom: (ep.top + $t.height())};
				intersects = !(tc.left > ec.right || tc.right < ec.left || tc.top > ec.bottom || tc.bottom < ec.top);
				if(intersects) return false;
			});
		}
		else{						
			var ep = $e.position();
			var ec = {left: ep.left, right: (ep.left + $e.width()), top: ep.top, bottom: (ep.top + $e.height())};
			intersects = !(tc.left > ec.right || tc.right < ec.left || tc.top > ec.bottom || tc.bottom < ec.top);
		}
		if(!intersects)
			return false;
		else{
			var eg = mockup.effectiveGroup($t);
			if(eg && ig.indexOf(eg) == -1){
				ig.push(eg);
				return false;
			}
			return true;
		}
	});	
	$.each(ig, function(i,l){
		ie = $('#page-container .member-group-'+this).add(ie);
	});
	//console.log(ie);
	return mockup.zIndexSort(ie,rev);
}
mockup.swapZIndex = function(a,b){
	var $a = $(a), $b = $(b);
	var t = $a.css('z-index');
	$a.css('z-index',$b.css('z-index'));
	$b.css('z-index',t);
}
mockup.moveElement = function(forward,extreme,selected,wa){
	var $selected = selected ? $(selected) : $('#page-container .selected');
	var se = mockup.zIndexSort($selected,!forward).get(0);
	if(!se) return;
	wa = (wa === false) ? false : true;
	var elements = mockup.getElementsIntersecting(se, forward);
	var me = [], dm = false;
	$(elements).each(function(){
		me.push($(this).attr('id'));
	});
	var l = elements.length;
	if(l < 2) return;
	var i = elements.index(se);
	if(i > -1 && i < (l-1)){
		var ne = elements.get(++i);
		var td = null, c=0;
		do{
			td = mockup.effectiveGroup(ne);
			ne = elements.get(++i);
			if(i>l)
				break;
			c++;
		}while(extreme ? true : mockup.effectiveGroup(ne) == td && td);
		mockup.zIndexSort($selected,forward).each(function(){
			for(j=0;j<c;++j){
				var $t = $(this);
				var k = elements.index($t);
				//console.log([k,$(this)]);
				mockup.swapZIndex($t,elements.get(k+j+1));
				if(!dm) dm = true;
			}
		});
	}
	if(wa && dm){
		var type;
		if(!forward && !extreme)
			type = 'backward';
		if(forward && !extreme)
			type = 'forward';
		if(!forward && extreme)
			type = 'back';
		if(forward && extreme)
			type = 'front';
		stateManager.writeAction(type,{elements: me});
	}
}
mockup.isMultiGrouped = function(sel){
	var gid = null, mg = false;
	sel = sel ? sel : '.selected';
	$(sel).each(function(){
		var tgid = $(this).data('groups');
		if(tgid && tgid.length > 0)
			tgid = tgid[tgid.length - 1];
		if(gid && gid != tgid)
			return mg = true;
		else
			gid = tgid;
	});				
	return mg;
}
mockup.isGrouped = function(sel){
	var ag = false;
	sel = sel ? sel : '.selected';
	$(sel).each(function(){
		if($(this).data('groups'))
			ag = true;
		else
			return ag = false;
	});
	return ag;
}
mockup.effectiveGroup = function(e){
	var g = $(e).data('groups');
	if(g && g.length > 0)
		return g[g.length - 1];
	else
		return null;
}
mockup.setHeight = function(v,e){
	var $e = e ? $(e) : stateManager.currentState.currentSelectedElement;
	$e.height(parseInt(v));
	//setSize($e);
}
mockup.setWidth = function(v,e){
	var $e = e ? $(e) : stateManager.currentState.currentSelectedElement;
	$e.width(parseInt(v));
	//setSize($e);
}
mockup.setTop = function(v,e,i){
	var $e = e ? $(e) : stateManager.currentState.currentSelectedElement;
	v = parseInt(v);
	if(i) v += parseInt($e.css('top'));
	$e.css('top', v);
	//setOrigin($e);
}
mockup.setLeft = function(v,e,i){
	var $e = e ? $(e) : stateManager.currentState.currentSelectedElement;
	v = parseInt(v);
	if(i) v += parseInt($e.css('left'));
	$e.css('left', v);
	//setOrigin($e);
}
mockup.pushData = function(e,d,v){
	var $e = $(e);
	var g = $e.data(d);
	if(!g) g = [];
	g.push(v);
	$e.data(d,g);				
}
mockup.groupElements = function(e,g,wa){
	var $e = e ? $(e) : $('#page-container .selected');
	wa = (wa === false) ? false : true;
	g = g ? g : ++stateManager.currentState.pagewiseUID;
	var se = [];
	var go = {};
	mockup.zIndexSort($e, true).each(function(){
		var $t = $(this);
		mockup.pushData($t,'groups',g)
		//unlockElements($(this),false);
		$t.addClass('member-group-' + g);
		go[$t.attr('id')] = $t.css('z-index');
		//se.push(go);
		$t.css({
			position: 'absolute',
			'z-index': ++stateManager.currentState.lastZIndex
		});
	});
	if(wa){
		stateManager.writeAction('group',{group: g, elements:go});
	}
	//$e.click();
	//actionsRegistry.push({type: 'group', group: stateManager.groupCount, elements: se});
	//$('#group').val('Ungroup');
	ui.setInterface();
}
mockup.popData = function(e,d){
	var $e = $(e);
	var g = $e.data(d);
	if(g) gv = g.pop();
	if(g && g.length > 0)
		$e.data(d,g);
	else
		$e.removeData(d);
	return gv;
}
mockup.ungroupElements = function(e,z,wa){
	var $e = e ? $(e) : $('#page-container .selected');
	mockup.deselectAll();
	wa = (wa === false) ? false : true;
	var se = [];
	var gv = null;
	$e.each(function(){
		var $t = $(this);
		gv = mockup.popData($t,'groups');
		if(gv) $t.removeClass('member-group-' + gv);
		if(z){
			//console.log(z[$(this).attr('id')]);
			$t.css('z-index', z[$t.attr('id')]);
		}
		se.push($t.attr('id'));
	});
	if(wa)
		stateManager.writeAction('ungroup',{group:gv,elements:se});
	//actionsRegistry.push({type: 'ungroup', group: gv, elements: se});
	mockup.deselectAll();
	ui.setInterface();
	//$('#group').val('Group');
}
mockup.alignElements = function(e,a,wa){
	if(typeof e == 'string' && !a)
		a = e, e = null;
	if(!a || a.length != 3) return;
	wa = (wa === false) ? false : true;
	var $e = e ? $(e) : $('#page-container .selected');
	var boxcoords, diff, el = [], o = [], n = [], pg = [];
	if(a.charAt(2) == 's')
		boxcoords = mockup.getBoxCoords($('#page-container .selected'));
	else{
		var $currentPage = $('#' + stateManager.currentState.currentPage);
		boxcoords = {top: parseInt($currentPage.css('top')), left: parseInt($currentPage.css('left')), width: $currentPage.width(), height: $currentPage.height()};
		boxcoords.right = boxcoords.left + boxcoords.width, boxcoords.bottom = boxcoords.top + boxcoords.height;
	}
	$e.each(function(){
		var $t = $(this);
		var eg = mockup.effectiveGroup($t), tc = null, te ,td, tc;
		if(eg && pg.indexOf(eg) == -1){
			te = $('#page-container .member-group-'+eg);
			tc = mockup.getBoxCoords(te);
			pg.push(eg);
		}
		else if(eg)
			return;
		if(!tc)
			tc = {height: $t.height(), width: $t.width()};
		el.push($t.attr('id'));
		if(a.charAt(0) == 'h'){
			o.push($(this).css('left'));
			if(a.charAt(1) == 'l')
				diff = boxcoords.left;
			else if(a.charAt(1) == 'r')
				diff = boxcoords.right - tc.width;
			else
				diff = boxcoords.right - (boxcoords.width/2) - (tc.width/2);
			$t.css('left',diff+'px');
			//$('#element-resizer-'+ $(this).attr('id')).css('left',diff+'px');
			mockup.refreshBorder(null, null, true);
			tc = 'left';
		}
		else if(a.charAt(0) == 'v'){
			o.push($(this).css('top'));
			if(a.charAt(1) == 't')
				diff = boxcoords.top;
			else if(a.charAt(1) == 'b')
				diff = boxcoords.bottom - tc.height;
			else
				diff = boxcoords.bottom - (boxcoords.height/2) - (tc.height/2);
			$t.css('top',diff+'px');
			//$('#element-resizer-'+ $(this).attr('id')).css('top',diff+'px');
			mockup.refreshBorder(null, null, true);
			tc = 'top';
		}
		if(te){
			td = parseInt(o[o.length-1]) - diff;
			if(td < 0)
				td = '+='+Math.abs(td)+'px';
			else
				td = '-='+td+'px';
			te.not($t).each(function(){
				var $th = $(this);
				el.push($th.attr('id'));
				$th.css(tc,td);
				mockup.refreshBorder(null, null, true);
			});
		}
	});
	if(wa)
		stateManager.writeAction('align',{elements:el});
}
mockup.createElement = function(element, pos, size){
	var ne = $('#' + element + '-definition', '#element-definitions').clone().removeAttr('id');
	if(!pos)
		pos = {top: -5000, left: -5000};
	ne.css({
		top: pos.top,
		left: pos.left,
		'z-index': ++stateManager.currentState.lastZIndex
	});
	$('#' + stateManager.currentState.currentPage).append(ne);
	if(size){
		ne.width(size.width);
		ne.height(size.height);
	}
	mockup.initElement(ne);
	return ne;
}
mockup.createElementTemplate = function(parent, ecid, ele, pos, wa, master){
	ele = ele ? ele : master ? persistanceManager.getMasterElementFromProject(ecid, stateManager.currentState.currentProject).e : persistanceManager.getElementTemplate(ecid).e;
	var ea = [], isDef = parent.indexOf('definition') != -1;
	if(!isDef) ++stateManager.currentState.pagewiseUID;
	for(var e in ele){
		var $div = $('#'+ele[e].element+'-definition').clone().removeAttr('id').appendTo(parent);
		if(!isDef)
			mockup.initElement($div, null, null, false);
		if(pos){
			ele[e].top += parseInt(pos.top);
			ele[e].left += parseInt(pos.left);
			ele[e]['z-index'] = ++stateManager.currentState.lastZIndex;
		}
		stateManager.restoreElementCSSandProperties($div, ele[e]);
		if(master){
			$div.data('master', parseInt(ecid));
			$div.addClass('member-master-'+ecid+' member-master'+' master-mid-'+e);
			if(!isDef){
				$div.addClass('master-iid-'+stateManager.currentState.pagewiseUID);
				$div.data('master-iid', stateManager.currentState.pagewiseUID);
			}
			$div.data('master-mid',parseInt(e));
		}
		if(isDef)
			$div.removeClass('element');
		paper.draw($div);
		ea.push($div.attr('id'));
	}
	if(wa !== false){
		var type = wa ? wa : 'add';
		stateManager.writeAction(type,{elements:ea});
	}
}
mockup.getPreparedSnapshot = function($sel, master, edit){
	var el = stateManager.snapshot($sel, master);
	var bc = mockup.getBoxCoords($sel);
	var ela = master ? {} : [], i = 0;
	for(var e in el){
		el[e].top -= bc.top;
		el[e].left -= bc.left;
		if(el[e].groups) delete el[e].groups;
		if(master){
			if(edit)
				i = el[e].mid;
			ela[i++] = el[e];
		}
		else
			ela.push(el[e]);
	}
	return {ela: ela, bc: bc};
}
mockup.deleteGroupedElement = function(eid, master){
	if(master){
		$('#page-container .member-master-'+eid).each(function(){
			var $t = $(this), mid = $t.data('master-mid'), iid = $t.data('master-iid'), m = $t.data('master');
			$t.removeClass('member-master-'+m).removeClass('master-mid-'+mid).removeClass('master-iid-'+iid).removeClass('member-master')
				.removeData('master-mid').removeData('master-iid').removeData('master');
		});
		persistanceManager.revertMasterInstancesForProject(eid, stateManager.currentState.currentProject);
		persistanceManager.removeMasterElementFromProject(eid, stateManager.currentState.currentProject);
		$('#master-'+eid+'-library-item').remove();
		ui.refreshOrInitScrollers('#masters-content');
	}
	else{
		persistanceManager.removeElementTemplate(eid);
		$('#template-'+eid+'-library-item').remove();
		ui.refreshOrInitScrollers('#templates-content');
	}
}
mockup.defineAsGroupedElement = function(name, master, $sel){
	var ela = mockup.getPreparedSnapshot($sel, master), bc;
	bc = ela.bc, ela = ela.ela;
	var eln = {}, index;
	if(master){
		index = ++stateManager.currentState.masterElementCount;
		eln[index] = {e: ela, n: name};
		persistanceManager.setMasterElementForProject(eln, stateManager.currentState.currentProject);
		persistanceManager.setMasterElementCountForProject(index, stateManager.currentState.currentProject);
		mockup.deleteElement('#page-container .selected', 'definemaster');
		mockup.createElementTemplate('#page-container .current-page', index, null, bc, 'definemaster', true);
	}
	else{
		index = ++stateManager.currentState.elementTemplateCount;
		eln[index] = {e: ela, n: name};
		persistanceManager.addNewElementTemplate(eln);
		persistanceManager.setElementTemplateCount(index);
	}
	ui.addItemToElementList(index, eln[index], master);
	if(master){
		ui.initElementRepresentations($('#master-'+index+'-library-item'));
		$('#masters-content').sbscroller('refresh');
		$('#masters-content .slider-vertical').slider('option', 'value', 0);
	}
	else{
		ui.initElementRepresentations($('#template-'+index+'-library-item'));
		$('#templates-content').sbscroller('refresh');
		$('#templates-content .slider-vertical').slider('option', 'value', 0);
	}	
}
mockup.editMasterElement = function($div){
	var mid = $div.data('master');
	if(!mid) return false;
	var iid = $div.data('master-iid');
	var ela = mockup.getPreparedSnapshot($('.member-master-'+mid+'.master-iid-'+iid, '#page-container'), true, true).ela;
	persistanceManager.editMasterElementForProject(mid, ela, stateManager.currentState.currentProject);	
	$('#master-'+mid+'-definition').remove();
	var $def = $('<div id="master-'+mid+'-definition" class="resizable element-definition member-master" data-element="master-'+mid+'"></div>').appendTo('#element-definitions');
	mockup.createElementTemplate('#master-'+mid+'-definition', mid, null, null, false, true);
	var $item = $('#master-'+mid+'-library-item');
	$item.draggable('destroy');
	ui.initLibraryElementDrag($item);
	ui.refreshElementRepresentations($item);

	//Apply changes to all master element instances on page
	var $members = $('.member-master-'+mid, '#page-container');
	var instances = [];
	$members.each(function(i,l){
		var iid2 = $(this).data('master-iid');
		if(iid2 != iid && instances.indexOf(iid2) == -1)
			instances.push(iid2);
	});
	for(var i in instances){
		var $el = $('.master-iid-'+instances[i], '#page-container');
		var bc = mockup.getBoxCoords($el);
		$el.each(function(i,l){
			var $t = $(this);
			$t.data('properties', ela[$t.data('master-mid')].custom);
			paper.draw($t);
		});
	}

	return $members;
}
mockup.defineElementTemplate = function(name){
	var $sel = $('#page-container .selected');
	mockup.defineAsGroupedElement(name, false, $sel);
}
mockup.defineMasterElement = function(name){
	var $sel = $('#page-container .selected');
	mockup.defineAsGroupedElement(name, true, $sel);
	/*
	$sel.addClass('member-master-'+stateManager.currentState.masterElementCount);
	$sel.addClass('member-master');
	$sel.data('master', stateManager.currentState.masterElementCount);
	*/
}
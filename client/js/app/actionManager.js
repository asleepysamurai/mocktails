var actionManager = {
	_actions: {},
	_pointer: {},
	undo: function(sheet){
		//$('#element-resizer').remove();
		mockup.removeBorder();
		if(!(actionManager._actions || actionManager._pointer)) return;
		var action = actionManager._actions[sheet][--actionManager._pointer[sheet]];
		if(!action) return ++actionManager._pointer[sheet];					
		for(var element in action){
			if(element != 'elements' && element != 'type' && element != 'custom'){
				stateManager.restoreElement('#'+element,action[element],element,action.custom);
				var $element = $('#' + element);
				if($element.is('.selected'))
					mockup.createBorder($element);
			}
		}
		$.each(action.elements,function(i,l){
			if(!action[this])
				mockup.removeElement($('#'+this));
		});
		var na = stateManager.diffSnapshot($('#'+action.elements.join(',#')), action.type, true);
		actionManager._actions[sheet][actionManager._pointer[sheet]] = na;
		ui.setStateInterface();
	},
	redo: function(sheet){
		//$('#element-resizer').remove();
		mockup.removeBorder();
		if(!(actionManager._actions || actionManager._pointer)) return;
		var action = actionManager._actions[sheet][actionManager._pointer[sheet]];
		if(!action) return;
		for(var element in action){
			if(element != 'elements' && element != 'type' && element != 'custom'){
				stateManager.restoreElement('#'+element,action[element],element,action.custom);
				var $element = $('#' + element);
				if($element.is('.selected'))
					mockup.createBorder($element);
			}
		}
		actionManager._actions[sheet][actionManager._pointer[sheet]] = stateManager.diffSnapshot($('#'+action.elements.join(',#')), action.type, true);
		++actionManager._pointer[sheet];
		ui.setStateInterface();
	},
	diff: function(old, sheet){
		if(!actionManager._actions[sheet]){
			actionManager._actions[sheet] = [];
			actionManager._pointer[sheet] = -1;
		}
		if(actionManager._pointer[sheet] > -1 && actionManager._pointer[sheet] < actionManager._actions[sheet].length)
			actionManager._actions[sheet] = actionManager._actions[sheet].slice(0, actionManager._pointer[sheet]);
		actionManager._actions[sheet].push(old);
		actionManager._pointer[sheet] = actionManager._actions[sheet].length;
	}
};
var stateManager = {
	currentState: {
		currentSelectedElement: null,
		pageCount: 0,
		lastZIndex: 10,
		pagewiseUID: 0,
		elementTemplateCount: 0,
		masterElementCount: 0,
		elementCount: 0,
		previewMode: false,
		currentProject: null,
		currentPage: null
	},
	newPageState: {		
		currentSelectedElement: null,
		lastZIndex: 10,
		pagewiseUID: 0,
		elementCount: 0,
	},
	newProjectState: {		
		pageCount: 0,
		masterElementCount: 0,
		currentPage: null
	},
	_commonCSS: {'top':0,'left':0,'width':60,'height':60,'z-index':'auto'},
	_getCustomProperties: function(e){
		var p = {};
		var $e = $(e);
		switch($e.attr('data-element')){
			case 'label':
				p = stateManager._getProperties($e, {'background-color':'none'});
				//p.text = $(e).text();
				break;
			case 'page':
				p = {
					title: $e.data('title'),
					_project: $e.data('project')
				};
				break;
		}
		return p;
	},
	_getProperties: function(e,props,nc){
		var p = {};
		var $e = $(e);
		$.each(props,function(i,l){
			//console.log([this,$(e).css(this+''),$(e).css('left')]);
			p[i] = $e.css(i+'');
			if(['top','left','width','height'].indexOf(i) != -1)
				p[i] = parseInt(p[i]);
		});
		if(nc){
			var groups = $e.data('groups');
			if(groups)
				p.groups = $.merge([], groups);
			p.master = $e.data('master');
			if(p.master){
				p.mid = $e.data('master-mid');
				p.iid = $e.data('master-iid')
			}
			else delete p.master;
			p.element = $(e).attr('data-element');
			if(p.element == 'page'){
				p.lzi = stateManager.currentState.lastZIndex;
				p.puid = stateManager.currentState.pagewiseUID;
				p.ec = stateManager.currentState.elementCount;
			}
		}
		return p;
	},
	snapshot: function(e, defineMaster){
		var es = {};
		var $e = e ? $(e) : $('.element,.page', '#page-container');
		$e.each(function(){
			var $t = $(this);
			var ep = stateManager._getProperties($t, stateManager._commonCSS, true);
			$.extend(ep, stateManager._getCustomProperties($t));
			if(typeof defineMaster == 'boolean' || !ep.master)
				ep.custom = $.extend(true, {}, $t.data('properties'));
			es[$t.attr('id')] = ep;
		});
		return es;
	},
	cleanup: function(sheet){
		var s = persistanceManager.getPageFromProject(sheet, stateManager.currentState.currentProject);
		if(!s) return;
		$.each(s,function(i,l){
			if(this.remove)
				delete s[i];
		});					
		persistanceManager.setPageForProject(sheet,s, stateManager.currentState.currentProject);
	},
	diffSnapshot: function(e, type, replace){
		var sheet = stateManager.currentState.currentPage;
		var s = persistanceManager.getPageFromProject(sheet, stateManager.currentState.currentProject);
		if(!s) s = {};
		var old = {}, $e = $(e), cprops;
		if(type == 'custom' && $e.hasClass('member-master')){
			old.custom = persistanceManager.getMasterElementFromProject($e.data('master'),stateManager.currentState.currentProject);
			$e = mockup.editMasterElement($e);
		}
		if($e.length == 0){
			e = typeof e == 'string' ? e : e.selector;
			old.elements = (e.substr(1).split(',#'));
			$.each(old.elements,function(i,l){
				old[this] = s[this];
				s[this] = {'remove':true};
			});
		}
		else{
			$e.each(function(){
				var $t = $(this);
				var tid = $t.attr('id');
				old[tid] = s[tid];
				if(!old[tid]) delete old[tid];
				if(!old.elements) old.elements = [];
				old.elements.push(tid);
			});
			var p = stateManager.snapshot($e);
			$.extend(s,p);
		}
		old.type = type;
		p = stateManager.snapshot($('#' + stateManager.currentState.currentPage));
		$.extend(s,p);
		persistanceManager.setPageForProject(sheet,s,stateManager.currentState.currentProject);
		persistanceManager.setCurrentPageForProject(sheet,stateManager.currentState.currentProject);
		if(!replace){
			if(type != 'closesheet' && type != 'definemaster') actionManager.diff(old, sheet);
		}
		else return old;
		stateManager._lastAction = type;
	},
	saveSnapshot: function(){
		var sheet = stateManager.currentState.currentPage;
		persistanceManager.setPageForProject(sheet,stateManager.snapshot(),stateManager.currentState.currentProject);
		persistanceManager.setCurrentPageForProject(sheet, stateManager.currentState.currentProject);
	},
	_restoreCustomProperties: function(e,p){
		switch(p.element){
			case 'label':
				stateManager._restoreCSS(e,{'background-color':'none'},p);
				//$(e).text(p.text);
				break;
		}
	},
	restoreElementCSSandProperties: function($ne,e){
		stateManager._restoreCSS($ne,stateManager._commonCSS,e);
		stateManager._restoreCustomProperties($ne,e);
		e = e.custom;
		if(e.linkto && !persistanceManager.getPageTitleForProject(e.linkto, stateManager.currentState.currentProject))
			delete e.linkto;
		else if(e.items){
			for(var i in e.items){
				for(var j in e.items[i])
					if(e.items[i][j].linkto && !persistanceManager.getPageTitleForProject(e.items[i][j].linkto, stateManager.currentState.currentProject))
						delete e.items[i][j].linkto;
			}
		}
		$ne.data('properties', $.extend(true,{},e));
	},
	restoreElement: function(ne,e,i,mprops){
		var $ne = $(ne);
		if(e.remove){
			$ne.remove();
			return;
		}
		if(!paper[e.element]) return;
		if($ne.length == 0){
			stateManager._createElement(i,e);
			return;
		}
		if(e.master){
			mprops = mprops ? mprops : persistanceManager.getMasterElementFromProject(e.master, stateManager.currentState.currentProject);
			e.custom = $.extend(true,{},mprops.e[e.mid].custom);
		}
		stateManager.restoreElementCSSandProperties($ne,e);
		if(e.element == 'page'){
			stateManager.currentState.lastZIndex = e.lzi ? e.lzi : 10;
			stateManager.currentState.pagewiseUID = e.puid ? e.puid : 0;
			stateManager.currentState.elementCount = e.ec ? e.ec : 0;
			$ne.data('title',e.title);
			$ne.data('project', e._project);
			//$('#workspace-tabs').html(persistanceManager.getProjectTitle(stateManager.currentState.currentProject) + '&nbsp;&nbsp;&#x00BB;&nbsp;&nbsp;'  + e.title);
			$('#page-title').text(e.title);
			$('#project-title').text(persistanceManager.getProjectTitle(stateManager.currentState.currentProject));
		}
		else
			paper.draw($ne);
		if(e.groups){
			$ne.data('groups',e.groups);
			$.each(e.groups,function(i,l){
				$ne.addClass('member-group-'+this);
				if(this >= stateManager.currentState.pagewiseUID)
					stateManager.currentState.pagewiseUID = this;
			});
		}
		else if($ne.data('groups')){
			$ne.attr('class',function(i, c){return c.replace(/\bmember-group-\S+/g, '');});
			$ne.removeData('groups');
		}
		if(e.master){
			$ne.data('master',e.master);
			$ne.addClass('member-master member-master-'+e.master+' master-mid-'+e.mid+' master-iid-'+e.iid);
			$ne.data('master-mid',e.mid);
			$ne.data('master-iid',e.iid);
		}
		else if($ne.data('master')){
			$ne.attr('class',function(i, c){return c.replace(/\bmember-master-\S+/g, '').replace('member-master', '');});
			$ne.removeData('master');
		}
		$ne.show();
		//if(e.locked)
		//	lockElements(ne,false);
	},
	_createElement: function(i,e){
		var $ne;
		if(e.element == 'page')
			$ne = $('#' + stateManager.currentState.currentPage);
		else{
			$ne = $('#'+e.element+'-definition', '#element-definitions').clone().removeAttr('id');
			$('#' + stateManager.currentState.currentPage).append($ne);
			mockup.initElement($ne, true, i, false, true);
		}
		stateManager.restoreElement($ne,e,i);
	},
	_restoreCSS: function(e,props,vals){
		$.each(props, function(i,l){
			if(!vals[i]) vals[i] = this;
			if(['top','left','width','height'].indexOf(i) != -1)
				vals[i] += 'px';
			if(vals.element == 'page' && i == 'width' && !vals[i])
				vals[i] = '1000px';
			else if(vals.element == 'page' && i == 'height' && !vals[i])
				vals[i] = '600px';
			$(e).css(i+'',vals[i]);
			if(i == 'z-index' && vals[i] != 'auto' && parseInt(vals[i]) >= stateManager.currentState.lastZIndex)
				stateManager.currentState.lastZIndex = parseInt(vals[i]);
		});
		if(vals.element != 'page')
			$(e).css('position', 'absolute');
	},
	restoreFromSnapshot: function(_page){
		//if(_page) stateManager._currentSheet = _page;
		var _page = _page ? _page : persistanceManager.getCurrentPage();
		if(!_page) return;
		var e = persistanceManager.getPageFromProject(_page, stateManager.currentState.currentProject);
		if(e)
			stateManager.cleanup(_page);
		else{
			page._new();
			stateManager.currentState.lastZIndex = 10;
			stateManager.currentState.pagewiseUID = 0;
			stateManager.currentState.elementCount = 0;
			stateManager.saveSnapshot();
			return;
		}
		$.each(e,function(i,l){
			if(this.element == 'page'){
				if(this.title){
					//$('#workspace-tabs').html(persistanceManager.getProjectTitle(stateManager.currentState.currentProject) + '&nbsp;&nbsp;&#x00BB;&nbsp;&nbsp;' +e.title);
					$('#page-title').text(e.title);
					$('#project-title').text(persistanceManager.getProjectTitle(stateManager.currentState.currentProject));
				}
				stateManager.restoreElement($('#' + stateManager.currentState.currentPage),this,i);
			}
			else
				stateManager._createElement(i,this);
		});					
	},
	clearState: function(_page){
		if(!_page) return;
		persistanceManager.removePageFromProject(_page, stateManager.currentState.currentProject);
		//persistanceManager.removeCurrentPage();
	},
	alterState: function(_page,e, prop, val){
		if(!_page || !e || !prop || !val) return;
		var pe = persistanceManager.getPageFromProject(_page, stateManager.currentState.currentProject);
		$.each(e,function(i,l){							
			if(pe[this])
				pe[this][prop] = val;
		});
		persistanceManager.setPageForProject(_page,pe,stateManager.currentState.currentProject);
	},
	writeAction: function(type,options){
		options.type = type;
		stateManager.diffState(type,options);
		ui.setStateInterface();
	},
	diffState: function(type,options){
		if(type == 'group'){
			var e = [];
			$.each(options.elements,function(i,l){
				e.push(i);
			});
			stateManager.diffSnapshot('#'+e.join(',#'),type);
		}
		else if(options.elements)
			stateManager.diffSnapshot('#'+options.elements.join(',#'),type);
		else if(options.element)
			stateManager.diffSnapshot('#'+options.element,type);
	},
};
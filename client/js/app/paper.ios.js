paper.navigationbutton = {
	properties: {
		min: {
			width: 30,
			height: 25
		},
		defaults: $.extend($.extend(true,{},paper.breadcrumbs.properties.defaults), {color:{bg:'#c3c3c3', t: '#000000', b:'#000000'}, items:[[{'t': 'Nav Button'}]]}),
		show: function($e, onshow){
			paper.breadcrumbs.properties.show($e, function(){
				$('#props-title').text('Navigation Button Properties');
				$('#div-itemsel').hide();
				$('#div-delete').remove();
			});
			$('#element-resizer').tooltip('destroy');
		}
	},
	draw: paper.breadcrumbs.draw
}
paper.iosswitch = {
	properties: {
		min: {
			width: 40,
			height: 20
		},
		defaults: $.extend(true,{},paper.button.properties.defaults, {'caption': 'ON', 'caption2': 'OFF', color: {fg: '#acacac'}, on: true, type:3}),
		show: function($e, onshow){
			paper.button.properties.show($e, function(){
				$('#props-title').text('Switch Properties');
				$('#type-r').remove();
				var props = paper._getDefaultedProperties($e.data('properties'), paper.iosswitch.properties.defaults);
				$('<button id="color-fg" class="btn btn-small"><i class="icon-stop"></i></button>').insertAfter('#color-bg');
				$('#color-fg').data('val', props.color.fg);
				$('#color-fg').css('color', props.color.fg);
				$('#color-fg').click(function(ev){
					paper._showColorPicker(ev, this, 'color.fg');
				});
				$('#color-fg').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Foreground', hideAfter: 2000});
				$('#div-icons').remove();
				$('#dropdown').removeAttr('id').attr('id','state');
				$('#dropup').remove();
				$('#input-caption').attr('rows','1');
				$('strong','#div-caption').text('On');
				$('<div id="div-caption2" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Off</strong></label><div class="controls"><textarea id="input-caption2" rows="1" style="width:135px">'+props.caption2+'</textarea></div></div>').insertAfter('#div-caption');
				$('#input-caption2').blur(function(ev){
					paper._changeProperty('caption2', $(this).val());
				});
				$('#input-caption2').keyup(function(ev){
					paper._changeProperty('caption2', $(this).val(), false);
				});
				$('#autosize').css('width','105px');
				$('#state').css('width','105px');
				if(props.on) $('#state').addClass('active');
				$('#state').text(props.on ? 'On' : 'Off');
				$('#state').click(function(ev){					
					if($(this).hasClass('active')){
						paper._changeProperty('on', false);
						$('#state').text('Off');
					}
					else{
						paper._changeProperty('on', true);
						$('#state').text('On');
					}
				});
				if(onshow) onshow();
			});
		}
	},
	draw: function(canvas,l,t,w,h,v){
		$div = $(canvas.canvas.parentElement)
		if(v.linkto)
			$div.addClass('has-link');
		if(v.autosize){
			var cp = '<p class="element-caption-para" style="margin: 0;vertical-align:middle;font-size:'+v.size+';text-align:'+v.align+';word-break:break-word;word-wrap:break-word;">'+v.caption.nl2br()+'</p>';
			var as = paper._getTextAutoSize(cp);
			cp = '<p class="element-caption-para" style="margin: 0;vertical-align:middle;font-size:'+v.size+';text-align:'+v.align+';word-break:break-word;word-wrap:break-word;">'+v.caption2.nl2br()+'</p>';
			var as1 = paper._getTextAutoSize(cp);
			w = Math.max(as.width, as1.width) + 10;
			h = Math.max(as.height, as1.height) + 10;
			w += h;			
			paper._resize($(canvas.canvas.parentElement),w,h);
		}
		
		var lv = $.extend(true,{},paper.label.properties.defaults);
		lv.caption = v.on ? v.caption : v.caption2;
		lv.size = v.size;
		lv.align = v.align;
		lv.autosize = false;
		lv.color.bg = 'none';

		v.autosize = false;
		v.caption = '';
		v.menu = null;
		paper.button.draw(canvas,l,t,w,h,v);
		v.color.bg = v.color.fg;
		paper.button.draw(canvas,l+(v.on ? w-h : 0),t,h,h,v);
		paper.label.draw(canvas,l+(v.on ? 2.5 : h-2.5),t,w-h,h,lv);
		paper._drawLink(v, $div);
	}
}
paper.pageselector = {
	properties: {
		min: {
			width: 10,
			height: 10
		},
		defaults: {
			width: 70,
			height: 10,
			type: 2,
			total: 5,
			selected: 2,
			color: {s: '#aeaeae', b: '#ffffff', bg: '#000000'}
		},
		show: function($e, onshow){
			$('#props').remove();
			$('#props-title').remove();
			var props = paper._getDefaultedProperties($e.data('properties'), paper.pageselector.properties.defaults);
			var autosizedrop = '<div id="div-autosizedrop" class="form-inline" style="text-align:center"><div class="btn-group" data-toggle="buttons-checkbox"><button style="width:210px" id="autosize" class="btn btn-small '+(props.autosize ? 'active' : '')+'">Autosize</button></div></div>';
			var type = '<div id="div-type" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Type</strong></label><div class="controls"><div class="btn-group" data-toggle="buttons-radio"><button id="type-r" class="btn btn-small '+(props.type == 2 ? 'active' : '')+'">R</button><button id="type-p" class="btn btn-small '+(props.type == 3 ? 'active' : '')+'">C</button></div></div></div>';
			var color = '<div id="div-color" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Colors</strong></label><div class="controls"><div id="color-options" class="btn-group" data-toggle="buttons-radio"><button id="color-b" class="btn btn-small"><i class="icon-stop"></i></button><button id="color-bg" class="btn btn-small"><i class="icon-stop"></i></button><button id="color-s" class="btn btn-small"><i class="icon-stop"></i></button></div></div></div>';
			var selected = '<div id="div-selected" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Selected</strong></label><div class="controls"><input class="input-small" id="input-selected"style="width:50px" type="text" value="'+(props.selected)+'" placeholder="'+(props.selected)+'"></div></div>';
			var total = '<div id="div-total" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Pages</strong></label><div class="controls"><input class="input-small" id="input-total"style="width:50px" type="text" value="'+(props.total)+'" placeholder="'+(props.total)+'"></div></div>';
			var html = '<div id="props"><div id="propsFor" style="display:none">'+$e.attr('id')+'</div><div class="form-horizontal">'+type+color+selected+total+'</div>'+autosizedrop+'</div>';
			$e.popover('destroy');
			$e.popover({title:  '<strong style="color:#3a3a3a;" id="props-title">Page Selector Properties</strong>', content: html, placement: 'auto', attachTo: '#tbs-popover', onShow: function(){
				paper._buildDimensions($e, '#div-total');
				paper._buildOpacity($e, '#div-total',props);
				paper._buildLinkInspector($e, '#div-total',props);
				$('#input-selected').spinner({min: 1, max: props.total});
				$('#input-selected').keydown(function(ev){
					if(ev.which == 13){
						paper._changeProperty('selected', parseInt($(this).val()), false);
						ev.preventDefault();
					}
				});
				$('#input-selected').blur(function(ev){
					paper._changeProperty('selected', parseInt($(this).val()));
				});
				$('#input-selected').change(function(ev){
					paper._changeProperty('selected', parseInt($(this).val()), false);
				});
				$('#input-total').spinner({min: 1, max: 25});
				$('#input-total').keydown(function(ev){
					if(ev.which == 13){
						paper._changeProperty('total', parseInt($(this).val()), false);
						ev.preventDefault();
					}
				});
				$('#input-total').blur(function(ev){
					paper._changeProperty('total', parseInt($(this).val()));
				});
				$('#input-total').change(function(ev){
					var val = parseInt($(this).val());
					paper._changeProperty('total', val, false, true);
					$('#input-selected').spinner('destroy');
					$('#input-selected').spinner({min: 1, max: val});
					if(val <= $('#input-selected').val()){
						$('#input-selected').val(val);
						paper._changeProperty('selected', val, false);
					}
				});
				$('#type-r').click(function(ev){
					paper._changeProperty('type', 2);
				});
				$('#type-p').click(function(ev){
					paper._changeProperty('type', 3);
				});
				$('#color-b').data('val', props.color.b);
				$('#color-bg').data('val', props.color.bg);
				$('#color-s').data('val', props.color.s);
				$('#color-b').css('color', props.color.b);
				$('#color-bg').css('color', props.color.bg);
				$('#color-s').css('color', props.color.s);
				$('#color-b').click(function(ev){
					paper._showColorPicker(ev, this, 'color.b');
				});
				$('#color-bg').click(function(ev){
					paper._showColorPicker(ev, this, 'color.bg');
				});
				$('#color-s').click(function(ev){
					paper._showColorPicker(ev, this, 'color.s');
				});
				$('#autosize').click(function(ev){
					if($(this).hasClass('active')){
						paper._changeProperty('autosize', false);
						mockup.initResizable($('#element-resizer'));
					}
					else{
						paper._changeProperty('autosize', true);
						$('#element-resizer').resizable('destroy');
					}
				});
				//Tooltips
				$('#type-r').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Rectangular', hideAfter: 2000});
				$('#type-p').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Circular', hideAfter: 2000});
				$('#color-b').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Border', hideAfter: 2000});
				$('#color-bg').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Background', hideAfter: 2000});
				$('#color-s').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Selected', hideAfter: 2000});
				if(onshow) onshow();
			}, onClick: function(ev){paper._popoverClick(ev,$e)}, onHide: function(){
				paper._destroyAllTooltips($e);
			}});
			$e.popover('show');
			ui.popoverContainers.push($e);
		}
	},
	draw: function(canvas,l,t,w,h,v){
		var $div = $(canvas.canvas.parentElement);
		if(v.linkto)
			$div.addClass('has-link');
		if(v.autosize){
			h = 10;
			w = (h+5)*v.total - 5;
			paper._resize($(canvas.canvas.parentElement),w,h);
		}
		else
			h = Math.min(Math.floor((w-(5*(v.total-1)))/v.total),h);
		var r = v.type == 3 ? h/2 : 0;
		for(var i=0;i<v.total;){
			canvas.rect(l+(i*(h+5))+1,t+1,h-2,h-2,r).attr({'stroke-width': '2px', 'stroke': v.color.b, 'fill': ++i==v.selected ? v.color.s : v.color.bg});
		}
		paper._drawLink(v, $div);
	}
}
paper.stepper = {
	properties:{
		min: {
			width: 30,
			height: 20
		},
		defaults: {
			width: 80,
			height: 20,
			color: {'b': '#000000', 'bg': '#c3c3c3', 't': '#000000'}
		},
		show: function($e, onshow){
			$('#props').remove();
			$('#props-title').remove();
			var props = paper._getDefaultedProperties($e.data('properties'), paper.pageselector.properties.defaults);
			var color = '<div id="div-color" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Colors</strong></label><div class="controls"><div id="color-options" class="btn-group" data-toggle="buttons-radio"><button id="color-b" class="btn btn-small"><i class="icon-stop"></i></button><button id="color-bg" class="btn btn-small"><i class="icon-stop"></i></button><button id="color-t" class="btn btn-small"><i class="icon-stop"></i></button></div></div></div>';
			var html = '<div id="props"><div id="propsFor" style="display:none">'+$e.attr('id')+'</div><div class="form-horizontal">'+color+'</div></div>';
			$e.popover('destroy');
			$e.popover({title:  '<strong style="color:#3a3a3a;" id="props-title">Stepper Properties</strong>', content: html, placement: 'auto', attachTo: '#tbs-popover', onShow: function(){
				paper._buildDimensions($e, '#div-color');
				paper._buildOpacity($e, '#div-color',props);
				paper._buildLinkInspector($e, '#div-color',props);
				$('#color-b').data('val', props.color.b);
				$('#color-bg').data('val', props.color.bg);
				$('#color-t').data('val', props.color.t);
				$('#color-b').css('color', props.color.b);
				$('#color-bg').css('color', props.color.bg);
				$('#color-t').css('color', props.color.t);
				$('#color-b').click(function(ev){
					paper._showColorPicker(ev, this, 'color.b');
				});
				$('#color-bg').click(function(ev){
					paper._showColorPicker(ev, this, 'color.bg');
				});
				$('#color-t').click(function(ev){
					paper._showColorPicker(ev, this, 'color.t');
				});
				//Tooltips
				$('#color-b').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Border', hideAfter: 2000});
				$('#color-bg').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Background', hideAfter: 2000});
				$('#color-t').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Signs', hideAfter: 2000});
				if(onshow) onshow();
			}, onClick: function(ev){paper._popoverClick(ev,$e)}, onHide: function(){
				paper._destroyAllTooltips($e);
			}});
			$e.popover('show');
			ui.popoverContainers.push($e);
		}
	},
	draw: function(canvas,l,t,w,h,v){
		var $div = $(canvas.canvas.parentElement);
		if(v.linkto)
			$div.addClass('has-link');
		canvas.rect(l+1,t+1,w-2,h-2,5).attr({'stroke-width': '2px', 'stroke': v.color.b, 'fill': v.color.bg});
		var iv = $.extend({}, paper.icon.properties.defaults);
		iv.color = {'bg': 'none', 's': v.color.t, 'b': 'clear'};
		iv.icon = 'minus';
		paper.icon.draw(canvas,l,t,w/2,h,iv);
		iv.icon = 'plus';
		paper.icon.draw(canvas,l+w/2,t,w/2,h,iv);
		paper._drawLink(v, $div);
	}
}
paper.tableview = {
	properties: {
		min: {
			width: 40,
			height: 30
		},
		defaults: $.extend($.extend(true,{}, paper.menu.properties.defaults), {items: [[{'t': 'Item 1', i:0},{'t': 'Item 2', i:0},{'t': 'Item 3', i:0}]]}),
		show: function($e, onshow){
			paper.menu.properties.show($e, function(){
				$('#props-title').text('iOS Table Properties');
				$('#div-caption2').css('display','block');
				$('#divider').remove();
				$('#selected').remove();
				$('#submenu').text('Disclosure Arrow');
				$('#submenu').css('width','210px');
				$('#disabled').remove();
				$('#delete').css('width','210px');
				$('#color-td').remove();
				$('#color-s').remove();
				if(onshow) onshow();
			});
		}
	},
	draw: function(canvas,l,t,w,h,v){
		var vitems = [];
		for(var i in v.items[0]){
			vitems.push(v.items[0][i]);
			vitems.push({'t':')__div__(', i:0});
		}
		v.items[0] = vitems;
		paper.menu.draw(canvas,l,t,w,h,v);
	}
}
paper.picker = {
	properties: {
		min: {
			width: 40,
			height: 30
		},
		defaults: $.extend(true,{},paper.table.properties.defaults,{type:1, color:{'fg': '#000000'}}),
		show: function($e, onshow){
			paper.table.properties.show($e,function(){
				$('#props-title').text('iOS Picker Properties');
				var props = paper._getDefaultedProperties($e.data('properties'), paper.picker.properties.defaults);
				$('<button id="color-fg" class="btn btn-small"><i class="icon-stop"></i></button>').insertAfter('#color-bg');
				$('#color-fg').data('val', props.color.fg);
				$('#color-fg').css('color', props.color.fg);
				$('#color-fg').click(function(ev){
					paper._showColorPicker(ev, this, 'color.fg');
				});
				$('#color-fg').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Foreground', hideAfter: 2000});
				$('#div-icons').remove();
				if(onshow) onshow();
			});
		}
	},
	draw: function(canvas,l,t,w,h,v){
		var $div = $(canvas.canvas.parentElement);
		if(v.linkto)
			$div.addClass('has-link');
		if(v.autosize){
			paper.table.draw(canvas,l,t,w,h,v, true);
			w = $div.width(), h = $div.height();
			w += 20, h += 10;			
			paper._resize($div,w,h);
		}
		canvas.rect(l+1,t+1,w-2,h-2).attr({'stroke-width':'2px', 'stroke': v.color.b, 'fill': v.color.fg});
		paper.table.draw(canvas,l+10,t+5,w-20,h-10,v);
	}
}
paper.iosdatepicker = {
	properties: {
		min: {
			width: 40,
			height: 30
		},
		defaults: $.extend($.extend(true,{},paper.picker.properties.defaults), {
			autosize: true,
			items: [
				[{'t': 'October'},{'t': '22'},{'t': '2011'}],
				[{'t': 'November'},{'t': '23'},{'t': '2012'}],
				[{'t': 'December'},{'t': '24'},{'t': '2013'}]	
			]}),
		show: function($e, onshow){
			paper.picker.properties.show($e,function(){
				$('#props-title').text('iOS Datepicker Properties');
				if(onshow) onshow();
			});
		}
	},
	draw: paper.picker.draw
}
paper.adbanner = {
	properties: {
		defaults: $.extend(true, {}, paper.plugin.properties.defaults, {caption: 'Ad Banner', 'width': 500, 'height':100}),
		show: function($e, onshow){
			paper.plugin.properties.show($e, function(){
				$('#props-title').text('Ad Banner Properties');
				if(onshow) onshow();
			})
		},
	},
	draw: paper.plugin.draw
}
paper.search = {
	properties: {
		min: {
			width: 35,
			height: 20
		},
		defaults: $.extend(true, {}, paper.textfield.properties.defaults, {type: 3,  width: 130, height: 25, caption: 'Search', align: 'center'}),
		show: function($e, onshow){
			paper.textfield.properties.show($e, function(){
				$('#props-title').text('Search Field Properties');
				if(onshow) onshow();
			});
		}
	},
	draw: function(canvas,l,t,w,h,v){
		var $div = $(canvas.canvas.parentElement);
		if(v.linkto)
			$div.addClass('has-link');
		ih = Math.floor(parseInt(v.size)*25/20) * 1.5;
		var r = v.type == 3 ? h/2 : v.type == 1 ? 5 : 0;
		if(v.autosize){
			var cp = '<p class="element-caption-para" style="margin: 0;vertical-align:middle;font-size:'+v.size+';text-align:'+v.align+';word-break:break-word;word-wrap:break-word;">'+v.caption.nl2br()+'</p>';
			var as = paper._getTextAutoSize(cp);
			w = as.width + 10 + ih/2 + 10, h = as.height + 10;
			paper._resize($div,w,h);
		}
		canvas.rect(l+1,t+1,Math.floor(w-2),h-2,r).attr({'stroke-width': v.icon ? '0px' : '2px', 'stroke':v.color.b, 'fill': v.color.bg});
		iv = $.extend(true, {}, paper.icon.properties.defaults);
		iv.icon = v.icon ? v.icon : 'search';
		iv.color.s = v.color.b;
		paper.icon.draw(canvas,l,t,ih,h,iv);
		iv = $.extend(true,{},paper.label.properties.defaults);
		iv.caption = v.caption;
		iv.autosize = false;
		iv.align = v.align;
		iv.size = v.size;
		iv.color.t = v.color.t;
		paper.label.draw(canvas, l+ih/2 + 5,t+1,w-ih,h-2,iv);
		paper._drawLink(v, $div);
	}
}
paper.pulltorefresh = {
	properties: {
		min: {
			width: 30,
			height: 20
		},
		defaults: $.extend(true, {}, paper.textfield.properties.defaults, {color: {bg:'none', 't': '#000000'}, type: 3,  width: 130, height: 25, caption: 'Pull To Refresh', align: 'center'}),
		show: function($e, onshow){
			paper.textfield.properties.show($e, function(){
				$('#props-title').text('Pull To Refresh Properties');
				$('#color-bg').remove();
				$('#color-b').tooltip('destroy');
				$('#color-b').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Icon', hideAfter: 2000});
				if(onshow) onshow();
			});
		}
	},
	draw: function(canvas,l,t,w,h,v){
		v.icon = 'arrow-down';
		paper.search.draw(canvas,l,t,w,h,v);
	}
}
paper.cutcopypastedefine = {
	properties: {
		defaults: $.extend($.extend(true, {},paper.menu.properties.defaults, {width:500, height:25, autosize: true,type: 3, points: 2}),{items: [[{'t': 'Cut', 'd': false},{'t': 'Copy', 'd': false},{'t': 'Paste', 'd': true},{'t': 'Define', 'd': false, 's': true}]]}),
		show: paper.menubar.properties.show
	},
	draw: paper.menubar.draw
}
paper.selectselectallpaste = {
	properties: {
		defaults: $.extend($.extend(true, {},paper.menu.properties.defaults, {width:500, height:25, autosize: true,type: 3, points: 2}),{items: [[{'t': 'Select', 'd': false},{'t': 'Select All', 'd': false, 's': true},{'t': 'Paste', 'd': true}]]}),
		show: paper.menubar.properties.show
	},
	draw: paper.menubar.draw
}
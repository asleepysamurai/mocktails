var project = {};
project.suffix = function(){
	var titles = persistanceManager.getAllProjectTitles();
	if(titles && titles.length > 0)
		titles = titles.map(function(t){ return t.title; });
	return ui._getTitleSuffix(titles,'New Project');
};
project.duplicateSuffix = function(){
	var titles = persistanceManager.getAllProjectTitles();
	if(titles && titles.length > 0)
		titles = titles.map(function(t){ return t.title; });
	return ui._getTitleSuffix(titles, persistanceManager.getProjectTitle(stateManager.currentState.currentProject)+' (Duplicate');
};
project.create = function(title){
	title = title ? title : project.suffix();
	var _project = {title: title};
	var projectCount = persistanceManager.getProjectCount();
	if(!projectCount)
		projectCount = 0;
	$.extend(stateManager.currentState, stateManager.newProjectState);
	persistanceManager.setProject('project-' + (++projectCount), _project);
	persistanceManager.setProjectCount(projectCount);
	$('#project-title').text(title);
	return 'project-' + projectCount;
};
project.duplicate = function(title){
	if(!title || title.length == 0) return;
	var _project = persistanceManager.getProject(stateManager.currentState.currentProject);
	_project.title = title;
	var projectCount = persistanceManager.getProjectCount();
	var newProject = 'project-'+(++projectCount);
	persistanceManager.setProject(newProject, _project);
	persistanceManager.setProjectCount(projectCount);
	var _pages = _project.pages;
	for(var p in _pages){
		var _page = persistanceManager.getPageFromProject(_pages[p], stateManager.currentState.currentProject);
		persistanceManager.setPageForProject(_pages[p], _page, newProject);
	}
	var _masters = persistanceManager.getAllMasterElementsForProject(stateManager.currentState.currentProject);
	persistanceManager.setAllMasterElementsForProject(_masters, newProject);
	project.open(newProject);
};
project.rename = function(title){
	if(!title) return;
	var $et = $('#project-title');
	if($et.text() == title) return;
	persistanceManager.setProjectTitle(stateManager.currentState.currentProject, title);
	$et.text(title);
	$('#project-title').text(title);
	ui.initMenus();
}
project.current = function(){
	stateManager.currentState.currentProject = null;
	stateManager.currentState.currentProject = persistanceManager.getCurrentProject();
	stateManager.currentState.currentProject = stateManager.currentState.currentProject ? stateManager.currentState.currentProject : persistanceManager.getFirstProject();
	stateManager.currentState.currentProject = stateManager.currentState.currentProject ? stateManager.currentState.currentProject : project.create();
	return stateManager.currentState.currentProject;
};
project.pageCount = function(){
	stateManager.currentState.pageCount = persistanceManager.getPageCountForProject(stateManager.currentState.currentProject);
	if(!stateManager.currentState.pageCount){
		var pages = persistanceManager.getAllPagesForProject(stateManager.currentState.currentProject), hn = 0;
		if(!pages) stateManager.currentState.pageCount = 0;
		else{
			$.each(pages, function(i,l){
				var tn = parseInt(this.replace(/\D/g,''));
				hn = hn > tn ? hn : tn;
			});
			stateManager.currentState.pageCount = hn;
		}
	}
	return stateManager.currentState.pageCount;
};
project.close = function(){
	page.close();
	$('#masters-content .master-element').remove();
	$('#element-definitions .member-master').remove();
	$('#project-title').text('');
	$('#explorer-content').text('');
	$('#leftPanels').css('left','-210px');
};
project._new = function(title){
	project.close();
	_gaq.push(['_trackEvent', 'project', 'new']);
	ui.initProject(project.create(title));
};
project.open = function(_project){
	if(_project == stateManager.currentState.currentProject) return;
	project.close();
	_gaq.push(['_trackEvent', 'project', 'open']);
	ui.initProject(_project);
};
project._delete = function(){
	project.close();
	_gaq.push(['_trackEvent', 'project', '_delete']);
	persistanceManager.removeProject(stateManager.currentState.currentProject);
	stateManager.currentState.currentProject = null;
	ui.initMenus();
	//initProject();
};
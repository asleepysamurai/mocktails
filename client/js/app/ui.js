var ui = {
	popoverContainers: [],
	tooltipIntervals: []
};
ui.dismissPopovers = function(){
	//$('button','#tbs-popover').tooltip('destroy');
	$('#page-container').removeData('dblc');
	$('.showingprops','#'+stateManager.currentState.currentPage).removeClass('showingprops');
	for(var p in ui.popoverContainers)
		ui.popoverContainers[p].popover('destroy');
	ui.popoverContainers = [];
}
ui.showProperties = function(e){
	var $e = e ? $(e) : $(stateManager.currentState.currentSelectedElement);
	if($e.data('dsp')) return $e.removeData('dsp');
	if($e.hasClass('showingprops')) return ui.dismissPopovers();
	stateManager.currentState.currentSelectedElement = $e;
	stateManager.currentState.currentSelectedElement.not('.current-page').addClass('showingprops');
	$('#element-resizer').addClass('showingprops');
	paper[$e.attr('data-element')].properties.show($e);
};	
ui.refreshOrInitScrollers = function(div){
	var $div = $(div);
	if($div.find('.scroll-content').length == 0)
		$div.sbscroller();
	else
		$div.sbscroller('refresh');

	var $scrollers = $div.find('.hslider-wrap .ui-slider-handle');
	$scrollers.on('mouseover', function(){
		$(this).parent().parent().css('width', '15px');
	});
	$scrollers.on('mouseout', function(){
		$(this).parent().parent().css('width', '10px');
	});
	var $scrollers = $div.find('.wslider-wrap .ui-slider-handle');
	$scrollers.on('mouseover', function(){
		$(this).parent().parent().css('height', '15px');
	});
	$scrollers.on('mouseout', function(){
		$(this).parent().parent().css('height', '10px');
	});
}
ui.initScrollers = function(w,h){
	ui.refreshOrInitScrollers('#workspace-area');
	ui.refreshOrInitScrollers('#elements-content');
	ui.refreshOrInitScrollers('#masters-content');
	ui.refreshOrInitScrollers('#templates-content');
	ui.refreshOrInitScrollers('#search-results-content');
	ui.refreshOrInitScrollers('#explorer-content');
}
ui.showMoveTooltip = function($t, pos){
	//$(stateManager.currentState.currentSelectedElement).popover('destroy');
	if(!pos)
		pos = $t.offset();
	ui.dismissPopovers();
	var $elementsize = $('#element-size');
	$elementsize.html('Top:' + parseInt($t.css('top')) + '  Left:' + parseInt($t.css('left')));
	$elementsize.addClass('in');
	$elementsize.removeAttr('style');
	$elementsize.css({
		top: pos.top, 
		left: pos.left
	});
	//paper.redraw($('.selected', '#' + stateManager.currentState.currentPage));
}
ui.showResizeTooltip = function($t, pos){
	//$(stateManager.currentState.currentSelectedElement).popover('destroy');
	if(!pos)
		pos = $t.offset();
	ui.dismissPopovers();
	var $elementsize = $('#element-size');
	$elementsize.html('Width:' + $t.width() + '  Height:' + $t.height());
	$elementsize.addClass('in');
	$elementsize.removeAttr('style');
	$elementsize.css({
		top: pos.top, 
		left: pos.left
	});
}
ui.hideTooltip = function(){
	var $es = $('#element-size');
	$es.removeClass('in');
	$es.on($.support.transition.end, function(ev){$(this).css('display','none');})
	//$('#element-size').hide();
}
ui.keyMove = function(ev, end){
	var $e = $('#page-container .selected');
	if($e.length > 0){
		var booster = 1, ctrl = false;
		if(ev.shiftKey)
			booster = 5;
		if(ev.metaKey || ev.ctrlKey)
			ctrl = true;

		var min, cw, ch;
		if(ctrl){
			min = paper[$e.attr('data-element')].properties.min;
			min = min ? min : {height: 20, width: 20};
			cw = parseInt($e.css('width')), ch = parseInt($e.css('height'));
		}

		switch(true){
			case ev.which == 37 && ctrl:
				if(cw && min.width+booster<=cw){
					mockup.resizeEnded($e, end ? null : function(el){
						var $el = $(el);
						mockup.setWidth(parseInt($el.css('width')) - booster, $el);
					},end);
				}
				break;
			case ev.which == 38 && ctrl:
				if(ch && min.height+booster<=ch){
					mockup.resizeEnded($e, end ? null : function(el){
						var $el = $(el);
						mockup.setHeight(parseInt($el.css('height')) - booster, $el);
					},end);
				}
				break;
			case ev.which == 39 && ctrl:
				mockup.resizeEnded($e, end ? null : function(el){
					var $el = $(el);
					mockup.setWidth(parseInt($el.css('width')) + booster, $el);
				},end);
				break;
			case ev.which == 40 && ctrl:
				mockup.resizeEnded($e, end ? null : function(el){
					var $el = $(el);
					mockup.setHeight(parseInt($el.css('height')) + booster, $el);
				},end);
				break;
			case ev.which == 37:
				mockup.moveAndResizeEnded($e,end ? null : function(el){
					mockup.setLeft(-booster, $(el), true);
				},null,end);
				break;
			case ev.which == 38:
				mockup.moveAndResizeEnded($e,end ? null : function(el){
					mockup.setTop(-booster, $(el), true);
				},null,end);
				break;
			case ev.which == 39:
				mockup.moveAndResizeEnded($e,end ? null : function(el){
					mockup.setLeft(booster, $(el), true);
				},null,end);
				break;
			case ev.which == 40:
				mockup.moveAndResizeEnded($e,end ? null : function(el){
					mockup.setTop(booster, $(el),true);
				},null,end);
				break;
		}
		
		if(end)
			ui.tooltipIntervals.push(window.setTimeout(ui.hideTooltip, 1000));
		else{
			for(var i in ui.tooltipIntervals){
				window.clearTimeout(ui.tooltipIntervals[i]);
			}
		}

	}					
}
ui.initKeyboard = function(){
	$(document).keyup(function(ev){
		var $target = $(ev.target);
		if($target.is('input') || $target.is('.editmode') || $target.is('textarea')) return;
		ui.keyMove(ev, true);
	});
	$(document).keydown(function(ev){
		if(stateManager.currentState.previewMode && ev.which != 80) return;
		var $target = $(ev.target);
		if($target.is('input') || $target.is('.editmode') || $target.is('textarea')) return;
		ev.stopPropagation();

		//ev.preventDefault();

		var booster = 1, ctrl = false;
		if(ev.metaKey || ev.ctrlKey)
			ctrl = true;
		
		var ourKey = ui.isOurkey(ev.which);
		//if($('#propsPanels').is(':visible')){
		ui.keyMove(ev, false);
		if($('#page-container .selected').length > 0){
			switch(true){
				case ev.which == 46:
					//Delete key
					mockup.deleteElement();
					break;
				case ev.which == 88 && ctrl:
					//ctrl+x
					mockup.cut();
					break;
				case ev.which == 67 && ctrl:
					//ctrl+c
					mockup.copy();
					break;
			}
		}
		switch(true){
			case ev.which == 86 && ctrl:
				//ctrl+v
				mockup.paste();
				break;
			case ev.which == 71 && ctrl && !$('#group').is(':disabled'):
				$('#group').click();
				break;
			/*
			case ev.which == 76 && ctrl && !$('#lock').is(':disabled'):
				$('#lock').click();
				break;
			*/
			case ev.which == 90 && (ctrl && ev.shiftKey) && !$('#redo').is(':disabled'):
				$('#redo').click();
				break;
			case ev.which == 90 && ctrl && !ev.shiftKey && !$('#undo').is(':disabled'):
				$('#undo').click();
				break;
			case ev.which == 65 && ctrl && ev.shiftKey:
				mockup.deselectAll();
				break;
			case ev.which == 65 && ctrl:
				mockup.selectAll();
				break;
			case ev.which == 80 && ctrl:
				$('#preview').click();
				break;
			case ev.which == 75 && ctrl:
				$('#element-library-search-input').focus();
				break;
			case ev.which == 219 && ctrl:
				$('#back').trigger('click');
				break;
			case ev.which == 221 && ctrl:
				$('#front').trigger('click');
				break;
			case ev.which == 219:
				$('#backward').trigger('click');
				break;
			case ev.which == 221:
				$('#forward').trigger('click');
				break;
			case ev.which == 49 && ctrl:
				$('#elements-tab-a').trigger('mousedown');
				$('#elements-tab-a').trigger('click');
				break;
			case ev.which == 50 && ctrl:
				$('#templates-tab-a').trigger('mousedown');
				$('#templates-tab-a').trigger('click');
				break;
			case ev.which == 51 && ctrl:
				$('#masters-tab-a').trigger('mousedown');
				$('#masters-tab-a').trigger('click');
				break;
		}
		if(ourKey)
			ev.preventDefault();
		//console.log('kd' + ev.which);
	});
}
ui.isOurkey = function(key){
	return [37,38,39,40,46,88,67,86,71,75,76,90,80,219,221,49,50,51].indexOf(key) == -1 ? false : true;
}
ui.setInterface = function(){
	ui.setStateInterface();
	ui.setGroupingInterface();
	ui.setGroupActionsInterface();
}
ui.setStateInterface = function(){
	if(actionManager._actions && actionManager._actions[stateManager.currentState.currentPage] && actionManager._pointer && (actionManager._pointer[stateManager.currentState.currentPage] == 0 || actionManager._pointer[stateManager.currentState.currentPage])){
		if(actionManager._pointer[stateManager.currentState.currentPage] > 0)
			$('#undo').prop('disabled', false);
		else
			$('#undo').prop('disabled', true);
		if(actionManager._pointer[stateManager.currentState.currentPage] < actionManager._actions[stateManager.currentState.currentPage].length)
			$('#redo').prop('disabled', false);
		else
			$('#redo').prop('disabled', true);					
	}
	else{
		$('#undo').prop('disabled', true);
		$('#redo').prop('disabled', true);
	}					
}
ui.setGroupingInterface = function(){
	var $group = $('#group');
	if($('#page-container .selected').length > 1){
		$group.prop('disabled',false);
		if(!mockup.isGrouped() || mockup.isMultiGrouped()){
			$group.removeClass('active');
			$group.data('action','group');
			$('#alignSelected').prop('disabled', false);
		}
		else{
			$group.addClass('active');
			$group.data('action','ungroup');
		}
	}
	else{
		$group.data('action','group');
		$group.prop('disabled',true);
		$('#alignSelected').prop('disabled', true);
	}
}
ui.setGroupActionsInterface = function(){
	var $se = $('.selected', '#'+stateManager.currentState.currentPage);
	if($se.length > 0)
		$('#defasce').prop('disabled', false);
	else
		$('#defasce').prop('disabled', true);
	var val = $('#defasce').prop('disabled');
	$('#defasme').prop('disabled', val);
	$('#back').prop('disabled', val);
	$('#backward').prop('disabled', val);
	$('#forward').prop('disabled', val);
	$('#front').prop('disabled', val);
}
ui.togglePreview = function(){
	/* Temporariily disabled. Re-enable after proper page sharing has been implemented. */
	/*
	stateManager.currentState.previewMode = !stateManager.currentState.previewMode;
	mockup.deselectAll();
	var $le = $('#workspace-area .has-link');
	$le.each(function(){
		var $t = $(this);
		while($t.parent().is('a'))
			$t.unwrap();
	});
	paper.redraw($le);
	$('#leftPanels').toggle();
	//$('#propsPanels').hide();
	$('#menu-edit').toggle();
	$('#menu-preview').toggle();
	var $workspace = $('#workspace'), $currentPage = $('#' + stateManager.currentState.currentPage);
	$currentPage.toggleClass('preview');
	if($currentPage.hasClass('preview')){
		$workspace.css({left:0});
		$('#page-container .element').unbind('click');
		$currentPage.unbind('click');
		$currentPage.resizable('destroy');
		$currentPage.css({overflow: 'hidden'});
		$currentPage.data('grid', $currentPage.hasClass('hasgrid'));
		$currentPage.removeClass('hasgrid');
		$('#page-container').off('mousedown');
	}
	else{
		$workspace.css({left:200});
		page.init($currentPage, true);
		mockup.reinitElement($('#page-container .element'));
		$currentPage.css({overflow: 'visible'});
		if($currentPage.data('grid'))
			$currentPage.addClass('hasgrid');
		ui.initDragSelector();
	}
	ui.initScrollers();
	*/
}
ui.initDragSelector = function(){
	var $container = $('#workspace-area');
	var $selection = $('<div>').addClass('selection-box');

	$container.on('mousedown', function(e) {
		//if(!$(e.target).is($container) && !$(e.target).is('.current-page')) return;
		var $target = $(e.target);
		if($target.is('#element-resizer') || $target.hasClass('ui-resizable-handle') || $target.hasClass('ui-slider-handle')) return;
		if(!(e.ctrlKey || e.metaKey))
			mockup.deselectAll();
		var wao = $container.offset();
		var click_y = e.pageY - wao.top;
		var click_x = e.pageX - wao.left;

		$selection.css({
			'top':    click_y,
			'left':   click_x,
			'width':  0,
			'height': 0,
			'position': 'absolute',
			'display': 'none'
		});
		$selection.appendTo($container);

		$container.on('mousemove', function(e) {
			$selection.show();
			var move_y = e.pageY - wao.top;
			var move_x = e.pageX - wao.left;
			width  = Math.abs(move_x - click_x),
			height = Math.abs(move_y - click_y);

			$selection.css({
				'width':  width,
				'height': height
			});
			if (move_x < click_x) { //mouse moving left instead of right
				$selection.css({
					'left': click_x - width
				});
			}
			if (move_y < click_y) { //mouse moving up instead of down
				$selection.css({
					'top': click_y - height
				});
			}
		}).on('mouseup', function(e) {
			e.stopPropagation();
			$container.off('mousemove');
			$container.off('mouseup');
			var sp = $selection.position();
			var cop = $container.find('.scroll-content').position();
			if(!cop) cop = {top: 0, left: 0};
			sp.top -= 15, sp.left -= 15;
			sp.top -= cop.top, sp.left -= cop.left;
			sp.bottom = sp.top + $selection.height(), sp.right = sp.left + $selection.width();
			var sel = $('#page-container .element').filter(function(){
				var $t = $(this);
				var tp = $t.position();
				tp.bottom = tp.top + $t.height(), tp.right = tp.left + $t.width();
				return !(tp.left > sp.right || tp.right < sp.left || tp.top > sp.bottom || tp.bottom < sp.top);
			});
			//var ev = jQuery.Event('click');
			//ev.ctrlKey = true;
			if(sel.length > 0) mockup.deselectAll();
			sel.each(function(){mockup.selectElementGroup($(this), true)});
			$selection.remove();
		});
	});				
}
ui.initElementDefinitions = function($e){
	var elements = $e ? $e : $('.element-definition', '#element-definitions');
	elements.each(function(){
		var $t= $(this);
		var pe = paper[$t.attr('data-element')];
		if(pe){
			$t.data('properties',$.extend(true, {}, pe.properties.defaults));
			$t.height(pe.properties.defaults.height);
			$t.width(pe.properties.defaults.width);
			$t.css({'z-index':'99999', 'position': 'absolute', 'text-align': 'center'});
			paper.draw($t);
		}
	});
}
ui.initLibraryElementDrag = function($e){
	$e.draggable({
		helper: function(){
			return $('#' + $(this).attr('data-element') + '-definition', '#element-definitions').clone().removeAttr('id').attr('id','removeclone');
		},
		appendTo: '#dragzone',
		scroll: false,
		cursorAt: {top: 5, left: 5},
		start: function(ev,_ui){
			mockup.startSnapElement($(_ui.helper),ev,_ui);
		},
		drag: function(ev, _ui){
			//if(_ui.position.left < 200) return;			
			var $s = $(_ui.helper);
			$s.css({'top': _ui.position.top, 'left': _ui.position.left});
			var posmod = $('#' + stateManager.currentState.currentPage).offset();
			posmod.top *= -1, posmod.left *= -1;
			var md = mockup.actionSnapElement($s,ev,_ui, posmod);
			//console.log(md);
			if(md.x){
				//$s.css('top', '-=' + md.x.d + 'px');				
				_ui.position.top = parseInt(md.x.ac) - posmod.top;
			}
			if(md.y){
				//$s.css('left', '-=' + md.y.d + 'px');				
				_ui.position.left = parseInt(md.y.ac) - posmod.left;
			}
		},
		stop: function(e,_ui){
			var $t = $(this);
			mockup.stopSnapElement($(_ui.helper),e,_ui);
			$('#removeclone').remove();
			var $currentPage = $('#' + stateManager.currentState.currentPage);
			if($currentPage.length == 0) return;
			var so = $currentPage.offset();
			if(e.pageX > so.left && e.pageX < (so.left + $currentPage.width())){
				if(e.pageY > so.top && e.pageY < (so.top + $currentPage.height())){
					var parentOffset = $currentPage.offset(); 
					var relX = _ui.position.left - parentOffset.left;
					var relY = _ui.position.top - parentOffset.top;
					if($t.hasClass('element-template'))
						mockup.createElementTemplate('#' + stateManager.currentState.currentPage, $t.attr('data-element').replace('template-',''), null, {top: relY, left: relX});
					else if($t.hasClass('master-element'))
						mockup.createElementTemplate('#' + stateManager.currentState.currentPage, $t.attr('data-element').replace('master-',''), null, {top: relY, left: relX}, null, true);
					else
						mockup.createElement($t.attr('data-element'), {top: relY, left: relX});
				}
			}
			$('#element-library-search-input:focus').trigger('blur');
		}
	});
}
ui.refreshElementRepresentations = function($elements, master){
	ui.initElementRepresentations($elements);
	ui.refreshOrInitScrollers(master ? '#masters-content' : '#templates-content');
}
ui.initElementRepresentations = function($elements, defaultElements){
	$elements.each(function(ev){
		var $t = $(this), de =$t.attr('data-element');
		$t.find('.element-representation').remove();
		var $cd = $('#'+de+'-definition');
		var cdh = 0, cdw = 0;
		var $cdc =  defaultElements ? $cd : $cd.children()
		$cdc.each(function(){
			var $t = $(this), tt = parseInt($t.css('top')), tl = parseInt($t.css('left'));
			tt = isNaN(tt) ? 0 : tt, tl = isNaN(tl) ? 0 : tl;
			cdh = Math.max(cdh, tt + parseInt($t.css('height')));
			cdw = Math.max(cdw, tl + parseInt($t.css('width')));
		});
		var wmod = defaultElements ? 190 : 190;
		var scale = Math.min(1, cdw > cdh ? wmod/cdw : wmod/cdh);
		var cdl = (wmod-(cdw*scale))/2;
		cdl /= scale;
		var $div = $('<div class="element-representation" data-element="'+de+'" style="z-index:99;height:'+(cdh*scale+5)+'px"></div>').insertBefore($('.element-name', $t));
		if(defaultElements)
			$div = $('<div></div>').prependTo($div);
		var $cdd = $cd.clone()
			.css({'z-index': '99','position': 'absolute'})
			.removeAttr('id')
			.prependTo($div);
		var $cddc = defaultElements ? $div.css({'position': 'absolute', 'left': (cdl+5)+'px', '-webkit-transform': 'scale('+scale+')','-moz-transform': 'scale('+scale+')','-o-transform': 'scale('+scale+')','-ms-transform': 'scale('+scale+')'})
		 	: $cdd.css({'-webkit-transform': 'scale('+scale+')','-moz-transform': 'scale('+scale+')','-o-transform': 'scale('+scale+')','-ms-transform': 'scale('+scale+')'}).children().css({'left': '+='+cdl+'px'});
	});
}
ui.initElementLibrary = function(){
	ui.initElementDefinitions();
	ui.loadElementTemplates();
	ui.initElementRepresentations($('#elements-content .element-library-item'), true);
	ui.initElementRepresentations($('#templates-content .element-template'));
	ui.initLibraryElementDrag($('#tab-elements-all .element-library-item'));
}
ui.initWorkspace = function(){
	$('#page-container').on('mousemove', function(e){
		$(this).data('moved', true);
	}).on('mouseup',function(e){
		var moved = $(this).data('moved'); 
		$(this).removeData('moved'); 
		if(moved) return;
		var $target = $(e.target);
		if(!$target.hasClass('element') && !$target.attr('id') == 'element-resizer'){
			mockup.deselectAll();
			//if(!$(e.target).is('.current-page'))
				//$('#propsPanels').hide();
		}
	});
}
ui.initProjectTitleItem = function(title){
	title = $.escapeHTML(title);
	var $explorerTitle = $('#project-title');
	$explorerTitle.html('<small><strong style="color:#3a3a3a">'+title+'</strong></small>');
	$explorerTitle.dblclick(function(e){
		modal.show('editProject');
	});
}
ui.initProject = function(_project){
	$('#leftPanels').css('left', '0px');
	stateManager.currentState.currentProject = _project ? _project : project.current();
	persistanceManager.setCurrentProject(stateManager.currentState.currentProject);
	ui.initProjectTitleItem(persistanceManager.getProjectTitle(stateManager.currentState.currentProject));
	stateManager.currentState.pageCount = project.pageCount();
	stateManager.currentState.masterElementCount = persistanceManager.getMasterElementCountForProject(stateManager.currentState.currentProject);
	stateManager.currentState.masterElementCount = stateManager.currentState.masterElementCount ? stateManager.currentState.masterElementCount : 0;
	var cp = persistanceManager.getCurrentPageForProject(stateManager.currentState.currentProject);
	if(!cp)
		cp = page._new();
	ui.initMenus();
	ui.loadElementTemplates(true);
	ui.initElementRepresentations($('#masters-content .master-element'));
	ui.initScrollers();
	$('#switch-'+cp).trigger('click');
}
ui._getTitleSuffix = function(collection,prefix){
	var ic = 0, nt;
	prefix = prefix.replace(/ \(Duplicate *\d*\) \(Duplicate$/,' (Duplicate');
	do{
		++ic;
		nt = $(collection).filter(function(){ if(this == prefix + ' ' + ic || this == prefix + ' ' + ic + ')') return true; return false;});
	}while(nt.length != 0)
	return prefix+' '+ic;
}
ui.initMenus = function(){
	if(!stateManager.currentState.currentProject){
		$('#menu-item-newPage').addClass('disabled');
		$('#menu-item-editPage').addClass('disabled');
		$('#menu-item-deletePage').addClass('disabled');
		$('#menu-item-duplicatePage').addClass('disabled');
		$('#menu-item-editProject').addClass('disabled');
		$('#menu-item-deleteProject').addClass('disabled');
		$('#menu-item-duplicateProject').addClass('disabled');
	}
	else{					
		$('#menu-item-newPage').removeClass('disabled');
		$('#menu-item-editPage').removeClass('disabled');
		$('#menu-item-deletePage').removeClass('disabled');
		$('#menu-item-duplicatePage').removeClass('disabled');
		$('#menu-item-editProject').removeClass('disabled');
		$('#menu-item-deleteProject').removeClass('disabled');
		$('#menu-item-duplicateProject').removeClass('disabled');
	}
	ui.initSwitchProjectMenu();
	ui.initSwitchPageMenu();
}
ui.initSwitcherSubmenu = function(type){
	/*
	type = 1 -> Project
	type = 2 -> Page
	*/
	var projects = type == 1 ? persistanceManager.getAllProjectTitles() : persistanceManager.getAllPageTitlesForProject(stateManager.currentState.currentProject);
	var switchProject = '<a href="#">Open '+ (type == 1 ? 'Project' : 'Page') +'</a>';
	var $smi = type == 1 ? $('#menu-item-switchProjectSubmenu') : $('#menu-item-switchPageSubmenu');
	if(projects.length == 0){
		$smi.addClass('disabled');
		$smi.removeClass('dropdown-submenu');
	}
	else{
		$smi.removeClass('disabled');
		$smi.addClass('dropdown-submenu');					
	}
	switchProject += '<ul class="dropdown-menu">';
	$.each(projects, function(i,l){
		var eid = $.escapeHTML(this.id);
		if(type == 1)
			switchProject += '<li><a href="#" class="switchProject '+(stateManager.currentState.currentProject == this.id ? 'active-project' : '')+'" id="switch-'+eid+'" data-project="' + eid + '">' + (stateManager.currentState.currentProject == this.id ? '<i class="icon-ok"></i>&nbsp;&nbsp;' : '') + $.escapeHTML(this.title) + '</a></li>';
		else
			switchProject += '<li><a href="#" class="switchPage '+(stateManager.currentState.currentPage == this.id ? 'active-page' : '')+'" id="switch-'+eid+'" data-page="' + eid + '">' + (stateManager.currentState.currentPage == this.id ? '<i class="icon-ok"></i>&nbsp;&nbsp;' : '') + $.escapeHTML(this.title) + '</a></li>';
	});
	$smi.html(switchProject);
	if(type == 1){
		$('#menu-item-switchProjectSubmenu .switchProject').click(function(ev){
			project.open($(this).attr('data-project'));
		});
	}
	else{		
		$('#menu-item-switchPageSubmenu .switchPage').click(function(ev){
			page.load($(this).attr('data-page'));
		});
	}
}
ui.initSwitchProjectMenu = function(){
	ui.initSwitcherSubmenu(1);
}
ui.initSwitchPageMenu = function(){
	ui.initSwitcherSubmenu(2);
}
ui.loadElementTemplates = function(master){
	var el = master ? persistanceManager.getAllMasterElementsForProject(stateManager.currentState.currentProject) : persistanceManager.getAllElementTemplates();
	for(var e in el){
		ui.addItemToElementList(e, el[e], master);
	}
}
ui.addItemToElementList = function(ecid, el, master){
	var $cid = master ? $('#masters-content') : $('#templates-content');
	var cidf = master ? '.master-element' : '.element-template';
	if($cid.find(cidf).length == 0)
		$cid.html('');
	el = el ? el : persistanceManager.getElementTemplate(ecid);
	var p = $cid.find('.scroll-content')
	p = p.length > 0 ? p : $cid;
	var classPrefix = master ? 'master-' : 'template-';
	var $div = $('<div class="boxdiv element-library-item '+(master ? 'master-element' : 'element-template')+'" data-tags="'+classPrefix+'" data-element="'+classPrefix+ecid+'" id="'+classPrefix+ecid+'-library-item"><button type="button" class="close pull-left '+classPrefix+'delete" style="z-index:100;position:absolute;" data-element="'+ecid+'">&times;</button><div class="element-name">'+el.n+'</div></div>').appendTo(p);
	var $def = $('<div id="'+classPrefix+ecid+'-definition" class="resizable element-definition '+(master ? 'member-master' : '')+'" data-element="'+classPrefix+ecid+'"></div>').appendTo('#element-definitions');
	mockup.createElementTemplate('#'+classPrefix+ecid+'-definition', ecid, el.e, null, false);
	ui.initLibraryElementDrag($div);
	ui.initGroupedElementDelete($('.'+classPrefix+'delete', $div), master);
}
ui.initGroupedElementDelete = function($btn, master){
	$btn.tooltip({html: false, placement: 'right', attachTo: '#tbs-tooltip-navbar', title: 'Delete '+(master ? 'Master' : 'Template'), hideAfter: 2000});
	$btn.click(function(ev){
		var $t = $(this), de = parseInt($t.attr('data-element')), master = $t.hasClass('master-delete');
		modal.show(master ? 'deleteMasterElement' : 'deleteElementTemplate', {id: de, master: master});
	});
}
ui.searchForElement = function(isval){
	var $src = $('#search-results-content');
	$src.html('');
	$('#tab-elements-all .element-library-item').each(function(){
		var $t = $(this);
		var td = $t.attr('data-tags') + ' ' + $t.text();
		if(!(!td || td.indexOf(isval) == -1)){
			var $tc = $t.clone().appendTo($src), $tcc = $tc.find('.element-name');
			$tc.find('.close').hide();
			$tcc.text($tcc.text()+($tc.hasClass('master-element') ? ' [Master]' : $tc.hasClass('element-template') ? ' [Template]' : ''));
			ui.initLibraryElementDrag($tc);
		}
	});
	$('#elements-tab li').hide();
	var $sli = $('#search-results-tab-li');
	$sli.show();
	$sli.find('a').trigger('click');
	ui.refreshOrInitScrollers('#search-results-content');
}
ui.initElementSearch = function(){
	$('#search-results-tab-li').hide();
	var $ea = $('#elements-tab a');
	$ea.mousedown(function(ev){
		$('#search-results-tab-li').data('activetab', $(this));
	});
	$('#element-library-search-input').focus(function(ev){
		ui.searchForElement($(this).val());
	});
	$('#element-library-search-input').keyup(function(ev){
		ui.searchForElement($(this).val());
	});
	$('#element-library-search-input').blur(function(ev){
		$('#elements-tab li').show();
		$sli = $('#search-results-tab-li');
		$sli.hide();
		$sli.data('activetab').trigger('click');
	});
	$ea.first().trigger('mousedown');
}
ui.initTooltips = function(){
	$('#undo').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip-navbar', title: 'Undo Last Action [ctrl+z]', hideAfter: 2000});
	$('#redo').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip-navbar', title: 'Redo Last Action [ctrl+shift+z]', hideAfter: 2000});
	
	$('#back').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip-navbar', title: 'Send to Back [ ctrl+ [ ]', hideAfter: 2000});
	$('#backward').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip-navbar', title: 'Send Backward [ [ ]', hideAfter: 2000});
	$('#forward').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip-navbar', title: 'Bring Forward [ ] ]', hideAfter: 2000});
	$('#front').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip-navbar', title: 'Bring to Front [ ctrl+ ] ]', hideAfter: 2000});

	$('#group').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip-navbar', title: 'Group/ungroup selected elements [ctrl+g]', hideAfter: 2000});

	$('#cut').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip-navbar', title: 'Cut selected elements [ctrl+x]', hideAfter: 2000});
	$('#copy').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip-navbar', title: 'Copy selected elements [ctrl+c]', hideAfter: 2000});
	$('#paste').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip-navbar', title: 'Paste selected elements [ctrl+v]', hideAfter: 2000});

	$('#defasce').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip-navbar', title: 'Define selected elements as template', hideAfter: 2000});
	$('#defasme').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip-navbar', title: 'Define selected elements as master', hideAfter: 2000});

	$('#grid').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip-navbar', title: 'Toggle grid', hideAfter: 2000});

	$('#preview').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip-navbar', title: 'Quick Preview [ctrl+p]', hideAfter: 2000});

	$('#project-title').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip-navbar', title: 'Click to rename project', hideAfter: 2000});
	$('#page-title').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip-navbar', title: 'Click to rename page', hideAfter: 2000});

	$('#elements-tab-a').tooltip({html: false, placement: 'right', attachTo: '#tbs-tooltip-navbar', title: '[ctrl+1]', hideAfter: 2000});
	$('#templates-tab-a').tooltip({html: false, placement: 'right', attachTo: '#tbs-tooltip-navbar', title: '[ctrl+2]', hideAfter: 2000});
	$('#masters-tab-a').tooltip({html: false, placement: 'right', attachTo: '#tbs-tooltip-navbar', title: '[ctrl+3]', hideAfter: 2000});
}
ui.initNotificationTimer = function(){
	$(document.body).data('notificationTimer', window.setTimeout(function(){
		if($(document.body).data('showingModal'))
			ui.initNotificationTimer();
		else
			modal.show('notificationRequest');
	}, 60000));
}
ui.initNotificationRequest = function(){
	if(!persistanceManager.getUserEmailForNotifications()){
		ui.initNotificationTimer();
	}
	$('#notificationRequest').click(function(ev){
		var $tid = $(document.body);
		window.clearTimeout($tid.data('notificationTimer'));
		$tid.removeData('notificationTimer');
		modal.show('notificationRequest');
	});
}
ui.toggleLoginState = function(user){
	var $liu = $('#loggedinuser');
	$liu.text($liu.text() == user ? 'Mocktails :' : user);
	$liu = $('#forgotPassword');
	$liu.text($liu.text().indexOf('Forgot') == -1 ? 'Forgot Password' : 'Change Password');
	$('#signuporlogin').toggle();
	$('#logout').toggle();
	$('#verifyEmail').toggle();
	$('#resendVerificationEmail').toggle();
	$('#mocktailsMenu .divider:last').toggle();
}
ui.logout = function(){
	api.logout({
		200: function(data, textStatus, xhr){
			ui.toggleLoginState(persistanceManager.getLoggedInUser());
			persistanceManager.removeLoggedInUser();
		},
		404: function(xhr, textStatus, error){
			console.log('No such session exists.');
			ui.toggleLoginState(persistanceManager.getLoggedInUser());
			persistanceManager.removeLoggedInUser();
		}
	});
}
ui.init = function(){
	persistanceManager.loadData();
	ui.initElementSearch();
	ui.initElementLibrary();
	ui.initDragSelector();
	ui.initWorkspace();
	$('#backward').click(function(e){					
		mockup.moveElement(false,false);
	});
	$('#forward').click(function(e){
		mockup.moveElement(true,false);
	});
	$('#back').click(function(e){
		mockup.moveElement(false,true);
	});
	$('#front').click(function(e){
		mockup.moveElement(true,true);
	});
	$('#undo').click(function(e){
		actionManager.undo(stateManager.currentState.currentPage);
	});
	$('#redo').click(function(e){
		actionManager.redo(stateManager.currentState.currentPage);
	});
	$('#copy').click(function(e){
		mockup.copy();
	});
	$('#cut').click(function(e){
		mockup.cut();
	});
	$('#paste').click(function(e){
		mockup.paste();
	});
	$('#align-hls').click(function(e){
		mockup.alignElements('hls');
	});
	$('#align-hcs').click(function(e){
		mockup.alignElements('hcs');
	});
	$('#align-hrs').click(function(e){
		mockup.alignElements('hrs');
	});
	$('#align-vts').click(function(e){
		mockup.alignElements('vts');
	});
	$('#align-vcs').click(function(e){
		mockup.alignElements('vcs');
	});
	$('#align-vbs').click(function(e){
		mockup.alignElements('vbs');
	});
	$('#align-hlp').click(function(e){
		mockup.alignElements('hlp');
	});
	$('#align-hcp').click(function(e){
		mockup.alignElements('hcp');
	});
	$('#align-hrp').click(function(e){
		mockup.alignElements('hrp');
	});
	$('#align-vtp').click(function(e){
		mockup.alignElements('vtp');
	});
	$('#align-vcp').click(function(e){
		mockup.alignElements('vcp');
	});
	$('#align-vbp').click(function(e){
		mockup.alignElements('vbp');
	});
	$('#newProject').click(function(e){
		if(!$(this).parent().hasClass('disabled')) modal.show('newProject');
	});
	$('#editProject').click(function(e){
		if(!$(this).parent().hasClass('disabled')) modal.show('editProject');
	});
	$('#deleteProject').click(function(e){
		if(!$(this).parent().hasClass('disabled')) modal.show('deleteProject');
	});
	$('#duplicateProject').click(function(e){
		if(!$(this).parent().hasClass('disabled')) modal.show('duplicateProject');
	});
	$('#newPage').click(function(e){
		if(!$(this).parent().hasClass('disabled')) modal.show('newPage');
	});
	$('#editPage').click(function(e){
		if(!$(this).parent().hasClass('disabled')) modal.show('editPage');
	});				
	$('#deletePage').click(function(e){
		if(!$(this).parent().hasClass('disabled')) modal.show('deletePage');
	});
	$('#duplicatePage').click(function(e){
		if(!$(this).parent().hasClass('disabled')) modal.show('duplicatePage');
	});
	$('#grid').click(function(e){
		var $currentPage = $('#page-container .current-page');
		$currentPage.toggleClass('hasgrid');
		persistanceManager.setGridSetting($currentPage.hasClass('hasgrid'));
	});	
	$('#defasce').click(function(e){
		modal.show('defineElementTemplate');
	});
	$('#defasme').click(function(e){
		modal.show('defineMasterElement');
	});
	$('#preview').click(function(e){
		//ui.togglePreview();
	});
	$('#group').click(function(e){
		if($(this).data('action') == 'group')
			mockup.groupElements();
		else
			mockup.ungroupElements();
	});
	$('#signuporlogin').click(function(e){
		modal.show('signup');
	});
	$('#verifyEmail').click(function(e){
		modal.show('verifyEmail');
	});
	$('#resendVerificationEmail').click(function(e){
		modal.show('resendVerification');
	});
	$('#forgotPassword').click(function(e){
		modal.show('forgotPassword');
	});
	$('#changePassword').click(function(e){
		modal.show('changePassword');
	});
	$('#logout').click(function(e){
		ui.logout();
	});
	ui.setInterface();
	$(window).resize(function(){
		ui.initScrollers();
		var top = $('#menu').height();
		$('#leftPanels').css('top', top+'px');
		$('#workspace').css('top', top+'px');
	});
	$('#elements-tab a').on('shown', function(ev){
		ui.initScrollers();
	});
	$('#project-title').click(function(ev){
		$('#editProject').trigger('click');
	});
	$('#page-title').click(function(ev){
		$('#editPage').trigger('click');
	});
	ui.initMenus();
	ui.initKeyboard();
	ui.initProject();
	ui.initTooltips();

	//Should only be used while in beta.
	ui.initNotificationRequest();

	//Initdata
	stateManager.currentState.elementTemplateCount = persistanceManager.getElementTemplateCount();
	stateManager.currentState.elementTemplateCount = stateManager.currentState.elementTemplateCount ? stateManager.currentState.elementTemplateCount : 0;

}
var page = {};
page.suffix = function(){				
	return ui._getTitleSuffix($('#menu-item-switchPageSubmenu .switchPage').map(function() { return $(this).text().trim() }).get(),'New Page');
}
page.duplicateSuffix = function(){
	return ui._getTitleSuffix($('#menu-item-switchPageSubmenu .switchPage').map(function() { return $(this).text().trim() }).get(), persistanceManager.getPageTitleForProject(stateManager.currentState.currentPage, stateManager.currentState.currentProject)+' (Duplicate');
}
page._new = function(title){
	page.close();
	_gaq.push(['_trackEvent', 'page', 'new']);
	title = title ? title : page.suffix();
	persistanceManager.setPageCountForProject(++stateManager.currentState.pageCount,stateManager.currentState.currentProject);
	$.extend(stateManager.currentState, stateManager.newPageState);
	$('#page-title').text(title);
	var _page = $('<div id="page-' + stateManager.currentState.pageCount + '" class="resizable page current-page" data-element="page"></div>').appendTo('#page-container');
	stateManager.currentState.currentPage = 'page-' + stateManager.currentState.pageCount;
	_page.css({ width: paper.page.properties.defaults.width, height: paper.page.properties.defaults.height, left: paper.page.properties.defaults.left, top: paper.page.properties.defaults.top, 'z-index': paper.page.properties.defaults.zindex });
	_page.data('title', title);
	page.init();
	stateManager.saveSnapshot()
	ui.initMenus();
	$('#explorer-content .slider-vertical').slider('option','value',0);
	$('#explorer-content .slider-horizontal').slider('option','value',100);
	return 'page-' + stateManager.currentState.pageCount;
};
page.duplicate = function(title){
	if(!title || title.length == 0) return;
	var _page = persistanceManager.getPageFromProject(stateManager.currentState.currentPage, stateManager.currentState.currentProject);
	var newPage = 'page-'+(++stateManager.currentState.pageCount);
	_page[newPage] = _page[stateManager.currentState.currentPage];
	delete _page[stateManager.currentState.currentPage];
	_page[newPage].title = title;
	persistanceManager.setPageCountForProject(stateManager.currentState.pageCount,stateManager.currentState.currentProject);
	persistanceManager.setPageForProject(newPage, _page, stateManager.currentState.currentProject);
	page.load(newPage);
};
page.rename = function(title,uid){
	if(!title) return;
	var $currentPage = $('#' + stateManager.currentState.currentPage);
	if($currentPage.data('title',title) == title) return;
	uid = uid ? uid : stateManager.currentState.currentPage;
	if(stateManager.currentState.currentPage == uid){
		$('#page-title').text(title);
		$currentPage.data('title',title);
	}
	persistanceManager.setPageTitleForProject(uid,title, stateManager.currentState.currentProject);				
};
page.close = function(){
	ui.dismissPopovers();
	$('#page-title').text('');
	$('#workspace-tabs').css('top', '-40px');
	var $cPage = $('#' + stateManager.currentState.currentPage);
	if($cPage.length < 1) return;
	stateManager.cleanup(stateManager.currentState.currentPage);
	stateManager.diffSnapshot($cPage,'closesheet');
	$cPage.remove();
	stateManager.currentState.currentPage = null;
	$('#page-container').html('');
	ui.initMenus();
};
page.load = function(pageName){
	page.close();
	_gaq.push(['_trackEvent', 'page', 'open']);
	var pageData = persistanceManager.getPageFromProject(pageName,stateManager.currentState.currentProject);
	if(!pageName) return;// page._new();
	if(!pageData) return page.load(persistanceManager.getFirstPage());
	$('#page-title').text(pageData[pageName].title);
	var _page = $('<div id="' + pageName + '" class="resizable page current-page" data-element="page"></div>').appendTo('#page-container');
	stateManager.currentState.currentPage = pageName;
	_page.css({ width: pageData[pageName].width, height: pageData[pageName].height });
	_page.data('title', pageData[pageName].title);
	page.init();
	persistanceManager.setCurrentPageForProject(pageName, stateManager.currentState.currentProject);
	var start = Date.now();
	stateManager.restoreFromSnapshot(pageName);
	var stop = Date.now();
	console.log('Page rendered in: ' + (stop-start) + 'ms');
	ui.initScrollers();
	ui.initMenus();	
};
page._delete = function(sn){
	//$('#explorer-item-'+sn).remove();
	_gaq.push(['_trackEvent', 'page', 'delete']);
	stateManager.clearState(sn);
	$('#page-container').html('');
	stateManager.currentState.currentPage = null;
	page.load(persistanceManager.getCurrentPageForProject(stateManager.currentState.currentProject));				
};
page.init = function(s, noInit){
	$('#workspace-tabs').css('top','0px');
	var $s = s ? $(s) : $('#' + stateManager.currentState.currentPage);
	$s.on('mousemove', function(e){
		$(this).data('moved', true);
	}).on('mouseup',function(e){
		var moved = $(this).data('moved'); 
		$(this).removeData('moved'); 
		ui.setInterface();
		if(moved) return;
		if($(e.target).hasClass('current-page')){
			mockup.deselectAll();
		}
	});
	$s.dblclick(function(e){
		if($(e.target).hasClass('current-page'))
			ui.showProperties($(this));
	});
	$s.resizable({
		handles: 's, e, se',
		start: function(e, _ui){
			$(this).data('wa', $('#workspace-area'));
		},
		resize: function(e,_ui){
			var $t = $(this), $wa = $t.data('wa'), handle = $t.data('handle');
			ui.showResizeTooltip($t, {top: e.pageY, left: e.pageX});
			if(handle.indexOf('ui-resizable-e') != -1 || handle.indexOf('ui-resizable-se') != -1){
				ui.refreshOrInitScrollers('#workspace-area');
				$wa.find('.slider-horizontal').slider('option','value',100);
			}
			if(handle.indexOf('ui-resizable-s') != -1 || handle.indexOf('ui-resizable-se') != -1){
				ui.refreshOrInitScrollers('#workspace-area');
				$wa.find('.slider-vertical').slider('option','value',0);
			}
		},
		stop: function(){
			ui.hideTooltip();
			mockup.resizeEnded($(this));
			ui.initScrollers();
		}
	});
	$s.find('.ui-resizable-handle').mousedown(function(ev){
		var $t = $(this);
		$t.parent().data('handle', $t.attr('class'));
	});
	if(!noInit)
		mockup.initElement($('#page-container .element'));
	ui.initScrollers();
	if(persistanceManager.getGridSetting())
		$('#grid').click().addClass('active');
}
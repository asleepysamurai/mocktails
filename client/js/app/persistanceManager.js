var persistanceManager = {
	_getItemFromLocalStorage: function(name, isJSON){
		return isJSON ? JSON.parse(localStorage.getItem(name)) : localStorage.getItem(name);
	},
	_saveItemToLocalStorage: function(name, val, isJSON){
		localStorage.setItem(name, isJSON ? JSON.stringify(val) : val);
	},
	_removeFromLocalStorage: function(name){
		localStorage.removeItem(name);
	},
	_getItemFromDocumentStorage: function(name){
		return documentStorage.getItem(name);
	},
	_saveItemToDocumentStorage: function(name, val){
		documentStorage.setItem(name, val);
	},
	_removeFromDocumentStorage: function(name){
		documentStorage.removeItem(name);
	},
	_getItemFromPersistanceMedium: function(name, isJSON){
		return persistanceManager._getItemFromDocumentStorage(name+'', isJSON);
	},
	_saveItemToPersistanceMedium: function(name, val, isJSON){
		persistanceManager._saveItemToDocumentStorage(name+'', val, isJSON);
		persistanceManager._saveItemToLocalStorage(name+'', val, isJSON);
	},
	_removeFromPersistanceMedium: function(name){
		persistanceManager._removeFromDocumentStorage(name+'');
		persistanceManager._removeFromLocalStorage(name+'');
	},

	loadData: function(){
		if(localStorage.length > 0){
			persistanceManager._saveItemToDocumentStorage('betaNotice', persistanceManager._getItemFromLocalStorage('betaNotice'));
			persistanceManager._saveItemToDocumentStorage('notificationEmail', persistanceManager._getItemFromLocalStorage('notificationEmail'));			
			persistanceManager._saveItemToDocumentStorage('current-project', persistanceManager._getItemFromLocalStorage('current-project'));
			persistanceManager._saveItemToDocumentStorage('grid', persistanceManager._getItemFromLocalStorage('grid', true));
			persistanceManager._saveItemToDocumentStorage('project-count', persistanceManager._getItemFromLocalStorage('project-count'));
			persistanceManager._saveItemToDocumentStorage('element-template-count', persistanceManager._getItemFromLocalStorage('element-template-count'));
			persistanceManager._saveItemToDocumentStorage('element-templates', persistanceManager._getItemFromLocalStorage('element-templates', true));
			
			var _projects = persistanceManager._getItemFromLocalStorage('projects', true);
			persistanceManager._saveItemToDocumentStorage('projects', _projects);
			for(var p in _projects){
				var _project = persistanceManager._getItemFromLocalStorage(_projects[p], true);
				persistanceManager._saveItemToDocumentStorage(_projects[p], _project);
				var key = _projects[p]+'-master-elements';
				persistanceManager._saveItemToDocumentStorage(key, persistanceManager._getItemFromLocalStorage(key, true));
				var _pages = _project.pages;
				for(var pg in _pages){
					var key = _pages[pg]+'-'+_projects[p];
					persistanceManager._saveItemToDocumentStorage(key, persistanceManager._getItemFromLocalStorage(key, true));
				}
			}
		}
	},

	//store data
	getCurrentPageForProject: function(_project){
		var prj =  persistanceManager.getProject(_project);
		if(prj && prj.currentPage) return prj.currentPage;
		return null;
	},
	setCurrentPageForProject: function(_page, _project){
		var prj =  persistanceManager.getProject(_project);
		if(!prj) return;
		prj.currentPage = _page;
		persistanceManager.setProject(_project, prj);
	},
	getCurrentProject: function(){
		return persistanceManager._getItemFromPersistanceMedium('current-project');
	},
	setCurrentProject: function(s){
		persistanceManager._saveItemToPersistanceMedium('current-project',s);
	},
	removeCurrentProject: function(){
		persistanceManager._removeFromPersistanceMedium('current-project');
	},
	getPageCountForProject: function(_project){
		var p = persistanceManager._getItemFromPersistanceMedium(_project,true);
		if(p && p.pageCount) return p.pageCount;
		return null;
	},
	setPageCountForProject: function(count, _project){
		var p = persistanceManager._getItemFromPersistanceMedium(_project,true);
		if(!p) p = {};
		p.pageCount = count;
		persistanceManager.setProject(_project, p);
	},
	getProjectCount: function(){
		return persistanceManager._getItemFromPersistanceMedium('project-count');
	},
	setProjectCount: function(v){
		return persistanceManager._saveItemToPersistanceMedium('project-count',v);
	},
	removeCurrentPageForProject: function(_project){
		var pData = persistanceManager.getProject(_project);
		delete pData.currentPage;
		persistanceManager.setProject(_project, pData);
	},
	getGridSetting: function(){
		return persistanceManager._getItemFromPersistanceMedium('grid',true);
	},
	setGridSetting: function(g){
		return persistanceManager._saveItemToPersistanceMedium('grid', g, true);
	},
	getPageFromProject: function(s,_project){
		return persistanceManager._getItemFromPersistanceMedium(s+'-'+_project, true);
	},
	setPageForProject: function(name, val, _project){
		if(!persistanceManager._getItemFromPersistanceMedium(name+'-'+_project, true))
			persistanceManager.addNewPageToProject(name, stateManager.currentState.currentProject);
		persistanceManager._saveItemToPersistanceMedium(name+'-'+_project, val, true);
	},
	getProject: function(name){
		return persistanceManager._getItemFromPersistanceMedium(name, true);	
	},
	setProject: function(name,val){
		if(!persistanceManager._getItemFromPersistanceMedium(name, true))
			persistanceManager.addNewProject(name);
		persistanceManager._saveItemToPersistanceMedium(name, val, true);					
	},
	removeProject: function(name){
		var pages = persistanceManager.getAllPagesForProject(name);
		for(var page in pages)
			persistanceManager.removePageFromProject(pages[page], name);
		persistanceManager._removeFromPersistanceMedium(name+'-master-elements');
		persistanceManager._removeFromPersistanceMedium(name);
		var prj = persistanceManager._getItemFromPersistanceMedium('projects', true);
		if(!prj) return;
		var prjIndex = prj.indexOf(name);
		var nextPrj;
		if(prjIndex != -1)
			prj.splice(prjIndex,1);
		if(prj.length > 0)
			nextPrj = prjIndex > 0 ? prj[prjIndex - 1] : prj[0];
		persistanceManager._saveItemToPersistanceMedium('projects', prj, true);
		if(nextPrj) persistanceManager.setCurrentProject(nextPrj);
		else persistanceManager.removeCurrentProject();
	},
	addNewProject: function(name){
		var prj= persistanceManager._getItemFromPersistanceMedium('projects', true);
		if(!prj) prj = [];
		prj.indexOf(name) == -1 ? prj.push(name) : prj;
		persistanceManager._saveItemToPersistanceMedium('projects', prj, true);
	},
	removePageFromProject: function(name, _project){
		persistanceManager._removeFromPersistanceMedium(name+'-'+_project);
		var prj = persistanceManager._getItemFromPersistanceMedium(_project, true);
		if(!prj || !prj.pages) return;
		var pageIndex = prj.pages.indexOf(name);
		var nextPage;
		if(pageIndex != -1)
			prj.pages.splice(pageIndex,1);
		if(prj.pages.length > 0)
			nextPage = pageIndex > 0 ? prj.pages[pageIndex - 1] : prj.pages[0];
		persistanceManager._saveItemToPersistanceMedium(_project, prj, true);
		if(nextPage) persistanceManager.setCurrentPageForProject(nextPage,stateManager.currentState.currentProject);
		else persistanceManager.removeCurrentPageForProject(stateManager.currentState.currentProject);
	},
	addNewPageToProject: function(name, _project){
		var prj= persistanceManager._getItemFromPersistanceMedium(_project, true);
		if(!prj) return;
		if(!prj.pages) 
			prj.pages = [];
		prj.pages.indexOf(name) == -1 ? prj.pages.push(name) : prj.pages;
		persistanceManager._saveItemToPersistanceMedium(_project, prj, true);
	},
	getAllPagesForProject: function(_project){
		var prj = persistanceManager._getItemFromPersistanceMedium(_project, true);
		if(prj && prj.pages) return prj.pages;
		return null;
	},
	getAllProjects: function(){
		return persistanceManager._getItemFromPersistanceMedium('projects', true);
	},
	getFirstProject: function(){					
		var prj = persistanceManager.getAllProjects();
		if(prj && prj[0])
			return prj[0];
		return null;
	},
	getLastProject: function(){					
		var prj = persistanceManager.getAllProjects();
		if(prj && prj.length > 0)
			return prj[prj.length - 1];
		return null;
	},
	getFirstPage: function(){
		var pages = persistanceManager._getItemFromPersistanceMedium('pages', true);
		if(pages && pages[0])
			return pages[0];
		return null;
	},
	getLastPage: function(){
		var pages = persistanceManager._getItemFromPersistanceMedium('pages', true);
		if(pages && pages.length > 0)
			return pages[pages.length - 1];
		return null;
	},
	getAllPageTitlesForProject: function(_project){
		var pages = persistanceManager.getAllPagesForProject(_project);
		var pageTitles = $(pages).map(function() { return {id: this, title: persistanceManager.getPageTitleForProject(this, _project)} }).get();
		return pageTitles;
	},
	getPageTitleForProject: function(_page, _project){
		var pdata = persistanceManager.getPageFromProject(_page, _project);
		if(pdata && pdata[_page] && pdata[_page].title)
			return pdata[_page].title;
		return null;
	},
	setPageTitleForProject: function(_page, title, _project){
		var pdata = persistanceManager.getPageFromProject(_page, _project);
		if(pdata && pdata[_page])
			pdata[_page].title = title;
		persistanceManager.setPageForProject(_page, pdata, _project);
	},
	getAllProjectTitles: function(){
		var prj = persistanceManager.getAllProjects();
		var prjTitles = $(prj).map(function() {  return {id: this, title: persistanceManager.getProjectTitle(this)}; }).get();
		return prjTitles;
	},
	getProjectTitle: function(_project){
		var pdata = persistanceManager.getProject(_project);
		if(pdata && pdata.title)
			return pdata.title;
		return null;
	},
	setProjectTitle: function(_project, title){
		var pdata = persistanceManager.getProject(_project);
		if(pdata)
			pdata.title = title;
		persistanceManager.setProject(_project, pdata);
	},
	getAllElementTemplates: function(){
		return persistanceManager._getItemFromPersistanceMedium('element-templates', true);
	},
	getElementTemplate: function(_templateid){
		var cg = persistanceManager._getItemFromPersistanceMedium('element-templates', true);
		return cg[_templateid];
	},
	addNewElementTemplate: function(_template){
		var cg = persistanceManager._getItemFromPersistanceMedium('element-templates', true);
		if(!cg)
			cg = {};
		$.extend(cg, _template);
		persistanceManager._saveItemToPersistanceMedium('element-templates', cg, true);
	},
	removeElementTemplate: function(_templateid){
		var cg = persistanceManager._getItemFromPersistanceMedium('element-templates', true);
		if(cg && cg[_templateid])
			delete cg[_templateid];
		persistanceManager._saveItemToPersistanceMedium('element-templates', cg, true);
	},
	getElementTemplateCount: function(){
		return parseInt(persistanceManager._getItemFromPersistanceMedium('element-template-count'));
	},
	setElementTemplateCount: function(v){
		return persistanceManager._saveItemToPersistanceMedium('element-template-count',v);
	},
	getMasterElementCountForProject: function(_project){
		var prj = persistanceManager.getProject(_project);		
		return prj.masterElementCount;
	},
	setMasterElementCountForProject: function(v,_project){
		var prj = persistanceManager.getProject(_project);		
		prj.masterElementCount = v;
		persistanceManager.setProject(_project, prj);
	},
	editMasterElementForProject: function(_id, _master, _project){
		var me = persistanceManager.getAllMasterElementsForProject(_project);
		if(!me) return false;
		var eln = {e: _master, n: me[_id].n};
		delete me[_id];
		me[_id] = $.extend({}, eln);
		persistanceManager._saveItemToPersistanceMedium(_project+'-master-elements', me, true);		
	},
	removeMasterElementFromProject: function(name, _project){
		var masters = persistanceManager.getAllMasterElementsForProject(_project);
		if(masters && masters[name])
			delete masters[name];
		persistanceManager._saveItemToPersistanceMedium(_project+'-master-elements', masters, true);
	},
	getAllMasterElementsForProject: function(_project){
		return persistanceManager._getItemFromPersistanceMedium(_project+'-master-elements', true);
	},
	setAllMasterElementsForProject: function(val, _project){
		persistanceManager._saveItemToPersistanceMedium(_project+'-master-elements', val, true);
	},
	getMasterElementFromProject: function(_id, _project){
		var me = persistanceManager.getAllMasterElementsForProject(_project);
		return me[_id];
	},
	setMasterElementForProject: function(_master, _project){
		var me = persistanceManager.getAllMasterElementsForProject(_project);
		if(!me) me = {};
		$.extend(true, me, _master);
		persistanceManager._saveItemToPersistanceMedium(_project+'-master-elements', me, true);
	},
	revertMasterInstancesForProject: function(_master, _project){
		var members = persistanceManager.getMasterElementFromProject(_master, _project).e;
		var pageids = persistanceManager.getAllPagesForProject(_project), pages = {};		
		for(var id in pageids){
			var page = persistanceManager.getPageFromProject(pageids[id], _project);
			for(var e in page){
				if(page[e].master){
					var master = persistanceManager.getMasterElementFromProject(page[e].master, _project).e[page[e].mid];
					page[e].custom = $.extend(true, {}, master.custom);
					delete page[e].master;
					delete page[e].mid;
					delete page[e].iid;
				}
			}
			persistanceManager.setPageForProject(pageids[id], page, _project);
		}
	},

	// User related methods
	setUserEmailForNotifications: function(val){
		persistanceManager._saveItemToPersistanceMedium('notificationEmail', val);
		api.subscribe(val);
	},
	getUserEmailForNotifications: function(){
		return persistanceManager._getItemFromPersistanceMedium('notificationEmail');
	},
	setBetaNoticeShown: function(val){
		persistanceManager._saveItemToPersistanceMedium('betaNotice', val);
	},
	getBetaNoticeShown: function(){
		return persistanceManager._getItemFromPersistanceMedium('betaNotice');
	},
	setLoggedInUser: function(val){
		persistanceManager._saveItemToPersistanceMedium('loggedInUser', val);
	},
	getLoggedInUser: function(){
		return persistanceManager._getItemFromPersistanceMedium('loggedInUser');
	},
	removeLoggedInUser: function(){
		persistanceManager._removeFromPersistanceMedium('loggedInUser');
	}
};
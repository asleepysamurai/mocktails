paper.page = {
	properties:{
		min: {
			width: 100,
			height: 20
		},
		defaults: {
			width: 1000,
			height: 600,
			top: '0px',
			left: '0px',
			zindex: 1
		},
		show: function($e, onshow){
			$('#props').remove();
			var html = '<div id="props"><div id="propsFor" style="display:none">'+$e.attr('id')+'</div><div class="form-horizontal"><div id="div-pseudo"></div></div></div>';
			//ui.dismissPopovers();
			$e.popover({title:  '<strong style="color:#3a3a3a;"  id="props-title">Page Properties</strong>', content: html, placement: 'auto', attachTo: '#tbs-popover', onShow: function(){
				paper._buildDimensions($e, '#div-pseudo');
				$('#tbs-popover .popover-title').html('<strong style="color:#3a3a3a;"  id="props-title">Page Properties</strong>');
				if(onshow) onshow();
			}, onClick: function(ev){paper._popoverClick(ev,$e)}, onHide: function(){
				paper._destroyAllTooltips($e);
			}});
			$e.popover('show');
			ui.popoverContainers.push($e);
		}
	},
	draw : function(){}
};
paper.icon = {
	properties: {
		defaults: {
			icon: 'star',
			height: 30,
			width: 30,
			padding: 10,
			border: null,
			color: {s: '#000000', b: '#000000', bg: 'none'}
		},
		show: function($e, onshow){
			$('#props').remove();
			var props = $e.data('properties');
			var icon = '<div class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Icon</strong></label><div class="controls"><button id="icon" class="btn btn-small"><i class="icon-'+props.icon+'"></i></button></div></div>';
			var border = '<div class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Border</strong></label><div class="controls"><div class="btn-group" data-toggle="buttons-radio"><button id="border-rr" class="btn btn-small '+(props.border == 0 ? 'active' : '')+'">RR</button><button id="border-r" class="btn btn-small '+(props.border == 1 ? 'active' : '')+'">R</button><button id="border-p" class="btn btn-small '+(props.border == 2 ? 'active' : '')+'">P</button><button id="border-n" class="btn btn-small '+(props.border === null ? 'active' : '')+'">N</button></div></div></div>';
			var color = '<div id="div-color" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Colors</strong></label><div class="controls"><div id="color-options" class="btn-group" data-toggle="buttons-radio"><button id="color-b" class="btn btn-small"><i class="icon-stop"></i></button><button id="color-bg" class="btn btn-small"><i class="icon-stop"></i></button><button id="color-s" class="btn btn-small"><i class="icon-stop"></i></button></div></div></div>';
			var html = '<div id="props"><div id="propsFor" style="display:none">'+$e.attr('id')+'</div><div class="form-horizontal" id="props-form">'+icon+border+color+'</div></div>';
			//ui.dismissPopovers();
			$e.popover({title:  '<strong style="color:#3a3a3a;"  id="props-title">Icon Properties</strong>', content: html, placement: 'auto', attachTo: '#tbs-popover', onShow:function(){
				paper._buildDimensions($e, '#div-color');
				paper._buildOpacity($e, '#div-color',props);
				paper._buildLinkInspector($e, '#div-color',props);
				$('#icon').click(function(ev){
					paper._showIconPicker(ev, this, 'icon');
				})
				$('#border-rr').click(function(ev){
					paper._changeProperty('border', 0);
				});
				$('#border-r').click(function(ev){
					paper._changeProperty('border', 1);
				});
				$('#border-p').click(function(ev){
					paper._changeProperty('border', 2);
				});
				$('#border-n').click(function(ev){
					paper._changeProperty('border', null);
				});
				$('#icon').click(function(ev){
					//Add show icon code
				});
				$('#color-b').data('val', props.color.b ? props.color.b : paper.icon.properties.defaults.color.b);
				$('#color-bg').data('val', props.color.bg ? props.color.bg : paper.icon.properties.defaults.color.bg);
				$('#color-s').data('val', props.color.s ? props.color.s : paper.icon.properties.defaults.color.s);
				$('#color-b').css('color', props.color.b ? props.color.b : paper.icon.properties.defaults.color.b);
				$('#color-bg').css('color', props.color.bg ? props.color.bg : paper.icon.properties.defaults.color.bg);
				$('#color-s').css('color', props.color.s ? props.color.s : paper.icon.properties.defaults.color.s);
				$('#color-b').click(function(ev){
					paper._showColorPicker(ev, this, 'color.b');
				});
				$('#color-bg').click(function(ev){
					paper._showColorPicker(ev, this, 'color.bg');
				});
				$('#color-s').click(function(ev){
					paper._showColorPicker(ev, this, 'color.s');
				});
				//Tooltips
				$('#border-rr').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Rounded Rect', hideAfter: 2000});
				$('#border-r').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Rectangular', hideAfter: 2000});
				$('#border-p').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Pill', hideAfter: 2000});
				$('#border-n').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'None', hideAfter: 2000});
				$('#color-b').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Border', hideAfter: 2000});
				$('#color-bg').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Background', hideAfter: 2000});
				$('#color-s').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Icon', hideAfter: 2000});
				if(onshow) onshow();
			}, onClick: function(ev){paper._popoverClick(ev,$e)}, onHide: function(){
				paper._destroyAllTooltips($e);
			}});
			$e.popover('show');
			ui.popoverContainers.push($e);
		}
	},
	draw: function(canvas, l, t, w, h, v){
		if(!canvas) return;
		if(!v) v = {};
		if(v.linkto)
			$(canvas.canvas.parentElement).addClass('has-link');
		if(v.border !== null){
			var r = v.border == 2 ? h/2 : v.border == 0 ? 5 : 0;
			if(v.icon =='radio')
				canvas.circle(w/2,h/2, w/2-2).attr({'stroke-width': '2px', 'stroke': v.color.b, 'fill': v.color.bg});
			else
				canvas.rect(l+1,t+1,w-2,h-2, r).attr({'stroke-width': '2px', 'stroke': v.color.b, 'fill': v.color.bg});
		}
		else
			canvas.rect(l+1,t+1,w-2,h-2, r).attr({'stroke-width': '0px', 'fill': v.color.bg});	
		var x1, y1;
		var w1, h1;
		var path;
		var wa = w/paper.icon.properties.defaults.width, ha = h/paper.icon.properties.defaults.height;
		v.padding = w/5;
		w1 = w - v.padding, h1 = h - v.padding;
		v.autosize = false;
		v.color.t = v.color.s;
		v.align = 'center';
		v.size = Math.min(w,h)*16/20 + 'px';
		switch(v.icon){
			case 'radio':
				path = canvas.circle(w/2, h/2, w1/4).attr({'stroke-width': '0px', 'fill':v.color.s, 'stroke':v.color.s});
				paper._drawLink(v, $(canvas.canvas));
				break;
			default:
				v.caption = '<i class="icon-'+v.icon+'"></i>';
				//paper.label.draw(canvas,l,t,w,h,lv);
				var $div = $(canvas.canvas.parentElement);
				var cp = '<p class="element-caption-para" style="display:table-cell;vertical-align:middle;font-size:'+v.size+';text-align:'+v.align+';word-wrap:break-word;">'+v.caption.nl2br()+'</p>';
				canvas.rect(l,t,w,h).attr({'stroke-width':'0px', 'fill': v.color.bg});
				$('<div class="element-caption" style="position:absolute;top:'+t+'px;left:'+l+'px;width:'+w+'px;height:'+h+'px;display:table;color:'+v.color.t+';">'+cp+'</div>').appendTo($div);
				paper._drawLink(v, $('.element-caption', $div));
				break;
		}
		if(path)
			path.attr({'stroke': v.color.s});
	}
};
paper.button = {
	properties: {
		defaults: {
			caption: 'Button',
			type: 1,
			color: {'b': '#000000','bg': '#c3c3c3', 't': '#000000'},
			align: 'center',
			size: '12px',
			height: 25,
			width: 75,
			icons: {right: null, left: null},
			autosize: false,
			menu: null
		},
		show: function($e, onshow){
			$('#props').remove();
			$('#props-title').remove();
			var props = paper._getDefaultedProperties($e.data('properties'), paper.button.properties.defaults);
			var autosizedrop = '<div id="div-autosizedrop" class="form-inline" style="text-align:center"><div class="btn-group" data-toggle="buttons-checkbox"><button id="autosize" class="btn btn-small '+(props.autosize ? 'active' : '')+'">&nbsp;Autosize&nbsp;</button><button id="dropdown" class="btn btn-small '+(props.menu == 1 ? 'active' : '')+'">Dropdown</button><button id="dropup" class="btn btn-small '+(props.menu == 2 ? 'active' : '')+'">&nbsp;Dropup&nbsp;</button></div></div>';
			var caption = '<div id="div-caption" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Caption</strong></label><div class="controls"><textarea id="input-caption" rows="2" style="width:135px">'+props.caption+'</textarea></div></div>';
			var type = '<div id="div-type" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Type</strong></label><div class="controls"><div class="btn-group" data-toggle="buttons-radio"><button id="type-rr" class="btn btn-small '+(props.type == 1 ? 'active' : '')+'">RR</button><button id="type-r" class="btn btn-small '+(props.type == 2 ? 'active' : '')+'">R</button><button id="type-p" class="btn btn-small '+(props.type == 3 ? 'active' : '')+'">P</button></div></div></div>';
			var icons = '<div id="div-icons" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Icons</strong></label><div class="controls"><div class="btn-group" data-toggle="buttons-checkbox"><button id="icon-left" class="btn btn-small '+(props.icons.left ? 'active' : '')+'">L</button><button id="icon-right" class="btn btn-small '+(props.icons.right ? 'active' : '')+'">R</button></div></div></div>';
			var color = '<div id="div-color" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Colors</strong></label><div class="controls"><div id="color-options" class="btn-group" data-toggle="buttons-radio"><button id="color-b" class="btn btn-small"><i class="icon-stop"></i></button><button id="color-bg" class="btn btn-small"><i class="icon-stop"></i></button><button id="color-t" class="btn btn-small"><i class="icon-stop"></i></button></div></div></div>';
			var size = '<div id="div-size" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Fontsize</strong></label><div class="controls"><input class="input-small" id="input-size"style="width:50px" type="text" value="'+(props.size ? props.size : paper.button.properties.defaults.size)+'" placeholder="'+(props.size ? props.size : paper.button.properties.defaults.size)+'"></div></div>';
			var align = '<div id="div-align" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Align</strong></label><div class="controls"><div class="btn-group" data-toggle="buttons-radio"><button id="align-l" class="btn btn-small '+(props.align == 'left' ? 'active' : '')+'"><i class="icon-align-left"></i></button><button id="align-c" class="btn btn-small '+(props.align == 'center' ? 'active' : '')+'"><i class="icon-align-center"></i></button><button id="align-j" class="btn btn-small '+(props.align == 'justify' ? 'active' : '')+'"><i class="icon-align-justify"></i></button><button id="align-r" class="btn btn-small '+(props.align == 'right' ? 'active' : '')+'"><i class="icon-align-right"></i></button></div></div></div>';
			var html = '<div id="props"><div id="propsFor" style="display:none">'+$e.attr('id')+'</div><div class="form-horizontal">'+caption+type+icons+color+align+size+'</div>'+autosizedrop+'</div>';
			//ui.dismissPopovers();
			$e.popover({title:  '<strong style="color:#3a3a3a;" id="props-title">Button Properties</strong>', content: html, placement: 'auto', attachTo: '#tbs-popover', onShow: function(){
				paper._buildDimensions($e, '#div-size');
				paper._buildOpacity($e, '#div-size',props);
				paper._buildLinkInspector($e, '#div-size',props);
				$('#input-size').spinner({min: 8, max: 98});
				$('#input-size').keydown(function(ev){
					if(ev.which == 13){
						paper._changeProperty('size', $(this).val() + 'px', false);
						ev.preventDefault();
					}
				});
				if(props.icons.right)
					$('#icon-right').html('<i class="icon-'+$.escapeHTML(props.icons.right)+'"></i>');
				if(props.icons.left)
					$('#icon-left').html('<i class="icon-'+$.escapeHTML(props.icons.left)+'"></i>');
				$('#input-size').blur(function(ev){
					paper._changeProperty('size', $(this).val() + 'px');
				});
				$('#input-size').change(function(ev){
					paper._changeProperty('size', $(this).val() + 'px', false);
				});
				$('#input-caption').blur(function(ev){
					paper._changeProperty('caption', $(this).val());
				});
				$('#input-caption').keyup(function(ev){
					paper._changeProperty('caption', $(this).val(), false);
				});
				$('#type-rr').click(function(ev){
					paper._changeProperty('type', 1);
				});
				$('#type-r').click(function(ev){
					paper._changeProperty('type', 2);
				});
				$('#type-p').click(function(ev){
					paper._changeProperty('type', 3);
				});
				$('#icon-left').click(function(ev){
					if($(this).hasClass('active'))
						paper._changeProperty('icons.left', false);
					else
						paper._showIconPicker(ev, this, 'icons.left');
				});
				$('#icon-right').click(function(ev){
					if($(this).hasClass('active'))
						paper._changeProperty('icons.right', false);
					else
						paper._showIconPicker(ev, this, 'icons.right');
				});
				$('#autosize').click(function(ev){
					if($(this).hasClass('active')){
						paper._changeProperty('autosize', false);
						mockup.initResizable($('#element-resizer'));
					}
					else{
						paper._changeProperty('autosize', true);
						$('#element-resizer').resizable('destroy');
					}
				});
				$('#dropdown').click(function(ev){
					if($(this).hasClass('active')){
						paper._changeProperty('menu', null);
					}
					else{	
						paper._changeProperty('menu', 'caret-down');
						$('#dropup').removeClass('active');
					}
				});
				$('#dropup').click(function(ev){
					if($(this).hasClass('active')){
						paper._changeProperty('menu', null);
					}
					else{	
						paper._changeProperty('menu', 'caret-up');
						$('#dropdown').removeClass('active');
					}
				});
				$('#align-l').click(function(ev){
					paper._changeProperty('align', 'left');
				});
				$('#align-c').click(function(ev){
					paper._changeProperty('align', 'center');
				});
				$('#align-j').click(function(ev){
					paper._changeProperty('align', 'justify');
				});
				$('#align-r').click(function(ev){
					paper._changeProperty('align', 'right');
				});
				$('#color-b').data('val', props.color.b ? props.color.b : paper.button.properties.defaults.color.b);
				$('#color-bg').data('val', props.color.bg ? props.color.bg : paper.button.properties.defaults.color.bg);
				$('#color-t').data('val', props.color.t ? props.color.t : paper.button.properties.defaults.color.t);
				$('#color-b').css('color', props.color.b ? props.color.b : paper.button.properties.defaults.color.b);
				$('#color-bg').css('color', props.color.bg ? props.color.bg : paper.button.properties.defaults.color.bg);
				$('#color-t').css('color', props.color.t ? props.color.t : paper.button.properties.defaults.color.t);
				$('#color-b').click(function(ev){
					paper._showColorPicker(ev, this, 'color.b');
				});
				$('#color-bg').click(function(ev){
					paper._showColorPicker(ev, this, 'color.bg');
				});
				$('#color-t').click(function(ev){
					paper._showColorPicker(ev, this, 'color.t');
				});
				//Tooltips
				$('#input-caption').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Hit Enter to Insert New Line', hideAfter: 2000});
				$('#icon-right').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Right', hideAfter: 2000});
				$('#icon-left').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Left', hideAfter: 2000});
				$('#type-rr').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Rounded Rect', hideAfter: 2000});
				$('#type-r').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Rectangular', hideAfter: 2000});
				$('#type-p').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Pill', hideAfter: 2000});
				$('#align-l').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Left', hideAfter: 2000});
				$('#align-c').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Center', hideAfter: 2000});
				$('#align-j').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Justified', hideAfter: 2000});
				$('#align-r').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Right', hideAfter: 2000});
				$('#color-b').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Border', hideAfter: 2000});
				$('#color-bg').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Background', hideAfter: 2000});
				$('#color-t').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Text', hideAfter: 2000});
				if(onshow) onshow();
			},onClick: function(ev){paper._popoverClick(ev,$e)}, onHide: function(){
				paper._destroyAllTooltips($e);
			}});
			$e.popover('show');
			ui.popoverContainers.push($e);
		}
	},
	_properties: {
		radius: 5
	},
	draw: function(canvas, l, t, w, h, v, redraw){
		if(!canvas) return;
		if(!v) v = {};
		v = paper._getDefaultedProperties(v, paper.button.properties.defaults);

		r = v.type == 2 ? 0 : v.type == 3 ? h/2 : paper.button._properties.radius;
		var $div = $(canvas.canvas.parentElement);
		if(v.linkto)
			$div.addClass('has-link');
		var cp = '<p class="element-caption-para" style="display:table-cell;vertical-align:middle;font-size:'+v.size+';text-align:'+v.align+';word-break:break-word;word-wrap:break-word;">'+v.caption.nl2br()+'</p>';
		l = Math.floor(l), t = Math.floor(t);
		var is = parseInt(v.size)*20/18;
		if(v.autosize){
			var as = paper._getTextAutoSize(cp);
			h = as.height, w = as.width;
			h += 10;
			if(v.type == 3){
				r = h/2;
				w += r*2;
			}
			else
				w += 10;

			w += (v.icons.right ? 25 : 0) + (v.icons.left ? 25 : 0) + (v.menu != null ? (v.type == 3 ? r + 5 : is + 5) : 0);
			w = Math.floor(w), h = Math.floor(h);

			paper._resize($div,w,h);
		}
		canvas.rect(l+1,t+1,w-2,h-2,r).attr({'stroke-width': '2px', 'stroke': v.color.b, 'fill': v.color.bg, border: null});

		var x1 = parseInt((w - ((v.type == 3) ? r*2 : is + 5)) - 2);
		var iv = $.extend(true,{}, paper.icon.properties.defaults);
		iv.color.b = v.color.b;
		iv.color.s = v.color.t;
		if(v.icons.left){
			iv.icon = v.icons.left;
			iv.color.bg = 'none';
			paper.icon.draw(canvas, l+1, t+2, w-x1-1,h-2, iv);
		}
		l += v.icons.left ? 25 : 0;
		if(v.menu != null){
			iv.icon = v.menu;
			if(v.color.fg){
				canvas.rect(x1+1,2,w-x1-3,h-4).attr({'stroke-width': '0px', 'fill': v.color.fg});
				paper.path(canvas, ['M',x1,1,'L',x1,(h-1)]).attr({'stroke-width':'2px', 'stroke': v.color.b});
				canvas.rect(l+1,t+1,w-2,h-2,r).attr({'stroke-width': '2px', 'stroke': v.color.b, 'fill': 'none'});
			}
			if(iv.icon == 'sort')
				paper.icon.draw(canvas, x1-1, t+2, w-x1-1,h-2, iv);
			else
				paper.icon.draw(canvas, x1-1, t+1, w-x1-1,h-2, iv);
			canvas.renderfix();
		}
		if(v.icons.right){
			iv.icon = v.icons.right;
			paper.icon.draw(canvas, v.menu ? x1-(w-x1-1) : l+x1+1-30, t+2, w-x1-1,h-2, iv);
		}

		if(v.type == 3 && r < (w/2-r))
			w -= (r*2), l += r;
		else
			w -= 10, l += 5;
		w -= (v.icons.right ? 25 : 0) + (v.icons.left ? 25 : 0) + (v.menu != null ? (v.type == 3 ? r+5 : is + 5) : 0);

		//h -= 10, t += 5;
		w = Math.ceil(w), h = Math.ceil(h), l = Math.ceil(l), t = Math.ceil(t);
		$('<div class="element-caption" style="position:absolute;top:'+t+'px;left:'+l+'px;width:'+w+'px;height:'+h+'px;display:table;color:'+v.color.t+';">'+cp+'</div>').appendTo($div);
		paper._drawLink(v, $div);
	}
};
paper.label = {
	properties: {
		defaults: {
			caption: 'Label',
			color: {'bg': 'none', 't': '#000000'},
			align: 'left',
			size: '14px',
			height: 25,
			width: 50,
			autosize: true
		},
		show: function($e, onshow){
			paper.button.properties.show($e, function(){				
				$('#div-type, #div-icons, #color-b, #dropdown, #dropup').remove();
				$('#autosize').css('width', '210px');
				$('#props-title').text('Label Properties');
				if(onshow) onshow();
			});
		}
	},
	draw: function(canvas, l, t, w, h, v){
		if(!canvas) return;
		if(!v) v = {};

		var $div = $(canvas.canvas.parentElement);
		if(v.linkto)
			$div.addClass('has-link');
		var cp = '<p class="element-caption-para" style="margin: 0;vertical-align:middle;font-size:'+v.size+';text-align:'+v.align+';word-wrap:break-word;">'+v.caption.nl2br()+'</p>';
		if(v.autosize){
			var as = paper._getTextAutoSize(cp);
			h = as.height, w = as.width;
			h += 10, w += 10 + (v.stroke ? v.stroke : 0);

			if(v.asinc){
				w += v.asinc.w;
				h += v.asinc.h;
			}

			paper._resize($div,w,h);
		}
		canvas.rect(l,t,w,h).attr({'stroke-width':'0px', 'fill': v.color.bg});
		w -= 10  + (v.stroke ? v.stroke : 0), l += 5;
		//h -= 10, t += 5;
		var $cpc = $('<div class="element-caption" style="position:absolute;overflow:hidden;top:'+t+'px;left:'+l+'px;width:'+w+'px;max-height:'+h+'px;height:'+h+'px;color:'+v.color.t+';"></div>').appendTo($div);
		var $cp = $(cp).appendTo($cpc);
		$cp.css({'top': Math.max(2.5,($cpc.height()-$cp.height())/2) + 'px', 'position': 'relative'});
		paper._drawLink(v, $('.element-caption', $div));
	}
};
paper.paragraph = {
	properties: {
		defaults: $.extend(true, {}, paper.label.properties.defaults, {'autosize': false, 'width':625, 'height':80, 'caption': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortor mauris molestie elit, et lacinia ipsum quam nec dui. Pellentesque arcu mauris, malesuada quis ornare accumsan, blandit sed diam.'}),
		show: function($e, onshow){
			paper.label.properties.show($e, function(){
				$('#props-title').text('Paragraph Properties');
				if(onshow) onshow();
			});
		}
	},
	draw: paper.label.draw
}
paper.link = {
	properties: {
		defaults: $.extend(true, {}, paper.label.properties.defaults,{caption: 'Link', width: 30, color: {'bg': 'none', 't': '#0d4bfd'}}),
		show: function($e, onshow){
			paper.label.properties.show($e, function(){
				$('#props-title').text('Link Properties');
				if(onshow) onshow();
			});
		}
	},
	draw: function(canvas, l, t, w, h, v){
		if(!canvas) return;
		paper.label.draw(canvas, l, t, w, h, v);
		$('.element-caption-para', canvas.canvas.parentElement).css('text-decoration','underline');
	}
};
paper.textfield = {
	properties: {
		defaults: {
			caption: 'Textfield',
			type: 2,
			color: {'b': '#000000','bg': '#ffffff', 't': '#000000'},
			align: 'left',
			size: '12px',
			height: 25,
			width: 75,
			autosize: false
		},
		show: function($e, onshow){
			paper.button.properties.show($e, function(ev){				
				$('#div-icons, #dropdown, #dropup').remove();
				$('#autosize').css('width', '210px');
				$('#props-title').text('Textfield Properties');
				if(onshow) onshow();
			});
		}
	},
	draw: function(canvas,l,t,w,h,v){
		paper.button.draw(canvas,l,t,w,h,v);
		$($('.element-caption-para',canvas.canvas.parentElement)[0]).css({'padding-top':'5px','vertical-align':'top'});
	}
}
paper.textarea = {
	properties: {
		defaults: $.extend(true, {}, paper.textfield.properties.defaults, {'caption': 'Multiline\nText', height:40}),
		show: function($e, onshow){
			paper.textfield.properties.show($e, function(){
				$('#props-title').text('Textarea Properties');
				if(onshow) onshow();
			});
		}
	},
	draw: paper.textfield.draw	
}
paper.heading = {
	properties: {
		defaults: $.extend(true, {}, paper.label.properties.defaults, {caption: 'Heading', size: '24px'}),
		show: function($e, onshow){
			paper.label.properties.show($e, function(){
				$('#props-title').text('Heading Properties');
				if(onshow) onshow();
			});
		}
	},
	draw: function(canvas, l, t, w, h, v){
		v.caption = '<strong>'+v.caption+'</strong>';
		paper.label.draw(canvas, l, t, w, h, v);
	}
}
paper.dropdown = {
	properties : {
		min: {
			width: 40,
			height: 20
		},
		defaults: {
			caption: 'Dropdown',
			type: 1,
			color: {'b': '#000000','bg': '#ffffff', 'fg': '#c3c3c3', 't': '#000000'},
			align: 'left',
			size: '12px',
			height: 25,
			width: 85,
			autosize: false,
			menu: 'caret-down'
		},
		show: function($e, onshow){
			paper.button.properties.show($e, function(){				
				props = paper._getDefaultedProperties($e.data('properties'), paper.dropdown.properties.defaults);
				$('#div-icons').remove();
				$('#props-title').text('Dropdown Properties');

				$('<button id="color-fg" class="btn btn-small"><i class="icon-stop"></i></button>').insertAfter('#color-bg');
				$('#color-fg').data('val', props.color.fg ? props.color.fg : paper.dropdown.properties.defaults.color.fg);
					$('#color-fg').css('color', props.color.fg ? props.color.fg : paper.dropdown.properties.defaults.color.fg);
				$('#color-fg').click(function(ev){
					paper._showColorPicker(ev, this, 'color.fg');
				});
				$('#color-fg').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Foreground', hideAfter: 2000});
				if(onshow) onshow();
			});
		}
	},
	draw: paper.button.draw
}
paper.checkbox = {
	properties : {
		min: {
			width: 35,
			height: 20
		},
		defaults: {
			caption: 'Checkbox',
			icon: 'check',
			type: 2,
			color: {'b': 'none','bg': 'none', 'fg': '#000000', 't': '#000000'},
			align: 'left',
			size: '12px',
			height: 25,
			width: 85,
			autosize: false,
			checked: false
		},
		show: function($e, onshow){
			paper.button.properties.show($e, function(){				
				props = paper._getDefaultedProperties($e.data('properties'), paper.checkbox.properties.defaults);
				$('#div-icons, #type-p, #dropdown, #dropup').remove();
				$('#props-title').text('Checkbox Properties');

				$('<button id="checked" class="btn btn-small '+(props.checked ? 'active' : '')+'">Checked</button>').insertAfter('#autosize');
				$('#checked').click(function(ev){
					if($(this).hasClass('active'))
						paper._changeProperty('checked', false);
					else
						paper._changeProperty('checked', true);
				});
				$('#div-type').remove();
				$('#autosize').width(80);
				$('#checked').width(80);

				$('#color-b').remove();
				$('#color-bg').remove();
				$('<button id="color-fg" class="btn btn-small"><i class="icon-stop"></i></button>').insertBefore('#color-t');
				$('#color-fg').data('val', props.color.fg ? props.color.fg : paper.dropdown.properties.defaults.color.fg);
				$('#color-fg').css('color', props.color.fg ? props.color.fg : paper.dropdown.properties.defaults.color.fg);
				$('#color-fg').click(function(ev){
					paper._showColorPicker(ev, this, 'color.fg');
				});
				$('#color-fg').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Foreground', hideAfter: 2000});
				if(onshow) onshow();
			});
		}
	},
	draw: function(canvas, l, t, w, h, v){
		v.border = v.type;		
		var $div = $(canvas.canvas.parentElement);
		if(v.linkto)
			$div.addClass('has-link');
		var r = v.type == 1 ? 5 : 0;
		var h1l = paper._getTextAutoSize('<p class="element-caption-para" style="margin: 0;vertical-align:middle;font-size:'+v.size+';text-align:'+v.align+';word-wrap:break-word;">&nbsp;</p>').height;
		if(v.autosize){
			var cp = '<p class="element-caption-para" style="margin: 0;vertical-align:middle;font-size:'+v.size+';text-align:'+v.align+';word-wrap:break-word;">'+v.caption.nl2br()+'</p>';
			var as = paper._getTextAutoSize(cp);
			h = as.height, w = as.width;
			h += 10, w += 10 + (v.stroke ? v.stroke : 0) + h1l + (v.icon == 'radio' ? 0 : 10);

			paper._resize($div,w,h);
		}
		if(v.icon == 'radio'){
			h1l -= 2;
			var r1 = h1l/2.5;
			canvas.circle(r1+6,r1+6,r1+2).attr({'stroke-width':'2px', 'stroke': v.color.fg});
			h1l += 2;
			if(v.checked)
				canvas.circle(6+r1,6+r1,r1/3+1).attr({'stroke-width':'2px', 'stroke': v.color.fg, 'fill': v.color.fg});
		}
		else
			v.caption = '<span style="font-size:'+(parseInt(v.size)+4)+'px;color:'+v.color.fg+'"><i class="'+(v.checked ? 'icon-check' : 'icon-check-empty')+'"></i></span>&nbsp;'+v.caption;
		v.color.bg = 'none';
		v.autosize = false;
		if(v.icon == 'radio'){
			paper.label.draw(canvas, l+h1l+2, t, w-l+h1l+2, h, v);
		}
		else{			
			paper.label.draw(canvas, l, t, w, h, v);
		}		
		paper._drawLink(v, $(canvas.canvas));
	}
}
paper.radiobutton = {
	properties: {
		min: {
			width: 35,
			height: 20
		},
		defaults: $.extend(true, {}, paper.checkbox.properties.defaults, {'caption': 'Radiobutton', 'icon': 'radio', 'width': 105}),
		show: function($e, onshow){
			paper.checkbox.properties.show($e, function(){
				$('#div-type').remove();
				$('#props-title').text('Radiobutton Properties');
				if(onshow) onshow();
			});
		}
	},
	draw: paper.checkbox.draw
}
paper.spinner = {
	properties: {
		min: {
			width: 40,
			height: 20
		},
		defaults: $.extend(true, {}, paper.dropdown.properties.defaults, {'caption': 'Spinner', 'menu': 'sort'}),
		show: function($e, onshow){
			paper.dropdown.properties.show($e, function(){				
				$('#dropdown, #dropup').remove();
				$('#autosize').css('width', '210px');
				$('#props-title').text('Spinner Properties');
				if(onshow) onshow();
			});
		}
	},
	draw: paper.dropdown.draw
}
paper.datepicker = {
	properties: {
		min: {
			width: 40,
			height: 20
		},
		defaults: $.extend(true, {}, paper.dropdown.properties.defaults, {'caption': '10/27/2012', 'menu': 'calendar', 'width': 105}),
		show: function($e, onshow){
			paper.spinner.properties.show($e, function(){
				$('#props-title').text('Date Picker Properties');
				if(onshow) onshow();
			});
		}
	},
	draw: paper.dropdown.draw
}
paper.slider = {
	properties : {
		min: {
			width: 30,
			height: 15
		},
		defaults: {
			slidertype: 3,
			selectortype: 3,
			color: {'b': '#000000','bg': '#ffffff', 'fg': '#ffffff'},
			value: 0.5,
			height: 25,
			width: 85,
			orient: 2
		},
		show: function($e, onshow){
			$('#props').remove();
			$('#props-title').remove();
			var props = paper._getDefaultedProperties($e.data('properties'), paper.slider.properties.defaults);
			var slidertype = '<div id="div-slidertype" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Slider</strong></label><div class="controls"><div class="btn-group" data-toggle="buttons-radio"><button id="slidertype-rr" class="btn btn-small '+(props.slidertype == 1 ? 'active' : '')+'">RR</button><button id="slidertype-r" class="btn btn-small '+(props.slidertype == 2 ? 'active' : '')+'">R</button><button id="slidertype-p" class="btn btn-small '+(props.slidertype == 3 ? 'active' : '')+'">P</button></div></div></div>';
			var selectortype = '<div id="div-selectortype" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Selector</strong></label><div class="controls"><div class="btn-group" data-toggle="buttons-radio"><button id="selectortype-rr" class="btn btn-small '+(props.selectortype == 1 ? 'active' : '')+'">RR</button><button id="selectortype-r" class="btn btn-small '+(props.selectortype == 2 ? 'active' : '')+'">R</button><button id="selectortype-p" class="btn btn-small '+(props.selectortype == 3 ? 'active' : '')+'">P</button></div></div></div>';
			var color = '<div id="div-color" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Colors</strong></label><div class="controls"><div id="color-options" class="btn-group" data-toggle="buttons-radio"><button id="color-b" class="btn btn-small"><i class="icon-stop"></i></button><button id="color-bg" class="btn btn-small"><i class="icon-stop"></i></button><button id="color-fg" class="btn btn-small"><i class="icon-stop"></i></button></div></div></div>';
			var value = '<div id="div-value" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Value</strong></label><div class="controls"><input class="input-small" id="input-value"style="width:50px" type="text" value="'+(props.value ? props.value : paper.slider.properties.defaults.value)+'" placeholder="'+(props.size ? props.size : paper.slider.properties.defaults.value)+'"></div></div>';
			var orientation = '<div id="div-orientation" class="form-inline"><div class="btn-group" data-toggle="buttons-radio"><button id="orientation-v" style="width:105px;" class="btn btn-small '+(props.orient == 1 ? 'active' : '')+'">Vertical</button><button id="orientation-h" style="width:105px;" class="btn btn-small '+(props.orient == 2 ? 'active' : '')+'">Horizontal</button></div></div>';
			var html = '<div id="props"><div id="propsFor" style="display:none">'+$e.attr('id')+'</div><div class="form-horizontal">'+slidertype+selectortype+color+value+'</div>'+orientation+'</div>';
			//ui.dismissPopovers();
			$e.popover({title:  '<strong style="color:#3a3a3a;" id="props-title">Slider Properties</strong>', content: html, placement: 'auto', attachTo: '#tbs-popover', onShow: function(){
				paper._buildDimensions($e, '#div-value');
				paper._buildOpacity($e, '#div-value',props);
				paper._buildLinkInspector($e, '#div-value',props);
				$('#input-value').spinner({min: 0, max: 1, places: 2, step: 0.01, largeStep: 0.1});
				$('#input-value').keydown(function(ev){
					if(ev.which == 13){
						paper._changeProperty('value', $(this).val(), false);
						ev.preventDefault();
					}
				});
				$('#input-value').blur(function(ev){
					paper._changeProperty('value', $(this).val());
				});
				$('#input-value').change(function(ev){
					paper._changeProperty('value', $(this).val(), false);
				});
				$('#slidertype-rr').click(function(ev){
					paper._changeProperty('slidertype', 1);
				});
				$('#slidertype-r').click(function(ev){
					paper._changeProperty('slidertype', 2);
				});
				$('#slidertype-p').click(function(ev){
					paper._changeProperty('slidertype', 3);
				});
				$('#selectortype-rr').click(function(ev){
					paper._changeProperty('selectortype', 1);
				});
				$('#selectortype-r').click(function(ev){
					paper._changeProperty('selectortype', 2);
				});
				$('#selectortype-p').click(function(ev){
					paper._changeProperty('selectortype', 3);
				});
				$('#orientation-v').click(function(ev){
					if(!$(this).hasClass('active'))
						paper._swapSize();
					paper._changeProperty('orient', 1);
				});
				$('#orientation-h').click(function(ev){
					if(!$(this).hasClass('active'))
						paper._swapSize();
					paper._changeProperty('orient', 2);
				});
				$('#color-b').data('val', props.color.b ? props.color.b : paper.slider.properties.defaults.color.b);
				$('#color-bg').data('val', props.color.bg ? props.color.bg : paper.slider.properties.defaults.color.bg);
				$('#color-fg').data('val', props.color.fg ? props.color.fg : paper.slider.properties.defaults.color.fg);
				$('#color-b').css('color', props.color.b ? props.color.b : paper.slider.properties.defaults.color.b);
				$('#color-bg').css('color', props.color.bg ? props.color.bg : paper.slider.properties.defaults.color.bg);
				$('#color-fg').css('color', props.color.fg ? props.color.fg : paper.slider.properties.defaults.color.fg);
				$('#color-b').click(function(ev){
					paper._showColorPicker(ev, this, 'color.b');
				});
				$('#color-bg').click(function(ev){
					paper._showColorPicker(ev, this, 'color.bg');
				});
				$('#color-fg').click(function(ev){
					paper._showColorPicker(ev, this, 'color.fg');
				});
				//Tooltips
				$('#slidertype-rr').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Rounded Rect', hideAfter: 2000});
				$('#slidertype-r').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Rectangular', hideAfter: 2000});
				$('#slidertype-p').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Pill', hideAfter: 2000});
				$('#selectortype-rr').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Rounded Rect', hideAfter: 2000});
				$('#selectortype-r').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Rectangular', hideAfter: 2000});
				$('#selectortype-p').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Circle', hideAfter: 2000});
				$('#color-b').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Border', hideAfter: 2000});
				$('#color-bg').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Slider', hideAfter: 2000});
				$('#color-fg').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Selector', hideAfter: 2000});
				if(onshow) onshow();
			}, onClick: function(ev){paper._popoverClick(ev,$e)}, onHide: function(){
				paper._destroyAllTooltips($e);
			}});
			$e.popover('show');
			ui.popoverContainers.push($e);
		}
	},
	draw: function(canvas, l, t, w, h, v){
		var $div = $(canvas.canvas.parentElement);
		if(v.linkto)
			$div.addClass('has-link');
		if(v.orient == 2){
			var h1 = h/5*2;
			var rc = [Math.floor(l+1),Math.floor(t+(h-h1)/2),Math.floor(w-2),Math.floor(h1)];
			canvas.rect(rc[0],rc[1],rc[2],rc[3], v.slidertype === 1 ? 5 : v.slidertype === 3 ? h1/2 : 0).attr({'stroke-width': '2px', 'stroke': v.color.b, 'fill': v.color.bg});
			var w1 = Math.min(w/10, 20);
			h1 = h -2;
			if(v.selectortype === 3)
				canvas.rect(l+(v.value * (w-h1-2))+1,t+(h-h1)/2,h1,h1, h/2).attr({'stroke-width': '2px', 'stroke': v.color.b, 'fill': v.color.fg});
			else
				canvas.rect(l+(v.value * (w-w1-2))+1,t+1,w1,h1, v.selectortype === 1 ? 5 : 0).attr({'stroke-width': '2px', 'stroke': v.color.b, 'fill': v.color.fg});
		}
		else{
			var w1 = w/5*2;
			var rc = [Math.floor(l+(w-w1)/2),Math.floor(t+1),Math.floor(w1),Math.floor(h-2)];
			canvas.rect(rc[0],rc[1],rc[2],rc[3],v.slidertype === 1 ? 5 : v.slidertype === 3 ? w1/2 : 0).attr({'stroke-width': '2px', 'stroke': v.color.b, 'fill': v.color.bg});
			var h1 = Math.min(h/10, 20);
			w1 = w -2;
			if(v.selectortype === 3)
				canvas.rect(l+(w-w1)/2,t+(v.value * (h-w1-2))+1,w1,w1, w/2).attr({'stroke-width': '2px', 'stroke': v.color.b, 'fill': v.color.fg});
			else
				canvas.rect(l+1,t+(v.value * (h-h1-2))+1,w1,h1, v.selectortype === 1 ? 5 : 0).attr({'stroke-width': '2px', 'stroke': v.color.b, 'fill': v.color.fg});
		}
		paper._drawLink(v, $div);
	}
}
paper.progressbar = {
	properties: {
		min: {
			width: 30,
			height: 10
		},
		defaults: {
			slidertype: 1,
			color: {'b': '#000000','bg': '#ffffff', 'fg': '#3a3a3a'},
			value: 0.5,
			height: 15,
			width: 85,
			orient: 2
		},
		show: function($e, onshow){
			paper.slider.properties.show($e, function(){				
				$('#props-title').text('Progressbar Properties');
				$('#div-selectortype').remove();
				if(onshow) onshow();
			});
		}
	},
	draw: function(canvas, l, t, w, h, v){
		var $div = $(canvas.canvas.parentElement);
		if(v.linkto)
			$div.addClass('has-link');
		if(v.orient == 2){
			var h1 = h-2;
			var r = v.slidertype === 1 ? 5 : v.slidertype === 3 ? h1/2 : 0;
			var rc = [Math.floor(l+1),Math.floor(t+(h-h1)/2),Math.floor(w-2),Math.floor(h1)];
			canvas.rect(rc[0],rc[1],rc[2],rc[3],r).attr({'stroke-width': '2px', 'stroke': v.color.b, 'fill': v.color.bg});
			if(v.value > 0){
				var w1 = v.value*(w -2);
				if(w1 < r)
					w1 = r;
				w1 = Math.floor(w1);				
				var xa = [Math.floor(l+1+r), Math.floor(l+1), Math.floor(w-r)];
				var ya = [Math.floor(t+1+(h-h1)/2), Math.floor(t+1+(h-h1)/2), Math.floor(t-1+(h+h1)/2)]
				var p = ['M',xa[0],ya[0],'L',l+Math.min(w1,xa[2]),ya[0],'l',0,h1-2,'L',xa[0],ya[2],'z'];
				paper.path(canvas, p).attr({'stroke-width': '2px', 'stroke': v.color.fg, 'fill': v.color.fg});
				if(v.slidertype == 1) canvas.rect(l+1,t+(h-h1)/2,r,h1-2,r).attr({'stroke-width': '2px', 'stroke': v.color.fg, 'fill': v.color.fg});
				else if(v.slidertype == 3) paper.path(canvas, paper._sector(l+1+r, t+h/2-1, r-1, 90, 270)).attr({'stroke-width': '2px', 'stroke': v.color.fg, 'fill': v.color.fg});
				if(w1 > xa[2]){
					if(v.slidertype == 1) canvas.rect(l-2+xa[2],t+(h-h1)/2,r,h1-2,r).attr({'stroke-width': '2px', 'stroke': v.color.fg, 'fill': v.color.fg});
					else if(v.slidertype == 3) paper.path(canvas, paper._sector(l-1+xa[2], t+h/2-1, r-1, 270, 90)).attr({'stroke-width': '2px', 'stroke': v.color.fg, 'fill': v.color.fg});
				}
			}
			canvas.rect(rc[0],rc[1],rc[2],rc[3],r).attr({'stroke-width': '2px', 'stroke': v.color.b});
		}
		else{
			var w1 = w-2;
			var r = v.slidertype === 1 ? 5 : v.slidertype === 3 ? w1/2 : 0;
			var rc = [Math.floor(l+(w-w1)/2), Math.floor(t+1),Math.floor(w1),Math.floor(h-2)];
			canvas.rect(rc[0],rc[1],rc[2],rc[3],r).attr({'stroke-width': '2px', 'stroke': v.color.b, 'fill': v.color.bg});
			if(v.value > 0){
				var h1 = v.value*(h -2);
				if(h1 < r)
					h1 = r;
				h1 = Math.floor(h1);
				var xa = [Math.floor(l+1+(w-w1)/2)];
				var ya = [Math.floor(t+1+r), Math.floor(h-r)]
				var p = ['M',xa[0],ya[0],'l',Math.floor(w1-1),0,'L',Math.floor(xa[0]+w1-1),Math.min(h1,ya[1]),'l',-w1+2,0,'z'];
				paper.path(canvas, p).attr({'stroke-width': '2px', 'stroke': v.color.fg, 'fill': v.color.fg});
				if(v.slidertype == 1) canvas.rect(l+1+(w-w1)/2,t+1,w1-2,r,r).attr({'stroke-width': '2px', 'stroke': v.color.fg, 'fill': v.color.fg});
				else if(v.slidertype == 3) paper.path(canvas, paper._sector(l+w/2, t+1+r, r-1, 0, 180)).attr({'stroke-width': '2px', 'stroke': v.color.fg, 'fill': v.color.fg});
				if(h1 > ya[1]){
					if(v.slidertype == 1) canvas.rect(l+1+(w-w1)/2,t+ya[1],w1-2,r/2,r).attr({'stroke-width': '2px', 'stroke': v.color.fg, 'fill': v.color.fg});
					else if(v.slidertype == 3) paper.path(canvas, paper._sector(l+w/2, t+ya[1]-1, r-1,180,0)).attr({'stroke-width': '2px', 'stroke': v.color.fg, 'fill': v.color.fg});
				}
			}
			canvas.rect(rc[0],rc[1],rc[2],rc[3],r).attr({'stroke-width': '2px', 'stroke': v.color.b});
		}
		paper._drawLink(v, $div);
	}
}
paper.scrollbar = {
	properties: {
		min: {
			width: 40,
			height: 15
		},
		defaults: {
			slidertype: 1,
			color: {'b': '#000000','bg': '#dbdbdb', 'fg': '#c3c3c3', 'a': '#3a3a3a'},
			height: 120,
			width: 15,
			orient: 1
		},
		show: function($e, onshow){
			paper.slider.properties.show($e, function(){
				props = paper._getDefaultedProperties($e.data('properties'), paper.scrollbar.properties.defaults);
				$('<button id="color-a" class="btn btn-small"><i class="icon-stop"></i></button>').insertAfter('#color-fg');
				$('#color-a').data('val', props.color.a ? props.color.a : paper.scrollbar.properties.defaults.color.a);
				$('#color-a').css('color', props.color.a ? props.color.a : paper.scrollbar.properties.defaults.color.a);
				$('#color-a').click(function(ev){
					paper._showColorPicker(ev, this, 'color.a');
				});
				$('#color-a').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Arrows', hideAfter: 2000});

				$('#props-title').text('Scrollbar Properties');
				$('#div-selectortype').remove();
				$('#div-value').remove();
				if(onshow) onshow();
			});
		}
	},
	draw: function(canvas, l, t, w, h, v){
		var $div = $(canvas.canvas.parentElement);
		if(v.linkto)
			$div.addClass('has-link');
		var ic = $.extend(true, {}, paper.icon.properties.defaults, {'border': null});
		ic.color.s = v.color.a;
		if(v.orient == 2){
			var h1 = h-2;
			var r = v.slidertype === 1 ? 5 : v.slidertype === 3 ? h1/2 : 0;
			var rc = [Math.floor(l+1),Math.floor(t+(h-h1)/2),Math.floor(w-2),Math.floor(h1)];
			canvas.rect(rc[0],rc[1],rc[2],rc[3],r).attr({'stroke-width': '2px', 'stroke': v.color.b, 'fill': v.color.bg});
			paper.icon.draw(canvas,l+(v.slidertype==3 ?1:0),t,h,h,$.extend(ic, {'icon':'caret-left'}));
			canvas.rect(rc[0]+h1,rc[1],rc[2]-2*h+4,rc[3]).attr({'stroke-width': '2px', 'stroke': v.color.b, 'fill': v.color.bg});
			var w1 = w/4;
			canvas.rect(l+(w-w1)/2,rc[1],w1,rc[3],r).attr({'stroke-width': '2px', 'stroke': v.color.b, 'fill': v.color.fg});
			paper.icon.draw(canvas,l+w-h-(v.slidertype==3 ?1:0),t,h,h,$.extend(ic, {'icon':'caret-right'}));
		}
		else{
			var w1 = w-2;
			var r = v.slidertype === 1 ? 5 : v.slidertype === 3 ? w1/2 : 0;
			var rc = [Math.floor(l+(w-w1)/2), Math.floor(t+1),Math.floor(w1),Math.floor(h-2)];
			canvas.rect(rc[0],rc[1],rc[2],rc[3],r).attr({'stroke-width': '2px', 'stroke': v.color.b, 'fill': v.color.bg});
			paper.icon.draw(canvas,l,t+(v.slidertype==3 ?1:0),w,w,$.extend(ic, {'icon':'caret-up'}));
			canvas.rect(rc[0],rc[1]+w1,rc[2],rc[3]-2*w+4).attr({'stroke-width': '2px', 'stroke': v.color.b, 'fill': v.color.bg});
			var h1 = h/4;
			canvas.rect(rc[0],rc[1]+(h-h1)/2,rc[2],h1,r).attr({'stroke-width': '2px', 'stroke': v.color.b, 'fill': v.color.fg});
			paper.icon.draw(canvas,l,t+h-w,w,w,$.extend(ic, {'icon':'caret-down'}));
		}
		paper._drawLink(v, $div);
	}
}
paper.calender = {
	properties: {
		defaults: {
			caption: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'][(new Date()).getMonth],			
			type: 1,
			color: {'b': '#000000', 'bg': '#ffffff', 'fg': '#c3c3c3', 't': '#000000'},
			width: 200,
			height: 180
		},
		show: function($e, onshow){
			$('#props').remove();
			var props = $e.data('properties');
			var color = '<div id="div-color" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Colors</strong></label><div class="controls"><div id="color-options" class="btn-group" data-toggle="buttons-radio"><button id="color-b" class="btn btn-small"><i class="icon-stop"></i></button><button id="color-bg" class="btn btn-small"><i class="icon-stop"></i></button><button id="color-fg" class="btn btn-small"><i class="icon-stop"></i></button><button id="color-t" class="btn btn-small"><i class="icon-stop"></i></button></div></div></div>';
			var html = '<div id="props"><div id="propsFor" style="display:none">'+$e.attr('id')+'</div><div class="form-horizontal">'+color+'</div></div>';
			//ui.dismissPopovers();
			$e.popover({title:  '<strong style="color:#3a3a3a;"  id="props-title">Calender Properties</strong>', content: html, placement: 'auto', attachTo: '#tbs-popover', onShow: function(){
				paper._buildLinkInspector($e, '#div-color',props);
				paper._buildDimensions($e, '#div-color');
				paper._buildOpacity($e, '#div-color',props);
				$('#color-b').data('val', props.color.b ? props.color.b : paper.calender.properties.defaults.color.b);
				$('#color-bg').data('val', props.color.bg ? props.color.bg : paper.calender.properties.defaults.color.bg);
				$('#color-fg').data('val', props.color.fg ? props.color.fg : paper.calender.properties.defaults.color.fg);
				$('#color-t').data('val', props.color.t ? props.color.t : paper.calender.properties.defaults.color.t);
				$('#color-b').css('color', props.color.b ? props.color.b : paper.calender.properties.defaults.color.b);
				$('#color-bg').css('color', props.color.bg ? props.color.bg : paper.calender.properties.defaults.color.bg);
				$('#color-fg').css('color', props.color.fg ? props.color.fg : paper.calender.properties.defaults.color.fg);
				$('#color-t').css('color', props.color.t ? props.color.t : paper.calender.properties.defaults.color.t);
				$('#color-b').click(function(ev){
					paper._showColorPicker(ev, this, 'color.b');
				});
				$('#color-bg').click(function(ev){
					paper._showColorPicker(ev, this, 'color.bg');
				});
				$('#color-fg').click(function(ev){
					paper._showColorPicker(ev, this, 'color.fg');
				});
				$('#color-t').click(function(ev){
					paper._showColorPicker(ev, this, 'color.t');
				});
				//Tooltips
				$('#color-b').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Border', hideAfter: 2000});
				$('#color-bg').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Background', hideAfter: 2000});
				$('#color-fg').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Foreground', hideAfter: 2000});
				$('#color-t').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Text', hideAfter: 2000});
				if(onshow) onshow();
			}, onClick: function(ev){paper._popoverClick(ev,$e)}, onHide: function(){
				paper._destroyAllTooltips($e);
			}});
			$e.popover('show');
			ui.popoverContainers.push($e);
		}
	},
	draw: function(canvas, l, t, w, h, v){
		var r = v.type == 1 ? 5 : 0;
		$div = $(canvas.canvas.parentElement);
		if(v.linkto)
			$div.addClass('has-link');
		$div.html('');
		$('<svg  xmlns:svg="http://www.w3.org/2000/svg"  xmlns="http://www.w3.org/2000/svg"  style="overflow: hidden; position: relative; "  height="'+h+'"  version="1.1"  width="'+w+'"  >  <desc  style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); "  id="desc4">Created with Raphaël 2.1.0</desc>  <defs  style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); "  id="defs6" />  <g  transform="scale('+w/paper.calender.properties.defaults.width+','+h/paper.calender.properties.defaults.height+')">  <rect  style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); "  x="1"  y="1"  width="198"  height="178"  r="5"  rx="5"  ry="5"  id="rect14"  stroke-width="2px"  stroke="'+v.color.b+'"  fill="'+v.color.bg+'" />  <rect  style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); "  x="2"  y="3"  width="196"  height="5"  r="5"  rx="5"  ry="5"  id="rect8"  stroke-width="2px"  stroke="'+v.color.fg+'"  fill="'+v.color.fg+'" />  <rect  style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); "  x="1"  y="7"  width="198"  height="35"  r="0"  rx="0"  ry="0"  id="rect10"  stroke-width="2px"  stroke="'+v.color.fg+'"  fill="'+v.color.fg+'" />  <path  style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); "  d="M1,42L199,42"  id="path12"  stroke-width="2px"  stroke="'+v.color.b+'"  fill="none" />  <path  style="fill:'+v.color.b+';stroke:'+v.color.b+'"  d="m 6.3416448,21.477545 12.0000002,-7.5 0,15 z"  id="path2994" />  <path  style="fill:'+v.color.b+';stroke:'+v.color.b+'"  d="m 179.2317,13.696996 12,7.5 -12,7.5 z"  id="path2996" />  <rect  style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); "  x="1"  y="1"  width="198"  height="178"  r="5"  rx="5"  ry="5"  id="rect14"  stroke-width="2px"  stroke="'+v.color.b+'"  fill="none" />  <g  style="font-size:16px;font-style:normal;font-weight:normal;line-height:125%;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;font-family:Sans"  id="text3062">  <path  d="m 50.408608,86.609016 -1.40625,0 0,-8.960937 c -0.338545,0.322925 -0.782555,0.645842 -1.332031,0.96875 -0.549482,0.322924 -1.042971,0.565112 -1.480469,0.726562 l 0,-1.359375 c 0.786456,-0.369782 1.473956,-0.817698 2.0625,-1.34375 0.588538,-0.526031 1.005204,-1.036447 1.25,-1.53125 l 0.90625,0 z"  style="fill:'+v.color.t+'"  id="path3017" />  <path  d="m 79.158608,85.257454 0,1.351562 -7.570312,0 c -0.01042,-0.338541 0.04427,-0.664061 0.164062,-0.976562 0.192708,-0.515624 0.501301,-1.023436 0.925782,-1.523438 0.424477,-0.499997 1.037757,-1.078121 1.839843,-1.734375 1.244787,-1.020828 2.085932,-1.829421 2.523438,-2.425781 0.437494,-0.596347 0.656243,-1.160148 0.65625,-1.691406 -7e-6,-0.557283 -0.199225,-1.027334 -0.597656,-1.410156 -0.398443,-0.382803 -0.917974,-0.574209 -1.558594,-0.574219 -0.677087,1e-5 -1.218753,0.203135 -1.625,0.609375 -0.406252,0.406259 -0.611981,0.968759 -0.617188,1.6875 l -1.445312,-0.148438 c 0.09896,-1.078115 0.471353,-1.899729 1.117187,-2.464843 0.645831,-0.565093 1.513018,-0.847645 2.601563,-0.847657 1.098953,1.2e-5 1.968743,0.304699 2.609375,0.914063 0.640617,0.609385 0.960929,1.364592 0.960937,2.265625 -8e-6,0.458341 -0.09376,0.908861 -0.28125,1.351562 -0.187507,0.442715 -0.498705,0.908861 -0.933593,1.398438 -0.434903,0.489588 -1.157558,1.161463 -2.167969,2.015625 -0.843754,0.708336 -1.38542,1.188804 -1.625,1.441406 -0.239586,0.252606 -0.437503,0.506512 -0.59375,0.761719 z"  style="fill:'+v.color.t+'"  id="path3019" />  <path  d="m 98.432046,83.585579 1.40625,-0.1875 c 0.161456,0.796877 0.436194,1.371096 0.824214,1.722656 0.38802,0.351564 0.86068,0.527345 1.41797,0.527344 0.66146,10e-7 1.22005,-0.229166 1.67578,-0.6875 0.45573,-0.458331 0.68359,-1.026039 0.6836,-1.703125 -10e-6,-0.645829 -0.21095,-1.178381 -0.63281,-1.597656 -0.42188,-0.419266 -0.95834,-0.628901 -1.60938,-0.628907 -0.26563,6e-6 -0.59636,0.05209 -0.99219,0.15625 l 0.15625,-1.234375 c 0.0937,0.01042 0.16927,0.01563 0.22657,0.01563 0.59895,7e-6 1.13801,-0.156243 1.61718,-0.46875 0.47916,-0.312492 0.71875,-0.794263 0.71875,-1.445312 0,-0.515616 -0.17448,-0.942699 -0.52343,-1.28125 -0.34897,-0.338532 -0.79949,-0.507802 -1.35157,-0.507813 -0.54688,1.1e-5 -1.0026,0.171886 -1.36718,0.515625 -0.36459,0.34376 -0.59896,0.859384 -0.703129,1.546875 l -1.40625,-0.25 c 0.171874,-0.942698 0.562499,-1.673166 1.171875,-2.191406 0.609374,-0.518218 1.367184,-0.777332 2.273434,-0.777344 0.625,1.2e-5 1.20052,0.134126 1.72657,0.402344 0.52603,0.26824 0.92837,0.634125 1.20703,1.097656 0.27864,0.463552 0.41796,0.955739 0.41797,1.476563 -1e-5,0.4948 -0.13282,0.94532 -0.39844,1.351562 -0.26563,0.406257 -0.65886,0.729174 -1.17969,0.96875 0.67708,0.156256 1.20312,0.480475 1.57813,0.972657 0.37499,0.492192 0.56249,1.108077 0.5625,1.847656 -1e-5,1.000002 -0.3646,1.847658 -1.09375,2.542969 -0.72918,0.695312 -1.65105,1.042968 -2.76563,1.042968 -1.00521,0 -1.83985,-0.299479 -2.503905,-0.898437 -0.664064,-0.598957 -1.04297,-1.374998 -1.136719,-2.328125 z"  style="fill:'+v.color.t+'"  id="path3021" />  <path  d="m 129.5883,86.609016 0,-2.742187 -4.96875,0 0,-1.289063 5.22656,-7.421875 1.14844,0 0,7.421875 1.54687,0 0,1.289063 -1.54687,0 0,2.742187 z m 0,-4.03125 0,-5.164062 -3.58594,5.164062 z"  style="fill:'+v.color.t+'"  id="path3023" />  <path  d="m 151.73673,83.609016 1.47657,-0.125 c 0.10937,0.718753 0.36327,1.259117 0.76171,1.621094 0.39844,0.36198 0.87891,0.54297 1.44141,0.542969 0.67708,10e-7 1.25,-0.255207 1.71875,-0.765625 0.46874,-0.510415 0.70312,-1.187497 0.70313,-2.03125 -1e-5,-0.802079 -0.22527,-1.434891 -0.67579,-1.898438 -0.45052,-0.463535 -1.04036,-0.695306 -1.76953,-0.695312 -0.45313,6e-6 -0.86198,0.102871 -1.22656,0.308594 -0.36459,0.205735 -0.65104,0.472661 -0.85937,0.800781 l -1.32032,-0.171875 1.10938,-5.882813 5.69531,0 0,1.34375 -4.57031,0 -0.61719,3.078125 c 0.6875,-0.479159 1.40885,-0.718742 2.16406,-0.71875 1,8e-6 1.84375,0.346362 2.53125,1.039063 0.6875,0.692714 1.03125,1.583338 1.03125,2.671875 0,1.036461 -0.30209,1.932294 -0.90625,2.6875 -0.73438,0.927084 -1.73698,1.390625 -3.00781,1.390625 -1.04167,0 -1.89193,-0.291667 -2.55078,-0.875 -0.65886,-0.583332 -1.03516,-1.356769 -1.12891,-2.320313 z"  style="fill:'+v.color.t+'"  id="path3025" />  <path  d="m 185.68986,77.960579 -1.39844,0.109375 c -0.12501,-0.552074 -0.30209,-0.953116 -0.53125,-1.203125 -0.38021,-0.401032 -0.84896,-0.601552 -1.40625,-0.601563 -0.44792,1.1e-5 -0.84115,0.125011 -1.17969,0.375 -0.44271,0.322927 -0.79167,0.79428 -1.04687,1.414063 -0.25521,0.6198 -0.38802,1.502611 -0.39844,2.648437 0.33854,-0.515618 0.7526,-0.89843 1.24219,-1.148437 0.48958,-0.249993 1.0026,-0.374993 1.53906,-0.375 0.9375,7e-6 1.73567,0.345059 2.39453,1.035156 0.65885,0.69011 0.98828,1.582036 0.98828,2.675781 0,0.718753 -0.15495,1.386722 -0.46484,2.003907 -0.3099,0.617188 -0.73568,1.089844 -1.27734,1.417968 -0.54168,0.328125 -1.15626,0.492188 -1.84375,0.492188 -1.17188,0 -2.12761,-0.430989 -2.86719,-1.292969 -0.73959,-0.861977 -1.10938,-2.282549 -1.10938,-4.261719 0,-2.213534 0.40886,-3.822907 1.22657,-4.828125 0.71354,-0.874989 1.67447,-1.312488 2.88281,-1.3125 0.90103,1.2e-5 1.63932,0.252616 2.21484,0.757813 0.57552,0.505218 0.92057,1.203134 1.03516,2.09375 z m -5.74219,4.9375 c 0,0.484378 0.10286,0.947919 0.30859,1.390625 0.20573,0.44271 0.49349,0.779949 0.86329,1.011719 0.36978,0.231772 0.7578,0.347657 1.16406,0.347656 0.59374,10e-7 1.10416,-0.239582 1.53125,-0.71875 0.42708,-0.479165 0.64062,-1.130206 0.64062,-1.953125 0,-0.791662 -0.21094,-1.41536 -0.63281,-1.871094 -0.42188,-0.455723 -0.95313,-0.683587 -1.59375,-0.683594 -0.63542,7e-6 -1.17448,0.227871 -1.61719,0.683594 -0.44271,0.455734 -0.66406,1.05339 -0.66406,1.792969 z"  style="fill:'+v.color.t+'"  id="path3027" />  <path  d="m 18.580483,96.655891 0,-1.351562 7.414063,0 0,1.09375 c -0.729174,0.776051 -1.45183,1.8073 -2.167969,3.09375 -0.716151,1.286461 -1.269536,2.609381 -1.660156,3.968751 -0.281254,0.95833 -0.460941,2.00781 -0.539063,3.14844 l -1.445312,0 c 0.01562,-0.90104 0.192706,-1.98958 0.53125,-3.26563 0.338538,-1.27604 0.824215,-2.5065 1.457031,-3.691405 0.632808,-1.184888 1.305984,-2.183584 2.019531,-2.996094 z"  style="fill:'+v.color.t+'"  id="path3029" />  <path  d="m 47.307046,100.39808 c -0.583336,-0.21354 -1.015627,-0.518224 -1.296875,-0.914064 -0.281251,-0.395825 -0.421876,-0.869783 -0.421875,-1.421875 -10e-7,-0.833324 0.299478,-1.533844 0.898437,-2.101562 0.598956,-0.567697 1.39583,-0.851551 2.390625,-0.851563 0.999995,1.2e-5 1.804682,0.290376 2.414063,0.871094 0.609368,0.580739 0.914055,1.28777 0.914062,2.121094 -7e-6,0.531258 -0.13933,0.993497 -0.417968,1.386719 -0.278653,0.393236 -0.70183,0.696617 -1.269532,0.910157 0.703119,0.22917 1.238274,0.59896 1.605469,1.10937 0.36718,0.51043 0.550773,1.1198 0.550781,1.82813 -8e-6,0.97917 -0.346362,1.80208 -1.039062,2.46875 -0.692715,0.66667 -1.604172,1 -2.734375,1 -1.130212,0 -2.041669,-0.33464 -2.734375,-1.00391 -0.692709,-0.66927 -1.039063,-1.5039 -1.039063,-2.5039 0,-0.74479 0.188802,-1.36849 0.566407,-1.8711 0.377602,-0.5026 0.915362,-0.84504 1.613281,-1.02734 z m -0.28125,-2.382814 c -3e-6,0.541675 0.174476,0.984383 0.523437,1.328125 0.348955,0.343757 0.80208,0.515632 1.359375,0.515625 0.541662,7e-6 0.985672,-0.170566 1.332032,-0.511718 0.346348,-0.341139 0.519525,-0.759107 0.519531,-1.253907 -6e-6,-0.515616 -0.178392,-0.949209 -0.535156,-1.300781 -0.356777,-0.351552 -0.800787,-0.527333 -1.332032,-0.527344 -0.536462,1.1e-5 -0.981774,0.171886 -1.335937,0.515625 -0.354169,0.34376 -0.531253,0.755218 -0.53125,1.234375 z m -0.453125,5.289064 c -2e-6,0.40104 0.09505,0.78906 0.285156,1.16406 0.190102,0.375 0.472654,0.66537 0.847656,0.8711 0.374997,0.20573 0.778642,0.30859 1.210938,0.30859 0.67187,0 1.226557,-0.21615 1.664062,-0.64844 0.437494,-0.43229 0.656244,-0.98177 0.65625,-1.64844 -6e-6,-0.67708 -0.225267,-1.23697 -0.675781,-1.67968 -0.450526,-0.44271 -1.014328,-0.66406 -1.691406,-0.66407 -0.661462,10e-6 -1.209639,0.21876 -1.644531,0.65625 -0.434898,0.43751 -0.652346,0.98438 -0.652344,1.64063 z"  style="fill:'+v.color.t+'"  id="path3031" />  <path  d="m 72.010171,103.96058 1.351562,-0.125 c 0.114581,0.63542 0.333331,1.09635 0.65625,1.38281 0.322914,0.28646 0.736976,0.42969 1.242188,0.42969 0.432287,0 0.811193,-0.099 1.136719,-0.29688 0.325515,-0.19791 0.592442,-0.46223 0.800781,-0.79296 0.208327,-0.33073 0.382806,-0.77735 0.523437,-1.33985 0.140619,-0.56249 0.210931,-1.13541 0.210938,-1.71875 -7e-6,-0.0625 -0.0026,-0.15624 -0.0078,-0.28125 -0.281256,0.44792 -0.66537,0.8112 -1.152343,1.08985 -0.486985,0.27865 -1.014328,0.41797 -1.582032,0.41796 -0.947919,10e-6 -1.750002,-0.34374 -2.40625,-1.03125 -0.656251,-0.68749 -0.984375,-1.59374 -0.984375,-2.718746 0,-1.16145 0.342447,-2.096345 1.027344,-2.804688 0.684894,-0.708322 1.542966,-1.062488 2.574219,-1.0625 0.744787,1.2e-5 1.425775,0.200533 2.042969,0.601563 0.61718,0.401052 1.08593,0.972666 1.40625,1.714844 0.320304,0.742196 0.48046,1.816413 0.480468,3.222657 -8e-6,1.46355 -0.158862,2.62891 -0.476562,3.49609 -0.317716,0.86719 -0.790372,1.52735 -1.417969,1.98047 -0.62761,0.45313 -1.363286,0.67969 -2.207031,0.67969 -0.895837,0 -1.627607,-0.2487 -2.195313,-0.74609 -0.567709,-0.4974 -0.908855,-1.19662 -1.023437,-2.09766 z m 5.757812,-5.054689 c -6e-6,-0.807283 -0.21485,-1.447907 -0.644531,-1.921875 -0.429693,-0.473948 -0.946619,-0.710927 -1.550781,-0.710937 -0.625004,1e-5 -1.169274,0.255218 -1.632813,0.765625 -0.463544,0.510426 -0.695314,1.171883 -0.695312,1.984375 -2e-6,0.729173 0.22005,1.321621 0.660156,1.777341 0.440101,0.45574 0.983069,0.6836 1.628906,0.6836 0.651037,0 1.186193,-0.22786 1.605469,-0.6836 0.419265,-0.45572 0.6289,-1.08723 0.628906,-1.894529 z"  style="fill:'+v.color.t+'"  id="path3033" />  <path  d="m 94.877358,106.60902 -1.40625,0 0,-8.960941 c -0.338545,0.322925 -0.782555,0.645842 -1.332031,0.96875 -0.549482,0.322924 -1.042971,0.565112 -1.480469,0.726562 l 0,-1.359375 c 0.786456,-0.369782 1.473956,-0.817698 2.0625,-1.34375 0.588538,-0.526031 1.005204,-1.036447 1.25,-1.53125 l 0.90625,0 z"  style="fill:'+v.color.t+'"  id="path3035" />  <path  d="m 98.486733,100.96058 c 0,-1.354161 0.139323,-2.444003 0.417969,-3.269532 0.278645,-0.825511 0.692707,-1.46223 1.242188,-1.910157 0.54948,-0.447905 1.24088,-0.671863 2.07422,-0.671875 0.61458,1.2e-5 1.15364,0.12371 1.61719,0.371094 0.46353,0.247407 0.84634,0.604177 1.14843,1.070313 0.30208,0.466155 0.53906,1.033863 0.71094,1.703125 0.17187,0.669278 0.25781,1.571621 0.25781,2.707032 0,1.34375 -0.13803,2.42839 -0.41406,3.25391 -0.27605,0.82552 -0.68881,1.46354 -1.23828,1.91406 -0.54949,0.45052 -1.2435,0.67578 -2.08203,0.67578 -1.10417,0 -1.97136,-0.39583 -2.601564,-1.1875 -0.755209,-0.95312 -1.132813,-2.50521 -1.132813,-4.65625 z m 1.445313,0 c -2e-6,1.88021 0.220054,3.13151 0.660154,3.75391 0.4401,0.62239 0.98307,0.93359 1.62891,0.93359 0.64583,0 1.18879,-0.3125 1.6289,-0.9375 0.4401,-0.625 0.66015,-1.875 0.66016,-3.75 -1e-5,-1.88541 -0.22006,-3.138013 -0.66016,-3.757814 -0.44011,-0.619781 -0.98828,-0.929677 -1.64453,-0.929687 -0.64583,1e-5 -1.16146,0.273447 -1.54687,0.820312 -0.48438,0.697926 -0.726566,1.986987 -0.726564,3.867189 z"  style="fill:'+v.color.t+'"  id="path3037" />  <path  d="m 121.56486,106.60902 -1.40625,0 0,-8.960941 c -0.33855,0.322925 -0.78256,0.645842 -1.33203,0.96875 -0.54948,0.322924 -1.04297,0.565112 -1.48047,0.726562 l 0,-1.359375 c 0.78645,-0.369782 1.47395,-0.817698 2.0625,-1.34375 0.58854,-0.526031 1.0052,-1.036447 1.25,-1.53125 l 0.90625,0 z"  style="fill:'+v.color.t+'"  id="path3039" />  <path  d="m 130.47111,106.60902 -1.40625,0 0,-8.960941 c -0.33855,0.322925 -0.78256,0.645842 -1.33203,0.96875 -0.54948,0.322924 -1.04297,0.565112 -1.48047,0.726562 l 0,-1.359375 c 0.78645,-0.369782 1.47395,-0.817698 2.0625,-1.34375 0.58854,-0.526031 1.0052,-1.036447 1.25,-1.53125 l 0.90625,0 z"  style="fill:'+v.color.t+'"  id="path3041" />  <path  d="m 148.25236,106.60902 -1.40625,0 0,-8.960941 c -0.33855,0.322925 -0.78256,0.645842 -1.33203,0.96875 -0.54948,0.322924 -1.04297,0.565112 -1.48047,0.726562 l 0,-1.359375 c 0.78645,-0.369782 1.47395,-0.817698 2.0625,-1.34375 0.58854,-0.526031 1.0052,-1.036447 1.25,-1.53125 l 0.90625,0 z"  style="fill:'+v.color.t+'"  id="path3043" />  <path  d="m 159.25236,105.25745 0,1.35157 -7.57031,0 c -0.0104,-0.33854 0.0443,-0.66407 0.16406,-0.97657 0.19271,-0.51562 0.5013,-1.02343 0.92578,-1.52343 0.42448,-0.5 1.03776,-1.07813 1.83984,-1.73438 1.24479,-1.02083 2.08594,-1.82942 2.52344,-2.42578 0.43749,-0.596347 0.65624,-1.160148 0.65625,-1.691406 -10e-6,-0.557283 -0.19922,-1.027334 -0.59766,-1.410156 -0.39844,-0.382803 -0.91797,-0.574209 -1.55859,-0.574219 -0.67709,1e-5 -1.21875,0.203135 -1.625,0.609375 -0.40625,0.406259 -0.61198,0.968759 -0.61719,1.6875 l -1.44531,-0.148438 c 0.099,-1.078115 0.47135,-1.899729 1.11719,-2.464843 0.64583,-0.565093 1.51302,-0.847645 2.60156,-0.847657 1.09895,1.2e-5 1.96874,0.304699 2.60938,0.914063 0.64061,0.609385 0.96093,1.364592 0.96093,2.265625 0,0.458341 -0.0937,0.908861 -0.28125,1.351562 -0.1875,0.442714 -0.4987,0.908864 -0.93359,1.398434 -0.4349,0.48959 -1.15756,1.16147 -2.16797,2.01563 -0.84375,0.70834 -1.38542,1.1888 -1.625,1.44141 -0.23959,0.2526 -0.4375,0.50651 -0.59375,0.76171 z"  style="fill:'+v.color.t+'"  id="path3045" />  <path  d="m 174.93986,106.60902 -1.40625,0 0,-8.960941 c -0.33855,0.322925 -0.78256,0.645842 -1.33203,0.96875 -0.54948,0.322924 -1.04297,0.565112 -1.48047,0.726562 l 0,-1.359375 c 0.78645,-0.369782 1.47395,-0.817698 2.0625,-1.34375 0.58854,-0.526031 1.0052,-1.036447 1.25,-1.53125 l 0.90625,0 z"  style="fill:'+v.color.t+'"  id="path3047" />  <path  d="m 178.55705,103.58558 1.40625,-0.1875 c 0.16145,0.79688 0.43619,1.37109 0.82421,1.72266 0.38802,0.35156 0.86068,0.52734 1.41797,0.52734 0.66146,0 1.22005,-0.22917 1.67578,-0.6875 0.45573,-0.45833 0.68359,-1.02604 0.6836,-1.70313 -1e-5,-0.64583 -0.21095,-1.17838 -0.63281,-1.59765 -0.42188,-0.41927 -0.95834,-0.6289 -1.60938,-0.62891 -0.26563,1e-5 -0.59636,0.0521 -0.99219,0.15625 l 0.15625,-1.234374 c 0.0937,0.01042 0.16927,0.01563 0.22657,0.01563 0.59895,7e-6 1.13801,-0.156243 1.61718,-0.46875 0.47916,-0.312492 0.71875,-0.794263 0.71875,-1.445312 0,-0.515616 -0.17448,-0.942699 -0.52343,-1.28125 -0.34897,-0.338532 -0.79949,-0.507802 -1.35157,-0.507813 -0.54688,1.1e-5 -1.0026,0.171886 -1.36718,0.515625 -0.36459,0.34376 -0.59896,0.859384 -0.70313,1.546875 l -1.40625,-0.25 c 0.17187,-0.942698 0.5625,-1.673166 1.17188,-2.191406 0.60937,-0.518218 1.36718,-0.777332 2.27343,-0.777344 0.625,1.2e-5 1.20052,0.134126 1.72657,0.402344 0.52603,0.26824 0.92837,0.634125 1.20703,1.097656 0.27864,0.463552 0.41796,0.955739 0.41797,1.476563 -1e-5,0.4948 -0.13282,0.94532 -0.39844,1.351562 -0.26563,0.406257 -0.65886,0.729169 -1.17969,0.968749 0.67708,0.15626 1.20312,0.48048 1.57813,0.97266 0.37499,0.49219 0.56249,1.10807 0.5625,1.84765 -1e-5,1.00001 -0.3646,1.84766 -1.09375,2.54297 -0.72918,0.69532 -1.65105,1.04297 -2.76563,1.04297 -1.00521,0 -1.83985,-0.29948 -2.50391,-0.89844 -0.66406,-0.59895 -1.04296,-1.37499 -1.13671,-2.32812 z"  style="fill:'+v.color.t+'"  id="path3049" />  <path  d="m 14.908608,126.60902 -1.40625,0 0,-8.96094 c -0.338545,0.32292 -0.782555,0.64584 -1.332031,0.96875 -0.549482,0.32292 -1.042971,0.56511 -1.480469,0.72656 l 0,-1.35937 c 0.786456,-0.36979 1.473956,-0.8177 2.0625,-1.34375 0.588538,-0.52603 1.005204,-1.03645 1.25,-1.53125 l 0.90625,0 z"  style="fill:'+v.color.t+'"  id="path3051" />  <path  d="m 23.025796,126.60902 0,-2.74219 -4.96875,0 0,-1.28906 5.226562,-7.42188 1.148438,0 0,7.42188 1.546875,0 0,1.28906 -1.546875,0 0,2.74219 z m 0,-4.03125 0,-5.16407 -3.585938,5.16407 z"  style="fill:'+v.color.t+'"  id="path3053" />  <path  d="m 41.596108,126.60902 -1.40625,0 0,-8.96094 c -0.338545,0.32292 -0.782555,0.64584 -1.332031,0.96875 -0.549482,0.32292 -1.042971,0.56511 -1.480469,0.72656 l 0,-1.35937 c 0.786456,-0.36979 1.473956,-0.8177 2.0625,-1.34375 0.588538,-0.52603 1.005204,-1.03645 1.25,-1.53125 l 0.90625,0 z"  style="fill:'+v.color.t+'"  id="path3055" />  <path  d="m 45.205483,123.60902 1.476563,-0.125 c 0.109373,0.71875 0.363279,1.25911 0.761719,1.62109 0.398434,0.36198 0.878902,0.54297 1.441406,0.54297 0.677078,0 1.249994,-0.25521 1.71875,-0.76563 0.468743,-0.51041 0.703118,-1.18749 0.703125,-2.03125 -7e-6,-0.80207 -0.225267,-1.43489 -0.675781,-1.89843 -0.450527,-0.46354 -1.04037,-0.69531 -1.769532,-0.69532 -0.453128,10e-6 -0.861982,0.10287 -1.226562,0.3086 -0.364586,0.20573 -0.651044,0.47266 -0.859375,0.80078 l -1.320313,-0.17188 1.109375,-5.88281 5.695313,0 0,1.34375 -4.570313,0 -0.617187,3.07813 c 0.687497,-0.47916 1.40885,-0.71875 2.164062,-0.71875 0.999995,0 1.843744,0.34636 2.53125,1.03906 0.687493,0.69271 1.031242,1.58334 1.03125,2.67187 -8e-6,1.03647 -0.302091,1.9323 -0.90625,2.6875 -0.734381,0.92709 -1.736984,1.39063 -3.007812,1.39063 -1.04167,0 -1.89193,-0.29167 -2.550781,-0.875 -0.658856,-0.58333 -1.035157,-1.35677 -1.128907,-2.32031 z"  style="fill:'+v.color.t+'"  id="path3057" />  <path  d="m 68.283608,126.60902 -1.40625,0 0,-8.96094 c -0.338545,0.32292 -0.782555,0.64584 -1.332031,0.96875 -0.549482,0.32292 -1.042971,0.56511 -1.480469,0.72656 l 0,-1.35937 c 0.786456,-0.36979 1.473956,-0.8177 2.0625,-1.34375 0.588538,-0.52603 1.005204,-1.03645 1.25,-1.53125 l 0.90625,0 z"  style="fill:'+v.color.t+'"  id="path3059" />  <path  d="m 79.189858,117.96058 -1.398437,0.10937 c -0.125007,-0.55207 -0.30209,-0.95311 -0.53125,-1.20312 -0.380214,-0.40103 -0.848964,-0.60155 -1.40625,-0.60156 -0.447921,1e-5 -0.84115,0.12501 -1.179688,0.375 -0.442711,0.32292 -0.791669,0.79428 -1.046875,1.41406 -0.25521,0.6198 -0.388022,1.50261 -0.398437,2.64844 0.338539,-0.51562 0.752601,-0.89843 1.242187,-1.14844 0.48958,-0.24999 1.0026,-0.37499 1.539063,-0.375 0.937494,1e-5 1.735671,0.34506 2.394531,1.03516 0.658847,0.69011 0.988273,1.58203 0.988281,2.67578 -8e-6,0.71875 -0.154955,1.38672 -0.464843,2.0039 -0.309904,0.61719 -0.735684,1.08985 -1.277344,1.41797 -0.541673,0.32813 -1.156255,0.49219 -1.84375,0.49219 -1.171878,0 -2.127607,-0.43099 -2.867188,-1.29297 -0.739584,-0.86198 -1.109375,-2.28255 -1.109375,-4.26172 0,-2.21353 0.408854,-3.82291 1.226563,-4.82812 0.713539,-0.87499 1.674476,-1.31249 2.882812,-1.3125 0.901036,1e-5 1.639317,0.25261 2.214844,0.75781 0.575514,0.50522 0.920565,1.20313 1.035156,2.09375 z m -5.742187,4.9375 c -2e-6,0.48438 0.102862,0.94792 0.308594,1.39062 0.205726,0.44271 0.493486,0.77995 0.863281,1.01172 0.369788,0.23177 0.757808,0.34766 1.164062,0.34766 0.593745,0 1.104161,-0.23958 1.53125,-0.71875 0.427077,-0.47917 0.640619,-1.13021 0.640625,-1.95313 -6e-6,-0.79166 -0.210944,-1.41536 -0.632812,-1.87109 -0.421881,-0.45572 -0.95313,-0.68359 -1.59375,-0.68359 -0.635421,0 -1.174483,0.22787 -1.617188,0.68359 -0.44271,0.45573 -0.664064,1.05339 -0.664062,1.79297 z"  style="fill:'+v.color.t+'"  id="path3061" />  <path  d="m 94.971108,126.60902 -1.40625,0 0,-8.96094 c -0.338545,0.32292 -0.782555,0.64584 -1.332031,0.96875 -0.549482,0.32292 -1.042971,0.56511 -1.480469,0.72656 l 0,-1.35937 c 0.786456,-0.36979 1.473956,-0.8177 2.0625,-1.34375 0.588538,-0.52603 1.005204,-1.03645 1.25,-1.53125 l 0.90625,0 z"  style="fill:'+v.color.t+'"  id="path3063" />  <path  d="m 98.674233,116.65589 0,-1.35156 7.414067,0 0,1.09375 c -0.72918,0.77605 -1.45183,1.8073 -2.16797,3.09375 -0.71615,1.28646 -1.26954,2.60938 -1.66016,3.96875 -0.28125,0.95833 -0.46094,2.00781 -0.53906,3.14844 l -1.44531,0 c 0.0156,-0.90104 0.1927,-1.98958 0.53125,-3.26563 0.33853,-1.27604 0.82421,-2.5065 1.45703,-3.6914 0.6328,-1.18489 1.30598,-2.18359 2.01953,-2.9961 z"  style="fill:'+v.color.t+'"  id="path3065" />  <path  d="m 121.65861,126.60902 -1.40625,0 0,-8.96094 c -0.33855,0.32292 -0.78256,0.64584 -1.33203,0.96875 -0.54948,0.32292 -1.04297,0.56511 -1.48047,0.72656 l 0,-1.35937 c 0.78645,-0.36979 1.47395,-0.8177 2.0625,-1.34375 0.58854,-0.52603 1.0052,-1.03645 1.25,-1.53125 l 0.90625,0 z"  style="fill:'+v.color.t+'"  id="path3067" />  <path  d="m 127.43205,120.39808 c -0.58334,-0.21354 -1.01563,-0.51822 -1.29688,-0.91406 -0.28125,-0.39583 -0.42188,-0.86979 -0.42187,-1.42188 -1e-5,-0.83332 0.29947,-1.53384 0.89843,-2.10156 0.59896,-0.5677 1.39583,-0.85155 2.39063,-0.85156 0.99999,1e-5 1.80468,0.29037 2.41406,0.87109 0.60937,0.58074 0.91406,1.28777 0.91406,2.12109 0,0.53126 -0.13933,0.9935 -0.41797,1.38672 -0.27865,0.39324 -0.70182,0.69662 -1.26953,0.91016 0.70312,0.22917 1.23828,0.59896 1.60547,1.10937 0.36718,0.51043 0.55078,1.1198 0.55078,1.82813 0,0.97917 -0.34636,1.80208 -1.03906,2.46875 -0.69271,0.66667 -1.60417,1 -2.73437,1 -1.13022,0 -2.04167,-0.33464 -2.73438,-1.00391 -0.69271,-0.66927 -1.03906,-1.5039 -1.03906,-2.5039 0,-0.74479 0.1888,-1.36849 0.5664,-1.8711 0.37761,-0.5026 0.91537,-0.84504 1.61329,-1.02734 z m -0.28125,-2.38281 c -1e-5,0.54167 0.17447,0.98438 0.52343,1.32812 0.34896,0.34376 0.80208,0.51563 1.35938,0.51563 0.54166,0 0.98567,-0.17057 1.33203,-0.51172 0.34635,-0.34114 0.51952,-0.75911 0.51953,-1.25391 -10e-6,-0.51561 -0.17839,-0.94921 -0.53516,-1.30078 -0.35677,-0.35155 -0.80078,-0.52733 -1.33203,-0.52734 -0.53646,1e-5 -0.98177,0.17188 -1.33593,0.51562 -0.35417,0.34376 -0.53126,0.75522 -0.53125,1.23438 z m -0.45313,5.28906 c 0,0.40104 0.0951,0.78906 0.28516,1.16406 0.1901,0.375 0.47265,0.66537 0.84765,0.8711 0.375,0.20573 0.77865,0.30859 1.21094,0.30859 0.67187,0 1.22656,-0.21615 1.66406,-0.64844 0.4375,-0.43229 0.65625,-0.98177 0.65625,-1.64844 0,-0.67708 -0.22526,-1.23697 -0.67578,-1.67968 -0.45052,-0.44271 -1.01433,-0.66406 -1.6914,-0.66407 -0.66147,10e-6 -1.20964,0.21876 -1.64454,0.65625 -0.43489,0.43751 -0.65234,0.98438 -0.65234,1.64063 z"  style="fill:'+v.color.t+'"  id="path3069" />  <path  d="m 148.34611,126.60902 -1.40625,0 0,-8.96094 c -0.33855,0.32292 -0.78256,0.64584 -1.33203,0.96875 -0.54948,0.32292 -1.04297,0.56511 -1.48047,0.72656 l 0,-1.35937 c 0.78645,-0.36979 1.47395,-0.8177 2.0625,-1.34375 0.58854,-0.52603 1.0052,-1.03645 1.25,-1.53125 l 0.90625,0 z"  style="fill:'+v.color.t+'"  id="path3071" />  <path  d="m 152.16642,123.96058 1.35156,-0.125 c 0.11458,0.63542 0.33333,1.09635 0.65625,1.38281 0.32292,0.28646 0.73698,0.42969 1.24219,0.42969 0.43229,0 0.81119,-0.099 1.13672,-0.29688 0.32551,-0.19791 0.59244,-0.46223 0.80078,-0.79296 0.20833,-0.33073 0.38281,-0.77735 0.52344,-1.33985 0.14062,-0.56249 0.21093,-1.13541 0.21094,-1.71875 -1e-5,-0.0625 -0.003,-0.15624 -0.008,-0.28125 -0.28125,0.44792 -0.66537,0.8112 -1.15234,1.08985 -0.48698,0.27865 -1.01433,0.41797 -1.58203,0.41796 -0.94792,10e-6 -1.75,-0.34374 -2.40625,-1.03125 -0.65625,-0.68749 -0.98438,-1.59374 -0.98438,-2.71875 0,-1.16145 0.34245,-2.09634 1.02735,-2.80468 0.68489,-0.70833 1.54296,-1.06249 2.57422,-1.0625 0.74478,1e-5 1.42577,0.20053 2.04296,0.60156 0.61719,0.40105 1.08593,0.97267 1.40625,1.71484 0.32031,0.7422 0.48047,1.81642 0.48047,3.22266 0,1.46355 -0.15886,2.62891 -0.47656,3.49609 -0.31771,0.86719 -0.79037,1.52735 -1.41797,1.98047 -0.62761,0.45313 -1.36328,0.67969 -2.20703,0.67969 -0.89584,0 -1.62761,-0.2487 -2.19531,-0.74609 -0.56771,-0.4974 -0.90886,-1.19662 -1.02344,-2.09766 z m 5.75781,-5.05469 c 0,-0.80728 -0.21485,-1.44791 -0.64453,-1.92187 -0.42969,-0.47395 -0.94662,-0.71093 -1.55078,-0.71094 -0.625,1e-5 -1.16927,0.25522 -1.63281,0.76562 -0.46355,0.51043 -0.69532,1.17189 -0.69531,1.98438 -1e-5,0.72917 0.22005,1.32162 0.66015,1.77734 0.4401,0.45574 0.98307,0.6836 1.62891,0.6836 0.65104,0 1.18619,-0.22786 1.60547,-0.6836 0.41926,-0.45572 0.6289,-1.08723 0.6289,-1.89453 z"  style="fill:'+v.color.t+'"  id="path3073" />  <path  d="m 177.12736,125.25745 0,1.35157 -7.57031,0 c -0.0104,-0.33854 0.0443,-0.66407 0.16406,-0.97657 0.19271,-0.51562 0.5013,-1.02343 0.92578,-1.52343 0.42448,-0.5 1.03776,-1.07813 1.83984,-1.73438 1.24479,-1.02083 2.08594,-1.82942 2.52344,-2.42578 0.43749,-0.59635 0.65624,-1.16015 0.65625,-1.69141 -10e-6,-0.55728 -0.19922,-1.02733 -0.59766,-1.41015 -0.39844,-0.3828 -0.91797,-0.57421 -1.55859,-0.57422 -0.67709,1e-5 -1.21875,0.20313 -1.625,0.60937 -0.40625,0.40626 -0.61198,0.96876 -0.61719,1.6875 l -1.44531,-0.14843 c 0.099,-1.07812 0.47135,-1.89973 1.11719,-2.46485 0.64583,-0.56509 1.51302,-0.84764 2.60156,-0.84765 1.09895,1e-5 1.96874,0.3047 2.60938,0.91406 0.64061,0.60938 0.96093,1.36459 0.96093,2.26562 0,0.45835 -0.0937,0.90887 -0.28125,1.35157 -0.1875,0.44271 -0.4987,0.90886 -0.93359,1.39843 -0.4349,0.48959 -1.15756,1.16147 -2.16797,2.01563 -0.84375,0.70834 -1.38542,1.1888 -1.625,1.44141 -0.23959,0.2526 -0.4375,0.50651 -0.59375,0.76171 z"  style="fill:'+v.color.t+'"  id="path3075" />  <path  d="m 178.64298,120.96058 c 0,-1.35416 0.13933,-2.444 0.41797,-3.26953 0.27865,-0.82551 0.69271,-1.46223 1.24219,-1.91016 0.54948,-0.4479 1.24088,-0.67186 2.07422,-0.67187 0.61458,1e-5 1.15364,0.12371 1.61719,0.37109 0.46353,0.24741 0.84634,0.60418 1.14843,1.07031 0.30208,0.46616 0.53906,1.03387 0.71094,1.70313 0.17187,0.66928 0.25781,1.57162 0.25781,2.70703 0,1.34375 -0.13803,2.42839 -0.41406,3.25391 -0.27605,0.82552 -0.68881,1.46354 -1.23828,1.91406 -0.54949,0.45052 -1.2435,0.67578 -2.08203,0.67578 -1.10417,0 -1.97136,-0.39583 -2.60156,-1.1875 -0.75521,-0.95312 -1.13282,-2.50521 -1.13282,-4.65625 z m 1.44532,0 c -1e-5,1.88021 0.22005,3.13151 0.66015,3.75391 0.4401,0.62239 0.98307,0.93359 1.62891,0.93359 0.64583,0 1.18879,-0.3125 1.6289,-0.9375 0.4401,-0.625 0.66015,-1.875 0.66016,-3.75 -10e-6,-1.88541 -0.22006,-3.13801 -0.66016,-3.75781 -0.44011,-0.61979 -0.98828,-0.92968 -1.64453,-0.92969 -0.64583,1e-5 -1.16146,0.27345 -1.54687,0.82031 -0.48438,0.69793 -0.72657,1.98699 -0.72656,3.86719 z"  style="fill:'+v.color.t+'"  id="path3077" />  <path  d="m 17.002358,145.25745 0,1.35157 -7.5703121,0 c -0.010417,-0.33854 0.04427,-0.66407 0.1640625,-0.97657 0.1927075,-0.51562 0.5013006,-1.02343 0.9257816,-1.52343 0.424477,-0.5 1.037757,-1.07813 1.839843,-1.73438 1.244787,-1.02083 2.085932,-1.82942 2.523438,-2.42578 0.437494,-0.59635 0.656243,-1.16015 0.65625,-1.69141 -7e-6,-0.55728 -0.199225,-1.02733 -0.597656,-1.41015 -0.398443,-0.3828 -0.917974,-0.57421 -1.558594,-0.57422 -0.677087,1e-5 -1.218753,0.20313 -1.625,0.60937 -0.406252,0.40626 -0.611981,0.96876 -0.617188,1.6875 l -1.4453121,-0.14843 c 0.098957,-1.07812 0.4713531,-1.89973 1.1171871,-2.46485 0.645831,-0.56509 1.513018,-0.84764 2.601563,-0.84765 1.098953,1e-5 1.968743,0.3047 2.609375,0.91406 0.640617,0.60938 0.960929,1.36459 0.960937,2.26562 -8e-6,0.45835 -0.09376,0.90887 -0.28125,1.35157 -0.187507,0.44271 -0.498705,0.90886 -0.933593,1.39843 -0.434903,0.48959 -1.157558,1.16147 -2.167969,2.01563 -0.843754,0.70834 -1.38542,1.1888 -1.625,1.44141 -0.239586,0.2526 -0.437503,0.50651 -0.59375,0.76171 z"  style="fill:'+v.color.t+'"  id="path3079" />  <path  d="m 23.814858,146.60902 -1.40625,0 0,-8.96094 c -0.338545,0.32292 -0.782555,0.64584 -1.332031,0.96875 -0.549482,0.32292 -1.042971,0.56511 -1.480469,0.72656 l 0,-1.35937 c 0.786456,-0.36979 1.473956,-0.8177 2.0625,-1.34375 0.588538,-0.52603 1.005204,-1.03645 1.25,-1.53125 l 0.90625,0 z"  style="fill:'+v.color.t+'"  id="path3081" />  <path  d="m 43.689858,145.25745 0,1.35157 -7.570312,0 c -0.01042,-0.33854 0.04427,-0.66407 0.164062,-0.97657 0.192708,-0.51562 0.501301,-1.02343 0.925782,-1.52343 0.424477,-0.5 1.037757,-1.07813 1.839843,-1.73438 1.244787,-1.02083 2.085932,-1.82942 2.523438,-2.42578 0.437494,-0.59635 0.656243,-1.16015 0.65625,-1.69141 -7e-6,-0.55728 -0.199225,-1.02733 -0.597656,-1.41015 -0.398443,-0.3828 -0.917974,-0.57421 -1.558594,-0.57422 -0.677087,1e-5 -1.218753,0.20313 -1.625,0.60937 -0.406252,0.40626 -0.611981,0.96876 -0.617188,1.6875 l -1.445312,-0.14843 c 0.09896,-1.07812 0.471353,-1.89973 1.117187,-2.46485 0.645831,-0.56509 1.513018,-0.84764 2.601563,-0.84765 1.098953,1e-5 1.968743,0.3047 2.609375,0.91406 0.640617,0.60938 0.960929,1.36459 0.960937,2.26562 -8e-6,0.45835 -0.09376,0.90887 -0.28125,1.35157 -0.187507,0.44271 -0.498705,0.90886 -0.933593,1.39843 -0.434903,0.48959 -1.157558,1.16147 -2.167969,2.01563 -0.843754,0.70834 -1.38542,1.1888 -1.625,1.44141 -0.239586,0.2526 -0.437503,0.50651 -0.59375,0.76171 z"  style="fill:'+v.color.t+'"  id="path3083" />  <path  d="m 52.596108,145.25745 0,1.35157 -7.570312,0 c -0.01042,-0.33854 0.04427,-0.66407 0.164062,-0.97657 0.192708,-0.51562 0.501301,-1.02343 0.925782,-1.52343 0.424477,-0.5 1.037757,-1.07813 1.839843,-1.73438 1.244787,-1.02083 2.085932,-1.82942 2.523438,-2.42578 0.437494,-0.59635 0.656243,-1.16015 0.65625,-1.69141 -7e-6,-0.55728 -0.199225,-1.02733 -0.597656,-1.41015 -0.398443,-0.3828 -0.917974,-0.57421 -1.558594,-0.57422 -0.677087,1e-5 -1.218753,0.20313 -1.625,0.60937 -0.406252,0.40626 -0.611981,0.96876 -0.617188,1.6875 l -1.445312,-0.14843 c 0.09896,-1.07812 0.471353,-1.89973 1.117187,-2.46485 0.645831,-0.56509 1.513018,-0.84764 2.601563,-0.84765 1.098953,1e-5 1.968743,0.3047 2.609375,0.91406 0.640617,0.60938 0.960929,1.36459 0.960937,2.26562 -8e-6,0.45835 -0.09376,0.90887 -0.28125,1.35157 -0.187507,0.44271 -0.498705,0.90886 -0.933593,1.39843 -0.434903,0.48959 -1.157558,1.16147 -2.167969,2.01563 -0.843754,0.70834 -1.38542,1.1888 -1.625,1.44141 -0.239586,0.2526 -0.437503,0.50651 -0.59375,0.76171 z"  style="fill:'+v.color.t+'"  id="path3085" />  <path  d="m 70.377358,145.25745 0,1.35157 -7.570312,0 c -0.01042,-0.33854 0.04427,-0.66407 0.164062,-0.97657 0.192708,-0.51562 0.501301,-1.02343 0.925782,-1.52343 0.424477,-0.5 1.037757,-1.07813 1.839843,-1.73438 1.244787,-1.02083 2.085932,-1.82942 2.523438,-2.42578 0.437494,-0.59635 0.656243,-1.16015 0.65625,-1.69141 -7e-6,-0.55728 -0.199225,-1.02733 -0.597656,-1.41015 -0.398443,-0.3828 -0.917974,-0.57421 -1.558594,-0.57422 -0.677087,1e-5 -1.218753,0.20313 -1.625,0.60937 -0.406252,0.40626 -0.611981,0.96876 -0.617188,1.6875 l -1.445312,-0.14843 c 0.09896,-1.07812 0.471353,-1.89973 1.117187,-2.46485 0.645831,-0.56509 1.513018,-0.84764 2.601563,-0.84765 1.098953,1e-5 1.968743,0.3047 2.609375,0.91406 0.640617,0.60938 0.960929,1.36459 0.960937,2.26562 -8e-6,0.45835 -0.09376,0.90887 -0.28125,1.35157 -0.187507,0.44271 -0.498705,0.90886 -0.933593,1.39843 -0.434903,0.48959 -1.157558,1.16147 -2.167969,2.01563 -0.843754,0.70834 -1.38542,1.1888 -1.625,1.44141 -0.239586,0.2526 -0.437503,0.50651 -0.59375,0.76171 z"  style="fill:'+v.color.t+'"  id="path3087" />  <path  d="m 71.900796,143.58558 1.40625,-0.1875 c 0.161456,0.79688 0.436195,1.37109 0.824219,1.72266 0.388017,0.35156 0.860673,0.52734 1.417968,0.52734 0.661454,0 1.220047,-0.22917 1.675782,-0.6875 0.455722,-0.45833 0.683587,-1.02604 0.683593,-1.70313 -6e-6,-0.64583 -0.210944,-1.17838 -0.632812,-1.59765 -0.421881,-0.41927 -0.958338,-0.6289 -1.609375,-0.62891 -0.265629,1e-5 -0.596358,0.0521 -0.992188,0.15625 l 0.15625,-1.23437 c 0.09375,0.0104 0.169267,0.0156 0.226563,0.0156 0.598954,1e-5 1.138016,-0.15624 1.617187,-0.46875 0.479161,-0.31249 0.718744,-0.79426 0.71875,-1.44531 -6e-6,-0.51562 -0.174485,-0.9427 -0.523437,-1.28125 -0.348964,-0.33853 -0.799484,-0.5078 -1.351563,-0.50781 -0.546878,1e-5 -1.002607,0.17188 -1.367187,0.51562 -0.364586,0.34376 -0.598961,0.85939 -0.703125,1.54688 l -1.40625,-0.25 c 0.171874,-0.9427 0.562499,-1.67317 1.171875,-2.19141 0.609372,-0.51822 1.367184,-0.77733 2.273437,-0.77734 0.624996,1e-5 1.200516,0.13412 1.726563,0.40234 0.526035,0.26824 0.928378,0.63413 1.207031,1.09766 0.278639,0.46355 0.417961,0.95573 0.417969,1.47656 -8e-6,0.4948 -0.13282,0.94532 -0.398438,1.35156 -0.265632,0.40626 -0.65886,0.72917 -1.179687,0.96875 0.677077,0.15626 1.203118,0.48048 1.578125,0.97266 0.374992,0.49219 0.562492,1.10807 0.5625,1.84765 -8e-6,1.00001 -0.364591,1.84766 -1.09375,2.54297 -0.729173,0.69532 -1.651047,1.04297 -2.765625,1.04297 -1.005212,0 -1.839846,-0.29948 -2.503906,-0.89844 -0.664064,-0.59895 -1.04297,-1.37499 -1.136719,-2.32812 z"  style="fill:'+v.color.t+'"  id="path3089" />  <path  d="m 97.064858,145.25745 0,1.35157 -7.570312,0 c -0.01042,-0.33854 0.04427,-0.66407 0.164062,-0.97657 0.192708,-0.51562 0.501301,-1.02343 0.925782,-1.52343 0.424477,-0.5 1.037757,-1.07813 1.839843,-1.73438 1.244787,-1.02083 2.085932,-1.82942 2.523438,-2.42578 0.437494,-0.59635 0.656243,-1.16015 0.65625,-1.69141 -7e-6,-0.55728 -0.199225,-1.02733 -0.597656,-1.41015 -0.398443,-0.3828 -0.917974,-0.57421 -1.558594,-0.57422 -0.677087,1e-5 -1.218753,0.20313 -1.625,0.60937 -0.406252,0.40626 -0.611981,0.96876 -0.617188,1.6875 l -1.445312,-0.14843 c 0.09896,-1.07812 0.471353,-1.89973 1.117187,-2.46485 0.645831,-0.56509 1.513018,-0.84764 2.601563,-0.84765 1.098953,1e-5 1.968743,0.3047 2.609375,0.91406 0.640617,0.60938 0.960929,1.36459 0.960937,2.26562 -8e-6,0.45835 -0.09376,0.90887 -0.28125,1.35157 -0.187507,0.44271 -0.498705,0.90886 -0.933593,1.39843 -0.434903,0.48959 -1.157558,1.16147 -2.167969,2.01563 -0.843754,0.70834 -1.38542,1.1888 -1.625,1.44141 -0.239586,0.2526 -0.437503,0.50651 -0.59375,0.76171 z"  style="fill:'+v.color.t+'"  id="path3091" />  <path  d="m 103.0883,146.60902 0,-2.74219 -4.968754,0 0,-1.28906 5.226564,-7.42188 1.14844,0 0,7.42188 1.54687,0 0,1.28906 -1.54687,0 0,2.74219 z m 0,-4.03125 0,-5.16407 -3.585942,5.16407 z"  style="fill:'+v.color.t+'"  id="path3093" />  <path  d="m 123.75236,145.25745 0,1.35157 -7.57031,0 c -0.0104,-0.33854 0.0443,-0.66407 0.16406,-0.97657 0.19271,-0.51562 0.5013,-1.02343 0.92578,-1.52343 0.42448,-0.5 1.03776,-1.07813 1.83984,-1.73438 1.24479,-1.02083 2.08594,-1.82942 2.52344,-2.42578 0.43749,-0.59635 0.65624,-1.16015 0.65625,-1.69141 -1e-5,-0.55728 -0.19922,-1.02733 -0.59766,-1.41015 -0.39844,-0.3828 -0.91797,-0.57421 -1.55859,-0.57422 -0.67709,1e-5 -1.21875,0.20313 -1.625,0.60937 -0.40625,0.40626 -0.61198,0.96876 -0.61719,1.6875 l -1.44531,-0.14843 c 0.099,-1.07812 0.47135,-1.89973 1.11719,-2.46485 0.64583,-0.56509 1.51302,-0.84764 2.60156,-0.84765 1.09895,1e-5 1.96874,0.3047 2.60938,0.91406 0.64061,0.60938 0.96093,1.36459 0.96093,2.26562 0,0.45835 -0.0937,0.90887 -0.28125,1.35157 -0.1875,0.44271 -0.4987,0.90886 -0.93359,1.39843 -0.4349,0.48959 -1.15756,1.16147 -2.16797,2.01563 -0.84375,0.70834 -1.38542,1.1888 -1.625,1.44141 -0.23959,0.2526 -0.4375,0.50651 -0.59375,0.76171 z"  style="fill:'+v.color.t+'"  id="path3095" />  <path  d="m 125.26798,143.60902 1.47657,-0.125 c 0.10937,0.71875 0.36327,1.25911 0.76171,1.62109 0.39844,0.36198 0.87891,0.54297 1.44141,0.54297 0.67708,0 1.25,-0.25521 1.71875,-0.76563 0.46874,-0.51041 0.70312,-1.18749 0.70313,-2.03125 -1e-5,-0.80207 -0.22527,-1.43489 -0.67579,-1.89843 -0.45052,-0.46354 -1.04036,-0.69531 -1.76953,-0.69532 -0.45313,1e-5 -0.86198,0.10287 -1.22656,0.3086 -0.36459,0.20573 -0.65104,0.47266 -0.85937,0.80078 l -1.32032,-0.17188 1.10938,-5.88281 5.69531,0 0,1.34375 -4.57031,0 -0.61719,3.07813 c 0.6875,-0.47916 1.40885,-0.71875 2.16406,-0.71875 1,0 1.84375,0.34636 2.53125,1.03906 0.6875,0.69271 1.03125,1.58334 1.03125,2.67187 0,1.03647 -0.30209,1.9323 -0.90625,2.6875 -0.73438,0.92709 -1.73698,1.39063 -3.00781,1.39063 -1.04167,0 -1.89193,-0.29167 -2.55078,-0.875 -0.65886,-0.58333 -1.03516,-1.35677 -1.12891,-2.32031 z"  style="fill:'+v.color.t+'"  id="path3097" />  <path  d="m 150.43986,145.25745 0,1.35157 -7.57031,0 c -0.0104,-0.33854 0.0443,-0.66407 0.16406,-0.97657 0.19271,-0.51562 0.5013,-1.02343 0.92578,-1.52343 0.42448,-0.5 1.03776,-1.07813 1.83984,-1.73438 1.24479,-1.02083 2.08594,-1.82942 2.52344,-2.42578 0.43749,-0.59635 0.65624,-1.16015 0.65625,-1.69141 -10e-6,-0.55728 -0.19922,-1.02733 -0.59766,-1.41015 -0.39844,-0.3828 -0.91797,-0.57421 -1.55859,-0.57422 -0.67709,1e-5 -1.21875,0.20313 -1.625,0.60937 -0.40625,0.40626 -0.61198,0.96876 -0.61719,1.6875 l -1.44531,-0.14843 c 0.099,-1.07812 0.47135,-1.89973 1.11719,-2.46485 0.64583,-0.56509 1.51302,-0.84764 2.60156,-0.84765 1.09895,1e-5 1.96874,0.3047 2.60938,0.91406 0.64061,0.60938 0.96093,1.36459 0.96093,2.26562 0,0.45835 -0.0937,0.90887 -0.28125,1.35157 -0.1875,0.44271 -0.4987,0.90886 -0.93359,1.39843 -0.4349,0.48959 -1.15756,1.16147 -2.16797,2.01563 -0.84375,0.70834 -1.38542,1.1888 -1.625,1.44141 -0.23959,0.2526 -0.4375,0.50651 -0.59375,0.76171 z"  style="fill:'+v.color.t+'"  id="path3099" />  <path  d="m 159.25236,137.96058 -1.39844,0.10937 c -0.12501,-0.55207 -0.30209,-0.95311 -0.53125,-1.20312 -0.38021,-0.40103 -0.84896,-0.60155 -1.40625,-0.60156 -0.44792,1e-5 -0.84115,0.12501 -1.17969,0.375 -0.44271,0.32292 -0.79167,0.79428 -1.04687,1.41406 -0.25521,0.6198 -0.38802,1.50261 -0.39844,2.64844 0.33854,-0.51562 0.7526,-0.89843 1.24219,-1.14844 0.48958,-0.24999 1.0026,-0.37499 1.53906,-0.375 0.9375,1e-5 1.73567,0.34506 2.39453,1.03516 0.65885,0.69011 0.98828,1.58203 0.98828,2.67578 0,0.71875 -0.15495,1.38672 -0.46484,2.0039 -0.3099,0.61719 -0.73568,1.08985 -1.27734,1.41797 -0.54168,0.32813 -1.15626,0.49219 -1.84375,0.49219 -1.17188,0 -2.12761,-0.43099 -2.86719,-1.29297 -0.73959,-0.86198 -1.10938,-2.28255 -1.10938,-4.26172 0,-2.21353 0.40886,-3.82291 1.22657,-4.82812 0.71354,-0.87499 1.67447,-1.31249 2.88281,-1.3125 0.90103,1e-5 1.63932,0.25261 2.21484,0.75781 0.57552,0.50522 0.92057,1.20313 1.03516,2.09375 z m -5.74219,4.9375 c 0,0.48438 0.10286,0.94792 0.30859,1.39062 0.20573,0.44271 0.49349,0.77995 0.86329,1.01172 0.36978,0.23177 0.7578,0.34766 1.16406,0.34766 0.59374,0 1.10416,-0.23958 1.53125,-0.71875 0.42708,-0.47917 0.64062,-1.13021 0.64062,-1.95313 0,-0.79166 -0.21094,-1.41536 -0.63281,-1.87109 -0.42188,-0.45572 -0.95313,-0.68359 -1.59375,-0.68359 -0.63542,0 -1.17448,0.22787 -1.61719,0.68359 -0.44271,0.45573 -0.66406,1.05339 -0.66406,1.79297 z"  style="fill:'+v.color.t+'"  id="path3101" />  <path  d="m 177.12736,145.25745 0,1.35157 -7.57031,0 c -0.0104,-0.33854 0.0443,-0.66407 0.16406,-0.97657 0.19271,-0.51562 0.5013,-1.02343 0.92578,-1.52343 0.42448,-0.5 1.03776,-1.07813 1.83984,-1.73438 1.24479,-1.02083 2.08594,-1.82942 2.52344,-2.42578 0.43749,-0.59635 0.65624,-1.16015 0.65625,-1.69141 -10e-6,-0.55728 -0.19922,-1.02733 -0.59766,-1.41015 -0.39844,-0.3828 -0.91797,-0.57421 -1.55859,-0.57422 -0.67709,1e-5 -1.21875,0.20313 -1.625,0.60937 -0.40625,0.40626 -0.61198,0.96876 -0.61719,1.6875 l -1.44531,-0.14843 c 0.099,-1.07812 0.47135,-1.89973 1.11719,-2.46485 0.64583,-0.56509 1.51302,-0.84764 2.60156,-0.84765 1.09895,1e-5 1.96874,0.3047 2.60938,0.91406 0.64061,0.60938 0.96093,1.36459 0.96093,2.26562 0,0.45835 -0.0937,0.90887 -0.28125,1.35157 -0.1875,0.44271 -0.4987,0.90886 -0.93359,1.39843 -0.4349,0.48959 -1.15756,1.16147 -2.16797,2.01563 -0.84375,0.70834 -1.38542,1.1888 -1.625,1.44141 -0.23959,0.2526 -0.4375,0.50651 -0.59375,0.76171 z"  style="fill:'+v.color.t+'"  id="path3103" />  <path  d="m 178.73673,136.65589 0,-1.35156 7.41407,0 0,1.09375 c -0.72918,0.77605 -1.45183,1.8073 -2.16797,3.09375 -0.71615,1.28646 -1.26954,2.60938 -1.66016,3.96875 -0.28125,0.95833 -0.46094,2.00781 -0.53906,3.14844 l -1.44531,0 c 0.0156,-0.90104 0.1927,-1.98958 0.53125,-3.26563 0.33853,-1.27604 0.82421,-2.5065 1.45703,-3.6914 0.6328,-1.18489 1.30598,-2.18359 2.01953,-2.9961 z"  style="fill:'+v.color.t+'"  id="path3105" />  <path  d="m 17.002358,165.25745 0,1.35157 -7.5703121,0 c -0.010417,-0.33854 0.04427,-0.66407 0.1640625,-0.97657 0.1927075,-0.51562 0.5013006,-1.02343 0.9257816,-1.52343 0.424477,-0.5 1.037757,-1.07813 1.839843,-1.73438 1.244787,-1.02083 2.085932,-1.82942 2.523438,-2.42578 0.437494,-0.59635 0.656243,-1.16015 0.65625,-1.69141 -7e-6,-0.55728 -0.199225,-1.02733 -0.597656,-1.41015 -0.398443,-0.3828 -0.917974,-0.57421 -1.558594,-0.57422 -0.677087,1e-5 -1.218753,0.20313 -1.625,0.60937 -0.406252,0.40626 -0.611981,0.96876 -0.617188,1.6875 l -1.4453121,-0.14843 c 0.098957,-1.07812 0.4713531,-1.89973 1.1171871,-2.46485 0.645831,-0.56509 1.513018,-0.84764 2.601563,-0.84765 1.098953,1e-5 1.968743,0.3047 2.609375,0.91406 0.640617,0.60938 0.960929,1.36459 0.960937,2.26562 -8e-6,0.45835 -0.09376,0.90887 -0.28125,1.35157 -0.187507,0.44271 -0.498705,0.90886 -0.933593,1.39843 -0.434903,0.48959 -1.157558,1.16147 -2.167969,2.01563 -0.843754,0.70834 -1.38542,1.1888 -1.625,1.44141 -0.239586,0.2526 -0.437503,0.50651 -0.59375,0.76171 z"  style="fill:'+v.color.t+'"  id="path3107" />  <path  d="m 20.682046,160.39808 c -0.583336,-0.21354 -1.015627,-0.51822 -1.296875,-0.91406 -0.281251,-0.39583 -0.421876,-0.86979 -0.421875,-1.42188 -1e-6,-0.83332 0.299478,-1.53384 0.898437,-2.10156 0.598956,-0.5677 1.39583,-0.85155 2.390625,-0.85156 0.999995,1e-5 1.804682,0.29037 2.414063,0.87109 0.609368,0.58074 0.914055,1.28777 0.914062,2.12109 -7e-6,0.53126 -0.13933,0.9935 -0.417968,1.38672 -0.278653,0.39324 -0.70183,0.69662 -1.269532,0.91016 0.703119,0.22917 1.238274,0.59896 1.605469,1.10937 0.36718,0.51043 0.550773,1.1198 0.550781,1.82813 -8e-6,0.97917 -0.346362,1.80208 -1.039062,2.46875 -0.692715,0.66667 -1.604172,1 -2.734375,1 -1.130212,0 -2.041669,-0.33464 -2.734375,-1.00391 -0.692709,-0.66927 -1.039063,-1.5039 -1.039063,-2.5039 0,-0.74479 0.188802,-1.36849 0.566407,-1.8711 0.377602,-0.5026 0.915362,-0.84504 1.613281,-1.02734 z m -0.28125,-2.38281 c -3e-6,0.54167 0.174476,0.98438 0.523437,1.32812 0.348955,0.34376 0.80208,0.51563 1.359375,0.51563 0.541662,0 0.985672,-0.17057 1.332032,-0.51172 0.346348,-0.34114 0.519525,-0.75911 0.519531,-1.25391 -6e-6,-0.51561 -0.178392,-0.94921 -0.535156,-1.30078 -0.356777,-0.35155 -0.800787,-0.52733 -1.332032,-0.52734 -0.536462,1e-5 -0.981774,0.17188 -1.335937,0.51562 -0.354169,0.34376 -0.531253,0.75522 -0.53125,1.23438 z m -0.453125,5.28906 c -2e-6,0.40104 0.09505,0.78906 0.285156,1.16406 0.190102,0.375 0.472654,0.66537 0.847656,0.8711 0.374997,0.20573 0.778642,0.30859 1.210938,0.30859 0.67187,0 1.226557,-0.21615 1.664062,-0.64844 0.437494,-0.43229 0.656244,-0.98177 0.65625,-1.64844 -6e-6,-0.67708 -0.225267,-1.23697 -0.675781,-1.67968 -0.450526,-0.44271 -1.014328,-0.66406 -1.691406,-0.66407 -0.661462,1e-5 -1.209639,0.21876 -1.644531,0.65625 -0.434898,0.43751 -0.652346,0.98438 -0.652344,1.64063 z"  style="fill:'+v.color.t+'"  id="path3109" />  <path  d="m 43.689858,165.25745 0,1.35157 -7.570312,0 c -0.01042,-0.33854 0.04427,-0.66407 0.164062,-0.97657 0.192708,-0.51562 0.501301,-1.02343 0.925782,-1.52343 0.424477,-0.5 1.037757,-1.07813 1.839843,-1.73438 1.244787,-1.02083 2.085932,-1.82942 2.523438,-2.42578 0.437494,-0.59635 0.656243,-1.16015 0.65625,-1.69141 -7e-6,-0.55728 -0.199225,-1.02733 -0.597656,-1.41015 -0.398443,-0.3828 -0.917974,-0.57421 -1.558594,-0.57422 -0.677087,1e-5 -1.218753,0.20313 -1.625,0.60937 -0.406252,0.40626 -0.611981,0.96876 -0.617188,1.6875 l -1.445312,-0.14843 c 0.09896,-1.07812 0.471353,-1.89973 1.117187,-2.46485 0.645831,-0.56509 1.513018,-0.84764 2.601563,-0.84765 1.098953,1e-5 1.968743,0.3047 2.609375,0.91406 0.640617,0.60938 0.960929,1.36459 0.960937,2.26562 -8e-6,0.45835 -0.09376,0.90887 -0.28125,1.35157 -0.187507,0.44271 -0.498705,0.90886 -0.933593,1.39843 -0.434903,0.48959 -1.157558,1.16147 -2.167969,2.01563 -0.843754,0.70834 -1.38542,1.1888 -1.625,1.44141 -0.239586,0.2526 -0.437503,0.50651 -0.59375,0.76171 z"  style="fill:'+v.color.t+'"  id="path3111" />  <path  d="m 45.416421,163.96058 1.351562,-0.125 c 0.114581,0.63542 0.333331,1.09635 0.65625,1.38281 0.322914,0.28646 0.736976,0.42969 1.242188,0.42969 0.432287,0 0.811193,-0.099 1.136719,-0.29688 0.325515,-0.19791 0.592442,-0.46223 0.800781,-0.79296 0.208327,-0.33073 0.382806,-0.77735 0.523437,-1.33985 0.140619,-0.56249 0.210931,-1.13541 0.210938,-1.71875 -7e-6,-0.0625 -0.0026,-0.15624 -0.0078,-0.28125 -0.281256,0.44792 -0.66537,0.8112 -1.152343,1.08985 -0.486985,0.27865 -1.014328,0.41797 -1.582032,0.41796 -0.947919,1e-5 -1.750002,-0.34374 -2.40625,-1.03125 -0.656251,-0.68749 -0.984375,-1.59374 -0.984375,-2.71875 0,-1.16145 0.342447,-2.09634 1.027344,-2.80468 0.684894,-0.70833 1.542966,-1.06249 2.574219,-1.0625 0.744787,1e-5 1.425775,0.20053 2.042969,0.60156 0.61718,0.40105 1.08593,0.97267 1.40625,1.71484 0.320304,0.7422 0.48046,1.81642 0.480468,3.22266 -8e-6,1.46355 -0.158862,2.62891 -0.476562,3.49609 -0.317716,0.86719 -0.790372,1.52735 -1.417969,1.98047 -0.62761,0.45313 -1.363286,0.67969 -2.207031,0.67969 -0.895837,0 -1.627607,-0.2487 -2.195313,-0.74609 -0.567709,-0.4974 -0.908855,-1.19662 -1.023437,-2.09766 z m 5.757812,-5.05469 c -6e-6,-0.80728 -0.21485,-1.44791 -0.644531,-1.92187 -0.429693,-0.47395 -0.946619,-0.71093 -1.550781,-0.71094 -0.625004,1e-5 -1.169274,0.25522 -1.632813,0.76562 -0.463544,0.51043 -0.695314,1.17189 -0.695312,1.98438 -2e-6,0.72917 0.22005,1.32162 0.660156,1.77734 0.440101,0.45574 0.983069,0.6836 1.628906,0.6836 0.651037,0 1.186193,-0.22786 1.605469,-0.6836 0.419265,-0.45572 0.6289,-1.08723 0.628906,-1.89453 z"  style="fill:'+v.color.t+'"  id="path3113" />  <path  d="m 62.994546,163.58558 1.40625,-0.1875 c 0.161456,0.79688 0.436195,1.37109 0.824219,1.72266 0.388017,0.35156 0.860673,0.52734 1.417968,0.52734 0.661454,0 1.220047,-0.22917 1.675782,-0.6875 0.455722,-0.45833 0.683587,-1.02604 0.683593,-1.70313 -6e-6,-0.64583 -0.210944,-1.17838 -0.632812,-1.59765 -0.421881,-0.41927 -0.958338,-0.6289 -1.609375,-0.62891 -0.265629,1e-5 -0.596358,0.0521 -0.992188,0.15625 l 0.15625,-1.23437 c 0.09375,0.0104 0.169267,0.0156 0.226563,0.0156 0.598954,1e-5 1.138016,-0.15624 1.617187,-0.46875 0.479161,-0.31249 0.718744,-0.79426 0.71875,-1.44531 -6e-6,-0.51562 -0.174485,-0.9427 -0.523437,-1.28125 -0.348964,-0.33853 -0.799484,-0.5078 -1.351563,-0.50781 -0.546878,1e-5 -1.002607,0.17188 -1.367187,0.51562 -0.364586,0.34376 -0.598961,0.85939 -0.703125,1.54688 l -1.40625,-0.25 c 0.171874,-0.9427 0.562499,-1.67317 1.171875,-2.19141 0.609372,-0.51822 1.367184,-0.77733 2.273437,-0.77734 0.624996,1e-5 1.200516,0.13412 1.726563,0.40234 0.526035,0.26824 0.928378,0.63413 1.207031,1.09766 0.278639,0.46355 0.417961,0.95573 0.417969,1.47656 -8e-6,0.4948 -0.13282,0.94532 -0.398438,1.35156 -0.265632,0.40626 -0.65886,0.72917 -1.179687,0.96875 0.677077,0.15626 1.203118,0.48048 1.578125,0.97266 0.374992,0.49219 0.562492,1.10807 0.5625,1.84765 -8e-6,1.00001 -0.364591,1.84766 -1.09375,2.54297 -0.729173,0.69532 -1.651047,1.04297 -2.765625,1.04297 -1.005212,0 -1.839846,-0.29948 -2.503906,-0.89844 -0.664064,-0.59895 -1.04297,-1.37499 -1.136719,-2.32812 z"  style="fill:'+v.color.t+'"  id="path3115" />  <path  d="m 71.892983,160.96058 c 0,-1.35416 0.139323,-2.444 0.417969,-3.26953 0.278645,-0.82551 0.692707,-1.46223 1.242188,-1.91016 0.549476,-0.4479 1.240882,-0.67186 2.074218,-0.67187 0.614579,1e-5 1.153641,0.12371 1.617188,0.37109 0.463535,0.24741 0.846347,0.60418 1.148437,1.07031 0.302076,0.46616 0.539055,1.03387 0.710938,1.70313 0.171867,0.66928 0.257804,1.57162 0.257812,2.70703 -8e-6,1.34375 -0.138028,2.42839 -0.414062,3.25391 -0.276049,0.82552 -0.688809,1.46354 -1.238281,1.91406 -0.549485,0.45052 -1.243495,0.67578 -2.082032,0.67578 -1.10417,0 -1.971356,-0.39583 -2.601562,-1.1875 -0.755209,-0.95312 -1.132813,-2.50521 -1.132813,-4.65625 z m 1.445313,0 c -2e-6,1.88021 0.22005,3.13151 0.660156,3.75391 0.440101,0.62239 0.983069,0.93359 1.628906,0.93359 0.645829,0 1.188797,-0.3125 1.628907,-0.9375 0.440097,-0.625 0.660149,-1.875 0.660156,-3.75 -7e-6,-1.88541 -0.220059,-3.13801 -0.660156,-3.75781 -0.44011,-0.61979 -0.988287,-0.92968 -1.644532,-0.92969 -0.645837,1e-5 -1.161461,0.27345 -1.546875,0.82031 -0.484377,0.69793 -0.726564,1.98699 -0.726562,3.86719 z"  style="fill:'+v.color.t+'"  id="path3117" />  <path  d="m 89.682046,163.58558 1.40625,-0.1875 c 0.161456,0.79688 0.436195,1.37109 0.824219,1.72266 0.388017,0.35156 0.860673,0.52734 1.417968,0.52734 0.661454,0 1.220047,-0.22917 1.675782,-0.6875 0.455722,-0.45833 0.683587,-1.02604 0.683593,-1.70313 -6e-6,-0.64583 -0.210944,-1.17838 -0.632812,-1.59765 -0.421881,-0.41927 -0.958338,-0.6289 -1.609375,-0.62891 -0.265629,1e-5 -0.596358,0.0521 -0.992188,0.15625 l 0.15625,-1.23437 c 0.09375,0.0104 0.169267,0.0156 0.226563,0.0156 0.598954,1e-5 1.138016,-0.15624 1.617187,-0.46875 0.479161,-0.31249 0.718744,-0.79426 0.71875,-1.44531 -6e-6,-0.51562 -0.174485,-0.9427 -0.523437,-1.28125 -0.348964,-0.33853 -0.799484,-0.5078 -1.351563,-0.50781 -0.546878,1e-5 -1.002607,0.17188 -1.367187,0.51562 -0.364586,0.34376 -0.598961,0.85939 -0.703125,1.54688 l -1.40625,-0.25 c 0.171874,-0.9427 0.562499,-1.67317 1.171875,-2.19141 0.609372,-0.51822 1.367184,-0.77733 2.273437,-0.77734 0.624996,1e-5 1.200516,0.13412 1.726563,0.40234 0.526035,0.26824 0.928378,0.63413 1.207031,1.09766 0.278639,0.46355 0.417961,0.95573 0.417969,1.47656 -8e-6,0.4948 -0.13282,0.94532 -0.398438,1.35156 -0.265632,0.40626 -0.65886,0.72917 -1.179687,0.96875 0.677077,0.15626 1.203118,0.48048 1.578125,0.97266 0.374992,0.49219 0.562492,1.10807 0.5625,1.84765 -8e-6,1.00001 -0.364591,1.84766 -1.09375,2.54297 -0.729173,0.69532 -1.651047,1.04297 -2.765625,1.04297 -1.005212,0 -1.839846,-0.29948 -2.503906,-0.89844 -0.664064,-0.59895 -1.04297,-1.37499 -1.136719,-2.32812 z"  style="fill:'+v.color.t+'"  id="path3119" />  <path  d="m 103.87736,166.60902 -1.40625,0 0,-8.96094 c -0.33855,0.32292 -0.78256,0.64584 -1.33203,0.96875 -0.54948,0.32292 -1.04297,0.56511 -1.480472,0.72656 l 0,-1.35937 c 0.786452,-0.36979 1.473952,-0.8177 2.062502,-1.34375 0.58854,-0.52603 1.0052,-1.03645 1.25,-1.53125 l 0.90625,0 z"  style="fill:'+v.color.t+'"  id="path3121" />  </g>  <g  style="font-size:20px;font-style:normal;font-weight:bold;line-height:125%;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;font-family:Sans;-inkscape-font-specification:Sans Bold"  id="text3082">  <path  d="m 19.783068,58.265808 2.242187,-0.117187 c 0.03125,0.713544 0.153643,1.195315 0.367188,1.445312 0.343746,0.406252 0.979162,0.609377 1.90625,0.609375 0.770827,2e-6 1.328118,-0.138019 1.671875,-0.414062 0.343742,-0.27604 0.515617,-0.609373 0.515625,-1 -8e-6,-0.338539 -0.140633,-0.624997 -0.421875,-0.859375 -0.197924,-0.171871 -0.740892,-0.450517 -1.628907,-0.835938 -0.888025,-0.385411 -1.536462,-0.707026 -1.945312,-0.964844 -0.408858,-0.257806 -0.730472,-0.592441 -0.964844,-1.003906 -0.234377,-0.411451 -0.351565,-0.893221 -0.351562,-1.445312 -3e-6,-0.963533 0.348955,-1.763011 1.046875,-2.398438 0.697912,-0.635405 1.708328,-0.953113 3.03125,-0.953125 1.343742,1.2e-5 2.384105,0.315116 3.121093,0.945313 0.736969,0.630218 1.141917,1.468759 1.214844,2.515625 l -2.257812,0.101562 c -0.0573,-0.546866 -0.255217,-0.966137 -0.59375,-1.257812 -0.33855,-0.291657 -0.838549,-0.437491 -1.5,-0.4375 -0.651048,9e-6 -1.115891,0.114593 -1.394532,0.34375 -0.27865,0.229175 -0.417973,0.518238 -0.417968,0.867187 -5e-6,0.328133 0.127599,0.598966 0.382812,0.8125 0.255203,0.218758 0.820307,0.518237 1.695313,0.898438 1.3177,0.567714 2.156241,1.028651 2.515625,1.382812 0.536448,0.520838 0.804677,1.190109 0.804687,2.007813 -10e-6,1.010419 -0.399749,1.873699 -1.199219,2.589843 -0.799487,0.716146 -1.912767,1.074219 -3.339843,1.074219 -0.98438,0 -1.837244,-0.165365 -2.558594,-0.496094 -0.721356,-0.330728 -1.230471,-0.790363 -1.527344,-1.378906 -0.296876,-0.588539 -0.434897,-1.265622 -0.414062,-2.03125 z"  style="font-size:16px;font-style:oblique;fill:'+v.color.t+';-inkscape-font-specification:Sans Bold Oblique"  id="path3124" />  <path  d="m 50.00963,61.976746 -2.234375,0 -0.414062,-9.570313 -1.820313,9.570313 -2.125,0 2.390625,-11.453125 3.359375,0 0.351563,8.023437 3.90625,-8.023437 3.398437,0 -2.398437,11.453125 -2.148438,0 2.257813,-9.5 z"  style="font-size:16px;font-style:oblique;fill:'+v.color.t+';-inkscape-font-specification:Sans Bold Oblique"  id="path3126" />  <path  d="m 75.064318,61.976746 -2.359375,0 2,-9.539063 -3.359375,0 0.398437,-1.914062 9.023438,0 -0.398438,1.914062 -3.3125,0 z"  style="font-size:16px;font-style:oblique;fill:'+v.color.t+';-inkscape-font-specification:Sans Bold Oblique"  id="path3128" />  <path  d="m 104.06432,61.976746 -2.38281,0 -0.25782,-8.375 -4.171872,8.375 -2.429688,0 -0.429687,-11.453125 2.304687,0 0.101563,8.015625 3.929687,-8.015625 2.55469,0 0.26562,7.929687 3.77344,-7.929687 2.28125,0 z"  style="font-size:16px;font-style:oblique;fill:'+v.color.t+';-inkscape-font-specification:Sans Bold Oblique"  id="path3130" />  <path  d="m 131.00182,61.976746 -2.35938,0 2,-9.539063 -3.35937,0 0.39844,-1.914062 9.02343,0 -0.39843,1.914062 -3.3125,0 z"  style="font-size:16px;font-style:oblique;fill:'+v.color.t+';-inkscape-font-specification:Sans Bold Oblique"  id="path3132" />  <path  d="m 153.51744,61.976746 2.39844,-11.453125 8.01563,0 -0.40625,1.914062 -5.65625,0 -0.57813,2.75 5.54688,0 -0.39844,1.914063 -5.54688,0 -1.01562,4.875 z"  style="font-size:16px;font-style:oblique;fill:'+v.color.t+';-inkscape-font-specification:Sans Bold Oblique"  id="path3134" />  <path  d="m 177.00182,58.265808 2.24219,-0.117187 c 0.0312,0.713544 0.15364,1.195315 0.36718,1.445312 0.34375,0.406252 0.97916,0.609377 1.90625,0.609375 0.77083,2e-6 1.32812,-0.138019 1.67188,-0.414062 0.34374,-0.27604 0.51561,-0.609373 0.51562,-1 -1e-5,-0.338539 -0.14063,-0.624997 -0.42187,-0.859375 -0.19793,-0.171871 -0.74089,-0.450517 -1.62891,-0.835938 -0.88802,-0.385411 -1.53646,-0.707026 -1.94531,-0.964844 -0.40886,-0.257806 -0.73047,-0.592441 -0.96484,-1.003906 -0.23438,-0.411451 -0.35157,-0.893221 -0.35157,-1.445312 0,-0.963533 0.34896,-1.763011 1.04688,-2.398438 0.69791,-0.635405 1.70833,-0.953113 3.03125,-0.953125 1.34374,1.2e-5 2.3841,0.315116 3.12109,0.945313 0.73697,0.630218 1.14192,1.468759 1.21485,2.515625 l -2.25782,0.101562 c -0.0573,-0.546866 -0.25521,-0.966137 -0.59375,-1.257812 -0.33855,-0.291657 -0.83855,-0.437491 -1.5,-0.4375 -0.65104,9e-6 -1.11589,0.114593 -1.39453,0.34375 -0.27865,0.229175 -0.41797,0.518238 -0.41797,0.867187 0,0.328133 0.1276,0.598966 0.38282,0.8125 0.2552,0.218758 0.8203,0.518237 1.69531,0.898438 1.3177,0.567714 2.15624,1.028651 2.51562,1.382812 0.53645,0.520838 0.80468,1.190109 0.80469,2.007813 -10e-6,1.010419 -0.39975,1.873699 -1.19922,2.589843 -0.79949,0.716146 -1.91277,1.074219 -3.33984,1.074219 -0.98438,0 -1.83725,-0.165365 -2.5586,-0.496094 -0.72135,-0.330728 -1.23047,-0.790363 -1.52734,-1.378906 -0.29688,-0.588539 -0.4349,-1.265622 -0.41406,-2.03125 z"  style="font-size:16px;font-style:oblique;fill:'+v.color.t+';-inkscape-font-specification:Sans Bold Oblique"  id="path3136" />  </g>  <g  style="font-size:20px;font-style:normal;font-weight:bold;line-height:125%;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;font-family:Sans;-inkscape-font-specification:Sans Bold"  id="text3086">  <path  d="m 49.742687,21.281431 c -10e-7,-1.458325 0.218098,-2.682282 0.654297,-3.671875 0.325519,-0.729155 0.769855,-1.383451 1.333008,-1.96289 0.563148,-0.579414 1.180009,-1.009101 1.850586,-1.289063 0.891921,-0.37759 1.920566,-0.566392 3.085937,-0.566406 2.109365,1.4e-5 3.797189,0.654311 5.063477,1.96289 1.266262,1.308605 1.899399,3.128265 1.899414,5.458985 -1.5e-5,2.311203 -0.628269,4.119469 -1.884766,5.424805 -1.256522,1.305338 -2.936208,1.958007 -5.039062,1.958007 -2.128912,0 -3.821619,-0.649413 -5.078125,-1.948242 -1.256512,-1.298825 -1.884767,-3.08756 -1.884766,-5.366211 z m 2.978516,-0.09766 c -4e-6,1.621099 0.374345,2.849939 1.123047,3.686523 0.748692,0.836592 1.699212,1.254885 2.851562,1.254883 1.152335,2e-6 2.097972,-0.415036 2.836914,-1.245117 0.738921,-0.830074 1.108387,-2.07519 1.108399,-3.735352 -1.2e-5,-1.640616 -0.359712,-2.864573 -1.079102,-3.671875 -0.719411,-0.80728 -1.674814,-1.210925 -2.866211,-1.210937 -1.191413,1.2e-5 -2.151698,0.40854 -2.880859,1.225586 -0.729171,0.817067 -1.093754,2.049162 -1.09375,3.696289 z"  style="fill:'+v.color.t+'"  id="path3002" />  <path  d="m 75.035656,23.088072 2.802734,0.888672 c -0.4297,1.562503 -1.144218,2.722983 -2.143554,3.481445 -0.99936,0.758464 -2.267262,1.137695 -3.803711,1.137695 -1.901048,0 -3.463546,-0.649413 -4.6875,-1.948242 -1.22396,-1.298825 -1.835939,-3.074539 -1.835938,-5.327148 -10e-7,-2.382803 0.615233,-4.233387 1.845703,-5.551758 1.230465,-1.318346 2.848302,-1.977525 4.853516,-1.977539 1.751293,1.4e-5 3.173817,0.517592 4.267578,1.552734 0.651029,0.611992 1.13931,1.490897 1.464844,2.636719 L 74.938,18.664244 C 74.768719,17.922067 74.415529,17.33613 73.878429,16.906431 73.341311,16.476756 72.688642,16.261912 71.920422,16.2619 c -1.061205,1.2e-5 -1.922206,0.380871 -2.583008,1.142578 -0.660812,0.761729 -0.991215,1.995452 -0.991211,3.701172 -4e-6,1.809901 0.325516,3.098962 0.976562,3.867187 0.651036,0.768232 1.49739,1.152346 2.539063,1.152344 0.768221,2e-6 1.429027,-0.244138 1.982422,-0.732422 0.553375,-0.488277 0.95051,-1.256506 1.191406,-2.304687 z"  style="fill:'+v.color.t+'"  id="path3004" />  <path  d="m 83.551281,28.351744 0,-11.894532 -4.248047,0 0,-2.421875 11.376953,0 0,2.421875 -4.238281,0 0,11.894532 z"  style="fill:'+v.color.t+'"  id="path3006" />  <path  d="m 91.96925,21.281431 c -10e-7,-1.458325 0.218098,-2.682282 0.654297,-3.671875 0.325519,-0.729155 0.769854,-1.383451 1.333007,-1.96289 0.563148,-0.579414 1.180009,-1.009101 1.850586,-1.289063 0.891922,-0.37759 1.920567,-0.566392 3.085938,-0.566406 2.109362,1.4e-5 3.797192,0.654311 5.063472,1.96289 1.26627,1.308605 1.8994,3.128265 1.89942,5.458985 -2e-5,2.311203 -0.62827,4.119469 -1.88477,5.424805 -1.25652,1.305338 -2.93621,1.958007 -5.03906,1.958007 -2.128912,0 -3.821618,-0.649413 -5.078125,-1.948242 -1.256512,-1.298825 -1.884766,-3.08756 -1.884765,-5.366211 z m 2.978515,-0.09766 c -3e-6,1.621099 0.374345,2.849939 1.123047,3.686523 0.748692,0.836592 1.699212,1.254885 2.851563,1.254883 1.152335,2e-6 2.097975,-0.415036 2.836915,-1.245117 0.73892,-0.830074 1.10839,-2.07519 1.1084,-3.735352 -1e-5,-1.640616 -0.35971,-2.864573 -1.0791,-3.671875 -0.71942,-0.80728 -1.67482,-1.210925 -2.866215,-1.210937 -1.191413,1.2e-5 -2.151699,0.40854 -2.88086,1.225586 -0.729171,0.817067 -1.093753,2.049162 -1.09375,3.696289 z"  style="fill:'+v.color.t+'"  id="path3008" />  <path  d="m 108.11183,14.035337 5.72265,0 c 1.13281,1.5e-5 1.97753,0.04721 2.53418,0.141602 0.55663,0.09441 1.05468,0.291355 1.49414,0.59082 0.43945,0.299493 0.80566,0.698255 1.09864,1.196289 0.29295,0.498059 0.43944,1.056327 0.43945,1.674805 -1e-5,0.670583 -0.18068,1.285817 -0.54199,1.845703 -0.36134,0.559904 -0.85125,0.979826 -1.46973,1.259766 0.87239,0.253913 1.54296,0.686856 2.01172,1.298828 0.46874,0.611985 0.70311,1.331385 0.70313,2.158203 -2e-5,0.651045 -0.15139,1.284183 -0.45411,1.899414 -0.30274,0.615236 -0.71615,1.106772 -1.24023,1.47461 -0.5241,0.367838 -1.17026,0.594075 -1.93848,0.67871 -0.48178,0.05208 -1.64389,0.08464 -3.48633,0.09766 l -4.87304,0 z m 2.89062,2.382813 0,3.310547 1.89453,0 c 1.1263,8e-6 1.82617,-0.01627 2.09961,-0.04883 0.49479,-0.05859 0.88378,-0.229484 1.167,-0.512696 0.28319,-0.283193 0.42479,-0.655914 0.4248,-1.118164 -1e-5,-0.442697 -0.12208,-0.802397 -0.36621,-1.079101 -0.24415,-0.276681 -0.60711,-0.444324 -1.08887,-0.50293 -0.28646,-0.03254 -1.11003,-0.04882 -2.4707,-0.04883 z m 0,5.693359 0,3.828125 2.67578,0 c 1.04166,3e-6 1.70247,-0.02929 1.98243,-0.08789 0.42967,-0.07812 0.77961,-0.268552 1.0498,-0.571289 0.27017,-0.302731 0.40526,-0.708004 0.40527,-1.215821 -1e-5,-0.429682 -0.10417,-0.794265 -0.3125,-1.09375 -0.20834,-0.299473 -0.50945,-0.517572 -0.90332,-0.654297 -0.39389,-0.136712 -1.24838,-0.205071 -2.56347,-0.205078 z"  style="fill:'+v.color.t+'"  id="path3010" />  <path  d="m 122.55519,28.351744 0,-14.316407 10.61523,0 0,2.421875 -7.72461,0 0,3.173829 7.1875,0 0,2.412109 -7.1875,0 0,3.896484 7.99805,0 0,2.41211 z"  style="fill:'+v.color.t+'"  id="path3012" />  <path  d="m 135.92433,28.351744 0,-14.316407 6.08398,0 c 1.52994,1.5e-5 2.64159,0.128595 3.33496,0.385743 0.69335,0.257175 1.24836,0.714531 1.66504,1.37207 0.41666,0.657564 0.62499,1.409516 0.625,2.255859 -1e-5,1.074228 -0.31577,1.961272 -0.94726,2.661133 -0.63153,0.699877 -1.57553,1.140957 -2.83203,1.323242 0.62499,0.36459 1.14094,0.76498 1.54785,1.201172 0.40689,0.436203 0.95539,1.210942 1.6455,2.324219 l 1.74805,2.792969 -3.45703,0 -2.08984,-3.115235 c -0.7422,-1.113277 -1.25001,-1.814773 -1.52344,-2.104492 -0.27345,-0.289708 -0.56316,-0.488275 -0.86914,-0.595703 -0.306,-0.107416 -0.79102,-0.161127 -1.45508,-0.161133 l -0.58594,0 0,5.976563 z m 2.89062,-8.261719 2.13867,0 c 1.38672,8e-6 2.2526,-0.05859 2.59766,-0.175781 0.34504,-0.117179 0.61523,-0.319002 0.81055,-0.605469 0.1953,-0.286449 0.29296,-0.644522 0.29297,-1.074219 -1e-5,-0.48176 -0.12859,-0.870757 -0.38575,-1.166992 -0.25717,-0.296212 -0.62012,-0.483387 -1.08886,-0.561523 -0.23439,-0.03254 -0.93751,-0.04882 -2.10938,-0.04883 l -2.25586,0 z"  style="fill:'+v.color.t+'"  id="path3014" />  </g>  </g>  </svg>').appendTo($div);
			//$('calender-group-'+$div.attr('id')).attr('transform','scale('+w/paper.calender.properties.defaults.width+','+h/paper.calender.properties.defaults.height+')');			
		paper._drawLink(v, $div);
	}
}
paper.dialog = {
	properties: {
		min: {
			width: 100,
			height: 85
		},
		defaults: {
			title: 'Dialog Title',
			text: 'Dialog Text',
			buttons : ['Yes','No'],
			type: 2,
			btype: 1,
			color: {'b': '#000000','bg': '#ececec', 'fg': '#dbdbdb', 't': '#000000'},
			autosize: false,
			size: 14,
			width: 125,
			height: 100,
			align: 'center'
		},
		show: function($e, onshow){
			$('#props').remove();
			$('#props-title').remove();
			var props = paper._getDefaultedProperties($e.data('properties'), paper.dialog.properties.defaults);
			var autosizedrop = '<div id="div-autosizedrop" class="form-inline" style="text-align:center"><div class="btn-group" data-toggle="buttons-checkbox"><button id="autosize" class="btn btn-small '+(props.autosize ? 'active' : '')+'" style="width:210px">Autosize</button></div></div>';
			var title = '<div id="div-title" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Title</strong></label><div class="controls"><textarea id="input-title" rows="1" style="width:135px">'+props.title+'</textarea></div></div>';
			var text = '<div id="div-text" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Text</strong></label><div class="controls"><textarea id="input-text" rows="2" style="width:135px">'+props.text+'</textarea></div></div>';
			var type = '<div id="div-type" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Type</strong></label><div class="controls"><div class="btn-group" data-toggle="buttons-radio"><button id="type-rr" class="btn btn-small '+(props.type == 1 ? 'active' : '')+'">RR</button><button id="type-r" class="btn btn-small '+(props.type == 2 ? 'active' : '')+'">R</button></div></div></div>';
			var b1text = '<div id="div-b1text" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Button1</strong></label><div class="controls"><textarea id="input-b1text" rows="1" style="width:135px">'+props.buttons[0]+'</textarea></div></div>';
			var b2text = '<div id="div-b2text" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Button2</strong></label><div class="controls"><textarea id="input-b2text" rows="1" style="width:135px">'+props.buttons[1]+'</textarea></div></div>';
			var btype = '<div id="div-btype" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Type</strong></label><div class="controls"><div class="btn-group" data-toggle="buttons-radio"><button id="btype-rr" class="btn btn-small '+(props.btype == 1 ? 'active' : '')+'">RR</button><button id="btype-r" class="btn btn-small '+(props.btype == 2 ? 'active' : '')+'">R</button><button id="btype-p" class="btn btn-small '+(props.btype == 3 ? 'active' : '')+'">P</button></div></div></div>';
			var color = '<div id="div-color" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Colors</strong></label><div class="controls"><div id="color-options" class="btn-group" data-toggle="buttons-radio"><button id="color-b" class="btn btn-small"><i class="icon-stop"></i></button><button id="color-bg" class="btn btn-small"><i class="icon-stop"></i></button><button id="color-fg" class="btn btn-small"><i class="icon-stop"></i></button><button id="color-t" class="btn btn-small"><i class="icon-stop"></i></button></div></div></div>';
			var size = '<div id="div-size" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Fontsize</strong></label><div class="controls"><input class="input-small" id="input-size"style="width:50px" type="text" value="'+(props.size ? props.size : paper.dialog.properties.defaults.size)+'" placeholder="'+(props.size ? props.size : paper.dialog.properties.defaults.size)+'"></div></div>';
			var align = '<div id="div-align" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Align</strong></label><div class="controls"><div class="btn-group" data-toggle="buttons-radio"><button id="align-l" class="btn btn-small '+(props.align == 'left' ? 'active' : '')+'"><i class="icon-align-left"></i></button><button id="align-c" class="btn btn-small '+(props.align == 'center' ? 'active' : '')+'"><i class="icon-align-center"></i></button><button id="align-j" class="btn btn-small '+(props.align == 'justify' ? 'active' : '')+'"><i class="icon-align-justify"></i></button><button id="align-r" class="btn btn-small '+(props.align == 'right' ? 'active' : '')+'"><i class="icon-align-right"></i></button></div></div></div>';
			var html = '<div id="props"><div id="propsFor" style="display:none">'+$e.attr('id')+'</div><div class="form-horizontal">'+title+text+type+b1text+b2text+btype+color+align+size+'</div>'+autosizedrop+'</div>';
			//ui.dismissPopovers();
			$e.popover({title:  '<strong style="color:#3a3a3a;" id="props-title">Dialog Properties</strong>', content: html, placement: 'auto', attachTo: '#tbs-popover', onShow: function(){
				paper._buildDimensions($e, '#div-size');
				paper._buildOpacity($e, '#div-size',props);
				paper._buildLinkInspector($e, '#div-size',props);
				$('#input-size').spinner({min: 8, max: 98});
				$('#input-size').keydown(function(ev){
					if(ev.which == 13){
						paper._changeProperty('size', $(this).val() + 'px', false);
						ev.preventDefault();
					}
				});
				$('#input-size').blur(function(ev){
					paper._changeProperty('size', $(this).val() + 'px');
				});
				$('#input-size').change(function(ev){
					paper._changeProperty('size', $(this).val() + 'px', false);
				});
				$('#input-title').blur(function(ev){
					paper._changeProperty('title', $(this).val());
				});
				$('#input-title').keyup(function(ev){
					paper._changeProperty('title', $(this).val(), false);
				});
				$('#input-text').blur(function(ev){
					paper._changeProperty('text', $(this).val());
				});
				$('#input-text').keyup(function(ev){
					paper._changeProperty('text', $(this).val(), false);
				});
				$('#type-rr').click(function(ev){
					paper._changeProperty('type', 1);
				});
				$('#type-r').click(function(ev){
					paper._changeProperty('type', 2);
				});
				$('#input-b1text').blur(function(ev){
					paper._changeProperty('buttons.0', $(this).val());
				});
				$('#input-b1text').keyup(function(ev){
					paper._changeProperty('buttons.0', $(this).val(), false);
				});
				$('#input-b2text').blur(function(ev){
					paper._changeProperty('buttons.1', $(this).val());
				});
				$('#input-b2text').keyup(function(ev){
					paper._changeProperty('buttons.1', $(this).val(), false);
				});
				$('#btype-rr').click(function(ev){
					paper._changeProperty('btype', 1);
				});
				$('#btype-r').click(function(ev){
					paper._changeProperty('btype', 2);
				});
				$('#btype-p').click(function(ev){
					paper._changeProperty('btype', 3);
				});
				$('#autosize').click(function(ev){
					if($(this).hasClass('active')){
						paper._changeProperty('autosize', false);
						mockup.initResizable($('#element-resizer'));								
					}
					else{
						paper._changeProperty('autosize', true);
						$('#element-resizer').resizable('destroy');
					}
				});
				$('#align-l').click(function(ev){
					paper._changeProperty('align', 'left');
				});
				$('#align-c').click(function(ev){
					paper._changeProperty('align', 'center');
				});
				$('#align-j').click(function(ev){
					paper._changeProperty('align', 'justify');
				});
				$('#align-r').click(function(ev){
					paper._changeProperty('align', 'right');
				});
				$('#color-b').data('val', props.color.b ? props.color.b : paper.dialog.properties.defaults.color.b);
				$('#color-bg').data('val', props.color.bg ? props.color.bg : paper.dialog.properties.defaults.color.bg);
				$('#color-fg').data('val', props.color.fg ? props.color.fg : paper.dialog.properties.defaults.color.fg);
				$('#color-t').data('val', props.color.t ? props.color.t : paper.dialog.properties.defaults.color.t);
				$('#color-b').css('color', props.color.b ? props.color.b : paper.dialog.properties.defaults.color.b);
				$('#color-bg').css('color', props.color.bg ? props.color.bg : paper.dialog.properties.defaults.color.bg);
				$('#color-fg').css('color', props.color.fg ? props.color.fg : paper.dialog.properties.defaults.color.fg);
				$('#color-t').css('color', props.color.t ? props.color.t : paper.dialog.properties.defaults.color.t);
				$('#color-b').click(function(ev){
					paper._showColorPicker(ev, this, 'color.b');
				});
				$('#color-bg').click(function(ev){
					paper._showColorPicker(ev, this, 'color.bg');
				});
				$('#color-fg').click(function(ev){
					paper._showColorPicker(ev, this, 'color.fg');
				});
				$('#color-t').click(function(ev){
					paper._showColorPicker(ev, this, 'color.t');
				});
				//Tooltips
				$('#input-title').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Hit Enter to Insert New Line', hideAfter: 2000});
				$('#input-text').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Hit Enter to Insert New Line', hideAfter: 2000});
				$('#type-rr').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Rounded Rect', hideAfter: 2000});
				$('#type-r').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Rectangular', hideAfter: 2000});
				$('#type-p').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Pill', hideAfter: 2000});
				$('#align-l').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Left', hideAfter: 2000});
				$('#align-c').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Center', hideAfter: 2000});
				$('#align-j').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Justified', hideAfter: 2000});
				$('#align-r').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Right', hideAfter: 2000});
				$('#color-b').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Border', hideAfter: 2000});
				$('#color-bg').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Background', hideAfter: 2000});
				$('#color-fg').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Foreground', hideAfter: 2000});
				$('#color-t').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Text', hideAfter: 2000});
				if(onshow) onshow();
			}, onClick: function(ev){paper._popoverClick(ev,$e)}, onHide: function(){
				paper._destroyAllTooltips($e);
			}});
			$e.popover('show');
			ui.popoverContainers.push($e);
		}
	},
	draw: function(canvas, l, t, w, h, v){
		var as1, as2;
		var $div = $(canvas.canvas.parentElement);
		if(v.linkto)
			$div.addClass('has-link');
		if(v.autosize){
			var cp = '<p class="element-caption-para" style="margin: 0;vertical-align:middle;font-size:'+v.size+';text-align:center;word-break:break-word;word-wrap:break-word;"><strong>'+v.title.nl2br()+'</strong></p>';
			var as = paper._getTextAutoSize(cp);
			w = as.width+20, h = as.height+10;
			
			cp = '<p class="element-caption-para" style="margin: 0;vertical-align:middle;font-size:'+v.size+';text-align:center;word-break:break-word;word-wrap:break-word;"><br><br>'+v.text.nl2br()+'</p>';
			as = paper._getTextAutoSize(cp);
			w = Math.max(w, as.width+20);
			h += as.height + 10;

			cp = '<p class="element-caption-para" style="margin: 0;vertical-align:middle;font-size:'+v.size+';text-align:center;word-break:break-word;word-wrap:break-word;">'+v.buttons[0].nl2br()+'</p>';
			var as2 = paper._getTextAutoSize(cp);
			as2.width += 20, as2.height += 10;
			cp = '<p class="element-caption-para" style="margin: 0;vertical-align:middle;font-size:'+v.size+';text-align:center;word-break:break-word;word-wrap:break-word;">'+v.buttons[1].nl2br()+'</p>';
			var as1 = paper._getTextAutoSize(cp);
			as1.width += 20, as1.height += 10;
			//as2.width = as2.width < w/4 ? w/4 : as2.width;
			w = Math.max(w,as2.width+as1.width+20);
			var tw = (as2.width + as1.width + 10);
			as1.width = (w* as1.width) / tw - 10;
			as2.width = (w* as2.width) / tw - 10;
			as1.height = Math.max(as1.height,as2.height);
			as2.height = as1.height;
			h += as2.height;
			paper._resize($(canvas.canvas.parentElement),w,h);
		}
		else{
			as1 = {width: w/2 - 15, height: parseInt(v.size)*25/15};
			as2 = {width: w/2 - 15, height: parseInt(v.size)*25/15};
		}
		var cp = '<p class="element-caption-para" style="margin: 0;vertical-align:middle;font-size:'+v.size+';text-align:center;word-break:break-word;word-wrap:break-word;"><strong>'+v.title.nl2br()+'</strong></p>';
		var as = paper._getTextAutoSize(cp);
		paper.label.draw(canvas,l,t+5,w-2,as.height,$.extend(true, {}, paper.label.properties.defaults, {caption: '<strong>'+v.title+'</strong>', autosize: false, size: v.size, align: 'center', color: {'bg':'none','t':v.color.t}}));
		var bc = $.extend(true,{}, paper.button.properties.defaults,{caption:'<br><br>'+v.text+'<br><br><br><br>',size: v.size, align: v.align, type: v.type, autosize: false});
		bc.color = v.color;
		paper.button.draw(canvas,l,t,w,h,bc);
		bc.autosize = false;
		bc.type = v.btype;
		bc.align = 'center';
		bc.color.bg = v.color.fg;
		bc.caption = v.buttons[0];
		paper.button.draw(canvas,l+10,t+h-as2.height - 10,Math.floor(as2.width),as2.height,bc);
		bc.caption = v.buttons[1];
		paper.button.draw(canvas,l+w-as1.width-10,t+h-as1 .height-10,Math.floor(as1.width),as1.height,bc);
		paper._drawLink(v, $div);
	}
}
paper.throbber = {
	properties: {
		min: {
			width: 15,
			height: 15
		},
		defaults: {
			color: {'b': '#000000'},
			strokes: 10,
			width: 25,
			height: 25
		},
		show: function($e, onshow){
			$('#props').remove();
			var props = $e.data('properties');
			var color = '<div id="div-color" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Color</strong></label><div class="controls"><div id="color-options" class="btn-group" data-toggle="buttons-radio"><button id="color-b" class="btn btn-small"><i class="icon-stop"></i></button></div></div></div>';
			var html = '<div id="props"><div id="propsFor" style="display:none">'+$e.attr('id')+'</div><div class="form-horizontal">'+color+'</div></div>';
			//ui.dismissPopovers();
			$e.popover({title:  '<strong style="color:#3a3a3a;"  id="props-title">Throbber Properties</strong>', content: html, placement: 'auto', attachTo: '#tbs-popover', onShow: function(){
				paper._buildLinkInspector($e, '#div-color',props);
				paper._buildDimensions($e, '#div-color');
				paper._buildOpacity($e, '#div-color',props);
				$('#color-b').data('val', props.color.b ? props.color.b : paper.calender.properties.defaults.color.b);
				$('#color-b').css('color', props.color.b ? props.color.b : paper.calender.properties.defaults.color.b);
				$('#color-b').click(function(ev){
					paper._showColorPicker(ev, this, 'color.b');
				});
				//Tooltips
				$('#color-b').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Border', hideAfter: 2000});
				if(onshow) onshow();
			}, onClick: function(ev){paper._popoverClick(ev,$e)}, onHide: function(){
				paper._destroyAllTooltips($e);
			}});
			$e.popover('show');
			//$('.popover').css('width','168px');
			ui.popoverContainers.push($e);
		}
	},
	draw: function(canvas, l, t, w, h, v){
		var r = Math.min(w/2,h/2), sectors = [], cx = w/2, cy = h/2, r1 = r/2, r2 = r1+(r*2/5);
		var $div = $(canvas.canvas.parentElement);
		if(v.linkto)
			$div.addClass('has-link');
		for (var i = 0; i < v.strokes; i++) {
			var beta = 2 * Math.PI / v.strokes, alpha = beta * i - Math.PI / 2, cos = Math.cos(alpha), sin = Math.sin(alpha);
			sectors[i] = paper.path(canvas, ['M',cx + r1 * cos, cy + r1 * sin,'L',cx + r2 * cos, cy + r2 * sin]).attr({'stroke': v.color.b, 'stroke-width': r/5, 'stroke-linecap': 'round'});
		}
		paper._drawLink(v, $div);
	}
}
paper.menu = {
	properties: {
		min: {
			width: 40,
			height: 30
		},
		_selectNextRow: function(){
			var $el = $('#' + $('#propsFor').text());
			var props = $el.data('properties');
			var i = $('#propsForRow').text();
			if(i >= props.items.length)
				i = props.items.length-1;
			if(i < 0 || i >= props.items.length)
				i = 'new';
			$('#rowitem-'+i).trigger('click');
		},
		_selectNextItem: function(){
			var $el = $('#' + $('#propsFor').text());
			var props = $el.data('properties');
			var i = $('#propsForItem').text();
			var j = $('#propsForRow').text();
			if(i >= props.items[j].length)
				i = props.items[j].length-1;
			$('#selitem-'+i).trigger('click');
		},
		_reconstructRowItems: function(items){
			var rowsel = '';
			for(var k in items){
				rowsel += '<li id="row-item-'+k+'" class="row-item"><a class="rowitem" id="rowitem-'+k+'" href="#">Row '+k+'&nbsp;</a><span class="itemid" style="display:none;">'+k+'</span></li>';
			}
			rowsel += '<li class="divider" id="row-item-divider"></li><li id="row-item-new" class="row-item"><a class="rowitem" id="rowitem-new" href="#">New Row</a><span class="itemid" style="display:none;">n</span></li>';
			$('#rowdropdown').html(rowsel);
			paper.menu.properties._initSortable($('#rowdropdown'));
			$('.rowitem','#div-itemsel').click(paper.menu.properties._handleRowSelection);
		},
		_reconstructSelItems: function(items){
			var itemsel = '';
			for(var j in items){
				itemsel += '<li id="sel-item-'+j+'" class="sel-item"><a class="selitem" id="selitem-'+j+'" href="#">'+(items[j].t == ')__div__(' ? '[Divider]' : items[j].t == '' ? '[Blank]' : items[j].t)+'&nbsp;</a><span class="itemid" style="display:none;">'+j+'</span></li>';
			}
			itemsel += '<li class="divider" id="sel-item-divider"></li><li id="sel-item-new" class="sel-item"><a class="selitem" id="selitem-new" href="#">New Item</a><span class="itemid" style="display:none;">n</span></li>';
			$('#itemdropdown').html(itemsel);
			paper.menu.properties._initSortable($('#itemdropdown'));
			$('.selitem','#div-itemsel').click(paper.menu.properties._handleItemSelection);
			if(items.length == 0) $('#selitem-new').trigger('click');
			else $('#selitem-0').trigger('click')
		},
		_initSortable: function($e){
			$e.sortable({
				scroll: false, 
				containment: 'parent', 
				items: 'li:not(.divider, #sel-item-new)',
				change: function(ev,ui){
					$(this).data('oindex', ui.item.index());
				},
				update: function(ev,ui){
					var nindex = ui.item.index();
					var oindex = $(this).data('oindex');
					var eid = $('#propsFor').text();
					var $el = $('#' + eid);
					var props = $el.data('properties');
					if(oindex < nindex){
						for(var i = oindex; i < nindex;++i){
							var t = props.items[0][i];
							props.items[0][i] = props.items[0][i+1];
							props.items[0][i+1] = t;
						}
					}
					else if(oindex > nindex){
						--oindex;
						for(var i = oindex; i > nindex;--i){
							var t = props.items[0][i];
							props.items[0][i] = props.items[0][i-1];
							props.items[0][i-1] = t;
						}
					}
					$el.data('properties', props);
					stateManager.writeAction('custom',{element: eid});
					paper.redraw($el);
				}
			});
		},
		_handleRowSelection: function(ev){
			var $t = $(this);
			var $el = $('#' + $('#propsFor').text());
			var props = $el.data('properties');
			var a = $('.itemid',$t.parent());
			var i = $.escapeHTML($('.itemid',$t.parent()).text());
			if(i == 'n'){
				i = props.items.length;
				props.items.push([]);
				$el.data('properties', props);
				stateManager.writeAction('custom',{element: $el.attr('id')});				
			}
			$('#propsForRow').text(i);
			$('#btn-rowsel').html('Row ' + i + '&nbsp;');
			paper.menu.properties._reconstructSelItems(props.items[i]);
		},
		_handleItemSelection: function(ev){
			var $t = $(this);
			var $el = $('#' + $('#propsFor').text());
			var props = $el.data('properties');
			var a = $('.itemid',$t.parent());
			var i = $('.itemid',$t.parent()).text();
			var j = $('#propsForRow').text();
			if(i == 'n'){
				$('#btn-cursel').html('[New Item]&nbsp;');
				$('#input-caption').prop('disabled', false);
				$('#input-caption').val('');
				$('#input-caption').attr('placeholder','');
				$('#input-caption2').prop('disabled', false);
				$('#input-caption2').val('');
				$('#input-caption2').attr('placeholder','');
				$('#input-indent').prop('disabled', false);
				$('#input-indent').val('0');
				$('#input-indent').attr('placeholder','0');
				$('#btn-linkinspector').html('Select Page&nbsp;<i class="icon-caret-down"></i>');
				$('#btn-linkinspector').prop('disabled', false);
				$('#divider').removeClass('active');
				$('#submenu').removeClass('active');
				$('#selected').removeClass('active');
				$('#disabled').removeClass('active');
				$('#divider').prop('disabled',false);
				$('#submenu').prop('disabled',false);
				$('#disabled').prop('disabled', false);
				$('#selected').prop('disabled', false);
				if(props.icon){
					$('#state-o').prop('disabled', false);
					$('#state-c').prop('disabled', false);
					$('#state-n').prop('disabled', false);
					$('#state-f').prop('disabled', false);
					$('#state-c').addClass('active');
				}
				
				i = props.items[j].length;
				$('#propsForItem').text(i);
				paper._putItemData({t:'',d:false,s:false,sm:false, st: 2});
				var $li = $('<li id="sel-item-'+i+'" class="sel-item"><a class="selitem" id="selitem-'+i+'" href="#">[New Item]&nbsp;</a><span class="itemid" style="display:none;">'+i+'</span></li>').insertBefore('#sel-item-divider');
				$('.selitem', $li).click(paper.menu.properties._handleItemSelection);
				paper.draw($el);
			}
			else{
				$('#propsForItem').text(i);
				var vi = props.items[j][i];
				$('#btn-cursel').html((vi.t == ')__div__(' ? '[Divider]' : $.escapeHTML(vi.t)) + '&nbsp;');

				if(vi.t == ')__div__(' ){
					$('#input-caption').prop('disabled', true);
					$('#input-caption').val('');
					$('#input-caption').attr('placeholder','');
					$('#input-caption2').prop('disabled', true);
					$('#input-caption2').val('');
					$('#input-caption2').attr('placeholder','');
					$('#input-indent').prop('disabled', true);
					$('#input-indent').val('');
					$('#input-indent').attr('placeholder','');
					$('#divider').addClass('active');
					$('#submenu').prop('disabled',true);
					$('#disabled').prop('disabled', true);
					$('#btn-linkinspector').prop('disabled', true);
					$('#selected').prop('disabled', true);
					if(props.icon){
						$('#state-o').prop('disabled', true);
						$('#state-c').prop('disabled', true);
						$('#state-n').prop('disabled', true);
						$('#state-f').prop('disabled', true);
					}
				}
				else{
					$('#input-caption').prop('disabled', false);
					$('#input-caption').val(vi.t)
					$('#input-caption').attr('placeholder', vi.t);
					$('#input-caption2').prop('disabled', false);
					$('#input-caption2').val(vi.t1)
					$('#input-caption2').attr('placeholder', vi.t1);
					$('#input-indent').prop('disabled', false);
					$('#input-indent').val(vi.i)
					$('#input-indent').attr('placeholder', vi.i);
					$('#divider').removeClass('active');
					$('#btn-linkinspector').prop('disabled', false);
					if(vi.linkto)
						$('#btn-linkinspector').text(persistanceManager.getPageTitleForProject(vi.linkto, stateManager.currentState.currentProject));
					else
						$('#btn-linkinspector').html('Select Page&nbsp;<i class="icon-caret-down"></i>');
					$('#submenu').prop('disabled',false);
					$('#disabled').prop('disabled', false);
					$('#selected').prop('disabled', false);
					if(props.icon){
						$('#state-o').prop('disabled', false);
						$('#state-c').prop('disabled', false);
						$('#state-n').prop('disabled', false);
						$('#state-f').prop('disabled', false);
						$('#state-o').removeClass('active');
						$('#state-c').removeClass('active');
						$('#state-n').removeClass('active');
						$('#state-f').removeClass('active');
						$('#state-'+(vi.st == 1 ? 'o' : vi.st == 2 ? 'c' : vi.st == 4 ? 'f' : 'n')).addClass('active');
					}
				}
				if(vi.sm)
					$('#submenu').addClass('active');
				else
					$('#submenu').removeClass('active');
				if(vi.d)
					$('#disabled').addClass('active');
				else
					$('#disabled').removeClass('active');
				if(vi.s)
					$('#selected').addClass('active');
				else
					$('#selected').removeClass('active');
			}
			$('#btn-cursel').blur();
		},
		defaults: {
			color: {'b':'#000000', 'bg': '#ffffff', 'te': '#000000', 'td': '#4d4d4d', 's': '#c3c3c3'},
			items: [[{'t': 'Menu Item 1', 'd': false, 's': true, 'sm': true, 'i': 0},{'t': 'Menu Item 2', 'd': true, 's':false, 'sm': false, 'i': 0},{'t':')__div__('},{'t': 'Menu Item 3', 'd': false, 's':false, 'sm': false, 'i': 0}]],
			size: '14px',
			type: 2,
			width: 100,
			height: 75,
			align: 'left',
			autosize: true,
		},
		show: function($e, onshow){
			$('#props').remove();
			$('#props-title').remove();
			var props = paper._getDefaultedProperties($e.data('properties'), paper.menu.properties.defaults);
			var autosizedrop = '<div id="div-autosizedrop" class="form-inline" style="text-align:center"><div class="btn-group" data-toggle="buttons-checkbox"><button id="autosize" class="btn btn-small '+(props.autosize ? 'active' : '')+'" style="width:210px;">&nbsp;Autosize&nbsp;</button></div></div>';
			var itemsel = '<div id="div-itemsel" style="padding-bottom:5px"><div class="btn-group" id="div-rowsel" style="display:none;padding:0;"><button class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#" id="btn-rowsel" style="width:100px;"></button><ul class="dropdown-menu" id="rowdropdown"></ul></div><div class="btn-group" id="div-cursel" style="display:inline-block;margin:0;"><button class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#" id="btn-cursel" style="width:210px;"></button><ul class="dropdown-menu" id="itemdropdown"></ul></div></div>';
			var caption = '<div id="div-caption" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Text</strong></label><div class="controls"><input class="input-small" id="input-caption"style="width:135px" type="text"></div></div>';
			var caption2 = '<div id="div-caption2" class="control-group" style="display:none"><label class="control-label"><strong style="color:#3a3a3a">Subtext</strong></label><div class="controls"><input class="input-small" id="input-caption2"style="width:135px" type="text"></div></div>';
			var indent = '<div id="div-indent" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Indent</strong></label><div class="controls"><input class="input-small" id="input-indent"style="width:50px" type="text"></div></div>';
			var dividersubmenu = '<div id="div-dividersubmenu"><div class="btn-group" data-toggle="buttons-checkbox"><button id="divider" style="width:70px;" class="btn btn-small">Divider</button><button id="submenu" style="width:70px;" class="btn btn-small">Submenu</button><button id="selected" style="width:70px;" class="btn btn-small">Selected</button></div></div>';
			var deletebtn = '<div id="div-delete" style="padding-bottom:10px;"><div class="btn-group"><button id="disabled" style="width:104px;" class="btn btn-small" data-toggle="button">Disabled</button><button id="delete" style="width:104px;" class="btn btn-small btn-danger">Delete</button></div></div>';
			var state = '<div id="div-state" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">State</strong></label><div class="controls"><div class="btn-group" data-toggle="buttons-radio"><button id="state-o" class="btn btn-small">O</button><button id="state-c" class="btn btn-small">C</button><button id="state-f" class="btn btn-small">F</button><button id="state-n" class="btn btn-small">N</button></div></div></div>';
			var type = '<div id="div-type" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Type</strong></label><div class="controls"><div class="btn-group" data-toggle="buttons-radio"><button id="type-rr" class="btn btn-small '+(props.type == 1 ? 'active' : '')+'">RR</button><button id="type-r" class="btn btn-small '+(props.type == 2 ? 'active' : '')+'">R</button></div></div></div>';
			var color = '<div id="div-color" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Colors</strong></label><div class="controls"><div id="color-options" class="btn-group" data-toggle="buttons-radio"><button id="color-b" class="btn btn-small"><i class="icon-stop"></i></button><button id="color-bg" class="btn btn-small"><i class="icon-stop"></i></button><button id="color-s" class="btn btn-small"><i class="icon-stop"></i></button><button id="color-te" class="btn btn-small"><i class="icon-stop"></i></button><button id="color-td" class="btn btn-small"><i class="icon-stop"></i></button></div></div></div>';
			var size = '<div id="div-size" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Fontsize</strong></label><div class="controls"><input class="input-small" id="input-size"style="width:50px" type="text" value="'+(props.size ? props.size : paper.menu.properties.defaults.size)+'" placeholder="'+(props.size ? props.size : paper.menu.properties.defaults.size)+'"></div></div>';
			var align = '<div id="div-align" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Align</strong></label><div class="controls"><div class="btn-group" data-toggle="buttons-radio"><button id="align-l" class="btn btn-small '+(props.align == 'left' ? 'active' : '')+'"><i class="icon-align-left"></i></button><button id="align-r" class="btn btn-small '+(props.align == 'right' ? 'active' : '')+'"><i class="icon-align-right"></i></button></div></div></div>';
			var html = '<div id="props">'+itemsel+'<div id="propsFor" style="display:none">'+$e.attr('id')+'</div><div id="propsForItem" style="display:none"></div><div id="propsForRow" style="display:none"></div><div class="form-horizontal">'+caption+caption2+indent+(props.icon ? state : '')+dividersubmenu+deletebtn+type+color+align+size+'</div>'+autosizedrop+'</div>';
			//ui.dismissPopovers();
			$e.popover({title:  '<strong style="color:#3a3a3a;" id="props-title">Menu Properties</strong>', content: html, placement: 'auto', attachTo: '#tbs-popover', onShow: function(){
				paper._buildDimensions($e, '#div-size');
				paper._buildOpacity($e, '#div-size',props);
				paper._buildLinkInspector($e, '#div-caption2',props, true);
				paper.menu.properties._reconstructRowItems(props.items);
				if(props.items.length > 0) $('#rowitem-0').trigger('click');
				if(props.items[0].length > 0) $('#selitem-0').trigger('click');
				else $('#selitem-new').trigger('click');
				$('#input-indent').spinner({min: 0, max: 10});
				$('#input-indent').keydown(function(ev){
					if(ev.which == 13){
						paper._changeItemData('i',$(this).val(),false);
						ev.preventDefault();
					}
				});
				$('#input-indent').blur(function(ev){
					paper._changeItemData('i',$(this).val());
				});
				$('#input-indent').change(function(ev){
					paper._changeItemData('i',$(this).val(),false);
				});
				$('#input-size').spinner({min: 8, max: 98});
				$('#input-size').keydown(function(ev){
					if(ev.which == 13){
						paper._changeProperty('size', $(this).val() + 'px', false);
						ev.preventDefault();
					}
				});
				$('#input-size').blur(function(ev){
					paper._changeProperty('size', $(this).val() + 'px');
				});
				$('#input-size').change(function(ev){
					paper._changeProperty('size', $(this).val() + 'px', false);
				});
				$('#input-caption').blur(function(ev){
					$('#btn-cursel').html($.escapeHTML($(this).val()) + '&nbsp;');
					paper._changeItemData('t',$(this).val());
				});
				$('#input-caption').keyup(function(ev){
					$('#btn-cursel').html($.escapeHTML($(this).val()) + '&nbsp;');
					paper._changeItemData('t',$(this).val(), false);
				});
				$('#input-caption2').blur(function(ev){
					paper._changeItemData('t1',$(this).val());
				});
				$('#input-caption2').keyup(function(ev){
					paper._changeItemData('t1',$(this).val(), false);
				});
				$('#state-o').click(function(ev){
					paper._changeItemData('st',1);
				});
				$('#state-c').click(function(ev){
					paper._changeItemData('st',2);
				});
				$('#state-n').click(function(ev){
					paper._changeItemData('st',3);
				});
				$('#state-f').click(function(ev){
					paper._changeItemData('st',4);
				});
				$('#type-rr').click(function(ev){
					paper._changeProperty('type', 1);
				});
				$('#type-r').click(function(ev){
					paper._changeProperty('type', 2);
				});
				//$('.selitem','#div-itemsel').click(paper.menu.properties._handleItemSelection);
				$('#autosize').click(function(ev){
					if($(this).hasClass('active')){
						paper._changeProperty('autosize', false);
						mockup.initResizable($('#element-resizer'));								
					}
					else{
						paper._changeProperty('autosize', true);
						$('#element-resizer').resizable('destroy');
					}
				});
				$('#divider').click(function(ev){
					var vi = paper._getItemData();
					if($(this).hasClass('active')){
						if($('#input-indent').val().length == 0)
							$('#input-indent').val('0');
						vi = {t: $('#input-caption').val(), s: false, d: false, sm: false, i: $('#input-indent').val()};
						$('#input-caption').prop('disabled', false);
						$('#input-caption2').prop('disabled', false);
						$('#input-indent').prop('disabled', false);
						$('#btn-cursel').html($.escapeHTML(vi.t) + '&nbsp;');
						$('#submenu').prop('disabled',false);
						$('#disabled').prop('disabled', false);
						$('#selected').prop('disabled', false);
						$('#submenu').removeClass('active');
						$('#disabled').removeClass('active');
						$('#selected').removeClass('active');
						if(v.icon){
							$('#state-o').prop('disabled', false);
							$('#state-c').prop('disabled', false);
							$('#state-n').prop('disabled', false);
							$('#state-f').prop('disabled', false);
							$('#state-o').removeClass('active');
							$('#state-c').removeClass('active');
							$('#state-n').removeClass('active');
							$('#state-f').removeClass('active');
							vi.st = 2;
						}
					}
					else{
						vi = {};
						vi.t = ')__div__(';
						$('#btn-cursel').html('Divider&nbsp;');
						$('#input-caption').prop('disabled', true);
						$('#input-caption2').prop('disabled', true);
						$('#input-indent').prop('disabled', true);
						$('#submenu').prop('disabled',true);
						$('#disabled').prop('disabled', true);
						$('#selected').prop('disabled', true);
						if(props.icon){
							$('#state-o').prop('disabled', true);
							$('#state-c').prop('disabled', true);
							$('#state-n').prop('disabled', true);
							$('#state-f').prop('disabled', true);
						}
					}
					paper._putItemData(vi);
					var eid = $('#propsFor').text();
					var $el = $('#'+eid);
					paper.redraw($el);
					stateManager.writeAction('custom',{element: eid});
					paper.menu.properties._reconstructSelItems($el.data('properties').items);
				});
				$('#delete').click(function(ev){
					var i = $('#propsForItem').text();
					var j = $('#propsForRow').text();
					if(i=='n') return;
					var $el = $('#' + $('#propsFor').text());
					var props = $el.data('properties');
					props.items[j].splice(i,1);
					if(props.items[j].length == 0){
						props.items.splice(j,1);
						paper.menu.properties._reconstructRowItems(props.items);
						paper.menu.properties._selectNextRow();
						j = $('#propsForRow').text();
					}
					else{
						paper.menu.properties._reconstructSelItems(props.items[j]);
						paper.menu.properties._selectNextItem();
					}
					$el.data('properties',props);
					paper.redraw($el);
					stateManager.writeAction('custom',{element: $el.attr('id')});
				});
				$('#submenu').click(function(ev){
					if($(this).hasClass('active'))
						paper._changeItemData('sm',false);
					else
						paper._changeItemData('sm',true);
				});
				$('#disabled').click(function(ev){
					if($(this).hasClass('active'))
						paper._changeItemData('d',false);
					else
						paper._changeItemData('d',true);
				});
				$('#selected').click(function(ev){
					if($(this).hasClass('active'))
						paper._changeItemData('s',false);
					else
						paper._changeItemData('s',true);
				});
				$('#align-l').click(function(ev){
					paper._changeProperty('align', 'left');
				});
				$('#align-r').click(function(ev){
					paper._changeProperty('align', 'right');
				});
				$('#color-b').data('val', props.color.b ? props.color.b : paper.menu.properties.defaults.color.b);
				$('#color-bg').data('val', props.color.bg ? props.color.bg : paper.menu.properties.defaults.color.bg);
				$('#color-te').data('val', props.color.te ? props.color.te : paper.menu.properties.defaults.color.te);
				$('#color-td').data('val', props.color.td ? props.color.td : paper.menu.properties.defaults.color.td);
				$('#color-s').data('val', props.color.s ? props.color.s : paper.menu.properties.defaults.color.s);
				$('#color-b').css('color', props.color.b ? props.color.b : paper.menu.properties.defaults.color.b);
				$('#color-bg').css('color', props.color.bg ? props.color.bg : paper.menu.properties.defaults.color.bg);
				$('#color-te').css('color', props.color.te ? props.color.te : paper.menu.properties.defaults.color.te);
				$('#color-td').css('color', props.color.td ? props.color.td : paper.menu.properties.defaults.color.td);
				$('#color-s').css('color', props.color.s ? props.color.s : paper.menu.properties.defaults.color.s);
				$('#color-b').click(function(ev){
					paper._showColorPicker(ev, this, 'color.b');
				});
				$('#color-bg').click(function(ev){
					paper._showColorPicker(ev, this, 'color.bg');
				});
				$('#color-te').click(function(ev){
					paper._showColorPicker(ev, this, 'color.te');
				});
				$('#color-td').click(function(ev){
					paper._showColorPicker(ev, this, 'color.td');
				});
				$('#color-s').click(function(ev){
					paper._showColorPicker(ev, this, 'color.s');
				});
				//Tooltips
				$('#itemdropdown').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Drag to change positions', hideAfter: 2000});
				$('#type-rr').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Rounded Rect', hideAfter: 2000});
				$('#type-r').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Rectangular', hideAfter: 2000});
				$('#align-l').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Left', hideAfter: 2000});
				$('#align-r').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Right', hideAfter: 2000});
				$('#color-b').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Border', hideAfter: 2000});
				$('#color-bg').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Background', hideAfter: 2000});
				$('#color-te').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Text Enabled', hideAfter: 2000});
				$('#color-td').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Text Disabled', hideAfter: 2000});
				$('#color-s').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Selected', hideAfter: 2000});
				$('#element-resizer').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Click an item to switch to its properties', hideAfter: 3000});
				if(onshow) onshow();
			}, onClick: function(ev){paper._popoverClick(ev,$e)}, onHide: function(){
				paper._destroyAllTooltips($e);
			}});
			$e.popover('show');
			ui.popoverContainers.push($e);
		}
	},
	draw: function(canvas, l, t, w, h, v){
		var $div = $(canvas.canvas.parentElement);
		var iostable = $div.attr('data-element')=='tableview';
		var r = v.type == 1 ? 5 : 0, sc = 0, ih = Math.floor(parseInt(v.size)*25/20), inp = (parseInt(v.size))*25/20;
		if(v.autosize) w = 0;
		if(!(v.noarrow === false || v.noarrow === true)) v.noarrow = true;
		v.items = v.items[0];
		var h3 = 0, linkflag = false;
		for(var i in v.items){
			if(v.items[i].t == ')__div__(') ++sc;
			else{
				var vi = v.items[i];
				if(!linkflag && vi.linkto)
					linkflag = true;
				var cp = '<p class="element-caption-para" style="margin: 0;vertical-align:middle;font-size:'+v.size+';text-align:'+v.align+';word-break:break-word;word-wrap:break-word;">'+(vi.t1 ? '<strong>'+vi.t+'</strong><br><small>'+vi.t1+'</small>' : vi.t)+'</p>';
				var as = paper._getTextAutoSize(cp);
				if(v.autosize)
					w = Math.max(w, as.width + (vi.i ? vi.i * inp : 0) + (vi.sm ? ih*6/9 : 0)) + (iostable ? (vi.sm ? -5 : 2) : 0);
				h3 = Math.max(h3, as.height + 10);
				if(vi.sm && v.noarrow) v.noarrow = false;
			}
		}
		if(linkflag)
			$div.addClass('has-link');
		else
			$div.removeClass('has-link');
		var nsc = (v.items.length-sc);
		var h1 = parseInt(v.size)*25/15, h2 = h/nsc;
		if(h2 > h1 && !v.autosize) h1 = h2;
		h1 = Math.max(h1,h3);
		if(v.autosize){
			h = h1*nsc+4;
			w += (v.noarrow ? 4 : ih + 3)+(v.icon ? 10 : 0)+15;
			paper._resize($div,w,h);
		}
		canvas.rect(l+1,t+1,Math.floor(w-2),Math.floor(h-2),r).attr({'stroke-width': '2px', 'stroke': v.color.b, 'fill': v.color.bg});
		for(var i=0,sc=0;i<v.items.length;++i){
			var vi = v.items[i];
			if(vi.s){
				var t1 = 0;
				if(r > 0){
					if(i == 0){
						t1 = t+1;
						canvas.rect(l+1,t1,w-2,r*2,r).attr({'stroke':'none', 'fill': v.color.s});
					}
					if(i == v.items.length-1){
						t1 = t+h-r*2;
						canvas.rect(l+1,t1,w-2,r*2,r).attr({'stroke':'none', 'fill': v.color.s});
					}
				}
				h2 = h1;
				if(i==0){
					t1 = t+1+r;
					h2 -= r;
				}
				else
					t1 = t+2+(i-sc)*h1;
				if(i==v.items.length-1)
					h2 -= r;
				canvas.rect(l+1,t1,w-2,h2).attr({'fill':v.color.s,'stroke':v.color.s});
			}
			if(vi.t == ')__div__('){
				if(i > 0 && i < v.items.length-1)
					paper.path(canvas, ['M', l+1,Math.floor(t+1+((i-sc)*h1)),'l',w-2,0]).attr({'stroke-width': '2px', 'stroke': v.color.b});
				sc++;
			}
			else{
				ih = parseInt(v.size)*25/13;
				var lc = $.extend(true, {}, paper.label.properties.defaults, {caption: (vi.t1 ? '<strong>'+vi.t+'</strong><br><small>'+vi.t1+'</small>' : iostable ? '<strong>'+vi.t+'</strong>' : vi.t)+'<span class="itemindex" style="display:none">'+(iostable ? parseInt(i/2) : i)+'</span>', autosize: false, size: v.size, align: v.align});
				lc.color = {'bg': 'none', 't': vi.d ? v.color.td : v.color.te};
				paper.label.draw(canvas,l+1,t+1+((i-sc)*h1),w-2,h1,lc);
				var $ecl = $('.element-caption:last', $div);
				if(v.align == 'left')
					$ecl.find('.element-caption-para').css({'left': ((vi.i ? vi.i*inp : 0) + (vi.st ? inp + parseInt(v.size)*5/14 : 0))+'px', 'width': (w-2)+'px'});
				else
					$ecl.find('.element-caption-para').css({'text-align': 'right', 'width': (w - (vi.i ? vi.i*inp : 0) - (v.icon ? ih : 0) - 5)+'px'});
				paper._drawLink(vi, $ecl);
				if(vi.sm && vi.t != ')__div__(' && !v.noarrow){
					var ic = $.extend(true, {}, paper.icon.properties.defaults, {'border': null});
					ic.color.s = vi.d ? v.color.td : v.color.te;
					if(iostable){
						if(v.align == 'left'){
							paper.icon.draw(canvas,Math.floor(l+w-ih-2+ih*1/5),Math.floor(t+1+((i-sc)*h1)+((h1-ih)/2)+ih*1/5),ih,ih*3/5,$.extend(ic, {'icon': 'chevron-right'}));
						}
						else if(v.align == 'right')
							paper.icon.draw(canvas,Math.floor(l+1-ih*1/5),Math.floor(t+1+((i-sc)*h1)+((h1-ih)/2)+ih*1/5),ih,ih*3/5,$.extend(ic, {'icon': 'chevron-left'}));
					}
					else{
						if(v.align == 'left')
							paper.icon.draw(canvas,Math.floor(l+w-ih*6/9-5),Math.floor(t+2+((i-sc)*h1)+((h1-ih)/2)),ih,ih,$.extend(ic, {'icon': 'caret-right'}));
						else if(v.align == 'right')
							paper.icon.draw(canvas,Math.floor(l+5-ih*3/9),Math.floor(t+2+((i-sc)*h1)+((h1-ih)/2)),ih,ih,$.extend(ic, {'icon': 'caret-left'}));
					}
				}
				else if(v.icon && vi.t != ')__div__(' && vi.st != 3){
					ih = parseInt(v.size)*25/13;
					var ic = $.extend(true, {}, paper.icon.properties.defaults, {'border': null});
					ic.color.s = vi.d ? v.color.td : v.color.te;
					var nvi = v.items[i+1];
					//var open = nvi && nvi.i > vi.i ? true : false;
					if(v.icon == 1){
						if(v.align=='left')
							paper.icon.draw(canvas,Math.floor(l+2+(vi.i ? vi.i*inp : 0)),Math.floor(t+1+((i-sc)*h1)+((h1-ih)/2)),ih,ih,$.extend(ic, {'icon': vi.st == 1 ? 'caret-down' : 'caret-right'}));
						else
							paper.icon.draw(canvas,Math.floor(l+w-ih-2-(vi.i ? vi.i*inp : 0)),Math.floor(t+1+((i-sc)*h1)+((h1-ih)/2)),ih,ih,$.extend(ic, {'icon': vi.st == 1 ? 'caret-down' : 'caret-left'}));
					}
					else if(v.icon == 2){
						if(v.align=='left'){
							//canvas.rect(Math.floor(l+4+(vi.i ? vi.i*ih : 0)),Math.floor(t+2+((i-sc)*h1)+((h1-ih)/2)),ih-2,ih).attr({'stroke-width': '2px', 'stroke':v.color.b});
							if(vi.st == 1)
								paper.icon.draw(canvas,Math.floor(l+(vi.i ? vi.i*inp : 0)),Math.floor(t+1+((i-sc)*h1)+((h1-ih)/2)),ih-2,ih,$.extend(ic, {'icon': 'check'}));
							else if(vi.st == 2)
								paper.icon.draw(canvas,Math.floor(l+(vi.i ? vi.i*inp : 0)),Math.floor(t+1+((i-sc)*h1)+((h1-ih)/2)),ih-2,ih,$.extend(ic, {'icon': 'check-empty'}));
						}
						else{
							//canvas.rect(Math.floor(l-2+w-ih-(vi.i ? vi.i*ih : 0)),Math.floor(t+3+((i-sc)*h1)+((h1-ih)/2)),ih-2,ih).attr({'stroke-width': '2px', 'stroke':v.color.b});
							if(vi.st == 1)
								paper.icon.draw(canvas,Math.floor(l-2+w-ih-(vi.i ? vi.i*inp : 0)),Math.floor(t+2+((i-sc)*h1)+((h1-ih)/2)),ih-2,ih,$.extend(ic, {'icon': 'check'}));
							else if(vi.st == 2)
								paper.icon.draw(canvas,Math.floor(l-2+w-ih-(vi.i ? vi.i*inp : 0)),Math.floor(t+2+((i-sc)*h1)+((h1-ih)/2)),ih-2,ih,$.extend(ic, {'icon': 'check-empty'}));
						}
					}
					else if(v.icon == 4){
						if(v.align=='left')
							paper.icon.draw(canvas,Math.floor(l+(vi.i ? vi.i*inp : 0)),Math.floor(t+1+((i-sc)*h1)+((h1-ih)/2)),ih,ih,$.extend(ic, {'icon': vi.st == 1 ? 'folder-open' : vi.st == 4 ? 'file' : 'folder-close'}));
						else
							paper.icon.draw(canvas,Math.floor(l+w-ih-2-(vi.i ? vi.i*inp : 0)),Math.floor(t+1+((i-sc)*h1)+((h1-ih)/2)),ih,ih,$.extend(ic, {'icon': vi.st == 1 ? 'folder-open' : vi.st == 4 ? 'file' : 'folder-close'}));
					}
					else if(v.icon == 3){
						if(v.align=='left')
							paper.icon.draw(canvas,Math.floor(l+2+(vi.i ? vi.i*inp : 0)),Math.floor(t+1+((i-sc)*h1)+((h1-ih)/2)),ih,ih,$.extend(ic, {'icon': vi.st == 1 ? 'minus-sign' : 'plus-sign'}));
						else
							paper.icon.draw(canvas,Math.floor(l+w-ih-2-(vi.i ? vi.i*inp : 0)),Math.floor(t+1+((i-sc)*h1)+((h1-ih)/2)),ih,ih,$.extend(ic, {'icon': vi.st == 1 ? 'minus-sign' : 'plus-sign'}));
					}
				}				
			}
		}
		canvas.rect(l+1,t+1,Math.floor(w-2),Math.floor(h-2),r).attr({'stroke-width': '2px', 'stroke': v.color.b});
		paper.setItemClickHandler($div);
	}
}
paper.selectbox = {
	properties: {
		min: {
			width: 30,
			height: 15
		},
		defaults: $.extend($.extend(true, {}, paper.menu.properties.defaults, {noarrow: true}), {items: [[{'t': 'Item 1', d: false, s:false},{'t': 'Item 2', d: false, s: true},{'t': 'Item 3', d: true, s:false}]]}),
		show: function($e, onshow){
			paper.menu.properties.show($e, function(){
				var props = paper._getDefaultedProperties($e.data('properties'), paper.selectbox.properties.defaults);
				$('#props-title').text('Selectbox Properties');
				$('#selected').prependTo($('.btn-group','#div-delete'));
				$('#selected').attr('data-toggle','button');
				$('#div-dividersubmenu').remove();
				$('#disabled').css('width','70px');
				$('#delete').css('width','70px');
				$('#div-indent').remove();
				$('<button id="align-c" class="btn btn-small '+(props.align == 'center' ? 'active' : '')+'"><i class="icon-align-center"></i></button>').insertBefore('#align-r');
				$('#align-c').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Center', hideAfter: 2000});
				$('#align-c').click(function(ev){
					paper._changeProperty('align', 'center');
				});
				if(onshow) onshow();
			});
		}
	},
	draw: paper.menu.draw
}
paper.tooltip = {
	properties: {
		min: {
			width: 40,
			height: 25
		},
		defaults: $.extend(true, {}, paper.button.properties.defaults, {'caption': 'Tooltip','ad':'l2', height: 50, color: {'b': '#000000', 'bg': '#ffffff', 't': '#000000'}}),
		_setApos: function(d){
			$('#apos-1').tooltip('destroy');
			$('#apos-2').tooltip('destroy');
			$('#apos-3').tooltip('destroy');
			switch(d){
				case 'l':
				case 'r':
					$('#apos-1').text('T');
					$('#apos-2').text('M');
					$('#apos-3').text('B');
					$('#apos-1').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Top', hideAfter: 2000});
					$('#apos-2').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Middle', hideAfter: 2000});
					$('#apos-3').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Bottom', hideAfter: 2000});
					break;
				case 't':
				case 'b':
					$('#apos-1').text('L');
					$('#apos-2').text('C');
					$('#apos-3').text('R');
					$('#apos-1').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Left', hideAfter: 2000});
					$('#apos-2').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Center', hideAfter: 2000});
					$('#apos-3').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Right', hideAfter: 2000});
					break;
			}
		},
		show: function($e, onshow){
			paper.button.properties.show($e, function(){
				var props = paper._getDefaultedProperties($e.data('properties'), paper.tooltip.properties.defaults);
				$('#props-title').text('Tooltip Properties');
				$('#div-icons').remove();
				$('#type-p').remove();
				$('<div id="div-apoints" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Points</strong></label><div class="controls"><div class="btn-group" data-toggle="buttons-radio"><button id="apoints-l" class="btn btn-small">L</button><button id="apoints-r" class="btn btn-small">R</button><button id="apoints-t" class="btn btn-small">T</button><button id="apoints-b" class="btn btn-small">B</button></div></div></div>').insertAfter('#div-type');
				$('<div id="div-apos" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Position</strong></label><div class="controls"><div class="btn-group" data-toggle="buttons-radio"><button id="apos-1" class="btn btn-small"></button><button id="apos-2" class="btn btn-small"></button><button id="apos-3" class="btn btn-small"></button></div></div></div>').insertAfter('#div-apoints');
				$('#apoints-'+props.ad.charAt(0)).addClass('active');
				$('#apoints-l').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Left', hideAfter: 2000});
				$('#apoints-r').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Right', hideAfter: 2000});
				$('#apoints-t').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Top', hideAfter: 2000});
				$('#apoints-b').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Bottom', hideAfter: 2000});
				$('#apos-'+props.ad.charAt(1)).addClass('active');
				paper.tooltip.properties._setApos(props.ad.charAt(0));
				$('.btn', '#div-apoints').click(function(ev){
					var a1 = $(this).attr('id').substr(-1);
					paper.tooltip.properties._setApos(a1);
					a1 = a1 + $('.active','#div-apos').attr('id').substr(-1);
					paper._changeProperty('ad', a1);
				});
				$('.btn', '#div-apos').click(function(ev){
					var a1 = $('.active','#div-apoints').attr('id').substr(-1);
					paper.tooltip.properties._setApos(a1);
					a1 =  a1 + $(this).attr('id').substr(-1);
					paper._changeProperty('ad', a1);
				});
				$('#dropdown').remove();
				$('#dropup').remove();
				$('#autosize').css('width','210px');
			});
		}
	},
	draw: function(canvas, l, t, w, h, v){
		var $div = $(canvas.canvas.parentElement);
		if(v.linkto)
			$div.addClass('has-link');
		var ad1 = v.ad.charAt(0);
		var ad2 = v.ad.charAt(1);
		var w1 = 15, h1 = 8;
		var l1,t1;

		if(v.autosize){
			var cp = '<p class="element-caption-para" style="margin: 0;vertical-align:middle;font-size:'+v.size+';text-align:'+v.align+';word-break:break-word;word-wrap:break-word;">'+v.caption.nl2br()+'</p>';
			var as = paper._getTextAutoSize(cp);
			h = as.height, w = as.width;
			h += 10;
			if(v.type == 2){
				r = h/2;
				w += r*2;
			}
			else
				w += 10;

			w += (v.icons.right ? 25 : 0) + (v.icons.left ? 25 : 0) + (v.menu != null ? (v.type == 2 ? r + 5 : is + 5) : 0);
			w = Math.floor(w), h = Math.floor(h);

			if(ad1 == 'l' || ad1 == 'r')
				w += h1;
			else
				h += h1;

			paper._resize($(canvas.canvas.parentElement),w,h);
		}
		if(ad1 == 'l' || ad1 == 'r'){
			w -= h1;
			v.ad = h < w1+15 ? ad1+'2' : v.ad;
		}
		else{
			h -= h1;
			v.ad = w < w1+15 ? ad1+'2' : v.ad;
		}
		if(ad1 == 't')
			t += h1;
		else if(ad1 == 'l')
			l += h1;
		v.autosize = false;
		paper.button.draw(canvas, l, t, w, h, v);

		switch(v.ad){
			case 'l1':				
				paper.path(canvas, ['M', l+1, t+10+w1,'l',-h1+1,-w1/2,'l',h1-1,-w1/2,'z']).attr({'stroke-width':'2px', 'stroke': v.color.b, 'fill':v.color.bg});
				paper.path(canvas, ['M', l+4, t+10+w1,'l',-h1+1,-w1/2,'l',h1-1,-w1/2,'z']).attr({'stroke-width':'2px', 'stroke': v.color.bg, 'fill':v.color.bg});
				break;
			case 'l2':
				paper.path(canvas, ['M', l+1, t+h/2+w1/2,'l',-h1+1,-w1/2,'l',h1-1,-w1/2,'z']).attr({'stroke-width':'2px', 'stroke': v.color.b, 'fill':v.color.bg});
				paper.path(canvas, ['M', l+4, t+h/2+w1/2,'l',-h1+1,-w1/2,'l',h1-1,-w1/2,'z']).attr({'stroke-width':'2px', 'stroke': v.color.bg, 'fill':v.color.bg});
				break;
			case 'l3':
				paper.path(canvas, ['M', l+1, t+h-10,'l',-h1+1,-w1/2,'l',h1-1,-w1/2,'z']).attr({'stroke-width':'2px', 'stroke': v.color.b, 'fill':v.color.bg});
				paper.path(canvas, ['M', l+4, t+h-10,'l',-h1+1,-w1/2,'l',h1-1,-w1/2,'z']).attr({'stroke-width':'2px', 'stroke': v.color.bg, 'fill':v.color.bg});
				break;
			case 'r1':
				paper.path(canvas, ['M', l-1+w, t+10,'l',h1-1,w1/2,'l',-h1+1,w1/2,'z']).attr({'stroke-width':'2px', 'stroke': v.color.b, 'fill':v.color.bg});
				paper.path(canvas, ['M', l-4+w, t+10,'l',h1-1,w1/2,'l',-h1+1,w1/2,'z']).attr({'stroke-width':'2px', 'stroke': v.color.bg, 'fill':v.color.bg});
				break;
			case 'r2':
				paper.path(canvas, ['M', l-1+w, t+h/2-w1/2,'l',h1-1,w1/2,'l',-h1+1,w1/2,'z']).attr({'stroke-width':'2px', 'stroke': v.color.b, 'fill':v.color.bg});
				paper.path(canvas, ['M', l-4+w, t+h/2-w1/2,'l',h1-1,w1/2,'l',-h1+1,w1/2,'z']).attr({'stroke-width':'2px', 'stroke': v.color.bg, 'fill':v.color.bg});
				break;
			case 'r3':
				paper.path(canvas, ['M', l-1+w, t+h-10-w1,'l',h1-1,w1/2,'l',-h1+1,w1/2,'z']).attr({'stroke-width':'2px', 'stroke': v.color.b, 'fill':v.color.bg});
				paper.path(canvas, ['M', l-4+w, t+h-10-w1,'l',h1-1,w1/2,'l',-h1+1,w1/2,'z']).attr({'stroke-width':'2px', 'stroke': v.color.bg, 'fill':v.color.bg});
				break;
			case 't1':
				paper.path(canvas, ['M', l+10, t+1,'l',w1/2,-h1+1,'l',w1/2,h1-1,'z']).attr({'stroke-width':'2px', 'stroke': v.color.b, 'fill':v.color.bg});
				paper.path(canvas, ['M', l+10, t+4,'l',w1/2,-h1+1,'l',w1/2,h1-1,'z']).attr({'stroke-width':'2px', 'stroke': v.color.bg, 'fill':v.color.bg});
				break;
			case 't2':
				paper.path(canvas, ['M', l+w/2-w1/2, t+1,'l',w1/2,-h1+1,'l',w1/2,h1-1,'z']).attr({'stroke-width':'2px', 'stroke': v.color.b, 'fill':v.color.bg});
				paper.path(canvas, ['M', l+w/2-w1/2, t+4,'l',w1/2,-h1+1,'l',w1/2,h1-1,'z']).attr({'stroke-width':'2px', 'stroke': v.color.bg, 'fill':v.color.bg});
				break;
			case 't3':
				paper.path(canvas, ['M', l+w-10-w1, t+1,'l',w1/2,-h1+1,'l',w1/2,h1-1,'z']).attr({'stroke-width':'2px', 'stroke': v.color.b, 'fill':v.color.bg});
				paper.path(canvas, ['M', l+w-10-w1, t+4,'l',w1/2,-h1+1,'l',w1/2,h1-1,'z']).attr({'stroke-width':'2px', 'stroke': v.color.bg, 'fill':v.color.bg});
				break;
			case 'b1':
				paper.path(canvas, ['M', l+10, t+h-1,'l',w1/2,h1-1,'l',w1/2,-h1+1,'z']).attr({'stroke-width':'2px', 'stroke': v.color.b, 'fill':v.color.bg});
				paper.path(canvas, ['M', l+10, t+h-4,'l',w1/2,h1-1,'l',w1/2,-h1+1,'z']).attr({'stroke-width':'2px', 'stroke': v.color.bg, 'fill':v.color.bg});
				break;
			case 'b2':
				paper.path(canvas, ['M', l+w/2-w1/2, t+h-1,'l',w1/2,h1-1,'l',w1/2,-h1+1,'z']).attr({'stroke-width':'2px', 'stroke': v.color.b, 'fill':v.color.bg});
				paper.path(canvas, ['M', l+w/2-w1/2, t+h-4,'l',w1/2,h1-1,'l',w1/2,-h1+1,'z']).attr({'stroke-width':'2px', 'stroke': v.color.bg, 'fill':v.color.bg});
				break;
			case 'b3':
				paper.path(canvas, ['M', l+w-10-w1, t+h-1,'l',w1/2,h1-1,'l',w1/2,-h1+1,'z']).attr({'stroke-width':'2px', 'stroke': v.color.b, 'fill':v.color.bg});
				paper.path(canvas, ['M', l+w-10-w1, t+h-4,'l',w1/2,h1-1,'l',w1/2,-h1+1,'z']).attr({'stroke-width':'2px', 'stroke': v.color.bg, 'fill':v.color.bg});
				break;
		}
		paper._drawLink(v, $('element-caption', $div));
	}
}
paper.barchart = {
	properties: {
		defaults: {
			orient: 2,
			gs: true,
			a: {x: true, y: true},
			height: 200,
			width: 180
		},
		show: function($e, onshow){
			$('#props').remove();
			$('#props-title').remove();
			var props = paper._getDefaultedProperties($e.data('properties'), paper.button.properties.defaults);
			var gs = '<div id="div-gs" class="form-inline" style="text-align:center"><button id="greyscale" style="width:210px;" class="btn btn-small '+(props.gs ?'active' : '')+'" data-toggle="button">Greyscale</button></div>';
			var axes = '<div id="div-axes" class="form-inline" style="text-align:center"><div class="btn-group" data-toggle="buttons-checkbox"><button id="axis-x" style="width:105px" class="btn btn-small '+(props.a.x ? 'active' : '')+'">X-Axis</button><button id="axis-y" style="width:105px;" class="btn btn-small '+(props.a.y ? 'active' : '')+'">Y-Axis</button></div></div>';
			var orientation = '<div id="div-orientation" class="form-inline"><div class="btn-group" data-toggle="buttons-radio"><button id="orientation-v" style="width:105px;" class="btn btn-small '+(props.orient == 1 ? 'active' : '')+'">Vertical</button><button id="orientation-h" style="width:105px;" class="btn btn-small '+(props.orient == 2 ? 'active' : '')+'">Horizontal</button></div></div>';
			var html = '<div id="props"><div id="propsFor" style="display:none">'+$e.attr('id')+'</div><div class="form-horizontal"><div id="div-pseudo" style="margin-top:5px;"></div></div><div>'+gs+orientation+axes+'</div></div>';
			//ui.dismissPopovers();
			$e.popover({title:  '<strong style="color:#3a3a3a;" id="props-title">Bar Chart Properties</strong>', content: html, placement: 'auto', attachTo: '#tbs-popover', onShow: function(){
				paper._buildDimensions($e, '#div-pseudo');
				paper._buildOpacity($e, '#div-pseudo',props);
				paper._buildLinkInspector($e, '#div-pseudo',props);
				$('#axis-x').click(function(ev){
					if($(this).hasClass('active')){
						paper._changeProperty('a.x', false);
					}
					else{
						paper._changeProperty('a.x', true);
					}
				});
				$('#axis-y').click(function(ev){
					if($(this).hasClass('active')){
						paper._changeProperty('a.y', false);
					}
					else{
						paper._changeProperty('a.y', true);
					}
				});
				$('#greyscale').click(function(ev){
					if($(this).hasClass('active')){
						paper._changeProperty('gs', false);
					}
					else{
						paper._changeProperty('gs', true);
					}
				});
				$('#orientation-v').click(function(ev){
					paper._changeProperty('orient', 1);
				});
				$('#orientation-h').click(function(ev){
					paper._changeProperty('orient', 2);
				});
				if(onshow) onshow();
			}, onClick: function(ev){paper._popoverClick(ev,$e)}, onHide: function(){
				paper._destroyAllTooltips($e);				
			}});
			$e.popover('show');
			ui.popoverContainers.push($e);
		}
	},
	draw: function(canvas, l, t, w, h, v){
		var $div = $(canvas.canvas.parentElement);
		if(v.linkto)
			$div.addClass('has-link');
		if(v.a.x)
			paper.path(canvas, ['M',l,t+h-1,'l',w,0]).attr({'stroke-width':'2px','stroke':'#000000'});
		if(v.a.y)
			paper.path(canvas, ['M',l+1,t+1,'l',0,h]).attr({'stroke-width':'2px','stroke':'#000000'});
		var bars = 5;
		var cs = ['#afb6ff','#7e7e7f','#fcfcff','#585b7f','#c9cacc'];

		if(v.orient == 2){
			var h1 = (h/4*3)/bars, h2 = (h/4)/(bars+1);
			var ws = [w/5*3,w/3*2,w/2,w/3,w/30*29];
			for(var i=0;i<bars;++i){
				canvas.rect(l+1,Math.floor(t+((h2+h1)*i)+h2),Math.floor(ws[i]),Math.floor(h1)).attr({'stroke-width': '2px', 'stroke': '#000000', 'fill': v.gs ? '#c3c3c3' : cs[i]});
			}
		}
		else{
			var w1 = (w/4*3)/bars, w2 = (w/4)/(bars+1);
			var hs = [h/5*3,h/3*2,h/2,h/3,h/30*29];
			for(var i=0;i<bars;++i){
				canvas.rect(Math.floor(l+((w2+w1)*i)+w2),t-1+h-Math.floor(hs[i]),Math.floor(w1),Math.floor(hs[i])).attr({'stroke-width': '2px', 'stroke': '#000000', 'fill':  v.gs ? '#c3c3c3' : cs[i]});
			}
		}
		paper._drawLink(v, $div);
	}
}
paper.piechart = {
	properties: {
		defaults: {
			gs: true,
			height: 176,
			width: 176
		},
		show: function($e, onshow){
			$('#props').remove();
			$('#props-title').remove();
			var props = paper._getDefaultedProperties($e.data('properties'), paper.button.properties.defaults);
			var gs = '<div id="div-gs" class="form-inline" style="text-align:center"><button id="greyscale" style="width:210px;" class="btn btn-small '+(props.gs ?'active' : '')+'" data-toggle="button">Greyscale</button></div>';
			var html = '<div id="props"><div id="propsFor" style="display:none">'+$e.attr('id')+'</div><div class="form-horizontal"><div id="div-pseudo" style="margin-top:5px;"></div></div><div>'+gs+'</div></div>';
			//ui.dismissPopovers();
			$e.popover({title:  '<strong style="color:#3a3a3a;" id="props-title">Pie Chart Properties</strong>', content: html, placement: 'auto', attachTo: '#tbs-popover', onShow: function(){
				paper._buildDimensions($e, '#div-pseudo');
				paper._buildOpacity($e, '#div-pseudo',props);
				paper._buildLinkInspector($e, '#div-pseudo',props);
				$('#greyscale').click(function(ev){
					if($(this).hasClass('active')){
						paper._changeProperty('gs', false);
					}
					else{
						paper._changeProperty('gs', true);
					}
				});
				if(onshow) onshow();
			}, onClick: function(ev){paper._popoverClick(ev,$e)}, onHide: function(){
				paper._destroyAllTooltips($e);
			}});
			$e.popover('show');
			ui.popoverContainers.push($e);
		}
	},
	draw: function(canvas, l, t, w, h, v){
		var $div = $(canvas.canvas.parentElement)
		if(v.linkto)
			$div.addClass('has-link');
		var cs = ['#afb6ff','#7e7e7f','#fcfcff','#585b7f','#c9cacc'];
		var gs = ['#9F9593','#C0BFBF','#FFFFFF','#141414','#404040'];
		var ca = [10,72,142,222,294,370];
		w /= 2, h /=2;
		//w -= 1, h -= 1;
		var r = Math.min(w,h);
		--r;
		for(var i=0;i<5;++i)
			paper.path(canvas, $.merge(paper._sector(w,h,r,ca[i],ca[i+1]),['L',w,h,'z'])).attr({'stroke-width': '2px', 'stroke': '#000000', 'fill': v.gs ? gs[i] : cs[i]});
		paper._drawLink(v, $div);
	}
}
paper.linechart = {
	properties: {
		defaults: {
			gs: true,
			a: {x: true, y: true},
			height: 150,
			width: 200
		},
		show: function($e, onshow){
			$('#props').remove();
			$('#props-title').remove();
			var props = paper._getDefaultedProperties($e.data('properties'), paper.button.properties.defaults);
			var gs = '<div id="div-gs" class="form-inline" style="text-align:center"><button id="greyscale" style="width:210px;" class="btn btn-small '+(props.gs ?'active' : '')+'" data-toggle="button">Greyscale</button></div>';
			var axes = '<div id="div-axes" class="form-inline" style="text-align:center"><div class="btn-group" data-toggle="buttons-checkbox"><button id="axis-x" style="width:105px" class="btn btn-small '+(props.a.x ? 'active' : '')+'">X-Axis</button><button id="axis-y" style="width:105px;" class="btn btn-small '+(props.a.y ? 'active' : '')+'">Y-Axis</button></div></div>';
			var html = '<div id="props"><div id="propsFor" style="display:none">'+$e.attr('id')+'</div><div class="form-horizontal"><div id="div-pseudo" style="margin-top:5px;"></div></div><div>'+gs+axes+'</div></div>';
			//ui.dismissPopovers();
			$e.popover({title:  '<strong style="color:#3a3a3a;" id="props-title">Line Chart Properties</strong>', content: html, placement: 'auto', attachTo: '#tbs-popover', onShow: function(){
				paper._buildDimensions($e, '#div-pseudo');
				paper._buildOpacity($e, '#div-pseudo',props);
				paper._buildLinkInspector($e, '#div-pseudo',props);
				$('#axis-x').click(function(ev){
					if($(this).hasClass('active')){
						paper._changeProperty('a.x', false);
					}
					else{
						paper._changeProperty('a.x', true);
					}
				});
				$('#axis-y').click(function(ev){
					if($(this).hasClass('active')){
						paper._changeProperty('a.y', false);
					}
					else{
						paper._changeProperty('a.y', true);
					}
				});
				$('#greyscale').click(function(ev){
					if($(this).hasClass('active')){
						paper._changeProperty('gs', false);
					}
					else{
						paper._changeProperty('gs', true);
					}
				});
				if(onshow) onshow();
			}, onClick: function(ev){paper._popoverClick(ev,$e)}, onHide: function(){
				paper._destroyAllTooltips($e);
			}});
			$e.popover('show');
			ui.popoverContainers.push($e);
		}
	},
	draw: function(canvas, l, t, w, h, v){
		var $div = $(canvas.canvas.parentElement);
		if(v.linkto)
			$div.addClass('has-link');
		var cs = ['#afb6ff','#7e7e7f','#fcfcff','#585b7f','#c9cacc'];
		var gs = ['#c0bfbf','#404040'];
		var bars = 5;
		var w1 = w/(bars-1);
		var h1 = [h/5*3,h/6,h/5*2,h/3*2,h/5*4];
		var h2 = [h/2,h/15*9,h/9,h/4,h/5*3];
		var p1 = ['M',l+1,t+h-1], p2 = $.merge([],p1);
		for(var i=1;i<bars;++i){
			$.merge(p1,['L',w1*i,h1[i]]);
			$.merge(p2,['L',w1*i,h2[i]]);
		}
		paper.path(canvas,p1).attr({'stroke-width': '2px', 'stroke': v.gs ? '#c0bfbf' : '#11929E'});
		paper.path(canvas,p2).attr({'stroke-width': '2px', 'stroke': v.gs ? '#404040' : '#9C001A'});

		if(v.a.x)
			paper.path(canvas, ['M',l,t+h-1,'l',w,0]).attr({'stroke-width':'2px','stroke':'#000000'});
		if(v.a.y)
			paper.path(canvas, ['M',l+1,t+1,'l',0,h]).attr({'stroke-width':'2px','stroke':'#000000'});
		paper._drawLink(v, $div);
	}
}
paper.tabs = {
	properties: {
		min: {
			width: 85,
			height: 25
		},
		defaults: $.extend(true, {}, paper.selectbox.properties.defaults, {'autosize': true, 'width': 290, 'height':30, 'points':'t', 'items': [[{'t': 'Tab 1', d: false, s:true},{'t': 'Tab 2', d: false, s: false},{'t': 'Tab 3', d: true, s:false}]]}),
		show: function($e, onshow){
			paper.selectbox.properties.show($e, function(){
				$('#props-title').text('Tabs Properties');
				var props = paper._getDefaultedProperties($e.data('properties'), paper.tabs.properties.defaults);
				$('<div id="div-apoints" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Points</strong></label><div class="controls"><div class="btn-group" data-toggle="buttons-radio"><button id="apoints-t" class="btn btn-small '+(props.points == 't' ? 'active' : '')+'">T</button><button id="apoints-b" class="btn btn-small '+(props.points == 'b' ? 'active' : '')+'">B</button></div></div></div>').insertAfter('#div-type');
				$('#apoints-t').click(function(ev){
					paper._changeProperty('points','t');
				});
				$('#apoints-b').click(function(ev){
					paper._changeProperty('points','b');
				});
				$('#div-type').remove();
				$('#div-indent').remove();
				$('#element-resizer').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Click an item to switch to its properties', hideAfter: 3000});
				if(onshow) onshow();
			});
		}
	},
	draw: function(canvas, l, t, w, h, v){
		$div = $(canvas.canvas.parentElement);
		v.items = v.items[0];
		var sc = 0, wa = [], ass = [], linkflag = false;
		for(var i in v.items){
			if(!linkflag && v.items[i].linkto)
				linkflag = true;
			var cp = '<p class="element-caption-para" style="margin: 0;vertical-align:middle;font-size:'+v.size+';text-align:'+v.align+';word-break:break-word;word-wrap:break-word;">'+v.items[i].t+'</p>';
			ass.push(paper._getTextAutoSize(cp));
		}
		if(linkflag)
			$div.addClass('has-link');
		else
			$div.removeClass('has-link');
		if(v.autosize){
			w = 0;
			h = 0;
			for(var i in v.items){
				w +=  ass[i].width + 15 + (v.bc ? 2 : 0);
				h = Math.max(h, ass[i].height + 10)
			}
			w += 11;
			paper._resize($div,w,h);
		}
		var w1 = 0, lw = l+1,r = v.type == 1 ? 5 : v.type == 3 ? h/2 : 0;
		if(v.bc){
			v.color.td = v.color.te;
			if(v.points == 'r'){
				if(v.type == 3){
					paper.path(canvas, paper._sector(l+r,t+r,r-1,90,270)).attr({'stroke-width':'2px', 'stroke': v.color.b, 'fill': v.color.bg});
					paper.path(canvas,['M',l+r-1,t+1,'l',w-r-15-2,0,'l',15,h/2-1,'l',-15,h/2-1,'l',-(w-r-15-2),0]).attr({'stroke-width':'2px', 'stroke': v.color.b, 'fill': v.color.bg});
				}
				else{
					paper.path(canvas,['M',l+r+1,t+1,'l',w-r-15-2,0,'l',15,h/2-1,'l',-15,h/2-1,'l',-(w-r-15-2),0,'q',-r,0,-r,-r,'l',0,-(h-2*r-2),'q',0,-r,r,-r]).attr({'stroke-width':'2px', 'stroke': v.color.b, 'fill': v.color.bg});
				}
			}
			else if(v.points == 'l'){
				if(v.type == 3){
					paper.path(canvas,['M',l+15+1,t+1,'l',w-r-15+1,0,'l',0,h-2,'l',-(w-r-15+1),0,'l',-15,-h/2+1,'l',15,-h/2+1]).attr({'stroke-width':'2px', 'stroke': v.color.b, 'fill': v.color.bg});
					paper.path(canvas, paper._sector(l-r+w,t+r,r-1,270,90)).attr({'stroke-width':'2px', 'stroke': v.color.b, 'fill': v.color.bg});
				}
				else{
					paper.path(canvas,['M',l+15+1,t+1,'l',w-r-15-2,0,'q',r,0,r,r,'l',0,h-2-r*2,'q',0,r,-r,r,'l',-(w-r-15-2),0,'l',-15,-h/2+1,'l',15,-h/2+1]).attr({'stroke-width':'2px', 'stroke': v.color.b, 'fill': v.color.bg});
				}
			}
		}
		for(var i=0,sc=0,vil=v.items.length;i<vil;++i){
			w1 = v.autosize ? ass[i].width+10 : (w/vil-8);
			var vi = v.items[i];
			var lc = $.extend(true, {}, paper.label.properties.defaults, {caption: vi.t+'<span class="itemindex" style="display:none">'+i+'</span>', autosize: false, size: v.size, align: v.align});
			lc.color = {'bg': 'none', 't': vi.d ? v.color.td : v.color.te};
			if(v.bc){
				if(i == 0){
					if(v.points == 'r')					
						paper.label.draw(canvas,lw+(v.type == 3 ? 7.5 : 0),t+1,w1,h-2,lc);
					else if(v.points = 'l')
						paper.label.draw(canvas,lw+7.5,t+1,w1,h-2,lc);
				}
				else{
					paper.label.draw(canvas,lw+7.5,t+1,w1,h-2,lc);
					var m = v.points == 'r' ? 1 : -1;
					paper.path(canvas, ['M',l+1+lw+(v.points == 'l' ? 15 : -5),t+1,'l',15*m,h/2-1,'l',-15*m,h/2-1]).attr({'stroke-width': '2px', 'stroke': v.color.b});
				}
			}
			else{
				paper.label.draw(canvas,lw+7.5,t+1,w1,h-2,lc);
				if(v.points == 'b')
					paper.path(canvas, ['M',l+1+lw,t+1,'l',9,h-7,'q',2,5,5,5,'l',w1-15-(v.autosize ? 0 : 2),0,'q',4,0,5,-5,'l',9,-h+7,'z']).attr({'stroke-width': '2px', 'stroke': v.color.b, 'fill': vi.s ? v.color.s : v.color.bg});
				else
					paper.path(canvas, ['M',l+1+lw,t+h-1,'l',9,-h+7,'q',2,-5,5,-5,'l',w1-15-(v.autosize ? 0 : 2),0,'q',4,0,5,5,'l',9,h-7,'z']).attr({'stroke-width': '2px', 'stroke': v.color.b, 'fill': vi.s ? v.color.s : v.color.bg});
			}
			lw += w1+5;
			paper._drawLink(vi, $('.element-caption:last', $div));
		}
		paper.setItemClickHandler($div);
		if(v.bc) return;
		var w1 = 0, lw = l+1;
		for(var i=0,sc=0,vil=v.items.length;i<vil;++i){
			w1 = v.autosize ? ass[i].width+10 : (w/vil-8);
			var vi = v.items[i];
			if(vi.s){
				if(v.points == 'b')
					paper.path(canvas, ['M',l+1+lw,t+1,'l',9,h-7,'q',2,5,5,5,'l',w1-15-(v.autosize ? 0 : 2),0,'q',4,0,5,-5,'l',9,-h+7,'z']).attr({'stroke-width': '2px', 'stroke': v.color.b, 'fill': vi.s ? v.color.s : v.color.bg});
				else
					paper.path(canvas, ['M',l+1+lw,t+h-1,'l',9,-h+7,'q',2,-5,5,-5,'l',w1-15-(v.autosize ? 0 : 2),0,'q',4,0,5,5,'l',9,h-7,'z']).attr({'stroke-width': '2px', 'stroke': v.color.b, 'fill': vi.s ? v.color.s : v.color.bg});
			}
			lw += w1+5;	
		}
	}
}
paper.linkbar = {
	properties: {
			defaults: $.extend(true, {}, paper.selectbox.properties.defaults, {'divider': '|', 'color': {'te': '#0d4bfd', 'bg': 'none', 's': '#000000', 'td': '#000000'}, items: [[{'t': 'Link 1', d: false, s:false},{'t': 'Link 2', d: false, s: true},{'t': 'Link 3', d: true, s:false}]]}),
		show: function($e, onshow){
			paper.selectbox.properties.show($e, function(){
				$('#props-title').text('Linkbar Properties');
				$('#color-b').remove();
				$('#color-te').html('<i class="icon-stop"></i>');
				$('#color-te').tooltip('destroy');
				$('#color-te').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Text', hideAfter: 2000});
				$('#color-td').html('<i class="icon-stop"></i>');
				$('#color-td').tooltip('destroy');
				$('#color-td').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Divider', hideAfter: 2000});
				$('#disabled').remove();
				$('#selected').css('width','105px');
				$('#delete').css('width','105px');
				$('#div-type').remove();
				$('#div-indent').remove();
				$('<div id="div-divider" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Divider</strong></label><div class="controls"><input class="input-small" id="input-divider"style="width:135px" type="text"></div></div>').insertAfter('#div-delete');
				var props = paper._getDefaultedProperties($e.data('properties'), paper.selectbox.properties.defaults);
				$('#input-divider').val(props.divider);
				$('#input-divider').attr('placeholder',props.divider);
				$('#input-divider').blur(function(ev){
					paper._changeProperty('divider',$(this).val());
				});
				$('#input-divider').keyup(function(ev){
					paper._changeProperty('divider',$(this).val(), false);
				});
				$('#element-resizer').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Click an item to switch to its properties', hideAfter: 3000});
				if(onshow) onshow();
			});
		}
	},
	draw: function(canvas, l, t, w, h, v){
		var $div = $(canvas.canvas.parentElement);
		var lv = paper._getDefaultedProperties(v, paper.selectbox.properties.defaults);
		var cp = [], linkflag = false;
		v.items = v.items[0];
		var w2 = paper._getTextAutoSize('<span style="color:'+v.color.td+'">&nbsp;'+v.divider+'&nbsp;</span>').width;
		var w1 = (w-(w2*(v.items.length-1)))/v.items.length;
		for(var i in v.items){
			var vi = v.items[i];
			if(!linkflag && vi.linkto)
				linkflag = true;
			if(stateManager.currentState.previewMode && vi.linkto)
				var lf = true;
			cp.push((lf ? '<a href="'+vi.linkto+'">':'')+'<span style="'+(v.autosize ? '' : 'display:inline-block;width:'+(w1-4)+'px;')+'color:'+(vi.s ? v.color.s : v.color.te)+';'+(vi.s ? '' : 'text-decoration:underline;')+'">'+vi.t+'</span><span class="itemindex" style="display:none">'+i+'</span>'+(lf ? '</a>':''));
		}
		if(linkflag)
			$div.addClass('has-link');
		else
			$div.removeClass('has-link');
		cp = cp.join('<span style="color:'+v.color.td+'">&nbsp;'+v.divider+'&nbsp;</span>');
		lv.caption = cp;
		lv.autosize = v.autosize;
		paper.label.draw(canvas, l, t, w, h, lv);
		paper.setItemClickHandler($(canvas.canvas.parentElement));	
	}
}
paper.buttongroup = {
	properties: {
		defaults: $.extend(true, {}, paper.selectbox.properties.defaults, {'divider': '|', 'color': {'te': paper.button.properties.defaults.color.t, 'bg': paper.button.properties.defaults.color.bg, 'b': paper.button.properties.defaults.color.b, 'td': paper.selectbox.properties.defaults.color.td}, items: [[{'t': 'Button 1', d: false, s:false},{'t': 'Button 2', d: false, s: true},{'t': 'Button 3', d: true, s:false}]]}),
		show: function($e,onshow){
			paper.selectbox.properties.show($e, function(){
				$('#props-title').text('Button Group Title');
				$('#selected').remove();
				$('#disabled').css('width','105px');
				$('#delete').css('width','105px');
				$('#color-s').remove();
				$('#div-indent').remove();
				var props = paper._getDefaultedProperties($e.data('properties'), paper.selectbox.properties.defaults);
				$('<button id="type-p" class="btn btn-small '+(props.type == 3 ? 'active' : '')+'">P</button>').insertAfter('#type-r');
				$('#type-p').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Pill', hideAfter: 2000});
				$('#type-p').click(function(ev){
					paper._changeProperty('type', 3);
				});
				$('#element-resizer').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Click an item to switch to its properties', hideAfter: 3000});
				if(onshow) onshow;
			});
		}
	},
	draw: function(canvas, l, t, w, h, v){
		var $div = $(canvas.canvas.parentElement), linkflag = false;
		var bv = paper._getDefaultedProperties($.extend(true,{},v), paper.button.properties.defaults);
		v.items = v.items[0];
		if(v.autosize){
			w = 0, h = 0;
			for(var i in v.items){
				var vi = v.items[i];
				if(!linkflag && vi.linkto)
					linkflag = true;
				v.caption = '<span style="color:'+(vi.d ? v.color.td : v.color.te)+';">'+vi.t+'</span>';
				var cp = '<p class="element-caption-para" style="margin: 0;vertical-align:middle;font-size:'+v.size+';text-align:'+v.align+';word-break:break-word;word-wrap:break-word;">'+v.caption.nl2br()+'</p>';
				var as = paper._getTextAutoSize(cp);
				w += as.width+10, h = Math.max(h, as.height+10);
			}
			w += (v.type == 3 ? 2*h : 0);
			paper._resize($div,w,h);
		}
		if(linkflag)
			$div.addClass('has-link');
		else
			$div.removeClass('has-link');
		canvas.rect(l+1,t+1,w-2,h-2,v.type == 3 ? h/2 : v.type == 1 ? 5 : 0).attr({'stroke-width':'0px', 'fill': v.color.bg});
		bv.color.b = 'none';
		bv.color.bg = 'none';
		var w1 = w/v.items.length;
		bv.autosize = false;
		for(var i in v.items){
			var vi = v.items[i];
			bv.caption = '<span style="color:'+(vi.d ? v.color.td : v.color.te)+';">'+vi.t+'</span><span class="itemindex" style="display:none">'+i+'</span>';			
			paper.button.draw(canvas,l+1+(i*w1),t+1,w1,h-2,bv);
			paper.path(canvas,['M',Math.floor(l+1+(i*w1)+w1),t+1,'l',0,h-2]).attr({'stroke-width':'2px', 'stroke': v.color.b});
			paper._drawLink(vi, $('.element-caption:last',$div));
		}
		canvas.rect(l+1,t+1,w-2,h-2,v.type == 3 ? h/2 : v.type == 1 ? 5 : 0).attr({'stroke-width':'2px', 'stroke': v.color.b});
		//paper.button.draw(canvas,l+1,t+1,w-2,h-2,bv);
		paper.setItemClickHandler($div);
	}
}
paper.treeview = {
	properties: {
		min: {
			width: 40,
			height: 30
		},
		defaults: $.extend({}, paper.menu.properties.defaults, {noarrow: true, icon: 1, items: [[{'t': 'No Indent, Open Icon', d: false, s:false, i:0, st: 1},{'t': '1 Indent, Closed Icon, Selected ', d: false, s: true,i:1, st:2},{'t': '2 Indent, No Icon, Disabled', d: true, s:false, i:2, st:3}]]}),
		_changeState: function(pi){
			$('#state-n').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'None', hideAfter: 2000});
			if(pi == 2){
				paper._changeTooltip('#state-o', 'Checked');
				paper._changeTooltip('#state-c', 'Unchecked');
				$('#state-o').text('C');
				$('#state-c').text('UC');				
			}
			else if(pi == 4){
				paper._changeTooltip('#state-o', 'Folder Open');
				paper._changeTooltip('#state-c', 'Folder Closed');
				paper._changeTooltip('#state-f', 'File');
				$('#state-f').show();
				$('#state-o').text('FO');
				$('#state-c').text('FC');
				$('#state-f').text('F');
			}
			else{
				paper._changeTooltip('#state-o', 'Open');
				paper._changeTooltip('#state-c', 'Closed');
				$('#state-o').text('O');
				$('#state-c').text('C');
			}
			if(pi !== 4){
				$('#state-f').hide();
				if(paper._getItemData().st == 4){
					$('#state-n').addClass('active');
					$('#state-f').removeClass('active');
					paper._changeItemData('st',3,false);					
				}
			}
		},
		show: function($e,onshow){
			paper.menu.properties.show($e, function(){
				$('#props-title').text('Treeview Properties');
				$('#submenu').remove();
				$('#divider, #selected').css('width', '104px');
				var props = paper._getDefaultedProperties($e.data('properties'), paper.treeview.properties.defaults);
				$('<div id="div-icon" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Iconset</strong></label><div class="controls"><div class="btn-group" data-toggle="buttons-radio"><button id="icon-arrow" class="btn btn-small '+(props.icon == 1 ? 'active' : '')+'">A</button><button id="icon-check" class="btn btn-small '+(props.icon == 2 ? 'active' : '')+'">CB</button><button id="icon-plus" class="btn btn-small '+(props.icon == 3 ? 'active' : '')+'">+-</button><button id="icon-folder" class="btn btn-small '+(props.icon == 4 ? 'active' : '')+'">F</button>').insertBefore('#div-type');
				$('#icon-arrow').click(function(ev){
					paper.treeview.properties._changeState(1);
					paper._changeProperty('icon',1);
				});
				$('#icon-check').click(function(ev){
					paper.treeview.properties._changeState(2);
					paper._changeProperty('icon',2);
				});
				$('#icon-plus').click(function(ev){
					paper.treeview.properties._changeState(3);
					paper._changeProperty('icon',3);
				});
				$('#icon-folder').click(function(ev){
					paper.treeview.properties._changeState(4);
					paper._changeProperty('icon',4);
				});
				paper.treeview.properties._changeState(props.icon);

				$('#icon-plus').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: '+/- Expanders', hideAfter: 2000});
				$('#icon-arrow').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Arrows', hideAfter: 2000});
				$('#icon-check').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Checkbox', hideAfter: 2000});
				$('#icon-folder').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Folder and File', hideAfter: 2000});

				if(onshow) onshow;
			});
		}
	},
	draw: function(canvas, l, t, w, h, v){
		v.noarrow = false;
		paper.menu.draw(canvas, l, t, w, h, v);
	}
}
paper.line = {
	properties: {
		min: {
			width: 10,
			height: 2,
			handles: 'nw, se'
		},
		defaults: {
			width: 200,
			height: 2,
			color: '#000000',
			orient: 2
		},
		show: function($e, onshow){
			$('#props').remove();
			var props = paper._getDefaultedProperties($e.data('properties'), paper.line.properties.defaults);
			var orientation = '<div id="div-orientation" class="form-inline"><div class="btn-group" data-toggle="buttons-radio"><button id="orientation-v" style="width:105px;" class="btn btn-small '+(props.orient == 1 ? 'active' : '')+'">Vertical</button><button id="orientation-h" style="width:105px;" class="btn btn-small '+(props.orient == 2 ? 'active' : '')+'">Horizontal</button></div></div>';
			var color = '<div id="div-color"><div class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Color</strong></label><div id="color-options" class="controls"><button id="color" class="btn btn-small"><i class="icon-stop"></i></button></div></div></div>';
			var html = '<div id="props"><div id="propsFor" style="display:none">'+$e.attr('id')+'</div><div class="form-horizontal">'+color+orientation+'</div></div>';
			//ui.dismissPopovers();
			$e.popover({title:  '<strong style="color:#3a3a3a;"  id="props-title">Line Properties</strong>', content: html, placement: 'auto', attachTo: '#tbs-popover', onShow: function(){
				paper._buildDimensions($e, '#div-color');
				paper._buildOpacity($e, '#div-color',props);
				paper._buildLinkInspector($e, '#div-color',props);
				$('#orientation-v').click(function(ev){
					if(!$(this).hasClass('active'))
						paper._swapSize();
					paper._changeProperty('orient', 1);
				});
				$('#orientation-h').click(function(ev){
					if(!$(this).hasClass('active'))
						paper._swapSize();
					paper._changeProperty('orient', 2);
				});
				$('#color').data('val', props.color ? props.color : paper.line.properties.defaults.color);
				$('#color').css('color', props.color ? props.color : paper.line.properties.defaults.color);
				$('#color').click(function(ev){
					paper._showColorPicker(ev, this, 'color');
				});			
				if(onshow) onshow();
			}, onClick: function(ev){paper._popoverClick(ev,$e)}, onHide: function(){
				paper._destroyAllTooltips($e);
			}});
			$e.popover('show');
			ui.popoverContainers.push($e);
		}
	},
	draw: function(canvas, l, t, w, h, v){
		var $div = $(canvas.canvas.parentElement);
		if(v.linkto)
			$div.addClass('has-link');
		canvas.rect(l,t,w,h).attr({'stroke-width':'0px', 'fill':v.color});
		paper._drawLink(v, $div);
	}
}
paper.rect = {
	properties: {
		defaults: {
			width: 200,
			height: 150,
			color: {'b':'#000000','bg':'#000000'},
			o: 1,
			r: 0
		},
		show: function($e, onshow){
			$('#props').remove();
			var props = paper._getDefaultedProperties($e.data('properties'), paper.rect.properties.defaults);
			var radius = '<div id="div-radius" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Radius</strong></label><div class="controls"><input class="input-small" id="input-radius"style="width:50px" type="text" value="'+(props.r)+'" placeholder="'+(props.r)+'"></div></div>';
			var color = '<div id="div-color"><div class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Color</strong></label><div id="color-options" class="controls btn-group"><button id="color-b" class="btn btn-small"><i class="icon-stop"></i></button><button id="color-bg" class="btn btn-small"><i class="icon-stop"></i></button></div></div></div>';
			var html = '<div id="props"><div id="propsFor" style="display:none">'+$e.attr('id')+'</div><div class="form-horizontal">'+radius+color+'</div></div>';
			//ui.dismissPopovers();
			$e.popover({title:  '<strong style="color:#3a3a3a;"  id="props-title">Rectangle Properties</strong>', content: html, placement: 'auto', attachTo: '#tbs-popover', onShow: function(){
				paper._buildDimensions($e, '#div-color');
				paper._buildOpacity($e, '#div-color',props);
				paper._buildLinkInspector($e, '#div-color',props);
				$('#input-radius').spinner({min: 0, max: 100});
				$('#input-radius').keydown(function(ev){
					if(ev.which == 13){
						paper._changeProperty('r', $(this).val(), false);
						ev.preventDefault();
					}
				});
				$('#input-radius').blur(function(ev){
					paper._changeProperty('r', $(this).val());
				});
				$('#input-radius').change(function(ev){
					paper._changeProperty('r', $(this).val(), false);
				});
				$('#color-b').data('val', props.color.b ? props.color.b : paper.rect.properties.defaults.color.b);
				$('#color-bg').data('val', props.color.bg ? props.color.bg : paper.rect.properties.defaults.color.bg);
				$('#color-b').css('color', props.color.b ? props.color.b : paper.rect.properties.defaults.color.b);
				$('#color-bg').css('color', props.color.bg ? props.color.bg : paper.rect.properties.defaults.color.bg);
				$('#color-b').click(function(ev){
					paper._showColorPicker(ev, this, 'color.b');
				});
				$('#color-bg').click(function(ev){
					paper._showColorPicker(ev, this, 'color.bg');
				});
				$('#color-b').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Border', hideAfter: 2000});
				$('#color-bg').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Background', hideAfter: 2000});	
				if(onshow) onshow();
			}, onClick: function(ev){paper._popoverClick(ev,$e)}, onHide: function(){
				paper._destroyAllTooltips($e);
			}});
			$e.popover('show');
			ui.popoverContainers.push($e);
		}
	},
	draw: function(canvas, l, t, w, h, v){
		$div = $(canvas.canvas.parentElement);
		if(v.linkto)
			$div.addClass('has-link');
		canvas.rect(l+1,t+1,w-2,h-2,v.r).attr({'stroke-width':'2px', 'stroke':v.color.b, 'fill':v.color.bg});
		paper._drawLink(v, $div);
	}
}
paper.circle = {
	properties: {
		defaults: $.extend(true, {}, paper.rect.properties.defaults),
		show: function($e, onshow){
			paper.rect.properties.show($e, function(){
				$('#props-title').text('Circle/Ellipse Properties');
				$('#div-radius').remove();
				if(onshow) onshow();
			});
		}
	},
	draw: function(canvas,l,t,w,h,v){
		$div = $(canvas.canvas.parentElement);
		if(v.linkto)
			$div.addClass('has-link');
		canvas.ellipse(w/2,h/2,w/2-1,h/2-1).attr({'stroke-width':'2px','stroke':v.color.b,'fill':v.color.bg});
		paper._drawLink(v, $div);
	}
}
paper.breadcrumbs = {
	properties: {
		defaults: $.extend(true, {}, paper.tabs.properties.defaults, {'points': 'r' ,'bc': true, 'type': 1, 'items': [[{'t': 'Item 1'},{'t': 'Item 2'},{'t': 'Item 3'}]]}),
		show: function($e, onshow){
			paper.tabs.properties.show($e, function(){
				var props = paper._getDefaultedProperties($e.data('properties'), paper.breadcrumbs.properties.defaults);
				$('#props-title').text('Breadcrumbs Properties');
				$('#selected').remove();
				$('#disabled').remove();
				$('#delete').css('width','210px');
				$('#div-apoints').remove();
				$('#color-td').remove();
				$('#color-s').remove();
				$('#color-te').html('<i class="icon-stop"></i>');
				$('#color-te').click(function(ev){
					paper._showColorPicker(ev, this, 'color.te',false);
					paper._showColorPicker(ev, this, 'color.td');
				});
				$('<div id="div-type" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Type</strong></label><div class="controls"><div class="btn-group" data-toggle="buttons-radio"><button id="type-rr" class="btn btn-small '+(props.type == 1 ? 'active' : '')+'">RR</button><button id="type-r" class="btn btn-small '+(props.type == 2 ? 'active' : '')+'">R</button><button id="type-p" class="btn btn-small '+(props.type == 3 ? 'active' : '')+'">P</button></div></div></div>').insertBefore('#div-color');
				$('#type-rr').click(function(ev){
					paper._changeProperty('type', 1);
				});
				$('#type-r').click(function(ev){
					paper._changeProperty('type', 2);
				});
				$('#type-p').click(function(ev){
					paper._changeProperty('type', 3);
				});				
				$('<div id="div-apoints" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Points</strong></label><div class="controls"><div class="btn-group" data-toggle="buttons-radio"><button id="apoints-l" class="btn btn-small '+(props.points == 'l' ? 'active' : '')+'">L</button><button id="apoints-r" class="btn btn-small '+(props.points == 'r' ? 'active' : '')+'">R</button></div></div></div>').insertAfter('#div-type');
				$('#color-te').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Text', hideAfter: 2000});
				$('#apoints-l').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Left', hideAfter: 2000});
				$('#apoints-r').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Right', hideAfter: 2000});
				$('#apoints-r').click(function(ev){
					paper._changeProperty('points', 'r');
				});
				$('#apoints-l').click(function(ev){
					paper._changeProperty('points', 'l');
				});
				if(onshow) onshow();
			})
		}
	},
	draw: paper.tabs.draw
}
paper.menubar  = {
	properties: {
		defaults: $.extend($.extend(true, {},paper.menu.properties.defaults, {width:500, height:25, autosize: false,type: 2, points: 0}),{items: [[{'t': 'Menu 1', 'd': false},{'t': 'Menu 2', 'd': true},{'t': 'Menu 3', 'd': false, 's': true}]]}),
		_setButtonProperties: function(v,vi,bv,i){
			bv.size = v.size;
			bv.align = 'center';
			bv.caption = vi.t+'<span class="itemindex" style="display:none;">'+i+"<span>";
			bv.color.b = vi.s ? v.color.b : 0;
			bv.color.bg = vi.s ? v.color.s : v.color.bg;
			bv.color.t = vi.d ? v.color.td : v.color.te;
			bv.autosize = false;
		},
		show: function($e, onshow){
			paper.menu.properties.show($e, function(){
				$('#props-title').text('Menubar Properties');
				$('#div-indent').remove();
				$('#selected').prependTo('#div-delete .btn-group');
				$('#selected').attr('data-toggle','button');
				$('#div-delete .btn').css('width','70px');
				$('#div-dividersubmenu').remove();
				var props = paper._getDefaultedProperties($e.data('properties'), paper.menubar.properties.defaults);
				$('<div id="div-apoints" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Pointer</strong></label><div class="controls"><div class="btn-group" data-toggle="buttons-radio"><button id="apoints-u" class="btn btn-small '+(props.points == 1 ? 'active' : '')+'">U</button><button id="apoints-d" class="btn btn-small '+(props.points == 2 ? 'active' : '')+'">D</button><button id="apoints-n" class="btn btn-small '+(props.points == 0 ? 'active' : '')+'">N</button></div></div></div>').insertAfter('#div-type');
				$('#apoints-u').click(function(ev){
					paper._changeProperty('points',1);
				});
				$('#apoints-d').click(function(ev){
					paper._changeProperty('points',2);
				});
				$('#apoints-n').click(function(ev){
					paper._changeProperty('points',0);
				});
				$('#apoints-n').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'None', hideAfter: 2000});
				$('#apoints-d').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Down', hideAfter: 2000});
				$('#apoints-u').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Up', hideAfter: 2000});
				$('#div-type').remove();
				$('#element-resizer').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Click an item to switch to its properties', hideAfter: 3000});
				if(onshow) onshow();
			});
		}
	},
	draw: function(canvas, l, t, w, h, v){
		var $div = $(canvas.canvas.parentElement);
		var wa = [], mh = 0, mw = 0, linkflag = false;
		var bv = $.extend(true,{},paper.button.properties.defaults);
		v.items = v.items[0];
		bv.type = 2;
		for(var i in v.items){
			if(!linkflag && v.items[i].linkto)
				linkflag = true;
			var cp = '<p class="element-caption-para" style="margin: 0;vertical-align:middle;font-size:'+v.size+';text-align:center;word-break:break-word;word-wrap:break-word;">'+v.items[i].t+'</p>';
			var as = paper._getTextAutoSize(cp);
			mh = Math.max(mh, as.height + 10);
			mw += as.width+20;
			wa.push(as.width+20);
		}
		if(linkflag)
			$div.addClass('has-link');
		else
			$div.removeClass('has-link');
		if(v.autosize){
			h = mh, w = mw;
			if(v.points > 0) h+=8;
			paper._resize($div,w,h);
		}
		if(v.points > 0)
			h-=8;
		if(v.points == 1)
			t+=8;
		canvas.rect(l+1,t+1,w-2,h-2,v.type==1?5:0).attr({'stroke-width':'0px', 'stroke':v.color.b, 'fill':v.color.bg});
		var w1 = 0;
		for(var i in v.items){
			var vi = v.items[i];
			paper.menubar.properties._setButtonProperties(v,vi,bv,i);
			if(!vi.s){
				if(v.align == 'left')
					paper.button.draw(canvas,l+w1-(vi.s?2:1),t,wa[i]+2,h,bv);
				else
					paper.button.draw(canvas,l+w-mw+w1-(vi.s?2:1),t,wa[i]+2,h,bv);
				paper._drawLink(vi, $('.element-caption:last', $div));
			}
			w1 += wa[i];
		}
		canvas.rect(l+1,t+1,w-2,h-2,v.type==1?5:0).attr({'stroke-width':'2px', 'stroke':v.color.b});
		var w1 = 0;
		for(var i in v.items){
			var vi = v.items[i];
			paper.menubar.properties._setButtonProperties(v,vi,bv,i);
			if(vi.s){
				if(v.align == 'left')
					paper.button.draw(canvas,l+w1-(vi.s?2:1),t,wa[i]+2,h,bv);
				else
					paper.button.draw(canvas,l+w-mw+w1-(vi.s?2:1),t,wa[i]+2,h,bv);
				var pw = 20;
				if(vi.s && v.points > 0){
					var m = v.points == 1 ? -1 : 1;
					paper.path(canvas, ['M',(v.align=='left'?l+w1-(vi.s?2:1):l+w-mw+w1-(vi.s?2:1))+(wa[i]-pw)/2+1,v.points==1?t+1:t+h-1,'l',pw/2,8*m,'l',pw/2,-8*m]).attr({'stroke-width':'2px'});
					paper.path(canvas, ['M',(v.align=='left'?l+w1-(vi.s?2:1):l+w-mw+w1-(vi.s?2:1))+(wa[i]-pw)/2+1,v.points==1?t+3:t+h-3,'l',pw/2,8*m,'l',pw/2,-8*m]).attr({'stroke-width':'2px','stroke':v.color.s,'fill':v.color.s});
					paper._drawLink(vi, $('.element-caption:last', $div));
				}
			}
			w1 += wa[i];
		}
		paper.path(canvas,['M',l+1,t+1,'l',0,h-2]).attr({'stroke-width':'2px','stroke':v.color.b});
		paper.setItemClickHandler($div);
	}
}
paper.table = {
	properties: {
		defaults: $.extend($.extend(true, {},paper.menu.properties.defaults, {align: 'center', width:425, height:100, autosize: false,type: 2, points: 0}),{
			items: [
				[{'t': 'Header 1'},{'t': 'Header 2'},{'t': 'Header 3'}],
				[{'t': 'Value 1'},{'t': 'Value 2'},{'t': 'Value 3'}],
				[{'t': 'Value 4'},{'t': 'Value 5'},{'t': 'Value 6'}]
			]
		}),
		show: function($e, onshow){
			paper.breadcrumbs.properties.show($e, function(){
				$('#props-title').text('Table Properties');
				$('#type-p').remove();
				$('#div-apoints').remove();
				var props = paper._getDefaultedProperties($e.data('properties'), paper.table.properties.defaults);
				$('#div-caption').remove();
				$('<div id="div-caption" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Caption</strong></label><div class="controls"><textarea id="input-caption" rows="2" style="width:135px"></textarea></div></div>').insertBefore('#div-delete');
				$('#div-rowsel').css({'display':'inline-block'});
				$('#btn-cursel').css({'display':'inline-block','width':'103px','margin-left':'5px'});
				$('#element-resizer').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Click an item to switch to its properties', hideAfter: 3000});
				if(onshow) onshow();
			});
		}
	},
	draw: function(canvas, l, t, w, h, v, dontDraw){
		var $div = $(canvas.canvas.parentElement);
		var mw=0, mh=0,tr=0,tc=0;
		var ha = [], wa = [], linkflag = false;
		for(var i in v.items){
			var mw1=0, mh1 = 0;
			++tr;
			if(v.items[i].length > tc)
				tc = v.items[i].length;
			for(var j in v.items[i]){				
				var vi = v.items[i][j];
				if(!linkflag && vi.linkto)
					linkflag = true;
				var cp = '<p class="element-caption-para" style="margin: 0;vertical-align:middle;font-size:'+v.size+';text-align:'+v.align+';word-break:break-word;word-wrap:break-word;">'+vi.t.nl2br()+'</p>';
				var as = paper._getTextAutoSize(cp);
				wa[j] = Math.max(wa[j] ? wa[j] : 0, as.width+20);
				mw1 = mw1 + wa[j];
				mh1 = Math.max(mh1, as.height + 10);
			}
			ha[i] = mh1;
			mh += mh1;
			mw = Math.max(mw,mw1);
		}
		if(linkflag)
			$div.addClass('has-link');
		else
			$div.removeClass('has-link');
		if(v.autosize){
			h = mh, w = mw;
			if($div.attr('data-element')=='table' || dontDraw)
				paper._resize($div,w,h);
		}
		if(dontDraw) return;
		var r = v.type == 1 ? 5 : 0;
		canvas.rect(l+1,t+1,w-2,h-2,r).attr({'stroke-width':'0px', 'fill': v.color.bg});
		var mh = 0, tw = mw,mw=0,mh1=0;
		for(var i=0;i<tr;++i){
			if(!v.autosize)
				ha[i] = Math.floor(Math.max(ha[i], h/tr));
			var t1 = t+mh;
			if(i > 0 && t1 < t+h)
				paper.path(canvas, ['M', l+1, t1,'l',w-2,0]).attr({'stroke-width':'2px', 'stroke': v.color.b});
			mw = 0;
			for(var j=0;j<tc;++j){
				if(!v.autosize){
					wa[j] = Math.floor(Math.max(wa[j], w/tc));
					//wa[j] = Math.floor(w * wa[j]/tw);
				}
				var l1 = l+1+mw+wa[j];
				if(j < tc-1 && l1 < l+w)
					paper.path(canvas, ['M', l1, t+mh,'l',0,ha[i]]).attr({'stroke-width':'2px', 'stroke': v.color.b});
				var vi = v.items[i][j];
				var lv =  $.extend(true,{},paper.label.properties.defaults);
				lv.color = v.color;
				lv.color.t = v.color.te;
				lv.color.bg = 'none';
				lv.align = v.align;
				lv.autosize = false;
				lv.caption = (vi ? vi.t : '') + '<span class="itemindex" style="display:none">'+i+','+j+'</span>';
				lv.size = v.size;
				paper.label.draw(canvas,l+2+mw,t+1+mh,wa[j]-2,ha[i]-2,lv);
				paper._drawLink(vi, $('.element-caption:last', $div));
				mw += wa[j];
			}
			mh += ha[i];
		}
		canvas.rect(l+1,t+1,w-2,h-2,r).attr({'stroke-width':'2px', 'stroke': v.color.b});
		paper.setItemClickHandler($div);
	}
}
paper.image = {
	properties: {
		defaults: {
			gs: false,
			width:200,
			height: 150
		},
		show:function($e, onshow){
			paper.piechart.properties.show($e, function(){
				$('#props-title').text('Image Properties');
			});
		}
	},
	draw: function(canvas,l,t,w,h,v){
		var $div = $(canvas.canvas.parentElement);
		if(v.linkto)
			$div.addClass('has-link');
		var sw = Math.floor(w*10/200), sw2 = Math.floor(sw/2);
		canvas.rect(l+sw2,t+sw2,w-sw,h-sw).attr({'stroke-width':'0px','stroke':'#3c3c3c','fill':'#acacac'});
		paper.path(canvas, ['M',l+sw,t+h-sw,'l',(w-sw)/3,-(h-sw)/2,'l',w/10,h/8,'l',w/3,-h/3,'L',l+w-sw+2,t+h/2,'l',0,h/2,'z']).attr({'stroke-width':'2px','stroke':'#3c3c3c','fill':'#c3c3c3'});
		canvas.circle(w/5,h/3,h/10).attr({'stroke-width':'2px','stroke':'#3c3c3c','fill': v.gs ? '#ececec' : '#ffe861'});
		canvas.rect(l+sw2,t+sw2,w-sw,h-sw).attr({'stroke-width':'10px','stroke':'#3c3c3c','fill':'none'});
		paper._drawLink(v, $div);
	}
}
paper.map = {
	properties: {
		defaults: {
			width: 200,
			height: 150,
			gs: false
		},
		show: function($e, onshow){
			paper.piechart.properties.show($e, function(){
				$('#props-title').text('Map Properties');
			});
		}
	},
	draw: function(canvas,l,t,w,h,v){
		var $div = $(canvas.canvas.parentElement);
		if(v.linkto)
			$div.addClass('has-link');
		canvas.rect(l+1,t+1,w-2,h-2).attr({'stroke-width':'0px','stroke':'#3c3c3c','fill':'#ececec'});
		paper.path(canvas, ['M',l-10-w/2,t+1,'l',w+20,h/2]).attr({'stroke-width': Math.min(h*10/150,w*10/200) + 'px','stroke':'#3c3c3c'});
		paper.path(canvas, ['M',l+w/2-20,t+1-10,'l',w+20,h/2]).attr({'stroke-width':Math.min(h*10/150,w*10/200) + 'px','stroke':'#3c3c3c'});
		paper.path(canvas, ['M',l+w+40,t+2+20,'l',-w+10,h+10]).attr({'stroke-width':Math.min(h*10/150,w*10/200) + 'px','stroke':'#3c3c3c'});
		paper.path(canvas, ['M',l+w+140,t+2+20,'l',-w+10,h+10]).attr({'stroke-width':Math.min(h*10/150,w*10/200) + 'px','stroke':'#3c3c3c'});
		paper.path(canvas, ['M',l-10,t+1,'l',w+20,h/2]).attr({'stroke-width':Math.min(h*10/150,w*30/200) + 'px','stroke':'#acacac'});
		paper.path(canvas, ['M',l+w+20,t+2,'l',-w-20,h]).attr({'stroke-width':Math.min(h*10/150,w*30/200) + '0px','stroke':'#acacac'});
		var iv = $.extend(true,{},paper.icon.properties.defaults,{icon:'map-marker'});
		iv.color.s = v.gs ? '#1e1e1e' : '#fd303a';
		paper.icon.draw(canvas,w/5,h/2.5,w/3,h/3,iv);
		canvas.rect(l+1,t+1,w-2,h-2).attr({'stroke-width':'2px','stroke':'#000000','fill':'none'});
		paper._drawLink(v,$div);
	}
}
paper.audio = {
	properties: {
		min: {
			width: 75,
			height: 20
		},
		defaults:{
			width: 200,
			height: 30,
			color: {b:'#000000',bg:'#ffffff',s:'#000000',fg:'#ececec'}
		},
		show: function($e, onshow){
			paper.circle.properties.show($e, function(){
				$('#props-title').text('Audio Player Properties');
				var props = paper._getDefaultedProperties($e.data('properties'), paper.audio.properties.defaults);

				$('<button id="color-fg" class="btn btn-small"><i class="icon-stop"></i></button>').insertAfter('#color-bg');
				$('#color-fg').data('val', props.color.fg ? props.color.fg : paper.audio.properties.defaults.color.fg);
				$('#color-fg').css('color', props.color.fg ? props.color.fg : paper.audio.properties.defaults.color.fg);
				$('#color-fg').click(function(ev){
					paper._showColorPicker(ev, this, 'color.fg');
				});
				$('#color-fg').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Foreground', hideAfter: 2000});

				$('<button id="color-i" class="btn btn-small"><i class="icon-stop"></i></button>').insertAfter('#color-fg');
				$('#color-i').data('val', props.color.s ? props.color.s : paper.audio.properties.defaults.color.s);
				$('#color-i').css('color', props.color.s ? props.color.s : paper.audio.properties.defaults.color.s);
				$('#color-i').click(function(ev){
					paper._showColorPicker(ev, this, 'color.s');
				});
				$('#color-i').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Icons', hideAfter: 2000});
				//$('#div-opacity').remove();
				if(onshow) onshow();
			});
		}
	},
	draw: function(canvas,l,t,w,h,v){
		var $div = $(canvas.canvas.parentElement)
		if(v.linkto)
			$div.addClass('has-link');
		canvas.rect(l+1,t+1,w-2,h-2).attr({'stroke-width':'2px','stroke':v.color.b,'fill': v.color.fg});
		var iv = $.extend(true,{}, paper.icon.properties.defaults,{'icon':'play'});
		iv.color.s = v.color.s;
		paper.icon.draw(canvas,l+1,t,h-2,h-2,iv);
		iv.icon = 'volume-down';
		var h1 = Math.floor(h*32/25);
		paper.icon.draw(canvas,l+1+w-h1,t+(h-h1)/2-1,h1,h1,iv);
		var iv = $.extend(true, {}, paper.slider.properties.defaults);
		iv.color = $.extend({}, v.color);
		iv.color.fg = iv.color.s;
		iv.color.b = v.color.s;
		paper.slider.draw(canvas,l+h-1,t+6,w-(2*h)-5,h-12,iv);
		paper._drawLink(v, $div);
	}	
}
paper.video = {
	properties: {
		min: {
			width: 75,
			height: 30
		},
		defaults: $.extend(true, {}, paper.audio.properties.defaults, {height: 150}),
		show: function($e, onshow){
			paper.audio.properties.show($e, function(){
				$('#props-title').text('Video Player Properties');
				if(onshow) onshow();
			});
		}
	},
	draw: function(canvas,l,t,w,h,v){
		var $div = $(canvas.canvas.parentElement);
		if(v.linkto)
			$div.addClass('has-link');
		canvas.rect(l+1,t+1,w-2,h-2).attr({'stroke-width':'2px','stroke':v.color.b,'fill': v.color.bg});
		paper.audio.draw(canvas,l,t+h-30,w,30,v);
	}	
}
paper.plugin = {
	properties:{
		min: {
			width: 75,
			height: 30
		},
		defaults: {
			caption: 'Java Content',
			color: {'bg': '#ececec', 't': '#acacac', 'b':'#000000'},
			align: 'center',
			size: '36px',
			height: 150,
			width: 200,
			autosize: false
		},
		show: function($e, onshow){		
			paper.label.properties.show($e, function(){
				$('#props-title').text('Plugin Content Properties');
				var props = paper._getDefaultedProperties($e.data('properties'), paper.plugin.properties.defaults);
				$('<button id="color-b" class="btn btn-small"><i class="icon-stop"></i></button>').insertBefore('#color-bg');
				$('#color-b').data('val', props.color.b ? props.color.b : paper.plugin.properties.defaults.color.b);
				$('#color-b').css('color', props.color.b ? props.color.b : paper.plugin.properties.defaults.color.b);
				$('#color-b').click(function(ev){
					paper._showColorPicker(ev, this, 'color.b');
				});
				$('#color-b').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Border', hideAfter: 2000});
				$('#div-autosizedrop').remove();
				if(onshow) onshow();
			});
		}
	},
	draw: function(canvas,l,t,w,h,v){
		var $div = $(canvas.canvas.parentElement);
		if(v.linkto)
			$div.addClass('has-link');
		paper.label.draw(canvas,l,t,w,h,v);
		canvas.rect(l+1,t+1,w-2,h-2).attr({'stroke-width':'2px', 'stroke':v.color.b})
		paper._drawLink(v,$div);
	}
}
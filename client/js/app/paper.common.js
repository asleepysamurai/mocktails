var paper = {};
paper.path = function(canvas, a){
	/*
	for(var i in a){
		if(typeof a[i] != 'string')
			a[i] = Math.floor(a[i]);
	}
	*/
	return canvas.path(a);
}
paper._sector = function(cx, cy, r, startAngle, endAngle) {
	var rad = Math.PI/180;
	var x1 = cx + r * Math.cos(-startAngle * rad),
	x2 = cx + r * Math.cos(-endAngle * rad),
	y1 = cy + r * Math.sin(-startAngle * rad),
	y2 = cy + r * Math.sin(-endAngle * rad);
	return ["M", cx, cy, "M", x1, y1, "A", r, r, 0, +(endAngle - startAngle > 180), 0, x2, y2];
}
paper._swapSize = function(){
	var $el = $('#' + $('#propsFor').text());
	var th = $el.height();
	mockup.setHeight($el.width(), $el);
	mockup.setWidth(th, $el);
	mockup.refreshBorder($el, null, true, true);
}
paper._getDefaultedProperties = function(p,d){
	return $.extend({}, d, p);
}
paper._getTextAutoSize = function(t){
	t = '<span style="display:inline-block;max-width:'+($('#'+stateManager.currentState.currentPage).width() - 20)+'px;">'+t+'</span>';
	var $t = $(t).appendTo(document.body);
	var as = {height: $t.height(), width: $t.width()};
	$t.remove();
	return as;
}
paper._invertHex = function(hex) {
	if (hex.length != 7 || hex.indexOf('#') != 0) return null;
	function pad(num) {
		if (num.length < 2) {
			return "0" + num;
		} else {
			return num;
		}
	}
	var r = (255 - parseInt(hex.substring(1, 3), 16)).toString(16);
	var g = (255 - parseInt(hex.substring(3, 5), 16)).toString(16);
	var b = (255 - parseInt(hex.substring(5, 7), 16)).toString(16);
	var inverse = "#" + pad(r) + pad(g) + pad(b);
	return inverse
};
paper._popoverClick = function(ev, $e){
	var lp = ui.popoverContainers.pop();
	if(lp && lp.is($e))
		ui.popoverContainers.push(lp);
	else
		lp.popover('destroy');
}
paper._showIconPicker = function(ev,e,ct){
	ev.preventDefault();
	ev.stopPropagation();	
	var lp = ui.popoverContainers.pop();
	if($(e).is(lp)) return;
	if(lp && lp.parent().attr('id')=='icon-options'){
		lp.popover('destroy');
		$('#div-iconpicker').remove();
	}
	else
		ui.popoverContainers.push(lp);	
	var $t = $(e);
	var c = $t.data('val');
	var picker = '<div id="div-iconpicker" style="font-size:24px" class="form-search"><input type="text" class="input-small" value="" placeholder="Search" style="width:190px;margin-bottom:10px;" id="icon-search"></input><div id="iconpicker-icons"><i class="icon-adjust" data-tags="adjust contrast brightness yin yang"></i><i class="icon-asterisk" data-tags="asterisk star *"></i><i class="icon-ban-circle" data-tags="ban stop danger"></i><i class="icon-bar-chart" data-tags="chart bar graph column row"></i><i class="icon-barcode" data-tags="barcode qr code"></i><i class="icon-beaker" data-tags="beaker chemical flask"></i><i class="icon-bell" data-tags="bell"></i><i class="icon-bolt" data-tags="bolt lightning strike"></i><i class="icon-book" data-tags="book"></i><i class="icon-bookmark" data-tags="bookmark tag"></i><i class="icon-bookmark-empty" data-tags="bookmark tag"></i><i class="icon-briefcase" data-tags="breifcase case folder"></i><i class="icon-bullhorn" data-tags="bull horn bullhorn speaker loudspeaker"></i><i class="icon-calendar" data-tags="calender date month year"></i><i class="icon-camera" data-tags="camera"></i><i class="icon-camera-retro" data-tags="camera"></i><i class="icon-certificate" data-tags="certificate star seal"></i><i class="icon-check" data-tags="check checkbox checkmark"></i><i class="icon-check-empty" data-tags="empty check checkbox checkmark"></i><i class="icon-cloud" data-tags="cloud"></i><i class="icon-cog" data-tags="cog cogs gear gears"></i><i class="icon-cogs" data-tags="cog cogs gear gear"></i><i class="icon-comment" data-tags="comment dark comments"></i><i class="icon-comment-alt" data-tags="comment light comments"></i><i class="icon-comments" data-tags="comment dark comments"></i><i class="icon-comments-alt" data-tags="comment light comments"></i><i class="icon-credit-card" data-tags="credit card debit card creditcard debitcard"></i><i class="icon-dashboard" data-tags="dashboard speedometer"></i><i class="icon-download" data-tags="download down arrow"></i><i class="icon-download-alt" data-tags="download down arrow"></i><i class="icon-edit" data-tags="edit"></i><i class="icon-envelope" data-tags="envelope email"></i><i class="icon-envelope-alt" data-tags="envelope email"></i><i class="icon-exclamation-sign" data-tags="exclamation sign !"></i><i class="icon-external-link" data-tags="link hyperlink anchor"></i><i class="icon-eye-close" data-tags="eye"></i><i class="icon-eye-open" data-tags="eye"></i><i class="icon-facetime-video" data-tags="video"></i><i class="icon-film" data-tags="film reel"></i><i class="icon-filter" data-tags="filter funnel"></i><i class="icon-fire" data-tags="fire"></i><i class="icon-flag" data-tags="flag"></i><i class="icon-folder-close" data-tags="folder dossier"></i><i class="icon-folder-open" data-tags="folder dossier"></i><i class="icon-gift" data-tags="gift present"></i><i class="icon-glass" data-tags="glass cup martini"></i><i class="icon-globe" data-tags="globe world earth"></i><i class="icon-group" data-tags="group people"></i><i class="icon-hdd" data-tags="hard disk hard disc hdd"></i><i class="icon-headphones" data-tags="heads headphones music"></i><i class="icon-heart" data-tags="heart love rating"></i><i class="icon-heart-empty" data-tags="heart love rating"></i><i class="icon-home" data-tags="home house"></i><i class="icon-inbox" data-tags="inbox"></i><i class="icon-info-sign" data-tags="i info sign"></i><i class="icon-key" data-tags="lock key"></i><i class="icon-leaf" data-tags="leaf leaves"></i><i class="icon-legal" data-tags="legal lawyer hammer gavel"></i><i class="icon-lemon" data-tags="lemon lime"></i><i class="icon-lock" data-tags="lock key"></i><i class="icon-unlock" data-tags="lock unlock key"></i><i class="icon-magic" data-tags="magic wand sparks"></i><i class="icon-magnet" data-tags="magnet horseshoe u"></i><i class="icon-map-marker" data-tags="map marker pointer"></i><i class="icon-minus" data-tags="minus plus math expander"></i><i class="icon-minus-sign" data-tags="minus plus expander"></i><i class="icon-money" data-tags="money cash dollar note"></i><i class="icon-move" data-tags="move drag cursor"></i><i class="icon-music" data-tags="music note lyric song"></i><i class="icon-off" data-tags="off power down abort"></i><i class="icon-ok" data-tags="ok tickmark checkmark"></i><i class="icon-ok-circle" data-tags="ok tickmark checkmark"></i><i class="icon-ok-sign" data-tags="ok tickmark checkmark"></i><i class="icon-pencil" data-tags="pencil note write"></i><i class="icon-picture" data-tags="picture image logo icon"></i><i class="icon-plane" data-tags="airplane aeroplane flight"></i><i class="icon-plus" data-tags="plus monis math expander"></i><i class="icon-plus-sign" data-tags="plus monis math expander"></i><i class="icon-print" data-tags="printer paper document ink"></i><i class="icon-pushpin" data-tags="pin group"></i><i class="icon-qrcode" data-tags="barcode qr code"></i><i class="icon-question-sign" data-tags="question mark answer"></i><i class="icon-random" data-tags="random shuffle switchover x crossing"></i><i class="icon-refresh" data-tags="refresh reload"></i><i class="icon-remove" data-tags="remove delete discard close"></i><i class="icon-remove-circle" data-tags="remove delete discard close"></i><i class="icon-remove-sign" data-tags="remove delete discard close"></i><i class="icon-reorder" data-tags="reorder rearrange"></i><i class="icon-resize-horizontal" data-tags="resize"></i><i class="icon-resize-vertical" data-tags="resize"></i><i class="icon-retweet" data-tags="retweet twitter"></i><i class="icon-road" data-tags="road path highway lane"></i><i class="icon-rss" data-tags="rss wifi signal"></i><i class="icon-screenshot" data-tags="screenshot target crosshair"></i><i class="icon-search" data-tags="search find"></i><i class="icon-share" data-tags="share arrow"></i><i class="icon-share-alt" data-tags="share arrow"></i><i class="icon-shopping-cart" data-tags="shopping cart trolley"></i><i class="icon-signal" data-tags="signal wifi 3g gsm"></i><i class="icon-signin" data-tags="signin login"></i><i class="icon-signout" data-tags="signout logout"></i><i class="icon-sitemap" data-tags="sitemap"></i><i class="icon-sort" data-tags="sort arrow"></i><i class="icon-sort-down" data-tags="sort arrow"></i><i class="icon-sort-up" data-tags="sort arrow"></i><i class="icon-star" data-tags="star * asterisk"></i><i class="icon-star-empty" data-tags="star * asterisk"></i><i class="icon-star-half" data-tags="star * asterisk"></i><i class="icon-tag" data-tags="tags bookmarks"></i><i class="icon-tags" data-tags="tags bookmarks"></i><i class="icon-tasks" data-tags="task list bars"></i><i class="icon-thumbs-down" data-tags="thumbs down dislike"></i><i class="icon-thumbs-up" data-tags="thumbs up like"></i><i class="icon-time" data-tags="time clock watch"></i><i class="icon-tint" data-tags="tint raindrop"></i><i class="icon-trash" data-tags="trash delete remove discard"></i><i class="icon-trophy" data-tags="trophy cup prize"></i><i class="icon-truck" data-tags="truck lorry vehicle"></i><i class="icon-umbrella" data-tags="umbrella rain"></i><i class="icon-upload" data-tags="upload file"></i><i class="icon-upload-alt" data-tags="upload file"></i><i class="icon-user" data-tags="user person people profile"></i><i class="icon-user-md" data-tags="md"></i><i class="icon-volume-off" data-tags="volume sound off mute"></i><i class="icon-volume-down" data-tags="reduce volume sound"></i><i class="icon-volume-up" data-tags="increase volume sound"></i><i class="icon-warning-sign" data-tags="warning exclamation"></i><i class="icon-wrench" data-tags="wrench settings"></i><i class="icon-zoom-in" data-tags="zoom in scale in"></i><i class="icon-zoom-out" data-tags="zoom out scale out"></i><i class="icon-file" data-tags="file folder directory document page"></i><i class="icon-cut" data-tags="cut copy paste duplicate"></i><i class="icon-copy" data-tags="cut copy paste duplicate"></i><i class="icon-paste" data-tags="cut copy paste duplicate"></i><i class="icon-save" data-tags="save floppy"></i><i class="icon-undo" data-tags="undo redo repeat"></i><i class="icon-repeat" data-tags="undo redo repeat"></i><i class="icon-paper-clip" data-tags="clip group"></i><i class="icon-text-height" data-tags="text height font"></i><i class="icon-text-width" data-tags="text width font"></i><i class="icon-align-left" data-tags="text align left font"></i><i class="icon-align-center" data-tags="text align center font"></i><i class="icon-align-right" data-tags="text align right font"></i><i class="icon-align-justify" data-tags="text align justify font"></i><i class="icon-indent-left" data-tags="text indent left font"></i><i class="icon-indent-right" data-tags="text indent right font"></i><i class="icon-font" data-tags="text font edit"></i><i class="icon-bold" data-tags="text font bold"></i><i class="icon-italic" data-tags="text font italic"></i><i class="icon-strikethrough" data-tags="text font strikethrough"></i><i class="icon-underline" data-tags="text font underline"></i><i class="icon-link" data-tags="link hyperlink"></i><i class="icon-columns" data-tags="column row table"></i><i class="icon-table" data-tags="column row table"></i><i class="icon-th-large" data-tags="thumbnails large"></i><i class="icon-th" data-tags="thumbnails"></i><i class="icon-th-list" data-tags="thumbnails lists"></i><i class="icon-list" data-tags="lists"></i><i class="icon-list-ol" data-tags="thumbnails large"></i><i class="icon-list-ul" data-tags="unordered list ul"></i><i class="icon-list-alt" data-tags="unordered list ul"></i><i class="icon-arrow-down" data-tags="arrow down pointer"></i><i class="icon-arrow-left" data-tags="arrow left pointer"></i><i class="icon-arrow-right" data-tags="arrow right pointer"></i><i class="icon-arrow-up" data-tags="arrow up pointer"></i><i class="icon-chevron-down" data-tags="arrow down pointer chevron"></i><i class="icon-circle-arrow-down" data-tags="arrow down pointer"></i><i class="icon-circle-arrow-left" data-tags="arrow left pointer"></i><i class="icon-circle-arrow-right" data-tags="arrow right pointer"></i><i class="icon-circle-arrow-up" data-tags="arrow up pointer"></i><i class="icon-chevron-left" data-tags="arrow left pointer chevron"></i><i class="icon-caret-down" data-tags="arrow down pointer caret"></i><i class="icon-caret-left" data-tags="arrow left pointer caret"></i><i class="icon-caret-right" data-tags="arrow right pointer caret"></i><i class="icon-caret-up" data-tags="arrow up pointer caret"></i><i class="icon-chevron-right" data-tags="arrow right pointer chevron"></i><i class="icon-hand-down" data-tags="arrow hand down pointer"></i><i class="icon-hand-left" data-tags="arrow hand left pointer"></i><i class="icon-hand-right" data-tags="arrow hand right pointer"></i><i class="icon-hand-up" data-tags="arrow hand up pointer"></i><i class="icon-chevron-up" data-tags="arrow chevron up pointer"></i><i class="icon-play-circle" data-tags="media audio video player"></i><i class="icon-play" data-tags="media audio video player"></i><i class="icon-pause" data-tags="media audio video pause"></i><i class="icon-stop" data-tags="media audio video stop"></i><i class="icon-step-backward" data-tags="media audio video step backward"></i><i class="icon-fast-backward" data-tags="media audio video fast forward"></i><i class="icon-backward" data-tags="media audio video step backward"></i><i class="icon-forward" data-tags="media audio video fast forward"></i><i class="icon-fast-forward" data-tags="media audio video fast forward"></i><i class="icon-step-forward" data-tags="media audio video fast forward"></i><i class="icon-eject" data-tags="media audio video eject remove change"></i><i class="icon-fullscreen" data-tags="media audio video fullscreen"></i><i class="icon-resize-full" data-tags="media audio video resize"></i><i class="icon-resize-small" data-tags="media audio video resize"></i><i class="icon-phone" data-tags="phone call gsm cdma cell mobile"></i><i class="icon-phone-sign" data-tags="phone call gsm cdma cell mobile"></i><i class="icon-facebook" data-tags="facebook social"></i><i class="icon-facebook-sign" data-tags="facebook social"></i><i class="icon-twitter" data-tags="twitter social tweet"></i><i class="icon-twitter-sign" data-tags="twitter social tweet"></i><i class="icon-github" data-tags="github code social"></i><i class="icon-github-sign" data-tags="github code social"></i><i class="icon-linkedin" data-tags="linkedin linkdin social"></i><i class="icon-linkedin-sign" data-tags="linkedin linkdin social"></i><i class="icon-pinterest" data-tags="pinterest social"></i><i class="icon-pinterest-sign" data-tags="pinterest social"></i><i class="icon-google-plus" data-tags="google plus social google+"></i><i class="icon-google-plus-sign" data-tags="google plus social google+"></i><i class="icon-sign-blank" data-tags="blank empty rectangle square"></i></div></div>';
	$t.popover({title: 'Pick Icon', content: picker, attachTo: '#tbs-popover', onShow: function(){
		$('#iconpicker-icons').css({'height':'280px', 'overflow': 'auto'});
		$('i','#iconpicker-icons').css({'margin-right':'15px', 'height':'30px'});
		$('#iconpicker-icons').sbscroller();
		$('#icon-search').focus();
	}});
	$t.popover('show');
	ui.popoverContainers.push($t);
	$('#icon-search').keyup(function(ev){
		var isval = $(this).val();
		$('i','#iconpicker-icons').each(function(){
			var $t = $(this);
			var td = $t.attr('data-tags');
			if(isval.length > 0 && (!td || td.indexOf(isval) == -1))
				$t.css('display','none');
			else
				$t.css('display','inline');
		});
	});
	$('i','#iconpicker-icons').each(function(ev){
		var $t = $(this);
		$t.tooltip({html: true, placement: 'bottom', attachTo: '#tbs-tooltip', title: '<span style="text-transform: capitalize">'+$t.attr('class').replace(/icon-/g,'').replace(/-/g,' ')+'</span>', hideAfter: 2000});
	});
	$('i','#iconpicker-icons').click(function(ev){
		paper._changeProperty(ct,$(this).attr('class').replace(/icon-/g,''), true);
		$t.html('<i class="'+$.escapeHTML($(this).attr('class'))+'"></i>');
	});
}
paper._showColorPicker = function(ev, e, ct){
	ev.preventDefault();
	ev.stopPropagation();
	var lp = ui.popoverContainers.pop();
	if($(e).is(lp)) return;
	if(lp && lp.parent().attr('id')=='color-options'){
		lp.popover('destroy');
		$('#colorpicker-container').remove();
	}
	else
		ui.popoverContainers.push(lp);
	var $t = $(e);
	var c = $t.data('val');
	var colorpicker = '<div id="colorpicker-container"><form class="form-inline"><button id="colorpicker-clear" class="btn">Clear</button><input type="text" id="colorpicker-color" name="color" style="width:55px;background-color:'+c+'" value="'+c+'" placeholder="'+c+'"/><button id="colorpicker-cancel" class="btn">Cancel</button></form><div id="colorpicker"></div></div>';
	var farbtastic;
	$t.popover({title: 'Pick Color', content: colorpicker, placement: 'auto', attachTo: '#tbs-popover', onShow: function(){
		farbtastic = $.farbtastic('#colorpicker', function(color){
			$('#colorpicker-color').val(color);
			$('#colorpicker-color').css({'background-color': color, 'color': paper._invertHex(color)});
			paper._changeProperty(ct, color, false);
			$t.data('val',color);
			$t.css('color',color);
		}).setColor($('#colorpicker-color').val());
		$('#colorpicker-cancel').click(function(ev){
			paper._changeColor(ev, ct, '#'+$t.attr('id'), c);
		});
		$('#colorpicker-clear').click(function(ev){
			paper._changeColor(ev, ct, '#'+$t.attr('id'), 'none');
		});
		$('#colorpicker-color').keydown(function(ev){
			farbtastic.setColor($(this).val());
			if(ev.which == 13)
				ev.preventDefault();
		});
		$('#colorpicker-color').blur(function(ev){
			farbtastic.setColor($(this).val());
		});
	},onHide: function(c){
		paper._changeProperty(ct,c);
	}});
	$t.popover('show');
	ui.popoverContainers.push($t);
};
paper._changeColor = function(ev, ct, cb,cpv){
	ev.preventDefault();
	paper._changeProperty(ct, cpv, false);
	var $cb = $(cb);
	$cb.data('val', cpv);
	ui.popoverContainers.pop();
	$cb.popover('destroy');
};
paper._changeProperty = function(k,v,wa,redraw){
	var $el = $('#' + $('#propsFor').text());
	var p = $el.data('properties');
	if(k.indexOf('.') != -1){
		var ks = k.split('.');
		p[ks[0]][ks[1]] = v;
	}
	else
		p[k] = v;
	$el.data('properties', p);
	if(redraw !== false)
		paper.redraw($el);
	if(k == 'orient')
		mockup.refreshBorder($div, null, true, true);
	if(wa !== false)
		stateManager.writeAction('custom',{element: $el.attr('id')});
	if(k == 'orient')
		paper._buildSize($el, $('#div-position'));
}
paper.draw = function($div, redraw, sanitize){
	var element = $div.attr('data-element');
	if(!paper[element]) return;
	var dim = {width: $div.width(), height: $div.height()};
	$div.html('');
	var canvas = Raphael($div[0], dim.width, dim.height);
	var props = $div.data('sanitized');
	if(sanitize && !props){
		props = paper._sanitize($.extend(true,{}, $div.data('properties')));
		$div.data('sanitized', props);
	}
	props = props ? $.extend(true, {}, props) : paper._sanitize($.extend(true,{}, $div.data('properties')));
	paper[element].draw(canvas, 0, 0, dim.width, dim.height, props, null, redraw);
	paper._drawOpacity(props, $div);
};
paper.redraw = function($div, sanitize){
	$div.each(function(){paper.draw($(this), true, sanitize);});
};
paper.setItemClickHandler = function($div){
	$('.element-caption', $div).click(function(ev){		
		var $t = $(ev.target), $parent = $t.parents('.element');
		if($parent.hasClass('showingprops')){
			ev.stopPropagation();
			$parent.data('dsp', true);
		}
		if(!$t.hasClass('element-caption-para'))
			$t = $($t.parents('.element-caption-para')[0]);
		var index = $('span.itemindex',$t);
		if(index.length == 0)
			index = $('span.itemindex',this).text();
		else if(index.length > 1)
			index = $(ev.target).next('.itemindex').text();
		else
			index = $(index[0]).text();
		if(index.indexOf(',')!=-1){
			index = index.split(',');
			$('#rowitem-'+index[0]).trigger('click');
			$('#selitem-'+index[1]).trigger('click');
		}
		else
			$('#selitem-'+index).trigger('click');
	});
}
paper._resize = function($div,w,h){
	mockup.setWidth(w, $div);
	mockup.setHeight(h, $div);
	$('svg',$div).css({height: '100%', width: '100%'});
	if($div.hasClass('selected'))
		mockup.refreshBorder($div, null, true, true);
}
paper._propertyChanged = function($div){
	if($div.hasClass('member-master'))
		mockup.editMasterElement($div);
}
paper._getItemData = function(){
	var $el = $('#' + $('#propsFor').text());
	var props = $el.data('properties');
	var i = $('#propsForItem').text();
	var j = $('#propsForRow').text();
	i = i == '' ? 0 : i;
	j = j == '' ? 0 : j;
	if(i=='n' || j=='n') return {};
	return props.items[j][i];
}
paper._putItemData = function(vi){
	var $el = $('#' + $('#propsFor').text());
	var props = $el.data('properties');
	var i = $('#propsForItem').text();
	var j = $('#propsForRow').text();
	i = i == '' ? 0 : i;
	j = j == '' ? 0 : j;
	if(i=='n')
		props.items[j].push(vi);
	else
		props.items[j][i] = vi;
	$el.data('properties', props);
}
paper._changeItemData = function(k,v,wa){
	var vi = paper._getItemData();
	vi[k] = v;
	paper._putItemData(vi);
	var eid = $('#propsFor').text();
	paper.redraw($('#'+eid));
	if(wa !== false)
		stateManager.writeAction('custom',{element: eid});
}
paper._buildLinkInspector = function($div, insertAfter, props, complex){
	var $link = $('<div id="div-linkinspector" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Link To</strong></label><div class="controls btn-group"><button class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#" id="btn-linkinspector" style="width:130px;">Select Page&nbsp;<i class="icon-caret-down"></i></button><ul class="dropdown-menu" id="linkdropdown"></ul></div></div>').insertAfter(insertAfter);
	var pages = persistanceManager.getAllPagesForProject(stateManager.currentState.currentProject);
	var titles = persistanceManager.getAllPageTitlesForProject(stateManager.currentState.currentProject);
	var link = '';
	for(var t in pages){
		var ep = $.escapeHTML(pages[t]);
		link += '<li><a href="#" class="linkinspector-item" id="linkinspector-item-'+ep+'" data-page="'+ep+'">'+$.escapeHTML(titles[t].title);+'</a></li>';
	}
	link += '<li class="divider"></li><li><a href="#" class="linkinspector-item" data-page="delete-link">Remove Link</a></li>';
	$('#linkdropdown').html(link);
	$('#linkdropdown .linkinspector-item').click(function(ev){
		var $t = $(this), page = $t.attr('data-page');
		if(page == 'delete-link'){
			$('#btn-linkinspector').html('Select Page&nbsp;<i class="icon-caret-down"></i>');
			page = null;
			if(!complex) $div.removeClass('has-link');
		}
		else{
			$('#btn-linkinspector').text($(this).text());
			$div.addClass('has-link');
		}
		if(complex)
			paper._changeItemData('linkto',page,true);
		else
			paper._changeProperty('linkto',page,true,false);
	});
	if(props.linkto)
		$('#linkinspector-item-'+props.linkto).click();
}
paper._drawLink = function(props, $div){
	if(stateManager.currentState.previewMode && props.linkto && !props.d)
		$div.wrap('<a href="'+props.linkto+'" />');
}
paper._buildOpacity = function($div, insertAfter, props){
	var $opacity = $('<div id="div-opacity" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Opacity</strong></label><div class="controls"><input class="input-small" id="input-opacity"style="width:50px" type="text" value="'+(props.opacity)+'" placeholder="'+(props.opacity)+'"></div></div>').insertAfter(insertAfter);
	$('#input-opacity').spinner({min: 0, max: 1, places: 2, step: 0.01, largeStep: 0.1});
	$('#input-opacity').keydown(function(ev){
		if(ev.which == 13){
			paper._changeProperty('opacity', $(this).val(), false);
			ev.preventDefault();
		}
	});
	$('#input-opacity').blur(function(ev){
		paper._changeProperty('opacity', $(this).val());
	});
	$('#input-opacity').change(function(ev){
		paper._changeProperty('opacity', $(this).val(), false);
	});	
}
paper._drawOpacity = function(props, $div){
	$div.css({'opacity': props.opacity, 'filter': 'alpha(opacity='+(props.opacity*100)+')'});
}
paper._changeCSS = function(key,val,redraw,wa){
	var $el = $('#' + $('#propsFor').text());
	$el.css(key, val+'px');
	mockup.refreshBorder($el, null, true, true);
	var isPage = $el.attr('data-element') == 'page';
	if(redraw && !isPage)
		paper.redraw($el);
	if(wa !== false){
		if(isPage)
			ui.refreshOrInitScrollers('#workspace-area');
		stateManager.writeAction('custom',{element: $el.attr('id')});	
	}
}
paper._changeWidth = function(val, wa){
	paper._changeCSS('width', val, true, wa);
}
paper._changeHeight = function(val, wa){
	paper._changeCSS('height', val, true, wa);
}
paper._changeTop = function(val, wa){
	paper._changeCSS('top', val, false, wa);
}
paper._changeLeft = function(val, wa){
	paper._changeCSS('left', val, false, wa);
}
paper._buildPosition = function($div, insertAfter){
	if($div.attr('data-element') == 'page') return;
	var pos = $div.position();
	$('#div-position').remove();
	var $pos = $('<div id="div-position" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Position</strong></label><div class="controls"><input class="input-small" id="input-position-left" style="width:50px" type="text" value="'+pos.left+'" placeholder="'+pos.left+'"><input class="input-small" id="input-position-top" style="width:50px" type="text" value="'+pos.top+'" placeholder="'+pos.top+'"></div></div>').insertAfter(insertAfter);	

	$('#input-position-left').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Left', hideAfter: 2000});
	$('#input-position-top').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Top', hideAfter: 2000});

	$('#input-position-left').spinner();
	$('#input-position-left').keydown(function(ev){
		if(ev.which == 13){
			paper._changeLeft($(this).val(), false);
			ev.preventDefault();
		}
	});
	$('#input-position-left').blur(function(ev){
		paper._changeLeft($(this).val());
	});
	$('#input-position-left').change(function(ev){
		paper._changeLeft($(this).val(), false);
	});
	$('#input-position-top').spinner();
	$('#input-position-top').keydown(function(ev){
		if(ev.which == 13){
			paper._changeTop($(this).val(), false);
			ev.preventDefault();
		}
	});
	$('#input-position-top').blur(function(ev){
		paper._changeTop($(this).val());
	});
	$('#input-position-top').change(function(ev){
		paper._changeTop($(this).val(), false);
	});
}
paper._buildSize = function($div, insertAfter){
	var dh = $div.height(), dw = $div.width();
	var min = paper[$div.attr('data-element')].properties.min;	
	var props = $div.data('properties');
	min = { 
		height: min && min.height ? (props && props.orient != 1 ? min.height : min.width) : 20,
		width: min && min.width ? (props && props.orient != 1 ? min.width : min.height) : 20
	}
	$('#div-dimsize').remove();
	var $pos = $('#div-position');
	insertAfter = $pos.length > 0 ? $pos : insertAfter;
	var $size = $('<div id="div-dimsize" class="control-group"><label class="control-label"><strong style="color:#3a3a3a">Size</strong></label><div class="controls"><input class="input-small" id="input-dimsize-width" style="width:50px" type="text" value="'+dw+'" placeholder="'+dw+'"><input class="input-small" id="input-dimsize-height" style="width:50px" type="text" value="'+dh+'" placeholder="'+dh+'"></div></div>').insertAfter(insertAfter);

	$('#input-dimsize-height').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Height', hideAfter: 2000});
	$('#input-dimsize-width').tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: 'Width', hideAfter: 2000});

	$('#input-dimsize-height').spinner({min: min.height});
	$('#input-dimsize-height').keydown(function(ev){
		if(ev.which == 13){
			paper._changeHeight($(this).val(), false);
			ev.preventDefault();
		}
	});
	$('#input-dimsize-height').blur(function(ev){
		paper._changeHeight($(this).val());
	});
	$('#input-dimsize-height').change(function(ev){
		paper._changeHeight($(this).val(), false);
	});
	$('#input-dimsize-width').spinner({min: min.width});
	$('#input-dimsize-width').keydown(function(ev){
		if(ev.which == 13){
			paper._changeWidth($(this).val(), false);
			ev.preventDefault();
		}
	});
	$('#input-dimsize-width').blur(function(ev){
		paper._changeWidth($(this).val());
	});
	$('#input-dimsize-width').change(function(ev){
		paper._changeWidth($(this).val(), false);
	});
}
paper._buildDimensions = function($div, insertAfter){
	paper._buildPosition($div, insertAfter);
	paper._buildSize($div, insertAfter);
}
paper._sanitize = function(props){
	for(var p in props){
		if(typeof props[p] == 'object')	
			props[p] = paper._sanitize(props[p]);
		else if(typeof props[p] == 'string')
			props[p] = $.escapeHTML(props[p]);
	}
	return props;
}
paper._destroyAllTooltips = function(){
	$('button','#tbs-popover').tooltip('destroy');
	$('input','#tbs-popover').tooltip('destroy');
	$('#element-resizer').tooltip('destroy');
}
paper._changeTooltip = function(e, text){
	var $e = $(e);
	$e.tooltip('destroy');
	$e.tooltip({html: false, placement: 'bottom', attachTo: '#tbs-tooltip', title: text, hideAfter: 2000});
}